package edu.leiden.aqosa.akka;

import org.opt4j.core.Individual;
import org.opt4j.core.Objectives;
import org.opt4j.core.problem.Evaluator;

import akka.actor.UntypedActor;
//import akka.event.Logging;
//import akka.event.LoggingAdapter;

public class AQOSAActor extends UntypedActor {
//	LoggingAdapter log = Logging.getLogger(getContext().system(), this);

	protected final Evaluator<Object> evaluator;
	
	public AQOSAActor(Evaluator<Object> evaluator) {
		this.evaluator = evaluator;
	}
	
	@Override
	public void onReceive(Object message) throws Exception {
		
		if (message instanceof Individual) {
//			log.info("Execution on " + getSelf().path());
			
			Individual individual = (Individual) message;
			Object phenotype = individual.getPhenotype();
			Objectives objectives = evaluator.evaluate(phenotype);
			individual.setObjectives(objectives);
			
			getSender().tell("DONE!", getSelf());
		} else {
			unhandled(message);
		}

	}

}
