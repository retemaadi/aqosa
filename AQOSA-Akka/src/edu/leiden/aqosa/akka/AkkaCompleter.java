package edu.leiden.aqosa.akka;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.opt4j.core.Genotype;
import org.opt4j.core.Individual;
import org.opt4j.core.Individual.State;
import org.opt4j.core.common.completer.SequentialIndividualCompleter;
import org.opt4j.core.optimizer.Control;
import org.opt4j.core.optimizer.IndividualCompleter;
import org.opt4j.core.optimizer.TerminationException;
import org.opt4j.core.problem.Decoder;
import org.opt4j.core.problem.Evaluator;

import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Address;
import akka.actor.Deploy;
import akka.actor.Props;
import akka.pattern.Patterns;
import akka.remote.RemoteScope;
import akka.util.Timeout;

import com.google.inject.Inject;

public class AkkaCompleter implements IndividualCompleter {

	protected final Decoder<Genotype, Object> decoder;
	protected final Evaluator<Object> evaluator;
	protected final Control control;
	
	protected static final int N = 8; 
	protected static final List<Address> nodes = new ArrayList<Address>();
	protected static int lastNode = -1;
	
	protected final Timeout timeout = new Timeout(Duration.create(30, TimeUnit.SECONDS));
	protected final ActorSystem system;


	/**
	 * Constructs a {@link SequentialIndividualCompleter}.
	 * 
	 * @param control
	 *            the optimization control
	 * @param decoder
	 *            the decoder
	 * @param evaluator
	 *            the evaluator
	 */
	@Inject
	public AkkaCompleter(Control control, Decoder<Genotype, Object> decoder, Evaluator<Object> evaluator) {
		super();
		this.control = control;
		this.decoder = decoder;
		this.evaluator = evaluator;
		
		for (int n=1; n<=N; n++) {
			//int startIP = 0;		// Leiden cluster
			int startIP = 110;		// VU cluster
			int nodeIP = startIP + n;
			for (int port : WorkerApp.ports) {
				nodes.add(new Address("akka.tcp", WorkerApp.systemName, WorkerApp.ipPrefix + nodeIP, port));
			}
		}
		
		system = ActorSystem.create(WorkerApp.systemName);
	}

	@Override
	public void complete(Iterable<? extends Individual> iterable) throws TerminationException {
		List<Future<Object>> futures = new ArrayList<Future<Object>>();
		for (Individual individual : iterable) {
			if (!individual.isEvaluated()) {
				control.checkpoint();
				decode(individual);
				control.checkpoint();
				Future<Object> f = evaluate(individual);
				futures.add(f);
				control.checkpoint();
			}
		}
		
		for (Future<Object> f : futures) {
			try {
				Await.ready(f, timeout.duration());
			} catch (TimeoutException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void complete(Individual... individuals) throws TerminationException {
		List<Individual> list = Arrays.asList(individuals);
		complete(list);
	}

	protected void decode(Individual individual) {
		State state = individual.getState();

		if (state == State.GENOTYPED) {
			individual.setState(State.DECODING);
			Genotype genotype = individual.getGenotype();
			Object phenotype = decoder.decode(genotype);
			individual.setPhenotype(phenotype);
		} else {
			throw new IllegalStateException("Cannot decode Individual, current state: " + state);
		}
	}
	
	/**
	protected void evaluate(Individual individual) {
		State state = individual.getState();

		if (state == State.PHENOTYPED) {
			individual.setState(State.EVALUATING);
			Object phenotype = individual.getPhenotype();

			Objectives objectives = evaluator.evaluate(phenotype);

			assert isSameLength(objectives.getKeys()) : "Objectives changed: " + objectives.getKeys();

			individual.setObjectives(objectives);
		} else {
			throw new IllegalStateException("Cannot evaluate Individual, current state: " + state);
		}
	}
	**/
	
	protected Future<Object> evaluate(Individual individual) {
		State state = individual.getState();

		if (state == State.PHENOTYPED) {
			individual.setState(State.EVALUATING);
			
			ActorRef ref = system.actorOf(Props.create(AQOSAActor.class, evaluator).withDeploy(
					new Deploy(new RemoteScope(nextNode()))
					), "aqosaActor" + individual.hashCode());
			
			return Patterns.ask(ref, individual, timeout);
		} else {
			throw new IllegalStateException("Cannot evaluate Individual, current state: " + state);
		}
	}
	
	public static Address nextNode() {
		lastNode++;
		if (lastNode == nodes.size())
			lastNode = 0;
		
		return nodes.get(lastNode);
	}	
}
