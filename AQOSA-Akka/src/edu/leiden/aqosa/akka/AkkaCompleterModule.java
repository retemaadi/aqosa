package edu.leiden.aqosa.akka;

import org.opt4j.core.optimizer.IndividualCompleter;
import org.opt4j.core.start.Opt4JModule;

public class AkkaCompleterModule extends Opt4JModule {

	@Override
	protected void config() {
		bind(IndividualCompleter.class).to(AkkaCompleter.class).in(SINGLETON);
	}

}