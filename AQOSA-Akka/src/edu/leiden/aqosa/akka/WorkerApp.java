package edu.leiden.aqosa.akka;

import akka.actor.ActorSystem;
import akka.kernel.Bootable;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class WorkerApp implements Bootable {
	public static final String systemName = "distAQOSA";
	// public static final String ipPrefix = "10.141.1.";		// Leiden Cluster
	public static final String ipPrefix = "10.141.0.";			// VU cluster
	public static final int[] ports = {2551, 2552, 2553, 2554, 2555, 2556, 2557, 2558};
	//public static final int[] ports = {2551, 2552};

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startup() {
		
		for (int port : ports) {
			Config config = ConfigFactory.parseString(
				"akka.remote.netty.tcp.port=" + port).withFallback(
				ConfigFactory.load());
			
			ActorSystem.create(systemName, config);
		}
	    
	}
}
