package edu.leiden.aqosa.architecture;

import edu.leiden.aqosa.qn.network.Link;
import edu.leiden.aqosa.qn.network.ResourceNode;
import edu.leiden.aqosa.qn.simulator.Simulator;

public class Bus extends ResourceNode {

	public Bus(Simulator simulator, int id, double bandwidth, double delay, Topology topology) {
		this(simulator, "Bus"+id, bandwidth, delay, topology);
	}

	protected Bus(Simulator simulator, String name, double bandwidth, double delay, Topology topology) {
		super(simulator, name, new CommDelay(bandwidth, delay), 1);
		super.setLink(topology);
	}
	
	@Override
	public void setLink(Link r) {
		assert false : "Invalid setLink Call";
	}
	
}
