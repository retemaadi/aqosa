package edu.leiden.aqosa.architecture;

import edu.leiden.aqosa.qn.network.Link;
import edu.leiden.aqosa.qn.network.ResourceNode;
import edu.leiden.aqosa.qn.simulator.Simulator;

public class CPU extends ResourceNode {

	public CPU(Simulator simulator, int id, double clock, Topology topology) {
		this(simulator, "CPU"+id, clock, topology);
	}
	
	protected CPU(Simulator simulator, String name, double clock, Topology topology) {
		super(simulator, name, new CompDelay(clock), 1);
		super.setLink(topology);
	}
	
	@Override
	public void setLink(Link r) {
		assert false : "Invalid setLink Call";
	}
	
}
