package edu.leiden.aqosa.architecture;

import edu.leiden.aqosa.qn.delay.Delay;
import edu.leiden.aqosa.qn.delay.DistributionSampler;
import edu.leiden.aqosa.qn.delay.Normal;
import edu.leiden.aqosa.qn.network.Customer;

public class CommDelay extends Delay {

	private double bandwidth;
	private double latency;
	
	public CommDelay(double bandwidth, double latency) {
		this.bandwidth = bandwidth;
		this.latency = latency;
	}
	
	@Override
	public double sample() {
		assert false : "Invalid BUS DELAY usage!";
		return 0.0;
	}
	
	@Override
	public double sample(Customer c) {
		Request req = (Request) c;
		double datasize = req.getDatasize();
		
		double totalDelay = 0; 
		int times = (int) (datasize / bandwidth) + 1;
		DistributionSampler ds = new Normal(latency, latency * req.getVariance());
		for (int t=0; t < times; t++) {
			totalDelay += ds.next();
		}
		
		return totalDelay;
	}
	
}
