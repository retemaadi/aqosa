package edu.leiden.aqosa.architecture;

import edu.leiden.aqosa.qn.delay.Delay;
import edu.leiden.aqosa.qn.delay.DistributionSampler;
import edu.leiden.aqosa.qn.delay.Normal;
import edu.leiden.aqosa.qn.network.Customer;

public class CompDelay extends Delay {

	private double cpuClock;
	
	public CompDelay(double clock) {
		super();
		cpuClock = clock;
	}

	@Override
	public double sample() {
		assert false : "Invalid CPU DELAY usage!";
		return 0.0;
	}
	
	@Override
	public double sample(Customer c) {
		Request req = (Request) c;
		double cycles = req.getCycles();
		
		double totalDelay = 0; 
		double delay = cycles / cpuClock;
		DistributionSampler ds = new Normal(delay, delay * req.getVariance());
		totalDelay = ds.next();
		
		return totalDelay;
	}
}
