package edu.leiden.aqosa.architecture;

import java.util.ArrayList;
import java.util.List;

import edu.leiden.aqosa.model.ArchScenario;
import edu.leiden.aqosa.model.Configuration;
import edu.leiden.aqosa.model.IR.FlowSet;
import edu.leiden.aqosa.solution.ArchSolution;

public class EvalModel {
	
	private ArrayList<Double> responseTimeScore = new ArrayList<Double>();
	private double responseTime;
	private ArrayList<Double> cpuUtilizationScore = new ArrayList<Double>();
	private double cpuUtilization;
	private ArrayList<Double> busUtilizationScore = new ArrayList<Double>();
	private double busUtilization;
	
	private ArrayList<Double[]> cpuUtilizationArray = new ArrayList<Double[]>();
	private ArrayList<Double[]> busUtilizationArray = new ArrayList<Double[]>();
	
	public EvalModel() {
	}
	
	
	public void simulate(ArchSolution solution, ArchScenario scenario, Configuration configuration, int simulationNo) throws InterruptedException {
		for (FlowSet fs : scenario.getFlowSet()) {
			FlowSimulator simulator = new FlowSimulator(solution, scenario, configuration, fs, simulationNo);
			
			if (!simulator.simulate()) {
				throw new InterruptedException();			/// Invalid solution
			}
			
			responseTimeScore.add(simulator.getResponseTimeScore());
			cpuUtilizationScore.add(simulator.getCpuUtilizationScore());
			busUtilizationScore.add(simulator.getBusUtilizationScore());
			
			cpuUtilizationArray.add(simulator.getCpuUtilizationArray());
			busUtilizationArray.add(simulator.getBusUtilizationArray());
		}
		
		calcBusUtilizationScore();
		calcCpuUtilizationScore();
		calcResponseTimeScore();
	}
	
	public void calcResponseTimeScore() {
		double score = 0;
		int n = 0;
		for (Double value : responseTimeScore) {
			score += value;
			n++;
		}
		
		responseTime = score / n;
	}
	
	public double getResponseTimeScore() {
		return responseTime;
	}
	
	public void calcCpuUtilizationScore() {
		double score = 0;
		int n = 0;
		for (Double value : cpuUtilizationScore) {
			score += value;
			n++;
		}
		
		cpuUtilization = score / n;
	}
	
	public double getCpuUtilizationScore() {
		return cpuUtilization;
	}
	
	public List<Double> getCpuUtilizationList() {
		List<Double> cpuUtils = new ArrayList<Double>();
		int nodeSize = cpuUtilizationArray.get(0).length;
		for (int i = 0; i < nodeSize; i++) {
			double total = 0;
			for (Double[] utils : cpuUtilizationArray) {
				double util = utils[i];
				total += util;
			}
			double avg = total / cpuUtilizationArray.size();
			cpuUtils.add(avg);
		}
		return cpuUtils;
	}
	
	public void calcBusUtilizationScore() {
		double score = 0;
		int n = 0;
		for (Double value : busUtilizationScore) {
			score += value;
			n++;
		}
		
		busUtilization = score / n;
	}
	
	public double getBusUtilizationScore() {
		return busUtilization;
	}
	
	public List<Double> getBusUtilizationList() {
		List<Double> busUtils = new ArrayList<Double>();
		int busSize = busUtilizationArray.get(0).length;
		for (int i = 0; i < busSize; i++) {
			double total = 0;
			for (Double[] utils : busUtilizationArray) {
				double util = utils[i];
				total += util;
			}
			double avg = total / busUtilizationArray.size();
			busUtils.add(avg);
		}
		return busUtils;
	}
}
