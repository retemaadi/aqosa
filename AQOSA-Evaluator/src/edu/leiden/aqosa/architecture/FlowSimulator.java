package edu.leiden.aqosa.architecture;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.leiden.aqosa.model.ArchScenario;
import edu.leiden.aqosa.model.Configuration;
import edu.leiden.aqosa.model.IR.Component;
import edu.leiden.aqosa.model.IR.FlowInstance;
import edu.leiden.aqosa.model.IR.FlowSet;
import edu.leiden.aqosa.model.IR.InPort;
import edu.leiden.aqosa.model.IR.OutPort;
import edu.leiden.aqosa.model.IR.Processor;
import edu.leiden.aqosa.model.IR.Service;
import edu.leiden.aqosa.qn.simulator.LogType;
import edu.leiden.aqosa.qn.simulator.Simulator;
import edu.leiden.aqosa.qn.simulator.Simulator.StopType;
import edu.leiden.aqosa.solution.ArchSolution;

public class FlowSimulator {

	private final ArchSolution solution;
	private final ArchScenario scenario;
	private final List<FlowInstance> flows;
	
	private final Topology topology;
	private final int simulationNo;
	private final double completionTime;
	private final double missedPercentage;
	
	private Simulator simulator;
	
	private List<Trigger> triggers;
	private List<CPU> cpus;
	private List<InternalBus> internalBuses;
	private List<Bus> buses;
	private Map<String, Output> outputs;
	private List<Integer> componentTurn;
	
	private double responseTime;
	private double cpuUtilization;
	private double busUtilization;
	
	private Double[] cpuUtilizationArray;
	private Double[] busUtilizationArray;
	
	public FlowSimulator(ArchSolution solution, ArchScenario scenario, Configuration configuration, FlowSet flowset, int simNo) {
		this.solution = solution;
		this.scenario = scenario;
		
		this.flows = new ArrayList<FlowInstance>();
		for (FlowInstance fi : flowset.getFlowinstance()) {
			if (configuration.isValidFlow(fi.getInstance()))
				flows.add(fi);
		}
		
		this.simulationNo = simNo;
		this.completionTime = flowset.getCompletionTime();
		this.missedPercentage = flowset.getMissedPercentage();
		
		this.componentTurn = new ArrayList<Integer>();
		for (int i=0; i<solution.getComponentInstances().size(); i++) {
			componentTurn.add(0);
		}
		this.topology = new Topology(this);
	}
	
	public boolean simulate() throws InterruptedException {
		simulator = new Simulator(StopType.StopByTime, completionTime);
		
		CreateTriggers();
		CreateCpus();
		CreateBuses();
		CreateOutputs();
		
		for (int i = 0; i < simulationNo; i++)
			simulator.simulate();
		
		if (missedDeadlines()) {
			return false;
		}
		
		if (!calcResponseTimeScore())
			return false;
		if (!calcCpuUtilizationScore())
			return false;;
		if (!calcBusUtilizationScore())
			return false;
		
		return true;
	}
	
	private void CreateTriggers() {
		triggers = new ArrayList<Trigger>();
		for(FlowInstance fi : flows) {
			//String name = flowset.getName() + "__" + fi.getInstance().getName();
			String name = fi.getInstance().getName();
			triggers.add(new Trigger(simulator, name, fi, topology));
		}
	}
	
	private void CreateCpus() {
		cpus = new ArrayList<CPU>();
		internalBuses = new ArrayList<InternalBus>();
		for (int i = 0; i < solution.getNodes(); i++) {
			Processor p = solution.getProcessors().get(i);
			cpus.add(new CPU(simulator, i+1, p.getClock(), topology));
			internalBuses.add(new InternalBus(simulator, i+1, p.getInternalBusBandwidth(), p.getInternalBusDelay(), topology));
		}		
	}
	
	private void CreateBuses() {
		buses = new ArrayList<Bus>();
		for (int i = 0; i < solution.getEdges(); i++) {
			edu.leiden.aqosa.model.IR.Bus b = solution.getBuses().get(i);
			buses.add(new Bus(simulator, i+1, b.getBandwidth(), b.getDelay(), topology));
		}
	}
	
	private void CreateOutputs() {
		outputs = new HashMap<String, Output>();
		for(FlowInstance fi : flows) {
			String id = fi.getInstance().getName();
			outputs.put(id, new Output(simulator, id, topology));
		}
	}
	
	private boolean missedDeadlines() {
		for (Trigger tr : triggers) {
			String id = tr.getType();
			double completed = simulator.getResult(LogType.CUSTOMERTYPE_COMPLETED, id);
			double missed = simulator.getResult(LogType.CUSTOMERTYPE_MISSED, id);
			double ratio = missed / completed;
			
			if (ratio > missedPercentage)
				return true;
		}
		
		return false;
	}
	
	private void setNextTurn(int component) {
		int deploySize = solution.getDeploy().get(component).size();
		int nextTurn = componentTurn.get(component) + 1;
		if (nextTurn >= deploySize)
			nextTurn = 0;
		
		componentTurn.set(component, nextTurn);
	}
	
	public Bus findBus(OutPort source, InPort destination) {
		// TODO find the best path from source to destination (non-busy route)!
		int srcIndex = scenario.getComponentIndex( (Component) source.eContainer() );
		int dstIndex = scenario.getComponentIndex( (Component) destination.eContainer() );
		
		int srcTurn = componentTurn.get(srcIndex);
		int src = solution.getDeploy().get(srcIndex).get(srcTurn) - 1;
		
		setNextTurn(dstIndex);
		int dstTurn = componentTurn.get(dstIndex);
		int dst = solution.getDeploy().get(dstIndex).get(dstTurn) - 1;
		
		if (src == dst) {
			return internalBuses.get(src);
		} else { 
			for (int i = 0; i < solution.getEdges(); i++) {
				List<Boolean> busConnections = solution.getBus2Nodes().get(i);
				boolean isSrcConnected = busConnections.get(src);
				boolean isDstConnected = busConnections.get(dst);
				
				if (isSrcConnected && isDstConnected)
					return buses.get(i);
			}
		}
		
		assert false : "Finding Bus: Couldnot find a proper route to communicate\n (This shouldnot happen!)";
		return null;
	}	

	public CPU getDeployedCPU(Component comp) {
		int compIndex = scenario.getComponentIndex(comp);
		int compTurn = componentTurn.get(compIndex);
		int deploy = solution.getDeploy().get(compIndex).get(compTurn) - 1;
		return cpus.get(deploy);
	}
	
	public Output getOutput(String type) {
		return outputs.get(type);
	}
	
	public double getVariance(Component comp) {
		int compIndex = scenario.getComponentIndex(comp);
		String instName = solution.getComponentInstance(compIndex);
		return scenario.getComponentInstance(instName).getVariancePercentage();
	}
	
	public double getCycles(Component comp, Service service) {
		int compIndex = scenario.getComponentIndex(comp);
		String instName = solution.getComponentInstance(compIndex);
		return scenario.getServiceInstance(instName, service).getCycles();
	}
	
	public double getDatasize(Component comp, Service service) {
		int compIndex = scenario.getComponentIndex(comp);
		String instName = solution.getComponentInstance(compIndex);
		return scenario.getServiceInstance(instName, service).getNetworkUsage();
	}
	
	private boolean calcResponseTimeScore() {
		double score = 0;
		for (Trigger tr : triggers) {
			String type = tr.getType();
			double meantime = simulator.getResult(LogType.CUSTOMERTYPE_MEANTIME, type);
			double deadline = tr.getDeadline();
			score += meantime / deadline;
		}
		score /= triggers.size();
		if (score > 1)
			return false;
		
		responseTime = score;
		return true;
	}
	
	public double getResponseTimeScore() {
		return responseTime;
	}
	
	private boolean calcCpuUtilizationScore() {
		double score = 0;
		int n = 0;
		cpuUtilizationArray = new Double[cpus.size()];
		for (CPU cpu : cpus) {
			String id = cpu.getName();
			double util = simulator.getResult(LogType.NODE_UTILIZATION, id);
			int key = Integer.parseInt(id.substring(3)) - 1;	// CPU contains 3 character
			cpuUtilizationArray[key] = util;
			score += util;
			n++;
		}
		
		cpuUtilization = score / n;
		return true;
	}
	
	public double getCpuUtilizationScore() {
		return cpuUtilization;
	}
	
	public Double[] getCpuUtilizationArray() {
		return cpuUtilizationArray;
	}
	
	private boolean calcBusUtilizationScore() {
		double score = 0;
		int n = 0;
		busUtilizationArray = new Double[buses.size()];
		for (Bus bus : buses) {
			String id = bus.getName();
			double util = simulator.getResult(LogType.NODE_UTILIZATION, id);
			int key = Integer.parseInt(id.substring(3)) - 1;	// Bus contains 3 character
			busUtilizationArray[key] = util;
			score += util;
			n++;
		}
		
		busUtilization = (n == 0 ? score : score / n);
		return true;
	}
	
	public double getBusUtilizationScore() {
		return busUtilization;
	}
	
	public Double[] getBusUtilizationArray() {
		return busUtilizationArray;
	}	
}
