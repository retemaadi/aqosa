package edu.leiden.aqosa.architecture;

import edu.leiden.aqosa.qn.simulator.Simulator;

public class InternalBus extends Bus {

	public InternalBus(Simulator simulator, int id, double bandwidth, double delay, Topology topology) {
		super(simulator, "InternalBus"+id, bandwidth, delay, topology);
	}
	
}
