package edu.leiden.aqosa.architecture;

import edu.leiden.aqosa.qn.network.Link;
import edu.leiden.aqosa.qn.network.Sink;
import edu.leiden.aqosa.qn.simulator.Simulator;

public class Output extends Sink {
	
	public Output(Simulator simulator, String id, Topology topology) {
		super(simulator, "Output__" + id);
		super.setLink(topology);
	}
	
	@Override
	public void setLink(Link r) {
		assert false : "Invalid setLink Call";
	}
	
}
