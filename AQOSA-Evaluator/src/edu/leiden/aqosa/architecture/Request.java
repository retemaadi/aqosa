package edu.leiden.aqosa.architecture;

import org.eclipse.emf.common.util.EList;

import edu.leiden.aqosa.model.IR.Action;
import edu.leiden.aqosa.qn.network.Customer;
import edu.leiden.aqosa.qn.simulator.Simulator;

public class Request extends Customer {

	private final EList<Action> actions;
	private int step = 0;
	private double cycles;
	private double datasize;
	private double variance;
	
	public Request(Simulator simulator, String type, double deadline, EList<Action> actions) {
		super(simulator, type, deadline);
		this.actions = actions;
	}
	
	public Action getNextAction() {
		if (step == actions.size())
			return null;	// End of Actions
		
		Action action = actions.get(step);
		step++;
		
		return action;
	}
	
	public void setCycles(double cycles) {
		this.cycles = cycles;
	}
	
	public double getCycles() {
		return cycles;
	}
	
	public void setDatasize(double datasize) {
		this.datasize = datasize;
	}
	
	public double getDatasize() {
		return datasize;
	}
	
	public void setVariance(double variance) {
		this.variance = variance;
	}
	
	public double getVariance() {
		return variance;
	}
	
}
