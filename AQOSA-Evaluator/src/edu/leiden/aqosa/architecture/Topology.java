package edu.leiden.aqosa.architecture;

import edu.leiden.aqosa.model.IR.Action;
import edu.leiden.aqosa.model.IR.CommunicateAction;
import edu.leiden.aqosa.model.IR.Component;
import edu.leiden.aqosa.model.IR.ComputeAction;
import edu.leiden.aqosa.model.IR.InPort;
import edu.leiden.aqosa.model.IR.OutPort;
import edu.leiden.aqosa.model.IR.Service;
import edu.leiden.aqosa.qn.network.Customer;
import edu.leiden.aqosa.qn.network.Link;

public class Topology extends Link {

	private final FlowSimulator fsimu;

	public Topology(FlowSimulator fsimu) {
		this.fsimu = fsimu;
	}

	@Override
	protected void move(Customer c) throws InterruptedException {
		Request req = (Request) c;
		Action action = req.getNextAction();
		
		if (action == null) {
			send(req, fsimu.getOutput(req.getType()));
			
		} else if (action instanceof ComputeAction) {
			ComputeAction pAct = (ComputeAction) action;
			Service service = pAct.getService();
			Component comp = (Component) service.eContainer();
			
			req.setCycles(fsimu.getCycles(comp, service));
			req.setDatasize(fsimu.getDatasize(comp, service));
			req.setVariance(fsimu.getVariance(comp));
			send(req, fsimu.getDeployedCPU(comp));
			
		} else if (action instanceof CommunicateAction) {
			CommunicateAction mAct = (CommunicateAction) action;
			OutPort source = mAct.getSource();
			InPort destination = mAct.getDestination();
			
			Bus route = fsimu.findBus(source, destination);
			if (route == null)
				throw new InterruptedException();			/// Invalid solution
			send(req, route);
			
		} else {
			assert false : "Unknown Action"; 
		}
	}
	
}
