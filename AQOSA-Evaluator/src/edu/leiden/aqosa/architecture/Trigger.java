package edu.leiden.aqosa.architecture;

import edu.leiden.aqosa.model.IR.FlowInstance;
import edu.leiden.aqosa.qn.delay.Deterministic;
import edu.leiden.aqosa.qn.network.Link;
import edu.leiden.aqosa.qn.network.Source;
import edu.leiden.aqosa.qn.simulator.Simulator;

public class Trigger extends Source {

	private final FlowInstance flowInstance;
	private final String type;
	
	public Trigger(Simulator simulator, String triggerName, FlowInstance fi, Topology topology) {
		super(simulator, triggerName, new DeterministicWithStart(fi.getStart(), fi.getTrigger()));
		this.flowInstance = fi;
		this.type = fi.getInstance().getName();
		super.setLink(topology);
	}
	
	public String getType() {
		return type;
	}
	
	public double getDeadline() {
		return flowInstance.getDeadline();
	}
	
	@Override
	protected void injectCustomers() throws InterruptedException {
		Request req = new Request(simulator, type, flowInstance.getDeadline(), flowInstance.getInstance().getAction());
		req.setLocation(this);
		forward(req);
	}
	
	@Override
	public void setLink(Link r) {
		assert false : "Invalid setLink Call";
	}
	
}

class DeterministicWithStart extends Deterministic {
	double start;
	boolean first = true;
	
	public DeterministicWithStart(double start, double period) {
		super(period);
		this.start = start;
	}
	
	@Override
	public double next() {
		if (first) {
			first = false;
			return start;
		}
		return super.next();
	}
}