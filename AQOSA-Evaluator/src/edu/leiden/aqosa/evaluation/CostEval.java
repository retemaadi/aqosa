package edu.leiden.aqosa.evaluation;

import java.util.ArrayList;
import java.util.Collection;

import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.model.IR.Bus;
import edu.leiden.aqosa.model.IR.Processor;
import edu.leiden.aqosa.objectives.CostObjective;
import edu.leiden.aqosa.objectives.EvalObjective;
import edu.leiden.aqosa.solution.ArchSolution;

public class CostEval extends EvaluationThread {

	public CostEval(ArchModel model, ArchSolution solution) {
		super(model, solution);
	}

	@Override
	public Collection<EvalObjective> call() throws InterruptedException {
		double score = 0;

		double minCost = model.getObjectives().getMinCost();
		double maxCost = model.getObjectives().getMaxCost();
		
		for (Processor p : solution.getProcessors()) {
			double cost = p.getCost();
			score += cost;
		}
		
		for (Bus b : solution.getBuses()) {
			double cost = b.getCost();
			score += cost;
		}
		
		score = (score - minCost) / (maxCost - minCost);
		
		Collection<EvalObjective> answer = new ArrayList<EvalObjective>();
		answer.add(new CostObjective(score));
		return answer;
	}	
}