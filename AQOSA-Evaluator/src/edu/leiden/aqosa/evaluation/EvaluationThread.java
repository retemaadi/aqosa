package edu.leiden.aqosa.evaluation;

import java.util.Collection;
import java.util.concurrent.Callable;

import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.objectives.EvalObjective;
import edu.leiden.aqosa.solution.ArchSolution;

public abstract class EvaluationThread implements Callable<Collection<EvalObjective>> {
	
	protected final ArchSolution solution;
	protected final ArchModel model;
	
	public EvaluationThread(ArchModel model, ArchSolution solution) {
		this.model = model;
		this.solution = solution;
	}
	
	public abstract Collection<EvalObjective> call() throws InterruptedException;
	
}
