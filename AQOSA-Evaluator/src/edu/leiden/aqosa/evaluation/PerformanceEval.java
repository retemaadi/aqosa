package edu.leiden.aqosa.evaluation;

import java.util.ArrayList;
import java.util.Collection;

import edu.leiden.aqosa.architecture.EvalModel;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.objectives.BusUtilizationObjective;
import edu.leiden.aqosa.objectives.CpuUtilizationObjective;
import edu.leiden.aqosa.objectives.EvalObjective;
import edu.leiden.aqosa.objectives.ResponseTimeObjective;
import edu.leiden.aqosa.solution.ArchSolution;

public class PerformanceEval extends EvaluationThread {
	
	public PerformanceEval(ArchModel model, ArchSolution solution) {
		super(model, solution);
	}
	
	@Override
	public Collection<EvalObjective> call() throws InterruptedException {
		EvalModel evaluator = new EvalModel();
		evaluator.simulate(solution, model.getScenario(), model.getConfiguration(), model.getObjectives().getNoRun());
		
		double responseTime = evaluator.getResponseTimeScore();
		double cpuUtil = evaluator.getCpuUtilizationScore();
		double busUtil = evaluator.getBusUtilizationScore();
		
		solution.setCpuUtilizationList(evaluator.getCpuUtilizationList());
		solution.setBusUtilizationList(evaluator.getBusUtilizationList());
		
		Collection<EvalObjective> answer = new ArrayList<EvalObjective>();
		answer.add(new ResponseTimeObjective(responseTime));
		answer.add(new CpuUtilizationObjective(cpuUtil));
		answer.add(new BusUtilizationObjective(busUtil));
		return answer;
	}
}