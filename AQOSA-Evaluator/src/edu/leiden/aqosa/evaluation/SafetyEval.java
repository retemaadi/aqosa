package edu.leiden.aqosa.evaluation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.eclipse.emf.common.util.EList;

import edu.leiden.aqosa.failure.FTAnalysis;
import edu.leiden.aqosa.failure.FaultTree;
import edu.leiden.aqosa.failure.InputPort;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.model.ArchScenario;
import edu.leiden.aqosa.model.Dependency;
import edu.leiden.aqosa.model.IR.Action;
import edu.leiden.aqosa.model.IR.Component;
import edu.leiden.aqosa.model.IR.ComputeAction;
import edu.leiden.aqosa.model.IR.Dependence;
import edu.leiden.aqosa.model.IR.ExternalPort;
import edu.leiden.aqosa.model.IR.FlowInstance;
import edu.leiden.aqosa.model.IR.FlowSet;
import edu.leiden.aqosa.model.IR.InPort;
import edu.leiden.aqosa.model.IR.OutPort;
import edu.leiden.aqosa.model.IR.RequirePort;
import edu.leiden.aqosa.model.IR.Service;
import edu.leiden.aqosa.model.IR.ServiceInstance;
import edu.leiden.aqosa.objectives.EvalObjective;
import edu.leiden.aqosa.objectives.SafetyObjective;
import edu.leiden.aqosa.solution.ArchSolution;

public class SafetyEval extends EvaluationThread {
	
	private final ArchScenario scenario;	
	
	public SafetyEval(ArchModel model, ArchSolution solution) {
		super(model, solution);
		this.scenario = model.getScenario();
	}
	
	@Override
	public Collection<EvalObjective> call() throws InterruptedException {
		int n = 0;
		Random rand = new Random();
		
		double score = 0.0;
		for (FlowSet fs : scenario.getFlowSet()) {
			double subscore = 0;
			int flowsize = 0;
			for(FlowInstance fi : fs.getFlowinstance()) {
				if (model.getConfiguration().isValidFlow(fi.getInstance())) {
					flowsize++;
					Dependency dep = calcDeps(fi);
					FTAnalysis ftAnalysis = new FTAnalysis(dep, solution, model.getRepository().getExtports());
					subscore += ftAnalysis.analysis(model.getObjectives().getNoSampling(), rand);
				}
			}
			
			subscore /= flowsize;
			double t = (score * n) + subscore;
			n++;
			score = t / n;
		}
		
		Collection<EvalObjective> answer = new ArrayList<EvalObjective>();
		answer.add(new SafetyObjective(score));
		return answer;
	}
	
	private Dependency calcDeps(FlowInstance fi) {
		EList<Action> actions = fi.getInstance().getAction();
		ComputeAction pAct = (ComputeAction) actions.get(actions.size() - 1);		// The last one
		
		Service serv = pAct.getService();
		Component c = (Component) serv.eContainer();
		int componentNo = scenario.getComponentIndex(c);
		
		String inst = solution.getComponentInstance(componentNo);
		ServiceInstance si = scenario.getServiceInstance(inst, serv);
			
		return calcDeps(si.getDepend().get(0));
	}

	private Dependency calcDeps(Dependence dep) {
		Component comp = (Component) ((ServiceInstance) dep.eContainer()).getInstance().eContainer();
		FaultTree tree = new FaultTree(scenario.getComponentIndex(comp));
		tree.setDependencyType(dep.getType());
		
		for (Dependence d : dep.getDepend()) {
			tree.addInput(calcDeps(d));
		}
		for (RequirePort port : dep.getRequire()) {
			InPort inPort = port.getInternal();
			if (inPort != null) {
				List<OutPort> outPorts = scenario.getSourcePort(inPort);
				for (OutPort source : outPorts) {
					Component c = (Component) source.eContainer();
					int componentNo = scenario.getComponentIndex(c);
					String inst = solution.getComponentInstance(componentNo);
					
					ServiceInstance si = scenario.getProviderService(inst, source);
					for (Dependence d : si.getDepend()) {
						tree.addInput(calcDeps(d));
					}
				}
			}
			ExternalPort extPort = port.getExternal();
			if (extPort != null) {
				tree.addInput(new InputPort(extPort.getId()));
			}
		}
		
		return tree;
	}
}