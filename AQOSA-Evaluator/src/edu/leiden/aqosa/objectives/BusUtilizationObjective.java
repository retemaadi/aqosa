package edu.leiden.aqosa.objectives;

import edu.leiden.aqosa.model.IR.Evaluations;

public class BusUtilizationObjective extends EvalObjective {

//	public static double MIN = 0;			// For CC
//	public static double MAX = 0.4;			// For CC
	public static double MIN = 0;			// For IPC
	public static double MAX = 0.40;		// For IPC
	
//	public static double MIN = 0;
//	public static double MAX = 1;
	
	public BusUtilizationObjective(double value) {
		super(Evaluations.BUS_UTILIZATION, value);
	}

	@Override
	public double getValue() {
		return normalize(MIN, MAX, value);
	}

}
