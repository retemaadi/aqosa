package edu.leiden.aqosa.objectives;

import edu.leiden.aqosa.model.IR.Evaluations;

public class CostObjective extends EvalObjective {

//	public static double MIN = 0.05;		// For CC
//	public static double MAX = 0.5;			// For CC
	public static double MIN = 0;			// For IPC
	public static double MAX = 1;			// For IPC
	
//	public static double MIN = 0;
//	public static double MAX = 1;
	
	public CostObjective(double value) {
		super(Evaluations.COST, value);
	}

	@Override
	public double getValue() {
		return normalize(MIN, MAX, value);
	}

}
