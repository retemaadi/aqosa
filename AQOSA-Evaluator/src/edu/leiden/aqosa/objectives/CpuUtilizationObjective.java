package edu.leiden.aqosa.objectives;

import edu.leiden.aqosa.model.IR.Evaluations;

public class CpuUtilizationObjective extends EvalObjective {

//	public static double MIN = 0.05;		// For CC
//	public static double MAX = 0.4;			// For CC
	public static double MIN = 0;			// For IPC
	public static double MAX = 0.40;		// For IPC
	
//	public static double MIN = 0;
//	public static double MAX = 1;
	
	public CpuUtilizationObjective(double value) {
		super(Evaluations.CPU_UTILIZATION, value);
	}

	@Override
	public double getValue() {
		return normalize(MIN, MAX, value);
	}

}
