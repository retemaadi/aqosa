package edu.leiden.aqosa.objectives;

import org.opt4j.core.Objective;
import org.opt4j.core.Objective.Sign;

import edu.leiden.aqosa.model.IR.Evaluations;

public abstract class EvalObjective {
	
	protected final Evaluations evaluation;
	protected final double value;
	
	protected EvalObjective(Evaluations evaluation, double value) {
		this.evaluation = evaluation;
		this.value = value;
	}
	
	public Objective getObjective() {
		return new Objective(evaluation.getName(), Sign.MIN);
	}
	
	public abstract double getValue();
	
	protected double normalize(double min, double max, double value) {
		return (value - min) / (max - min);
	}
	
}
