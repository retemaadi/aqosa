package edu.leiden.aqosa.objectives;

import edu.leiden.aqosa.model.IR.Evaluations;

public class ResponseTimeObjective extends EvalObjective {

//	public static double MIN = 0.2;			// For CC
//	public static double MAX = 0.6;			// For CC
	public static double MIN = 0.05;		// For IPC
	public static double MAX = 0.67;		// For IPC
	
//	public static double MIN = 0;
//	public static double MAX = 1;
	
	public ResponseTimeObjective(double value) {
		super(Evaluations.RESPONSE_TIME, value);
	}

	@Override
	public double getValue() {
		return normalize(MIN, MAX, value);
	}

}
