package edu.leiden.aqosa.objectives;

import edu.leiden.aqosa.model.IR.Evaluations;

public class SafetyObjective extends EvalObjective {

//	public static double MIN = 0.015;		// For CC
//	public static double MAX = 0.017;		// For CC
	public static double MIN = 0.410;		// For IPC
	public static double MAX = 0.438;		// For IPC
	
//	public static double MIN = 0;
//	public static double MAX = 1;
	
	public SafetyObjective(double value) {
		super(Evaluations.SAFETY, value);
	}

	@Override
	public double getValue() {
		return normalize(MIN, MAX, value);
	}
	
}
