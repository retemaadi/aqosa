package edu.leiden.aqosa.failure;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import edu.leiden.aqosa.model.Dependency;
import edu.leiden.aqosa.model.IR.DependencyType;
import edu.leiden.aqosa.model.IR.Processor;
import edu.leiden.aqosa.solution.ArchSolution;

public class FTAnalysis {
	
	private final Dependency fail;
	private final ArchSolution solution;
	private final Map<String, Double[]> extports;
	private List<Integer> componentTurn;
	
	public FTAnalysis(Dependency fail, ArchSolution solution, Map<String, Double[]> extports) {
		this.fail = fail;
		this.solution = solution;
		this.extports = extports;
		
		this.componentTurn = new ArrayList<Integer>();
		for (int i=0; i<solution.getComponentInstances().size(); i++) {
			componentTurn.add(0);
		}
	}
	
	public Double analysis(int samples, Random random) throws InterruptedException {
		Double[] scores = new Double[samples];
		double totalScore = 0;
		
		for (int i=0; i < samples; i++) {
			scores[i] = analyze(random);
			totalScore += scores[i]; 
		}
		
		return (totalScore / samples);
	}
	
	private Double analyze(Random random) throws InterruptedException {
		Dependency root = fail;
		return analyze(random, root);
	}
	
	private Double analyze(Random random, Dependency dep) throws InterruptedException {
		if (dep instanceof InputPort) {
			InputPort in = (InputPort) dep;
			Double[] values = extports.get(in.getId());
			
			return getProbability(random, values[0], values[1]);
			
		} else {
			FaultTree ft = (FaultTree) dep;
			DependencyType depType = ft.getDependencyType();
			
			double failure = 1;
			switch (depType) {
			case AND:
				for (Dependency port : ft.getInputs())
					failure *= (1 - analyze(random, port));
				failure = 1 - failure;
				break;
			case OR:
				for (Dependency port : ft.getInputs())
					failure *= analyze(random, port);
				break;
			//case XOR:
			//	throw new Exception("XOR Gate Exception: NOT IMPLEMENTED YET");
			}
			
			try {
				int comp = ft.getComponentNo();
				int turn = componentTurn.get(comp);
				Integer cpuNo = solution.getDeploy().get(comp).get(turn) - 1;
				setNextTurn(comp);
				Processor proc = solution.getProcessors().get(cpuNo);
				
				double hardwardFail = getProbability(random, proc.getLowerFail(), proc.getUpperFail());
				failure *= ( 1 + hardwardFail );
				
				return failure;
			} catch (Exception ex) {
				throw new InterruptedException();			/// Invalid solution
			}
		}
	}
	
	private Double getProbability(Random random, Double lower, Double upper) {
		double interval = upper - lower;
		double rand = random.nextDouble();
		
		return (rand*interval + lower);
	}
	
	private void setNextTurn(int component) {
		int deploySize = solution.getDeploy().get(component).size();
		int nextTurn = componentTurn.get(component) + 1;
		if (nextTurn >= deploySize)
			nextTurn = 0;
		
		componentTurn.set(component, nextTurn);
	}
}
