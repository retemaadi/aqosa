package edu.leiden.aqosa.failure;

import java.util.ArrayList;
import java.util.List;

import edu.leiden.aqosa.model.Dependency;
import edu.leiden.aqosa.model.IR.DependencyType;

public class FaultTree extends Dependency {
	
	private List<Dependency> inputs;
	private DependencyType depType;
	
	public FaultTree(Integer componentNo) {
		super(componentNo.toString());
		this.depType = DependencyType.AND;
		this.inputs = new ArrayList<Dependency>();
	}
	
	public DependencyType getDependencyType() {
		return depType;
	}
	
	public void setDependencyType(DependencyType depType) {
		this.depType = depType;
	}
	
	public List<Dependency> getInputs() {
		return inputs;
	}
	
	public void addInput(Dependency input) {
		inputs.add(input);
	}
	
	public int getComponentNo() {
		return Integer.parseInt(id);
	}
	
}
