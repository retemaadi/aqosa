package edu.leiden.aqosa.failure;

import edu.leiden.aqosa.model.Dependency;

public class InputPort extends Dependency {
	
	public InputPort(String title) {
		super(title);
	}
	
}
