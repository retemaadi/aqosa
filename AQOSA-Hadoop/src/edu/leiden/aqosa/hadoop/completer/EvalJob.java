package edu.leiden.aqosa.hadoop.completer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.lang.SerializationUtils;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.SequenceFileInputFormat;
import org.apache.hadoop.mapred.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.opt4j.core.Objectives;
import org.opt4j.core.optimizer.TerminationException;
import org.opt4j.core.problem.Evaluator;

import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.model.ModelResource;
import edu.leiden.aqosa.optimizer.ArchEvaluator;
import edu.leiden.aqosa.solution.ArchSolution;
import edu.leiden.aqosa.solution.Quality;

public class EvalJob extends Configured implements Tool {

	public static final Path INPUT_PATH = new Path(HadoopCompleterModule.TMP_DIR, "eval_input");
	public static final Path OUTPUT_PATH = new Path(HadoopCompleterModule.TMP_DIR, "eval_output");
	
	public EvalJob() throws Exception {
		FileSystem fs = FileSystem.get(new JobConf());
		if (fs.exists(INPUT_PATH)) {
			fs.delete(INPUT_PATH, true);
		}
		fs.mkdirs(INPUT_PATH);
	}
	
	@Override
	public int run(String[] args) throws Exception {
		JobConf jobConf = new JobConf(super.getConf());
		
		jobConf.setInputFormat(SequenceFileInputFormat.class);
		jobConf.setOutputKeyClass(IntWritable.class);
		jobConf.setOutputValueClass(BytesWritable.class);
		jobConf.setOutputFormat(SequenceFileOutputFormat.class);
		jobConf.setMapperClass(EvalMapper.class);
		jobConf.setReducerClass(EvalReducer.class);
		
		File runner = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
		if (runner.isDirectory()) {
			System.err.println("NO JAR file!!!  -  " + runner);
			throw new TerminationException();
		} else {
			jobConf.setJar(runner.getName());
		}
		
		FileSystem fs = FileSystem.get(jobConf);
		if (fs.exists(OUTPUT_PATH)) {
			fs.delete(OUTPUT_PATH, true);
		}
		FileInputFormat.addInputPath(jobConf, INPUT_PATH);
		FileOutputFormat.setOutputPath(jobConf, OUTPUT_PATH);
		
		setConf(jobConf);
		
		Job job = new Job(jobConf);
		job.waitForCompletion(true);
		
		return 0;
	}

	public static boolean assignTasks(ArrayList<ArchSolution> phenotypes) {
		boolean written = false;

		final JobConf jobConf = new JobConf();
		try {
			FileSystem fs = FileSystem.get(jobConf);
			final Path file = new Path(INPUT_PATH, "i" + phenotypes.hashCode());
			
			final BytesWritable value = new BytesWritable(SerializationUtils.serialize(phenotypes));
			
			SequenceFile.Writer writer = SequenceFile.createWriter(fs, jobConf,
					file, IntWritable.class, BytesWritable.class, CompressionType.NONE);
			
			for (int i = 0; i<phenotypes.size(); i++) {
				final IntWritable key = new IntWritable(i);
				writer.append(key, value);
			}
			writer.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return written;
	}
	
	public SequenceFile.Reader readResults() {
		SequenceFile.Reader reader = null;
		final JobConf jobConf = (JobConf) getConf();
		try {
			FileSystem fs = FileSystem.get(jobConf);
			final Path outputPath = FileOutputFormat.getOutputPath(jobConf);
			FileStatus[] status = fs.globStatus(new Path(outputPath, "part-*"));
			if (status.length > 0) {
				FileStatus fileStat = status[0];
				reader = new SequenceFile.Reader(fs, fileStat.getPath(), jobConf);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return reader;
	}
}

class EvalMapper extends MapReduceBase implements Mapper<IntWritable, BytesWritable, IntWritable, BytesWritable> {
	
	@Override
	public void map(IntWritable key, BytesWritable value, OutputCollector<IntWritable, BytesWritable> collector, Reporter reporter) throws IOException {
		ModelResource modelResource = new ModelResource();
		
		final JobConf jobConf = new JobConf();
		FileSystem fs = FileSystem.get(jobConf);
		
		Path modelfile = new Path(HadoopCompleterModule.TMP_DIR, HadoopCompleterModule.COMMON_MODEL);
		FSDataInputStream inputModelStream = new FSDataInputStream(fs.open(modelfile));
		
		ArchModel model = new ArchModel(modelResource.readModel(inputModelStream));
		Evaluator<ArchSolution> evaluator = new ArchEvaluator(model);
		
		byte[] data = value.getBytes();
		@SuppressWarnings("unchecked")
		ArrayList<ArchSolution> array = (ArrayList<ArchSolution>) SerializationUtils.deserialize(data);
		
		Quality quality = null;
		ArchSolution phenotype = array.get(key.get());
		if (phenotype != null) {
			Objectives objs = evaluator.evaluate(phenotype);
			quality = new Quality(objs);
		}
		
		BytesWritable output = new BytesWritable(SerializationUtils.serialize(quality));
		collector.collect(key, output);
	}	
}

class EvalReducer extends MapReduceBase implements Reducer<IntWritable, BytesWritable, IntWritable, BytesWritable> {

	@Override
	public void reduce(IntWritable key, Iterator<BytesWritable> values, OutputCollector<IntWritable, BytesWritable> collector, Reporter reporter) throws IOException {
		Quality quality = null;
		if (values.hasNext()) {
			byte[] data = values.next().getBytes();
			quality = (Quality) SerializationUtils.deserialize(data);
		}
		
		BytesWritable value = new BytesWritable(SerializationUtils.serialize(quality));
		collector.collect(key, value);
	}	
}
