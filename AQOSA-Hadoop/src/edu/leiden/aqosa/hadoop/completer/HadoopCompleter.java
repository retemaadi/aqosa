package edu.leiden.aqosa.hadoop.completer;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.apache.commons.lang.SerializationUtils;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.util.ToolRunner;
import org.opt4j.core.Genotype;
import org.opt4j.core.Individual;
import org.opt4j.core.Individual.State;
import org.opt4j.core.Objectives;
import org.opt4j.core.optimizer.Control;
import org.opt4j.core.optimizer.IndividualCompleter;
import org.opt4j.core.optimizer.TerminationException;
import org.opt4j.core.problem.Decoder;
import org.opt4j.core.problem.Evaluator;

import com.google.inject.Inject;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.solution.ArchSolution;
import edu.leiden.aqosa.solution.Quality;

public class HadoopCompleter implements IndividualCompleter {
	
	protected final Control control;
	protected final Decoder<Genotype, Object> decoder;
	protected final Evaluator<ArchSolution> evaluator;
	protected final Objectives invalid;
	protected final String[] libjars = new String[2];

	/**
	 * Constructs a {@link DistCompleter}.
	 * 
	 * @param control
	 *            the optimization control
	 * @param decoder
	 *            the decoder
	 * @param evaluator
	 *            the evaluator
	 * @throws TerminationException 
	 */
	@Inject
	public HadoopCompleter(Control control, Decoder<Genotype, Object> decoder, Evaluator<ArchSolution> evaluator) throws TerminationException {
		super();
		this.control = control;
		this.decoder = decoder;
		this.evaluator = evaluator;
		this.invalid = evaluator.evaluate(null);
		
		try {
			File runner = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
			JarFile jar = new JarFile(runner);
			Manifest manifest = jar.getManifest();
			Attributes mainAttribs = manifest.getMainAttributes();
			String classpath = mainAttribs.getValue("Class-Path");
			classpath = "\"" + classpath.replaceAll(" ", ",") + "\""; 
			
			libjars[0] = "-libjars";
			libjars[1] = classpath;
			
			jar.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			throw new TerminationException();
		} catch (URISyntaxException ex) {
			ex.printStackTrace();
			throw new TerminationException();
		}
	}

	@Override
	public void complete(Individual... individuals) throws TerminationException {
		List<Individual> list = Arrays.asList(individuals);
		complete(list);
	}
	
	@Override
	public void complete(Iterable<? extends Individual> iterable) throws TerminationException {
		try {
			ArrayList<ArchSolution> phenotypes = new ArrayList<ArchSolution>();
			for (Individual individual : iterable) {
				if (!individual.isEvaluated()) {
					control.checkpoint();
					decode(individual);
					control.checkpoint();
					ArchSolution phenotype = (ArchSolution) individual.getPhenotype();
					
					phenotypes.add(phenotype);
				}
			}
			if (phenotypes.size() == 0)
				return;
			
			EvalJob evalJob = new EvalJob();
			EvalJob.assignTasks(phenotypes);
			ToolRunner.run(evalJob, libjars);
			
			Map<Integer, Quality> evals = pickResults(evalJob);
			int i = 0;
			for (Individual individual : iterable) {
				if (!individual.isEvaluated()) {
					Quality quality = evals.get(i);
					Objectives objectives = (quality == null) ? invalid : quality.getObjectives();
					
					evaluate(individual, objectives);
					i++;
				}
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new TerminationException();
		}			
	}

	protected void decode(Individual individual) {
		State state = individual.getState();
		if (state == State.GENOTYPED) {
			individual.setState(State.DECODING);
			ArchGenotype genotype = (ArchGenotype) individual.getGenotype();
			ArchSolution phenotype = (ArchSolution) decoder.decode(genotype);
			individual.setPhenotype(phenotype);
		} else {
			throw new IllegalStateException("Cannot decode Individual, current state: " + state);
		}
	}

	protected void evaluate(Individual individual, Objectives objectives) {
		State state = individual.getState();
		if (state == State.PHENOTYPED) {
			individual.setState(State.EVALUATING);
			//assert isSameLength(objectives.getKeys()) : "Objectives changed: " + objectives.getKeys();
			if (individual.getPhenotype() == null) {
				individual.setObjectives(invalid);
			} else {
				individual.setObjectives(objectives);
			}
		} else {
			throw new IllegalStateException("Cannot evaluate Individual, current state: " + state);
		}
	}

	protected Map<Integer, Quality> pickResults(EvalJob evalJob) {
		Map<Integer, Quality> map = new HashMap<Integer, Quality>();
		
		try {
			IntWritable key = new IntWritable();
			BytesWritable value = new BytesWritable();
			SequenceFile.Reader reader = evalJob.readResults();
			if (reader != null) {
				while (reader.next(key, value)) {
					Quality quality = (Quality) SerializationUtils.deserialize(value.getBytes());
					map.put(key.get(), quality);
				}
				
				reader.close();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
}
