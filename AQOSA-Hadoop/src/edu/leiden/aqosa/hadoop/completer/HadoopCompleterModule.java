package edu.leiden.aqosa.hadoop.completer;

import java.io.IOException;

import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.JobConf;
import org.opt4j.core.optimizer.IndividualCompleter;
import org.opt4j.core.start.Opt4JModule;

import edu.leiden.aqosa.model.ModelResource;

public class HadoopCompleterModule extends Opt4JModule {

	public static final Path TMP_DIR = new Path(".__TMP__");
	public static final String COMMON_MODEL = new String("hadoop-model.aqosa");
	
	protected final ModelResource modelResource;
	
	public HadoopCompleterModule(ModelResource modelResource) {
		this.modelResource = modelResource;
	}
	
	@Override
	protected void config() {
		try {
			final JobConf jobConf = new JobConf();
			FileSystem fs = FileSystem.get(jobConf);
			FSDataOutputStream outputModelStream = fs.create(new Path(TMP_DIR, COMMON_MODEL));
			modelResource.writeModel(outputModelStream);
			
			bind(IndividualCompleter.class).to(HadoopCompleter.class).in(SINGLETON);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}