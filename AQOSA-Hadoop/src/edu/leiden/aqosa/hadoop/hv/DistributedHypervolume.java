package edu.leiden.aqosa.hadoop.hv;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.apache.commons.lang.SerializationUtils;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.SequenceFileInputFormat;
import org.apache.hadoop.mapred.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.opt4j.core.Individual;
import org.opt4j.core.optimizer.TerminationException;
import org.opt4j.core.start.Constant;
import org.opt4j.optimizers.ea.Hypervolume;

import com.google.inject.Inject;

public class DistributedHypervolume extends Hypervolume {

	protected final String[] libjars = new String[2];
	
	@Inject
	public DistributedHypervolume(@Constant(value = "offset", namespace = Hypervolume.class) double offset) throws TerminationException {
		super(offset);
		
		try {
			File runner = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
			JarFile jar = new JarFile(runner);
			Manifest manifest = jar.getManifest();
			Attributes mainAttribs = manifest.getMainAttributes();
			String classpath = mainAttribs.getValue("Class-Path");
			classpath = "\"" + classpath.replaceAll(" ", ",") + "\""; 
			
			libjars[0] = "-libjars";
			libjars[1] = classpath;
			
			jar.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			throw new TerminationException();
		} catch (URISyntaxException ex) {
			ex.printStackTrace();
			throw new TerminationException();
		}
	}

	@Override
	public double calculateHypervolume(List<double[]> front, int nObjectives) {
		return super.calculateHypervolume(front, nObjectives);
	}
	
	@Override
	protected Map<Individual, Double> calculateHypervolumeContributionN(List<Individual> individuals, double offset) {
		Map<Individual, Double> hvc = new HashMap<Individual, Double>();
		ArrayList<double[]> front = (ArrayList<double[]>) invert(normalize(getMinValues(individuals)), offset);

		int m = front.get(0).length;
		double hvAll = calculateHypervolume(front, m);

		try {
			HVCJob job = new HVCJob();
			HVCJob.assignTasks(front);
			
			ToolRunner.run(job, libjars);
			
			Map<Integer, Double> results = pickResults(job);
			for (int i = 0; i < front.size(); i++) {
				hvc.put(individuals.get(i), hvAll - results.get(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return hvc;
	}

	protected Map<Integer, Double> pickResults(HVCJob job) {
		Map<Integer, Double> map = new HashMap<Integer, Double>();
		
		try {
			IntWritable key = new IntWritable();
			DoubleWritable value = new DoubleWritable();
			SequenceFile.Reader reader = job.readResults();
			if (reader != null) {
				while (reader.next(key, value)) {
					map.put(key.get(), value.get());
				}
				
				reader.close();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return map;
	}
}

class HVCJob extends Configured implements Tool {

	public static final Path INPUT_PATH = new Path(DistributedSMSModule.TMP_DIR, "hv_input");
	public static final Path OUTPUT_PATH = new Path(DistributedSMSModule.TMP_DIR, "hv_output");
	
	
	public HVCJob() throws Exception {
		FileSystem fs = FileSystem.get(new JobConf());
		if (fs.exists(INPUT_PATH)) {
			fs.delete(INPUT_PATH, true);
		}
		fs.mkdirs(INPUT_PATH);
	}
	
	@Override
	public int run(String[] arg0) throws Exception {
		JobConf jobConf = new JobConf(super.getConf());
		
		jobConf.setOutputKeyClass(IntWritable.class);
		jobConf.setOutputValueClass(DoubleWritable.class);
		
		jobConf.setInputFormat(SequenceFileInputFormat.class);
		jobConf.setOutputFormat(SequenceFileOutputFormat.class);
		
		jobConf.setMapperClass(HVMapper.class);
		jobConf.setReducerClass(HVReducer.class);
		
		File runner = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
		if (runner.isDirectory()) {
			System.err.println("NO JAR file!!!  -  " + runner);
			throw new TerminationException();
		} else {
			jobConf.setJar(runner.getName());
		}
		
		FileSystem fs = FileSystem.get(jobConf);
		if (fs.exists(OUTPUT_PATH)) {
			fs.delete(OUTPUT_PATH, true);
		}
		FileInputFormat.addInputPath(jobConf, INPUT_PATH);
		FileOutputFormat.setOutputPath(jobConf, OUTPUT_PATH);
		
		setConf(jobConf);
		
		Job job = new Job(jobConf);
		job.waitForCompletion(true);
		
		return 0;
	}

	public static void assignTasks(ArrayList<double[]> front) {
		final JobConf jobConf = new JobConf();
		try {
			FileSystem fs = FileSystem.get(jobConf);
			final Path file = new Path(INPUT_PATH, "i" + front.hashCode());
			
			final BytesWritable value = new BytesWritable(SerializationUtils.serialize(front));
			
			SequenceFile.Writer writer = SequenceFile.createWriter(fs, jobConf,
					file, IntWritable.class, BytesWritable.class, CompressionType.NONE);
			
			for (int i = 0; i<front.size(); i++) {
				final IntWritable key = new IntWritable(i);
				writer.append(key, value);
			}
			writer.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public SequenceFile.Reader readResults() {
		SequenceFile.Reader reader = null;
		final JobConf jobConf = (JobConf) getConf();
		try {
			FileSystem fs = FileSystem.get(jobConf);
			final Path outputPath = FileOutputFormat.getOutputPath(jobConf);
			FileStatus[] status = fs.globStatus(new Path(outputPath, "part-*"));
			if (status.length > 0) {
				FileStatus fileStat = status[0];
				reader = new SequenceFile.Reader(fs, fileStat.getPath(), jobConf);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return reader;
	}	
}

class HVMapper extends MapReduceBase implements Mapper<IntWritable, BytesWritable, IntWritable, DoubleWritable> {

	@Override
	public void map(IntWritable key, BytesWritable value, OutputCollector<IntWritable, DoubleWritable> collector, Reporter reporter)
			throws IOException {
		@SuppressWarnings("unchecked")
		ArrayList<double[]> front = (ArrayList<double[]>) SerializationUtils.deserialize(value.getBytes());
		
		front.remove(key.get());
		double offset = (new DistributedSMSModule()).getOffset();
		double iHv = 0;
		try {
			iHv = (new DistributedHypervolume(offset)).calculateHypervolume(front, front.get(0).length);
		} catch (TerminationException e) {
			e.printStackTrace();
		}
		
		DoubleWritable output = new DoubleWritable(iHv);
		collector.collect(key, output);
	}
}

class HVReducer extends MapReduceBase implements Reducer<IntWritable, DoubleWritable, IntWritable, DoubleWritable> {
	
	@Override
	public void reduce(IntWritable key, Iterator<DoubleWritable> hvc, OutputCollector<IntWritable, DoubleWritable> collector, Reporter reporter)
			throws IOException {
		double val = 0;
		if (hvc.hasNext()) {
			val = hvc.next().get();
		}
		
		DoubleWritable value = new DoubleWritable(val);
		collector.collect(key, value);
	}
}
