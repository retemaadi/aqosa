package edu.leiden.aqosa.hadoop.hv;

import org.apache.hadoop.fs.Path;
import org.opt4j.core.common.archive.FrontDensityIndicator;
import org.opt4j.optimizers.ea.Nsga2;
import org.opt4j.optimizers.ea.SMSModule;

public class DistributedSMSModule extends SMSModule {
	
	public static final Path TMP_DIR = new Path(".__TMP__");
	
	@Override
	public void config() {
		bindSelector(Nsga2.class);
		bind(FrontDensityIndicator.class).to(DistributedHypervolume.class);
	}
}
