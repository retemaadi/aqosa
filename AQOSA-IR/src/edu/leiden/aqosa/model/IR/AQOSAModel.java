/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.AQOSAModel#getAssembly <em>Assembly</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.AQOSAModel#getScenarios <em>Scenarios</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.AQOSAModel#getRepository <em>Repository</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.AQOSAModel#getObjectives <em>Objectives</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.AQOSAModel#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.AQOSAModel#getFeatureModel <em>Feature Model</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getAQOSAModel()
 * @model
 * @generated
 */
public interface AQOSAModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Assembly</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly</em>' containment reference.
	 * @see #setAssembly(Assembly)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getAQOSAModel_Assembly()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Assembly getAssembly();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.AQOSAModel#getAssembly <em>Assembly</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly</em>' containment reference.
	 * @see #getAssembly()
	 * @generated
	 */
	void setAssembly(Assembly value);

	/**
	 * Returns the value of the '<em><b>Scenarios</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenarios</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenarios</em>' containment reference.
	 * @see #setScenarios(Scenarios)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getAQOSAModel_Scenarios()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Scenarios getScenarios();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.AQOSAModel#getScenarios <em>Scenarios</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scenarios</em>' containment reference.
	 * @see #getScenarios()
	 * @generated
	 */
	void setScenarios(Scenarios value);

	/**
	 * Returns the value of the '<em><b>Repository</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Repository</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repository</em>' containment reference.
	 * @see #setRepository(Repository)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getAQOSAModel_Repository()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Repository getRepository();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.AQOSAModel#getRepository <em>Repository</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repository</em>' containment reference.
	 * @see #getRepository()
	 * @generated
	 */
	void setRepository(Repository value);

	/**
	 * Returns the value of the '<em><b>Objectives</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Objectives</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Objectives</em>' containment reference.
	 * @see #setObjectives(Objectives)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getAQOSAModel_Objectives()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Objectives getObjectives();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.AQOSAModel#getObjectives <em>Objectives</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Objectives</em>' containment reference.
	 * @see #getObjectives()
	 * @generated
	 */
	void setObjectives(Objectives value);

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference.
	 * @see #setConstraints(Constraints)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getAQOSAModel_Constraints()
	 * @model containment="true"
	 * @generated
	 */
	Constraints getConstraints();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.AQOSAModel#getConstraints <em>Constraints</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraints</em>' containment reference.
	 * @see #getConstraints()
	 * @generated
	 */
	void setConstraints(Constraints value);

	/**
	 * Returns the value of the '<em><b>Feature Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Model</em>' containment reference.
	 * @see #setFeatureModel(FeatureModel)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getAQOSAModel_FeatureModel()
	 * @model containment="true"
	 * @generated
	 */
	FeatureModel getFeatureModel();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.AQOSAModel#getFeatureModel <em>Feature Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Model</em>' containment reference.
	 * @see #getFeatureModel()
	 * @generated
	 */
	void setFeatureModel(FeatureModel value);

} // AQOSAModel
