/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.leiden.aqosa.model.IR.AQOSAFactory
 * @model kind="package"
 * @generated
 */
public interface AQOSAPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "IR";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://se.liacs.nl/aqosa/ir";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "aqosa.ir";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AQOSAPackage eINSTANCE = edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.AQOSAModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAModelImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getAQOSAModel()
	 * @generated
	 */
	int AQOSA_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Assembly</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQOSA_MODEL__ASSEMBLY = 0;

	/**
	 * The feature id for the '<em><b>Scenarios</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQOSA_MODEL__SCENARIOS = 1;

	/**
	 * The feature id for the '<em><b>Repository</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQOSA_MODEL__REPOSITORY = 2;

	/**
	 * The feature id for the '<em><b>Objectives</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQOSA_MODEL__OBJECTIVES = 3;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQOSA_MODEL__CONSTRAINTS = 4;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQOSA_MODEL__FEATURE_MODEL = 5;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQOSA_MODEL_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.AssemblyImpl <em>Assembly</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.AssemblyImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getAssembly()
	 * @generated
	 */
	int ASSEMBLY = 1;

	/**
	 * The feature id for the '<em><b>Component</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY__COMPONENT = 0;

	/**
	 * The feature id for the '<em><b>Flow</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY__FLOW = 1;

	/**
	 * The number of structural features of the '<em>Assembly</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ComponentImpl <em>Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ComponentImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getComponent()
	 * @generated
	 */
	int COMPONENT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Service</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__SERVICE = 1;

	/**
	 * The feature id for the '<em><b>Inport</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__INPORT = 2;

	/**
	 * The feature id for the '<em><b>Outport</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__OUTPORT = 3;

	/**
	 * The number of structural features of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ServiceImpl <em>Service</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ServiceImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getService()
	 * @generated
	 */
	int SERVICE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__NAME = 0;

	/**
	 * The number of structural features of the '<em>Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.FlowImpl <em>Flow</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.FlowImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getFlow()
	 * @generated
	 */
	int FLOW = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW__NAME = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW__ACTION = 1;

	/**
	 * The number of structural features of the '<em>Flow</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.InPortImpl <em>In Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.InPortImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getInPort()
	 * @generated
	 */
	int IN_PORT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PORT__NAME = 0;

	/**
	 * The number of structural features of the '<em>In Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PORT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.OutPortImpl <em>Out Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.OutPortImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getOutPort()
	 * @generated
	 */
	int OUT_PORT = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_PORT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Out Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_PORT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ActionImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 7;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ComputeActionImpl <em>Compute Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ComputeActionImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getComputeAction()
	 * @generated
	 */
	int COMPUTE_ACTION = 8;

	/**
	 * The feature id for the '<em><b>Service</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_ACTION__SERVICE = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Compute Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.CommunicateActionImpl <em>Communicate Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.CommunicateActionImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getCommunicateAction()
	 * @generated
	 */
	int COMMUNICATE_ACTION = 9;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATE_ACTION__SOURCE = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATE_ACTION__DESTINATION = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Communicate Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATE_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ScenariosImpl <em>Scenarios</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ScenariosImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getScenarios()
	 * @generated
	 */
	int SCENARIOS = 10;

	/**
	 * The feature id for the '<em><b>Flowset</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIOS__FLOWSET = 0;

	/**
	 * The number of structural features of the '<em>Scenarios</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIOS_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.FlowSetImpl <em>Flow Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.FlowSetImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getFlowSet()
	 * @generated
	 */
	int FLOW_SET = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_SET__NAME = 0;

	/**
	 * The feature id for the '<em><b>Flowinstance</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_SET__FLOWINSTANCE = 1;

	/**
	 * The feature id for the '<em><b>Completion Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_SET__COMPLETION_TIME = 2;

	/**
	 * The feature id for the '<em><b>Missed Percentage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_SET__MISSED_PERCENTAGE = 3;

	/**
	 * The number of structural features of the '<em>Flow Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_SET_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.FlowInstanceImpl <em>Flow Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.FlowInstanceImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getFlowInstance()
	 * @generated
	 */
	int FLOW_INSTANCE = 12;

	/**
	 * The feature id for the '<em><b>Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_INSTANCE__INSTANCE = 0;

	/**
	 * The feature id for the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_INSTANCE__START = 1;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_INSTANCE__TRIGGER = 2;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_INSTANCE__DEADLINE = 3;

	/**
	 * The number of structural features of the '<em>Flow Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_INSTANCE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.RepositoryImpl <em>Repository</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.RepositoryImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getRepository()
	 * @generated
	 */
	int REPOSITORY = 13;

	/**
	 * The feature id for the '<em><b>Componentinstance</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__COMPONENTINSTANCE = 0;

	/**
	 * The feature id for the '<em><b>Processor</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__PROCESSOR = 1;

	/**
	 * The feature id for the '<em><b>Bus</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__BUS = 2;

	/**
	 * The feature id for the '<em><b>Externalport</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__EXTERNALPORT = 3;

	/**
	 * The number of structural features of the '<em>Repository</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ComponentInstanceImpl <em>Component Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ComponentInstanceImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getComponentInstance()
	 * @generated
	 */
	int COMPONENT_INSTANCE = 14;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__ID = 0;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__COST = 1;

	/**
	 * The feature id for the '<em><b>Compatible</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__COMPATIBLE = 2;

	/**
	 * The feature id for the '<em><b>Service</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__SERVICE = 3;

	/**
	 * The feature id for the '<em><b>Variance Percentage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__VARIANCE_PERCENTAGE = 4;

	/**
	 * The number of structural features of the '<em>Component Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ServiceInstanceImpl <em>Service Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ServiceInstanceImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getServiceInstance()
	 * @generated
	 */
	int SERVICE_INSTANCE = 15;

	/**
	 * The feature id for the '<em><b>Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_INSTANCE__INSTANCE = 0;

	/**
	 * The feature id for the '<em><b>Cycles</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_INSTANCE__CYCLES = 1;

	/**
	 * The feature id for the '<em><b>Memory Usage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_INSTANCE__MEMORY_USAGE = 2;

	/**
	 * The feature id for the '<em><b>Network Usage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_INSTANCE__NETWORK_USAGE = 3;

	/**
	 * The feature id for the '<em><b>Provide</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_INSTANCE__PROVIDE = 4;

	/**
	 * The feature id for the '<em><b>Depend</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_INSTANCE__DEPEND = 5;

	/**
	 * The number of structural features of the '<em>Service Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_INSTANCE_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ProvidePortImpl <em>Provide Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ProvidePortImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getProvidePort()
	 * @generated
	 */
	int PROVIDE_PORT = 16;

	/**
	 * The feature id for the '<em><b>Connects</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDE_PORT__CONNECTS = 0;

	/**
	 * The number of structural features of the '<em>Provide Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDE_PORT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.RequirePortImpl <em>Require Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.RequirePortImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getRequirePort()
	 * @generated
	 */
	int REQUIRE_PORT = 17;

	/**
	 * The feature id for the '<em><b>Internal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRE_PORT__INTERNAL = 0;

	/**
	 * The feature id for the '<em><b>External</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRE_PORT__EXTERNAL = 1;

	/**
	 * The number of structural features of the '<em>Require Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRE_PORT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.DependenceImpl <em>Dependence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.DependenceImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getDependence()
	 * @generated
	 */
	int DEPENDENCE = 18;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCE__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Require</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCE__REQUIRE = 1;

	/**
	 * The feature id for the '<em><b>Depend</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCE__DEPEND = 2;

	/**
	 * The number of structural features of the '<em>Dependence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ProcessorImpl <em>Processor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ProcessorImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getProcessor()
	 * @generated
	 */
	int PROCESSOR = 19;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSOR__ID = 0;

	/**
	 * The feature id for the '<em><b>Clock</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSOR__CLOCK = 1;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSOR__COST = 2;

	/**
	 * The feature id for the '<em><b>Internal Bus Bandwidth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSOR__INTERNAL_BUS_BANDWIDTH = 3;

	/**
	 * The feature id for the '<em><b>Internal Bus Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSOR__INTERNAL_BUS_DELAY = 4;

	/**
	 * The feature id for the '<em><b>Lower Fail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSOR__LOWER_FAIL = 5;

	/**
	 * The feature id for the '<em><b>Upper Fail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSOR__UPPER_FAIL = 6;

	/**
	 * The number of structural features of the '<em>Processor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSOR_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.BusImpl <em>Bus</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.BusImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getBus()
	 * @generated
	 */
	int BUS = 20;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUS__ID = 0;

	/**
	 * The feature id for the '<em><b>Bandwidth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUS__BANDWIDTH = 1;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUS__DELAY = 2;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUS__COST = 3;

	/**
	 * The number of structural features of the '<em>Bus</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUS_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ExternalPortImpl <em>External Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ExternalPortImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getExternalPort()
	 * @generated
	 */
	int EXTERNAL_PORT = 21;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_PORT__ID = 0;

	/**
	 * The feature id for the '<em><b>Lower Fail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_PORT__LOWER_FAIL = 1;

	/**
	 * The feature id for the '<em><b>Upper Fail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_PORT__UPPER_FAIL = 2;

	/**
	 * The number of structural features of the '<em>External Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_PORT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ObjectivesImpl <em>Objectives</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ObjectivesImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getObjectives()
	 * @generated
	 */
	int OBJECTIVES = 22;

	/**
	 * The feature id for the '<em><b>Settings</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECTIVES__SETTINGS = 0;

	/**
	 * The number of structural features of the '<em>Objectives</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECTIVES_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.SettingsImpl <em>Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.SettingsImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getSettings()
	 * @generated
	 */
	int SETTINGS = 23;

	/**
	 * The feature id for the '<em><b>Evaluations</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTINGS__EVALUATIONS = 0;

	/**
	 * The feature id for the '<em><b>No Run</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTINGS__NO_RUN = 1;

	/**
	 * The feature id for the '<em><b>No Sampling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTINGS__NO_SAMPLING = 2;

	/**
	 * The feature id for the '<em><b>No Duplicate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTINGS__NO_DUPLICATE = 3;

	/**
	 * The feature id for the '<em><b>Min Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTINGS__MIN_COST = 4;

	/**
	 * The feature id for the '<em><b>Max Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTINGS__MAX_COST = 5;

	/**
	 * The number of structural features of the '<em>Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTINGS_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ConstraintsImpl <em>Constraints</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ConstraintsImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getConstraints()
	 * @generated
	 */
	int CONSTRAINTS = 24;

	/**
	 * The feature id for the '<em><b>Deployment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__DEPLOYMENT = 0;

	/**
	 * The feature id for the '<em><b>Replacement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__REPLACEMENT = 1;

	/**
	 * The number of structural features of the '<em>Constraints</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ComponentDeploymentImpl <em>Component Deployment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ComponentDeploymentImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getComponentDeployment()
	 * @generated
	 */
	int COMPONENT_DEPLOYMENT = 25;

	/**
	 * The number of structural features of the '<em>Component Deployment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DEPLOYMENT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.HardwareConstraintsImpl <em>Hardware Constraints</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.HardwareConstraintsImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getHardwareConstraints()
	 * @generated
	 */
	int HARDWARE_CONSTRAINTS = 26;

	/**
	 * The number of structural features of the '<em>Hardware Constraints</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARDWARE_CONSTRAINTS_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.ConflictComponentsImpl <em>Conflict Components</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.ConflictComponentsImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getConflictComponents()
	 * @generated
	 */
	int CONFLICT_COMPONENTS = 27;

	/**
	 * The feature id for the '<em><b>Component1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFLICT_COMPONENTS__COMPONENT1 = COMPONENT_DEPLOYMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFLICT_COMPONENTS__COMPONENT2 = COMPONENT_DEPLOYMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Conflict Components</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFLICT_COMPONENTS_FEATURE_COUNT = COMPONENT_DEPLOYMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.CoupleComponentsImpl <em>Couple Components</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.CoupleComponentsImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getCoupleComponents()
	 * @generated
	 */
	int COUPLE_COMPONENTS = 28;

	/**
	 * The feature id for the '<em><b>Component1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUPLE_COMPONENTS__COMPONENT1 = COMPONENT_DEPLOYMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUPLE_COMPONENTS__COMPONENT2 = COMPONENT_DEPLOYMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Couple Components</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUPLE_COMPONENTS_FEATURE_COUNT = COMPONENT_DEPLOYMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.FeatureModelImpl <em>Feature Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.FeatureModelImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getFeatureModel()
	 * @generated
	 */
	int FEATURE_MODEL = 29;

	/**
	 * The feature id for the '<em><b>Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__FEATURES = 0;

	/**
	 * The number of structural features of the '<em>Feature Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.impl.FeatureImpl <em>Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.impl.FeatureImpl
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getFeature()
	 * @generated
	 */
	int FEATURE = 30;

	/**
	 * The feature id for the '<em><b>Component Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__COMPONENT_RELATION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__FEATURES = 2;

	/**
	 * The number of structural features of the '<em>Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.DependencyType <em>Dependency Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.DependencyType
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getDependencyType()
	 * @generated
	 */
	int DEPENDENCY_TYPE = 31;

	/**
	 * The meta object id for the '{@link edu.leiden.aqosa.model.IR.Evaluations <em>Evaluations</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.leiden.aqosa.model.IR.Evaluations
	 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getEvaluations()
	 * @generated
	 */
	int EVALUATIONS = 32;


	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.AQOSAModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see edu.leiden.aqosa.model.IR.AQOSAModel
	 * @generated
	 */
	EClass getAQOSAModel();

	/**
	 * Returns the meta object for the containment reference '{@link edu.leiden.aqosa.model.IR.AQOSAModel#getAssembly <em>Assembly</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Assembly</em>'.
	 * @see edu.leiden.aqosa.model.IR.AQOSAModel#getAssembly()
	 * @see #getAQOSAModel()
	 * @generated
	 */
	EReference getAQOSAModel_Assembly();

	/**
	 * Returns the meta object for the containment reference '{@link edu.leiden.aqosa.model.IR.AQOSAModel#getScenarios <em>Scenarios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Scenarios</em>'.
	 * @see edu.leiden.aqosa.model.IR.AQOSAModel#getScenarios()
	 * @see #getAQOSAModel()
	 * @generated
	 */
	EReference getAQOSAModel_Scenarios();

	/**
	 * Returns the meta object for the containment reference '{@link edu.leiden.aqosa.model.IR.AQOSAModel#getRepository <em>Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repository</em>'.
	 * @see edu.leiden.aqosa.model.IR.AQOSAModel#getRepository()
	 * @see #getAQOSAModel()
	 * @generated
	 */
	EReference getAQOSAModel_Repository();

	/**
	 * Returns the meta object for the containment reference '{@link edu.leiden.aqosa.model.IR.AQOSAModel#getObjectives <em>Objectives</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Objectives</em>'.
	 * @see edu.leiden.aqosa.model.IR.AQOSAModel#getObjectives()
	 * @see #getAQOSAModel()
	 * @generated
	 */
	EReference getAQOSAModel_Objectives();

	/**
	 * Returns the meta object for the containment reference '{@link edu.leiden.aqosa.model.IR.AQOSAModel#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Constraints</em>'.
	 * @see edu.leiden.aqosa.model.IR.AQOSAModel#getConstraints()
	 * @see #getAQOSAModel()
	 * @generated
	 */
	EReference getAQOSAModel_Constraints();

	/**
	 * Returns the meta object for the containment reference '{@link edu.leiden.aqosa.model.IR.AQOSAModel#getFeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature Model</em>'.
	 * @see edu.leiden.aqosa.model.IR.AQOSAModel#getFeatureModel()
	 * @see #getAQOSAModel()
	 * @generated
	 */
	EReference getAQOSAModel_FeatureModel();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Assembly <em>Assembly</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assembly</em>'.
	 * @see edu.leiden.aqosa.model.IR.Assembly
	 * @generated
	 */
	EClass getAssembly();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Assembly#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Component</em>'.
	 * @see edu.leiden.aqosa.model.IR.Assembly#getComponent()
	 * @see #getAssembly()
	 * @generated
	 */
	EReference getAssembly_Component();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Assembly#getFlow <em>Flow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Flow</em>'.
	 * @see edu.leiden.aqosa.model.IR.Assembly#getFlow()
	 * @see #getAssembly()
	 * @generated
	 */
	EReference getAssembly_Flow();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component</em>'.
	 * @see edu.leiden.aqosa.model.IR.Component
	 * @generated
	 */
	EClass getComponent();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Component#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.leiden.aqosa.model.IR.Component#getName()
	 * @see #getComponent()
	 * @generated
	 */
	EAttribute getComponent_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Component#getService <em>Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Service</em>'.
	 * @see edu.leiden.aqosa.model.IR.Component#getService()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Service();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Component#getInport <em>Inport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inport</em>'.
	 * @see edu.leiden.aqosa.model.IR.Component#getInport()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Inport();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Component#getOutport <em>Outport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Outport</em>'.
	 * @see edu.leiden.aqosa.model.IR.Component#getOutport()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Outport();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Service <em>Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service</em>'.
	 * @see edu.leiden.aqosa.model.IR.Service
	 * @generated
	 */
	EClass getService();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Service#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.leiden.aqosa.model.IR.Service#getName()
	 * @see #getService()
	 * @generated
	 */
	EAttribute getService_Name();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Flow <em>Flow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Flow</em>'.
	 * @see edu.leiden.aqosa.model.IR.Flow
	 * @generated
	 */
	EClass getFlow();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Flow#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.leiden.aqosa.model.IR.Flow#getName()
	 * @see #getFlow()
	 * @generated
	 */
	EAttribute getFlow_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Flow#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action</em>'.
	 * @see edu.leiden.aqosa.model.IR.Flow#getAction()
	 * @see #getFlow()
	 * @generated
	 */
	EReference getFlow_Action();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.InPort <em>In Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Port</em>'.
	 * @see edu.leiden.aqosa.model.IR.InPort
	 * @generated
	 */
	EClass getInPort();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.InPort#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.leiden.aqosa.model.IR.InPort#getName()
	 * @see #getInPort()
	 * @generated
	 */
	EAttribute getInPort_Name();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.OutPort <em>Out Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Out Port</em>'.
	 * @see edu.leiden.aqosa.model.IR.OutPort
	 * @generated
	 */
	EClass getOutPort();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.OutPort#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.leiden.aqosa.model.IR.OutPort#getName()
	 * @see #getOutPort()
	 * @generated
	 */
	EAttribute getOutPort_Name();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see edu.leiden.aqosa.model.IR.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.ComputeAction <em>Compute Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compute Action</em>'.
	 * @see edu.leiden.aqosa.model.IR.ComputeAction
	 * @generated
	 */
	EClass getComputeAction();

	/**
	 * Returns the meta object for the reference '{@link edu.leiden.aqosa.model.IR.ComputeAction#getService <em>Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Service</em>'.
	 * @see edu.leiden.aqosa.model.IR.ComputeAction#getService()
	 * @see #getComputeAction()
	 * @generated
	 */
	EReference getComputeAction_Service();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.CommunicateAction <em>Communicate Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communicate Action</em>'.
	 * @see edu.leiden.aqosa.model.IR.CommunicateAction
	 * @generated
	 */
	EClass getCommunicateAction();

	/**
	 * Returns the meta object for the reference '{@link edu.leiden.aqosa.model.IR.CommunicateAction#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see edu.leiden.aqosa.model.IR.CommunicateAction#getSource()
	 * @see #getCommunicateAction()
	 * @generated
	 */
	EReference getCommunicateAction_Source();

	/**
	 * Returns the meta object for the reference '{@link edu.leiden.aqosa.model.IR.CommunicateAction#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Destination</em>'.
	 * @see edu.leiden.aqosa.model.IR.CommunicateAction#getDestination()
	 * @see #getCommunicateAction()
	 * @generated
	 */
	EReference getCommunicateAction_Destination();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Scenarios <em>Scenarios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scenarios</em>'.
	 * @see edu.leiden.aqosa.model.IR.Scenarios
	 * @generated
	 */
	EClass getScenarios();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Scenarios#getFlowset <em>Flowset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Flowset</em>'.
	 * @see edu.leiden.aqosa.model.IR.Scenarios#getFlowset()
	 * @see #getScenarios()
	 * @generated
	 */
	EReference getScenarios_Flowset();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.FlowSet <em>Flow Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Flow Set</em>'.
	 * @see edu.leiden.aqosa.model.IR.FlowSet
	 * @generated
	 */
	EClass getFlowSet();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.FlowSet#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.leiden.aqosa.model.IR.FlowSet#getName()
	 * @see #getFlowSet()
	 * @generated
	 */
	EAttribute getFlowSet_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.FlowSet#getFlowinstance <em>Flowinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Flowinstance</em>'.
	 * @see edu.leiden.aqosa.model.IR.FlowSet#getFlowinstance()
	 * @see #getFlowSet()
	 * @generated
	 */
	EReference getFlowSet_Flowinstance();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.FlowSet#getCompletionTime <em>Completion Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Completion Time</em>'.
	 * @see edu.leiden.aqosa.model.IR.FlowSet#getCompletionTime()
	 * @see #getFlowSet()
	 * @generated
	 */
	EAttribute getFlowSet_CompletionTime();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.FlowSet#getMissedPercentage <em>Missed Percentage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Missed Percentage</em>'.
	 * @see edu.leiden.aqosa.model.IR.FlowSet#getMissedPercentage()
	 * @see #getFlowSet()
	 * @generated
	 */
	EAttribute getFlowSet_MissedPercentage();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.FlowInstance <em>Flow Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Flow Instance</em>'.
	 * @see edu.leiden.aqosa.model.IR.FlowInstance
	 * @generated
	 */
	EClass getFlowInstance();

	/**
	 * Returns the meta object for the reference '{@link edu.leiden.aqosa.model.IR.FlowInstance#getInstance <em>Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Instance</em>'.
	 * @see edu.leiden.aqosa.model.IR.FlowInstance#getInstance()
	 * @see #getFlowInstance()
	 * @generated
	 */
	EReference getFlowInstance_Instance();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.FlowInstance#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start</em>'.
	 * @see edu.leiden.aqosa.model.IR.FlowInstance#getStart()
	 * @see #getFlowInstance()
	 * @generated
	 */
	EAttribute getFlowInstance_Start();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.FlowInstance#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Trigger</em>'.
	 * @see edu.leiden.aqosa.model.IR.FlowInstance#getTrigger()
	 * @see #getFlowInstance()
	 * @generated
	 */
	EAttribute getFlowInstance_Trigger();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.FlowInstance#getDeadline <em>Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deadline</em>'.
	 * @see edu.leiden.aqosa.model.IR.FlowInstance#getDeadline()
	 * @see #getFlowInstance()
	 * @generated
	 */
	EAttribute getFlowInstance_Deadline();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Repository <em>Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Repository</em>'.
	 * @see edu.leiden.aqosa.model.IR.Repository
	 * @generated
	 */
	EClass getRepository();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Repository#getComponentinstance <em>Componentinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Componentinstance</em>'.
	 * @see edu.leiden.aqosa.model.IR.Repository#getComponentinstance()
	 * @see #getRepository()
	 * @generated
	 */
	EReference getRepository_Componentinstance();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Repository#getProcessor <em>Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Processor</em>'.
	 * @see edu.leiden.aqosa.model.IR.Repository#getProcessor()
	 * @see #getRepository()
	 * @generated
	 */
	EReference getRepository_Processor();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Repository#getBus <em>Bus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Bus</em>'.
	 * @see edu.leiden.aqosa.model.IR.Repository#getBus()
	 * @see #getRepository()
	 * @generated
	 */
	EReference getRepository_Bus();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Repository#getExternalport <em>Externalport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Externalport</em>'.
	 * @see edu.leiden.aqosa.model.IR.Repository#getExternalport()
	 * @see #getRepository()
	 * @generated
	 */
	EReference getRepository_Externalport();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.ComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Instance</em>'.
	 * @see edu.leiden.aqosa.model.IR.ComponentInstance
	 * @generated
	 */
	EClass getComponentInstance();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.ComponentInstance#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see edu.leiden.aqosa.model.IR.ComponentInstance#getId()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EAttribute getComponentInstance_Id();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.ComponentInstance#getCost <em>Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cost</em>'.
	 * @see edu.leiden.aqosa.model.IR.ComponentInstance#getCost()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EAttribute getComponentInstance_Cost();

	/**
	 * Returns the meta object for the reference '{@link edu.leiden.aqosa.model.IR.ComponentInstance#getCompatible <em>Compatible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Compatible</em>'.
	 * @see edu.leiden.aqosa.model.IR.ComponentInstance#getCompatible()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EReference getComponentInstance_Compatible();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.ComponentInstance#getService <em>Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Service</em>'.
	 * @see edu.leiden.aqosa.model.IR.ComponentInstance#getService()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EReference getComponentInstance_Service();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.ComponentInstance#getVariancePercentage <em>Variance Percentage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Variance Percentage</em>'.
	 * @see edu.leiden.aqosa.model.IR.ComponentInstance#getVariancePercentage()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EAttribute getComponentInstance_VariancePercentage();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.ServiceInstance <em>Service Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Instance</em>'.
	 * @see edu.leiden.aqosa.model.IR.ServiceInstance
	 * @generated
	 */
	EClass getServiceInstance();

	/**
	 * Returns the meta object for the reference '{@link edu.leiden.aqosa.model.IR.ServiceInstance#getInstance <em>Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Instance</em>'.
	 * @see edu.leiden.aqosa.model.IR.ServiceInstance#getInstance()
	 * @see #getServiceInstance()
	 * @generated
	 */
	EReference getServiceInstance_Instance();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.ServiceInstance#getCycles <em>Cycles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cycles</em>'.
	 * @see edu.leiden.aqosa.model.IR.ServiceInstance#getCycles()
	 * @see #getServiceInstance()
	 * @generated
	 */
	EAttribute getServiceInstance_Cycles();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.ServiceInstance#getMemoryUsage <em>Memory Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Memory Usage</em>'.
	 * @see edu.leiden.aqosa.model.IR.ServiceInstance#getMemoryUsage()
	 * @see #getServiceInstance()
	 * @generated
	 */
	EAttribute getServiceInstance_MemoryUsage();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.ServiceInstance#getNetworkUsage <em>Network Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Network Usage</em>'.
	 * @see edu.leiden.aqosa.model.IR.ServiceInstance#getNetworkUsage()
	 * @see #getServiceInstance()
	 * @generated
	 */
	EAttribute getServiceInstance_NetworkUsage();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.ServiceInstance#getProvide <em>Provide</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provide</em>'.
	 * @see edu.leiden.aqosa.model.IR.ServiceInstance#getProvide()
	 * @see #getServiceInstance()
	 * @generated
	 */
	EReference getServiceInstance_Provide();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.ServiceInstance#getDepend <em>Depend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Depend</em>'.
	 * @see edu.leiden.aqosa.model.IR.ServiceInstance#getDepend()
	 * @see #getServiceInstance()
	 * @generated
	 */
	EReference getServiceInstance_Depend();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.ProvidePort <em>Provide Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provide Port</em>'.
	 * @see edu.leiden.aqosa.model.IR.ProvidePort
	 * @generated
	 */
	EClass getProvidePort();

	/**
	 * Returns the meta object for the reference '{@link edu.leiden.aqosa.model.IR.ProvidePort#getConnects <em>Connects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Connects</em>'.
	 * @see edu.leiden.aqosa.model.IR.ProvidePort#getConnects()
	 * @see #getProvidePort()
	 * @generated
	 */
	EReference getProvidePort_Connects();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.RequirePort <em>Require Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Require Port</em>'.
	 * @see edu.leiden.aqosa.model.IR.RequirePort
	 * @generated
	 */
	EClass getRequirePort();

	/**
	 * Returns the meta object for the reference '{@link edu.leiden.aqosa.model.IR.RequirePort#getInternal <em>Internal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Internal</em>'.
	 * @see edu.leiden.aqosa.model.IR.RequirePort#getInternal()
	 * @see #getRequirePort()
	 * @generated
	 */
	EReference getRequirePort_Internal();

	/**
	 * Returns the meta object for the reference '{@link edu.leiden.aqosa.model.IR.RequirePort#getExternal <em>External</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>External</em>'.
	 * @see edu.leiden.aqosa.model.IR.RequirePort#getExternal()
	 * @see #getRequirePort()
	 * @generated
	 */
	EReference getRequirePort_External();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Dependence <em>Dependence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dependence</em>'.
	 * @see edu.leiden.aqosa.model.IR.Dependence
	 * @generated
	 */
	EClass getDependence();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Dependence#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see edu.leiden.aqosa.model.IR.Dependence#getType()
	 * @see #getDependence()
	 * @generated
	 */
	EAttribute getDependence_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Dependence#getRequire <em>Require</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Require</em>'.
	 * @see edu.leiden.aqosa.model.IR.Dependence#getRequire()
	 * @see #getDependence()
	 * @generated
	 */
	EReference getDependence_Require();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Dependence#getDepend <em>Depend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Depend</em>'.
	 * @see edu.leiden.aqosa.model.IR.Dependence#getDepend()
	 * @see #getDependence()
	 * @generated
	 */
	EReference getDependence_Depend();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Processor <em>Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Processor</em>'.
	 * @see edu.leiden.aqosa.model.IR.Processor
	 * @generated
	 */
	EClass getProcessor();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Processor#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see edu.leiden.aqosa.model.IR.Processor#getId()
	 * @see #getProcessor()
	 * @generated
	 */
	EAttribute getProcessor_Id();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Processor#getClock <em>Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Clock</em>'.
	 * @see edu.leiden.aqosa.model.IR.Processor#getClock()
	 * @see #getProcessor()
	 * @generated
	 */
	EAttribute getProcessor_Clock();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Processor#getCost <em>Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cost</em>'.
	 * @see edu.leiden.aqosa.model.IR.Processor#getCost()
	 * @see #getProcessor()
	 * @generated
	 */
	EAttribute getProcessor_Cost();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Processor#getInternalBusBandwidth <em>Internal Bus Bandwidth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Internal Bus Bandwidth</em>'.
	 * @see edu.leiden.aqosa.model.IR.Processor#getInternalBusBandwidth()
	 * @see #getProcessor()
	 * @generated
	 */
	EAttribute getProcessor_InternalBusBandwidth();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Processor#getInternalBusDelay <em>Internal Bus Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Internal Bus Delay</em>'.
	 * @see edu.leiden.aqosa.model.IR.Processor#getInternalBusDelay()
	 * @see #getProcessor()
	 * @generated
	 */
	EAttribute getProcessor_InternalBusDelay();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Processor#getLowerFail <em>Lower Fail</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower Fail</em>'.
	 * @see edu.leiden.aqosa.model.IR.Processor#getLowerFail()
	 * @see #getProcessor()
	 * @generated
	 */
	EAttribute getProcessor_LowerFail();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Processor#getUpperFail <em>Upper Fail</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper Fail</em>'.
	 * @see edu.leiden.aqosa.model.IR.Processor#getUpperFail()
	 * @see #getProcessor()
	 * @generated
	 */
	EAttribute getProcessor_UpperFail();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Bus <em>Bus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bus</em>'.
	 * @see edu.leiden.aqosa.model.IR.Bus
	 * @generated
	 */
	EClass getBus();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Bus#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see edu.leiden.aqosa.model.IR.Bus#getId()
	 * @see #getBus()
	 * @generated
	 */
	EAttribute getBus_Id();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Bus#getBandwidth <em>Bandwidth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bandwidth</em>'.
	 * @see edu.leiden.aqosa.model.IR.Bus#getBandwidth()
	 * @see #getBus()
	 * @generated
	 */
	EAttribute getBus_Bandwidth();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Bus#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delay</em>'.
	 * @see edu.leiden.aqosa.model.IR.Bus#getDelay()
	 * @see #getBus()
	 * @generated
	 */
	EAttribute getBus_Delay();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Bus#getCost <em>Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cost</em>'.
	 * @see edu.leiden.aqosa.model.IR.Bus#getCost()
	 * @see #getBus()
	 * @generated
	 */
	EAttribute getBus_Cost();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.ExternalPort <em>External Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Port</em>'.
	 * @see edu.leiden.aqosa.model.IR.ExternalPort
	 * @generated
	 */
	EClass getExternalPort();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.ExternalPort#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see edu.leiden.aqosa.model.IR.ExternalPort#getId()
	 * @see #getExternalPort()
	 * @generated
	 */
	EAttribute getExternalPort_Id();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.ExternalPort#getLowerFail <em>Lower Fail</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower Fail</em>'.
	 * @see edu.leiden.aqosa.model.IR.ExternalPort#getLowerFail()
	 * @see #getExternalPort()
	 * @generated
	 */
	EAttribute getExternalPort_LowerFail();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.ExternalPort#getUpperFail <em>Upper Fail</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper Fail</em>'.
	 * @see edu.leiden.aqosa.model.IR.ExternalPort#getUpperFail()
	 * @see #getExternalPort()
	 * @generated
	 */
	EAttribute getExternalPort_UpperFail();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Objectives <em>Objectives</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Objectives</em>'.
	 * @see edu.leiden.aqosa.model.IR.Objectives
	 * @generated
	 */
	EClass getObjectives();

	/**
	 * Returns the meta object for the containment reference '{@link edu.leiden.aqosa.model.IR.Objectives#getSettings <em>Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Settings</em>'.
	 * @see edu.leiden.aqosa.model.IR.Objectives#getSettings()
	 * @see #getObjectives()
	 * @generated
	 */
	EReference getObjectives_Settings();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Settings <em>Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Settings</em>'.
	 * @see edu.leiden.aqosa.model.IR.Settings
	 * @generated
	 */
	EClass getSettings();

	/**
	 * Returns the meta object for the attribute list '{@link edu.leiden.aqosa.model.IR.Settings#getEvaluations <em>Evaluations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Evaluations</em>'.
	 * @see edu.leiden.aqosa.model.IR.Settings#getEvaluations()
	 * @see #getSettings()
	 * @generated
	 */
	EAttribute getSettings_Evaluations();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Settings#getNoRun <em>No Run</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>No Run</em>'.
	 * @see edu.leiden.aqosa.model.IR.Settings#getNoRun()
	 * @see #getSettings()
	 * @generated
	 */
	EAttribute getSettings_NoRun();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Settings#getNoSampling <em>No Sampling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>No Sampling</em>'.
	 * @see edu.leiden.aqosa.model.IR.Settings#getNoSampling()
	 * @see #getSettings()
	 * @generated
	 */
	EAttribute getSettings_NoSampling();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Settings#getNoDuplicate <em>No Duplicate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>No Duplicate</em>'.
	 * @see edu.leiden.aqosa.model.IR.Settings#getNoDuplicate()
	 * @see #getSettings()
	 * @generated
	 */
	EAttribute getSettings_NoDuplicate();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Settings#getMinCost <em>Min Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Cost</em>'.
	 * @see edu.leiden.aqosa.model.IR.Settings#getMinCost()
	 * @see #getSettings()
	 * @generated
	 */
	EAttribute getSettings_MinCost();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Settings#getMaxCost <em>Max Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Cost</em>'.
	 * @see edu.leiden.aqosa.model.IR.Settings#getMaxCost()
	 * @see #getSettings()
	 * @generated
	 */
	EAttribute getSettings_MaxCost();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Constraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constraints</em>'.
	 * @see edu.leiden.aqosa.model.IR.Constraints
	 * @generated
	 */
	EClass getConstraints();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Constraints#getDeployment <em>Deployment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Deployment</em>'.
	 * @see edu.leiden.aqosa.model.IR.Constraints#getDeployment()
	 * @see #getConstraints()
	 * @generated
	 */
	EReference getConstraints_Deployment();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Constraints#getReplacement <em>Replacement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Replacement</em>'.
	 * @see edu.leiden.aqosa.model.IR.Constraints#getReplacement()
	 * @see #getConstraints()
	 * @generated
	 */
	EReference getConstraints_Replacement();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.ComponentDeployment <em>Component Deployment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Deployment</em>'.
	 * @see edu.leiden.aqosa.model.IR.ComponentDeployment
	 * @generated
	 */
	EClass getComponentDeployment();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.HardwareConstraints <em>Hardware Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hardware Constraints</em>'.
	 * @see edu.leiden.aqosa.model.IR.HardwareConstraints
	 * @generated
	 */
	EClass getHardwareConstraints();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.ConflictComponents <em>Conflict Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conflict Components</em>'.
	 * @see edu.leiden.aqosa.model.IR.ConflictComponents
	 * @generated
	 */
	EClass getConflictComponents();

	/**
	 * Returns the meta object for the reference '{@link edu.leiden.aqosa.model.IR.ConflictComponents#getComponent1 <em>Component1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component1</em>'.
	 * @see edu.leiden.aqosa.model.IR.ConflictComponents#getComponent1()
	 * @see #getConflictComponents()
	 * @generated
	 */
	EReference getConflictComponents_Component1();

	/**
	 * Returns the meta object for the reference '{@link edu.leiden.aqosa.model.IR.ConflictComponents#getComponent2 <em>Component2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component2</em>'.
	 * @see edu.leiden.aqosa.model.IR.ConflictComponents#getComponent2()
	 * @see #getConflictComponents()
	 * @generated
	 */
	EReference getConflictComponents_Component2();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.CoupleComponents <em>Couple Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Couple Components</em>'.
	 * @see edu.leiden.aqosa.model.IR.CoupleComponents
	 * @generated
	 */
	EClass getCoupleComponents();

	/**
	 * Returns the meta object for the reference '{@link edu.leiden.aqosa.model.IR.CoupleComponents#getComponent1 <em>Component1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component1</em>'.
	 * @see edu.leiden.aqosa.model.IR.CoupleComponents#getComponent1()
	 * @see #getCoupleComponents()
	 * @generated
	 */
	EReference getCoupleComponents_Component1();

	/**
	 * Returns the meta object for the reference '{@link edu.leiden.aqosa.model.IR.CoupleComponents#getComponent2 <em>Component2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component2</em>'.
	 * @see edu.leiden.aqosa.model.IR.CoupleComponents#getComponent2()
	 * @see #getCoupleComponents()
	 * @generated
	 */
	EReference getCoupleComponents_Component2();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.FeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Model</em>'.
	 * @see edu.leiden.aqosa.model.IR.FeatureModel
	 * @generated
	 */
	EClass getFeatureModel();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.FeatureModel#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Features</em>'.
	 * @see edu.leiden.aqosa.model.IR.FeatureModel#getFeatures()
	 * @see #getFeatureModel()
	 * @generated
	 */
	EReference getFeatureModel_Features();

	/**
	 * Returns the meta object for class '{@link edu.leiden.aqosa.model.IR.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature</em>'.
	 * @see edu.leiden.aqosa.model.IR.Feature
	 * @generated
	 */
	EClass getFeature();

	/**
	 * Returns the meta object for the reference list '{@link edu.leiden.aqosa.model.IR.Feature#getComponentRelation <em>Component Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Component Relation</em>'.
	 * @see edu.leiden.aqosa.model.IR.Feature#getComponentRelation()
	 * @see #getFeature()
	 * @generated
	 */
	EReference getFeature_ComponentRelation();

	/**
	 * Returns the meta object for the attribute '{@link edu.leiden.aqosa.model.IR.Feature#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.leiden.aqosa.model.IR.Feature#getName()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.leiden.aqosa.model.IR.Feature#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Features</em>'.
	 * @see edu.leiden.aqosa.model.IR.Feature#getFeatures()
	 * @see #getFeature()
	 * @generated
	 */
	EReference getFeature_Features();

	/**
	 * Returns the meta object for enum '{@link edu.leiden.aqosa.model.IR.DependencyType <em>Dependency Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Dependency Type</em>'.
	 * @see edu.leiden.aqosa.model.IR.DependencyType
	 * @generated
	 */
	EEnum getDependencyType();

	/**
	 * Returns the meta object for enum '{@link edu.leiden.aqosa.model.IR.Evaluations <em>Evaluations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Evaluations</em>'.
	 * @see edu.leiden.aqosa.model.IR.Evaluations
	 * @generated
	 */
	EEnum getEvaluations();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AQOSAFactory getAQOSAFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.AQOSAModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAModelImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getAQOSAModel()
		 * @generated
		 */
		EClass AQOSA_MODEL = eINSTANCE.getAQOSAModel();

		/**
		 * The meta object literal for the '<em><b>Assembly</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQOSA_MODEL__ASSEMBLY = eINSTANCE.getAQOSAModel_Assembly();

		/**
		 * The meta object literal for the '<em><b>Scenarios</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQOSA_MODEL__SCENARIOS = eINSTANCE.getAQOSAModel_Scenarios();

		/**
		 * The meta object literal for the '<em><b>Repository</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQOSA_MODEL__REPOSITORY = eINSTANCE.getAQOSAModel_Repository();

		/**
		 * The meta object literal for the '<em><b>Objectives</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQOSA_MODEL__OBJECTIVES = eINSTANCE.getAQOSAModel_Objectives();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQOSA_MODEL__CONSTRAINTS = eINSTANCE.getAQOSAModel_Constraints();

		/**
		 * The meta object literal for the '<em><b>Feature Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQOSA_MODEL__FEATURE_MODEL = eINSTANCE.getAQOSAModel_FeatureModel();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.AssemblyImpl <em>Assembly</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.AssemblyImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getAssembly()
		 * @generated
		 */
		EClass ASSEMBLY = eINSTANCE.getAssembly();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSEMBLY__COMPONENT = eINSTANCE.getAssembly_Component();

		/**
		 * The meta object literal for the '<em><b>Flow</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSEMBLY__FLOW = eINSTANCE.getAssembly_Flow();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ComponentImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getComponent()
		 * @generated
		 */
		EClass COMPONENT = eINSTANCE.getComponent();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT__NAME = eINSTANCE.getComponent_Name();

		/**
		 * The meta object literal for the '<em><b>Service</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__SERVICE = eINSTANCE.getComponent_Service();

		/**
		 * The meta object literal for the '<em><b>Inport</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__INPORT = eINSTANCE.getComponent_Inport();

		/**
		 * The meta object literal for the '<em><b>Outport</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__OUTPORT = eINSTANCE.getComponent_Outport();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ServiceImpl <em>Service</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ServiceImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getService()
		 * @generated
		 */
		EClass SERVICE = eINSTANCE.getService();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE__NAME = eINSTANCE.getService_Name();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.FlowImpl <em>Flow</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.FlowImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getFlow()
		 * @generated
		 */
		EClass FLOW = eINSTANCE.getFlow();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOW__NAME = eINSTANCE.getFlow_Name();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FLOW__ACTION = eINSTANCE.getFlow_Action();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.InPortImpl <em>In Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.InPortImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getInPort()
		 * @generated
		 */
		EClass IN_PORT = eINSTANCE.getInPort();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_PORT__NAME = eINSTANCE.getInPort_Name();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.OutPortImpl <em>Out Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.OutPortImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getOutPort()
		 * @generated
		 */
		EClass OUT_PORT = eINSTANCE.getOutPort();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OUT_PORT__NAME = eINSTANCE.getOutPort_Name();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ActionImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ComputeActionImpl <em>Compute Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ComputeActionImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getComputeAction()
		 * @generated
		 */
		EClass COMPUTE_ACTION = eINSTANCE.getComputeAction();

		/**
		 * The meta object literal for the '<em><b>Service</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPUTE_ACTION__SERVICE = eINSTANCE.getComputeAction_Service();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.CommunicateActionImpl <em>Communicate Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.CommunicateActionImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getCommunicateAction()
		 * @generated
		 */
		EClass COMMUNICATE_ACTION = eINSTANCE.getCommunicateAction();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATE_ACTION__SOURCE = eINSTANCE.getCommunicateAction_Source();

		/**
		 * The meta object literal for the '<em><b>Destination</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATE_ACTION__DESTINATION = eINSTANCE.getCommunicateAction_Destination();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ScenariosImpl <em>Scenarios</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ScenariosImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getScenarios()
		 * @generated
		 */
		EClass SCENARIOS = eINSTANCE.getScenarios();

		/**
		 * The meta object literal for the '<em><b>Flowset</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIOS__FLOWSET = eINSTANCE.getScenarios_Flowset();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.FlowSetImpl <em>Flow Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.FlowSetImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getFlowSet()
		 * @generated
		 */
		EClass FLOW_SET = eINSTANCE.getFlowSet();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOW_SET__NAME = eINSTANCE.getFlowSet_Name();

		/**
		 * The meta object literal for the '<em><b>Flowinstance</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FLOW_SET__FLOWINSTANCE = eINSTANCE.getFlowSet_Flowinstance();

		/**
		 * The meta object literal for the '<em><b>Completion Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOW_SET__COMPLETION_TIME = eINSTANCE.getFlowSet_CompletionTime();

		/**
		 * The meta object literal for the '<em><b>Missed Percentage</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOW_SET__MISSED_PERCENTAGE = eINSTANCE.getFlowSet_MissedPercentage();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.FlowInstanceImpl <em>Flow Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.FlowInstanceImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getFlowInstance()
		 * @generated
		 */
		EClass FLOW_INSTANCE = eINSTANCE.getFlowInstance();

		/**
		 * The meta object literal for the '<em><b>Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FLOW_INSTANCE__INSTANCE = eINSTANCE.getFlowInstance_Instance();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOW_INSTANCE__START = eINSTANCE.getFlowInstance_Start();

		/**
		 * The meta object literal for the '<em><b>Trigger</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOW_INSTANCE__TRIGGER = eINSTANCE.getFlowInstance_Trigger();

		/**
		 * The meta object literal for the '<em><b>Deadline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOW_INSTANCE__DEADLINE = eINSTANCE.getFlowInstance_Deadline();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.RepositoryImpl <em>Repository</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.RepositoryImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getRepository()
		 * @generated
		 */
		EClass REPOSITORY = eINSTANCE.getRepository();

		/**
		 * The meta object literal for the '<em><b>Componentinstance</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPOSITORY__COMPONENTINSTANCE = eINSTANCE.getRepository_Componentinstance();

		/**
		 * The meta object literal for the '<em><b>Processor</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPOSITORY__PROCESSOR = eINSTANCE.getRepository_Processor();

		/**
		 * The meta object literal for the '<em><b>Bus</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPOSITORY__BUS = eINSTANCE.getRepository_Bus();

		/**
		 * The meta object literal for the '<em><b>Externalport</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPOSITORY__EXTERNALPORT = eINSTANCE.getRepository_Externalport();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ComponentInstanceImpl <em>Component Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ComponentInstanceImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getComponentInstance()
		 * @generated
		 */
		EClass COMPONENT_INSTANCE = eINSTANCE.getComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT_INSTANCE__ID = eINSTANCE.getComponentInstance_Id();

		/**
		 * The meta object literal for the '<em><b>Cost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT_INSTANCE__COST = eINSTANCE.getComponentInstance_Cost();

		/**
		 * The meta object literal for the '<em><b>Compatible</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE__COMPATIBLE = eINSTANCE.getComponentInstance_Compatible();

		/**
		 * The meta object literal for the '<em><b>Service</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE__SERVICE = eINSTANCE.getComponentInstance_Service();

		/**
		 * The meta object literal for the '<em><b>Variance Percentage</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT_INSTANCE__VARIANCE_PERCENTAGE = eINSTANCE.getComponentInstance_VariancePercentage();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ServiceInstanceImpl <em>Service Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ServiceInstanceImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getServiceInstance()
		 * @generated
		 */
		EClass SERVICE_INSTANCE = eINSTANCE.getServiceInstance();

		/**
		 * The meta object literal for the '<em><b>Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_INSTANCE__INSTANCE = eINSTANCE.getServiceInstance_Instance();

		/**
		 * The meta object literal for the '<em><b>Cycles</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE_INSTANCE__CYCLES = eINSTANCE.getServiceInstance_Cycles();

		/**
		 * The meta object literal for the '<em><b>Memory Usage</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE_INSTANCE__MEMORY_USAGE = eINSTANCE.getServiceInstance_MemoryUsage();

		/**
		 * The meta object literal for the '<em><b>Network Usage</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE_INSTANCE__NETWORK_USAGE = eINSTANCE.getServiceInstance_NetworkUsage();

		/**
		 * The meta object literal for the '<em><b>Provide</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_INSTANCE__PROVIDE = eINSTANCE.getServiceInstance_Provide();

		/**
		 * The meta object literal for the '<em><b>Depend</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_INSTANCE__DEPEND = eINSTANCE.getServiceInstance_Depend();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ProvidePortImpl <em>Provide Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ProvidePortImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getProvidePort()
		 * @generated
		 */
		EClass PROVIDE_PORT = eINSTANCE.getProvidePort();

		/**
		 * The meta object literal for the '<em><b>Connects</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROVIDE_PORT__CONNECTS = eINSTANCE.getProvidePort_Connects();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.RequirePortImpl <em>Require Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.RequirePortImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getRequirePort()
		 * @generated
		 */
		EClass REQUIRE_PORT = eINSTANCE.getRequirePort();

		/**
		 * The meta object literal for the '<em><b>Internal</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIRE_PORT__INTERNAL = eINSTANCE.getRequirePort_Internal();

		/**
		 * The meta object literal for the '<em><b>External</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIRE_PORT__EXTERNAL = eINSTANCE.getRequirePort_External();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.DependenceImpl <em>Dependence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.DependenceImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getDependence()
		 * @generated
		 */
		EClass DEPENDENCE = eINSTANCE.getDependence();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCE__TYPE = eINSTANCE.getDependence_Type();

		/**
		 * The meta object literal for the '<em><b>Require</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPENDENCE__REQUIRE = eINSTANCE.getDependence_Require();

		/**
		 * The meta object literal for the '<em><b>Depend</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPENDENCE__DEPEND = eINSTANCE.getDependence_Depend();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ProcessorImpl <em>Processor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ProcessorImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getProcessor()
		 * @generated
		 */
		EClass PROCESSOR = eINSTANCE.getProcessor();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESSOR__ID = eINSTANCE.getProcessor_Id();

		/**
		 * The meta object literal for the '<em><b>Clock</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESSOR__CLOCK = eINSTANCE.getProcessor_Clock();

		/**
		 * The meta object literal for the '<em><b>Cost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESSOR__COST = eINSTANCE.getProcessor_Cost();

		/**
		 * The meta object literal for the '<em><b>Internal Bus Bandwidth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESSOR__INTERNAL_BUS_BANDWIDTH = eINSTANCE.getProcessor_InternalBusBandwidth();

		/**
		 * The meta object literal for the '<em><b>Internal Bus Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESSOR__INTERNAL_BUS_DELAY = eINSTANCE.getProcessor_InternalBusDelay();

		/**
		 * The meta object literal for the '<em><b>Lower Fail</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESSOR__LOWER_FAIL = eINSTANCE.getProcessor_LowerFail();

		/**
		 * The meta object literal for the '<em><b>Upper Fail</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESSOR__UPPER_FAIL = eINSTANCE.getProcessor_UpperFail();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.BusImpl <em>Bus</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.BusImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getBus()
		 * @generated
		 */
		EClass BUS = eINSTANCE.getBus();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUS__ID = eINSTANCE.getBus_Id();

		/**
		 * The meta object literal for the '<em><b>Bandwidth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUS__BANDWIDTH = eINSTANCE.getBus_Bandwidth();

		/**
		 * The meta object literal for the '<em><b>Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUS__DELAY = eINSTANCE.getBus_Delay();

		/**
		 * The meta object literal for the '<em><b>Cost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUS__COST = eINSTANCE.getBus_Cost();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ExternalPortImpl <em>External Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ExternalPortImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getExternalPort()
		 * @generated
		 */
		EClass EXTERNAL_PORT = eINSTANCE.getExternalPort();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTERNAL_PORT__ID = eINSTANCE.getExternalPort_Id();

		/**
		 * The meta object literal for the '<em><b>Lower Fail</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTERNAL_PORT__LOWER_FAIL = eINSTANCE.getExternalPort_LowerFail();

		/**
		 * The meta object literal for the '<em><b>Upper Fail</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTERNAL_PORT__UPPER_FAIL = eINSTANCE.getExternalPort_UpperFail();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ObjectivesImpl <em>Objectives</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ObjectivesImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getObjectives()
		 * @generated
		 */
		EClass OBJECTIVES = eINSTANCE.getObjectives();

		/**
		 * The meta object literal for the '<em><b>Settings</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECTIVES__SETTINGS = eINSTANCE.getObjectives_Settings();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.SettingsImpl <em>Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.SettingsImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getSettings()
		 * @generated
		 */
		EClass SETTINGS = eINSTANCE.getSettings();

		/**
		 * The meta object literal for the '<em><b>Evaluations</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SETTINGS__EVALUATIONS = eINSTANCE.getSettings_Evaluations();

		/**
		 * The meta object literal for the '<em><b>No Run</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SETTINGS__NO_RUN = eINSTANCE.getSettings_NoRun();

		/**
		 * The meta object literal for the '<em><b>No Sampling</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SETTINGS__NO_SAMPLING = eINSTANCE.getSettings_NoSampling();

		/**
		 * The meta object literal for the '<em><b>No Duplicate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SETTINGS__NO_DUPLICATE = eINSTANCE.getSettings_NoDuplicate();

		/**
		 * The meta object literal for the '<em><b>Min Cost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SETTINGS__MIN_COST = eINSTANCE.getSettings_MinCost();

		/**
		 * The meta object literal for the '<em><b>Max Cost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SETTINGS__MAX_COST = eINSTANCE.getSettings_MaxCost();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ConstraintsImpl <em>Constraints</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ConstraintsImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getConstraints()
		 * @generated
		 */
		EClass CONSTRAINTS = eINSTANCE.getConstraints();

		/**
		 * The meta object literal for the '<em><b>Deployment</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINTS__DEPLOYMENT = eINSTANCE.getConstraints_Deployment();

		/**
		 * The meta object literal for the '<em><b>Replacement</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINTS__REPLACEMENT = eINSTANCE.getConstraints_Replacement();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ComponentDeploymentImpl <em>Component Deployment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ComponentDeploymentImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getComponentDeployment()
		 * @generated
		 */
		EClass COMPONENT_DEPLOYMENT = eINSTANCE.getComponentDeployment();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.HardwareConstraintsImpl <em>Hardware Constraints</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.HardwareConstraintsImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getHardwareConstraints()
		 * @generated
		 */
		EClass HARDWARE_CONSTRAINTS = eINSTANCE.getHardwareConstraints();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.ConflictComponentsImpl <em>Conflict Components</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.ConflictComponentsImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getConflictComponents()
		 * @generated
		 */
		EClass CONFLICT_COMPONENTS = eINSTANCE.getConflictComponents();

		/**
		 * The meta object literal for the '<em><b>Component1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFLICT_COMPONENTS__COMPONENT1 = eINSTANCE.getConflictComponents_Component1();

		/**
		 * The meta object literal for the '<em><b>Component2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFLICT_COMPONENTS__COMPONENT2 = eINSTANCE.getConflictComponents_Component2();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.CoupleComponentsImpl <em>Couple Components</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.CoupleComponentsImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getCoupleComponents()
		 * @generated
		 */
		EClass COUPLE_COMPONENTS = eINSTANCE.getCoupleComponents();

		/**
		 * The meta object literal for the '<em><b>Component1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COUPLE_COMPONENTS__COMPONENT1 = eINSTANCE.getCoupleComponents_Component1();

		/**
		 * The meta object literal for the '<em><b>Component2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COUPLE_COMPONENTS__COMPONENT2 = eINSTANCE.getCoupleComponents_Component2();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.FeatureModelImpl <em>Feature Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.FeatureModelImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getFeatureModel()
		 * @generated
		 */
		EClass FEATURE_MODEL = eINSTANCE.getFeatureModel();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL__FEATURES = eINSTANCE.getFeatureModel_Features();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.impl.FeatureImpl <em>Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.impl.FeatureImpl
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getFeature()
		 * @generated
		 */
		EClass FEATURE = eINSTANCE.getFeature();

		/**
		 * The meta object literal for the '<em><b>Component Relation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE__COMPONENT_RELATION = eINSTANCE.getFeature_ComponentRelation();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__NAME = eINSTANCE.getFeature_Name();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE__FEATURES = eINSTANCE.getFeature_Features();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.DependencyType <em>Dependency Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.DependencyType
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getDependencyType()
		 * @generated
		 */
		EEnum DEPENDENCY_TYPE = eINSTANCE.getDependencyType();

		/**
		 * The meta object literal for the '{@link edu.leiden.aqosa.model.IR.Evaluations <em>Evaluations</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.leiden.aqosa.model.IR.Evaluations
		 * @see edu.leiden.aqosa.model.IR.impl.AQOSAPackageImpl#getEvaluations()
		 * @generated
		 */
		EEnum EVALUATIONS = eINSTANCE.getEvaluations();

	}

} //AQOSAPackage
