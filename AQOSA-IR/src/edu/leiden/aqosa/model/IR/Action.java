/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getAction()
 * @model abstract="true"
 * @generated
 */
public interface Action extends EObject {
} // Action
