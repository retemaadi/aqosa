/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communicate Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.CommunicateAction#getSource <em>Source</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.CommunicateAction#getDestination <em>Destination</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getCommunicateAction()
 * @model
 * @generated
 */
public interface CommunicateAction extends Action {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(OutPort)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getCommunicateAction_Source()
	 * @model required="true"
	 * @generated
	 */
	OutPort getSource();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.CommunicateAction#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(OutPort value);

	/**
	 * Returns the value of the '<em><b>Destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination</em>' reference.
	 * @see #setDestination(InPort)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getCommunicateAction_Destination()
	 * @model required="true"
	 * @generated
	 */
	InPort getDestination();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.CommunicateAction#getDestination <em>Destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Destination</em>' reference.
	 * @see #getDestination()
	 * @generated
	 */
	void setDestination(InPort value);

} // CommunicateAction
