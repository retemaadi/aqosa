/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.Component#getName <em>Name</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Component#getService <em>Service</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Component#getInport <em>Inport</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Component#getOutport <em>Outport</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getComponent()
 * @model
 * @generated
 */
public interface Component extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getComponent_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Component#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Service</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.Service}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getComponent_Service()
	 * @model containment="true"
	 * @generated
	 */
	EList<Service> getService();

	/**
	 * Returns the value of the '<em><b>Inport</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.InPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inport</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inport</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getComponent_Inport()
	 * @model containment="true"
	 * @generated
	 */
	EList<InPort> getInport();

	/**
	 * Returns the value of the '<em><b>Outport</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.OutPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outport</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outport</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getComponent_Outport()
	 * @model containment="true"
	 * @generated
	 */
	EList<OutPort> getOutport();

} // Component
