/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Deployment</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getComponentDeployment()
 * @model abstract="true"
 * @generated
 */
public interface ComponentDeployment extends EObject {
} // ComponentDeployment
