/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.ComponentInstance#getId <em>Id</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.ComponentInstance#getCost <em>Cost</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.ComponentInstance#getCompatible <em>Compatible</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.ComponentInstance#getService <em>Service</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.ComponentInstance#getVariancePercentage <em>Variance Percentage</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getComponentInstance()
 * @model
 * @generated
 */
public interface ComponentInstance extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getComponentInstance_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.ComponentInstance#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cost</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cost</em>' attribute.
	 * @see #setCost(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getComponentInstance_Cost()
	 * @model
	 * @generated
	 */
	double getCost();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.ComponentInstance#getCost <em>Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cost</em>' attribute.
	 * @see #getCost()
	 * @generated
	 */
	void setCost(double value);

	/**
	 * Returns the value of the '<em><b>Compatible</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compatible</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compatible</em>' reference.
	 * @see #setCompatible(Component)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getComponentInstance_Compatible()
	 * @model required="true"
	 * @generated
	 */
	Component getCompatible();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.ComponentInstance#getCompatible <em>Compatible</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compatible</em>' reference.
	 * @see #getCompatible()
	 * @generated
	 */
	void setCompatible(Component value);

	/**
	 * Returns the value of the '<em><b>Service</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.ServiceInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getComponentInstance_Service()
	 * @model containment="true"
	 * @generated
	 */
	EList<ServiceInstance> getService();

	/**
	 * Returns the value of the '<em><b>Variance Percentage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variance Percentage</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variance Percentage</em>' attribute.
	 * @see #setVariancePercentage(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getComponentInstance_VariancePercentage()
	 * @model
	 * @generated
	 */
	double getVariancePercentage();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.ComponentInstance#getVariancePercentage <em>Variance Percentage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variance Percentage</em>' attribute.
	 * @see #getVariancePercentage()
	 * @generated
	 */
	void setVariancePercentage(double value);

} // ComponentInstance
