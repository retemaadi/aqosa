/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constraints</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.Constraints#getDeployment <em>Deployment</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Constraints#getReplacement <em>Replacement</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getConstraints()
 * @model
 * @generated
 */
public interface Constraints extends EObject {
	/**
	 * Returns the value of the '<em><b>Deployment</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.ComponentDeployment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deployment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deployment</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getConstraints_Deployment()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComponentDeployment> getDeployment();

	/**
	 * Returns the value of the '<em><b>Replacement</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.HardwareConstraints}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Replacement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Replacement</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getConstraints_Replacement()
	 * @model containment="true"
	 * @generated
	 */
	EList<HardwareConstraints> getReplacement();

} // Constraints
