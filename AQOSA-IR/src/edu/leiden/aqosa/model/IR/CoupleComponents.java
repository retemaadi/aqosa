/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Couple Components</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.CoupleComponents#getComponent1 <em>Component1</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.CoupleComponents#getComponent2 <em>Component2</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getCoupleComponents()
 * @model
 * @generated
 */
public interface CoupleComponents extends ComponentDeployment {
	/**
	 * Returns the value of the '<em><b>Component1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component1</em>' reference.
	 * @see #setComponent1(Component)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getCoupleComponents_Component1()
	 * @model required="true"
	 * @generated
	 */
	Component getComponent1();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.CoupleComponents#getComponent1 <em>Component1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component1</em>' reference.
	 * @see #getComponent1()
	 * @generated
	 */
	void setComponent1(Component value);

	/**
	 * Returns the value of the '<em><b>Component2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component2</em>' reference.
	 * @see #setComponent2(Component)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getCoupleComponents_Component2()
	 * @model required="true"
	 * @generated
	 */
	Component getComponent2();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.CoupleComponents#getComponent2 <em>Component2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component2</em>' reference.
	 * @see #getComponent2()
	 * @generated
	 */
	void setComponent2(Component value);

} // CoupleComponents
