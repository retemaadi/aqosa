/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dependence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.Dependence#getType <em>Type</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Dependence#getRequire <em>Require</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Dependence#getDepend <em>Depend</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getDependence()
 * @model
 * @generated
 */
public interface Dependence extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link edu.leiden.aqosa.model.IR.DependencyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see edu.leiden.aqosa.model.IR.DependencyType
	 * @see #setType(DependencyType)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getDependence_Type()
	 * @model
	 * @generated
	 */
	DependencyType getType();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Dependence#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see edu.leiden.aqosa.model.IR.DependencyType
	 * @see #getType()
	 * @generated
	 */
	void setType(DependencyType value);

	/**
	 * Returns the value of the '<em><b>Require</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.RequirePort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Require</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Require</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getDependence_Require()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequirePort> getRequire();

	/**
	 * Returns the value of the '<em><b>Depend</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.Dependence}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Depend</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Depend</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getDependence_Depend()
	 * @model containment="true"
	 * @generated
	 */
	EList<Dependence> getDepend();

} // Dependence
