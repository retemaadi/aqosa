/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.ExternalPort#getId <em>Id</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.ExternalPort#getLowerFail <em>Lower Fail</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.ExternalPort#getUpperFail <em>Upper Fail</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getExternalPort()
 * @model
 * @generated
 */
public interface ExternalPort extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getExternalPort_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.ExternalPort#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Lower Fail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lower Fail</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lower Fail</em>' attribute.
	 * @see #setLowerFail(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getExternalPort_LowerFail()
	 * @model
	 * @generated
	 */
	double getLowerFail();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.ExternalPort#getLowerFail <em>Lower Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower Fail</em>' attribute.
	 * @see #getLowerFail()
	 * @generated
	 */
	void setLowerFail(double value);

	/**
	 * Returns the value of the '<em><b>Upper Fail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Upper Fail</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upper Fail</em>' attribute.
	 * @see #setUpperFail(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getExternalPort_UpperFail()
	 * @model
	 * @generated
	 */
	double getUpperFail();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.ExternalPort#getUpperFail <em>Upper Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper Fail</em>' attribute.
	 * @see #getUpperFail()
	 * @generated
	 */
	void setUpperFail(double value);

} // ExternalPort
