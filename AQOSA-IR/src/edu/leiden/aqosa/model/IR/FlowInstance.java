/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flow Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.FlowInstance#getInstance <em>Instance</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.FlowInstance#getStart <em>Start</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.FlowInstance#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.FlowInstance#getDeadline <em>Deadline</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getFlowInstance()
 * @model
 * @generated
 */
public interface FlowInstance extends EObject {
	/**
	 * Returns the value of the '<em><b>Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance</em>' reference.
	 * @see #setInstance(Flow)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getFlowInstance_Instance()
	 * @model required="true"
	 * @generated
	 */
	Flow getInstance();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.FlowInstance#getInstance <em>Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance</em>' reference.
	 * @see #getInstance()
	 * @generated
	 */
	void setInstance(Flow value);

	/**
	 * Returns the value of the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' attribute.
	 * @see #setStart(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getFlowInstance_Start()
	 * @model
	 * @generated
	 */
	double getStart();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.FlowInstance#getStart <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' attribute.
	 * @see #getStart()
	 * @generated
	 */
	void setStart(double value);

	/**
	 * Returns the value of the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trigger</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger</em>' attribute.
	 * @see #setTrigger(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getFlowInstance_Trigger()
	 * @model
	 * @generated
	 */
	double getTrigger();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.FlowInstance#getTrigger <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger</em>' attribute.
	 * @see #getTrigger()
	 * @generated
	 */
	void setTrigger(double value);

	/**
	 * Returns the value of the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deadline</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deadline</em>' attribute.
	 * @see #setDeadline(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getFlowInstance_Deadline()
	 * @model
	 * @generated
	 */
	double getDeadline();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.FlowInstance#getDeadline <em>Deadline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deadline</em>' attribute.
	 * @see #getDeadline()
	 * @generated
	 */
	void setDeadline(double value);

} // FlowInstance
