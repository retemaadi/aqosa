/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flow Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.FlowSet#getName <em>Name</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.FlowSet#getFlowinstance <em>Flowinstance</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.FlowSet#getCompletionTime <em>Completion Time</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.FlowSet#getMissedPercentage <em>Missed Percentage</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getFlowSet()
 * @model
 * @generated
 */
public interface FlowSet extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getFlowSet_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.FlowSet#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Flowinstance</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.FlowInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Flowinstance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Flowinstance</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getFlowSet_Flowinstance()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<FlowInstance> getFlowinstance();

	/**
	 * Returns the value of the '<em><b>Completion Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Completion Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Completion Time</em>' attribute.
	 * @see #setCompletionTime(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getFlowSet_CompletionTime()
	 * @model
	 * @generated
	 */
	double getCompletionTime();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.FlowSet#getCompletionTime <em>Completion Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Completion Time</em>' attribute.
	 * @see #getCompletionTime()
	 * @generated
	 */
	void setCompletionTime(double value);

	/**
	 * Returns the value of the '<em><b>Missed Percentage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Missed Percentage</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Missed Percentage</em>' attribute.
	 * @see #setMissedPercentage(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getFlowSet_MissedPercentage()
	 * @model
	 * @generated
	 */
	double getMissedPercentage();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.FlowSet#getMissedPercentage <em>Missed Percentage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Missed Percentage</em>' attribute.
	 * @see #getMissedPercentage()
	 * @generated
	 */
	void setMissedPercentage(double value);

} // FlowSet
