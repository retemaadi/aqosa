/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Processor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.Processor#getId <em>Id</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Processor#getClock <em>Clock</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Processor#getCost <em>Cost</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Processor#getInternalBusBandwidth <em>Internal Bus Bandwidth</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Processor#getInternalBusDelay <em>Internal Bus Delay</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Processor#getLowerFail <em>Lower Fail</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Processor#getUpperFail <em>Upper Fail</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getProcessor()
 * @model
 * @generated
 */
public interface Processor extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getProcessor_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Processor#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Clock</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clock</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clock</em>' attribute.
	 * @see #setClock(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getProcessor_Clock()
	 * @model
	 * @generated
	 */
	double getClock();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Processor#getClock <em>Clock</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Clock</em>' attribute.
	 * @see #getClock()
	 * @generated
	 */
	void setClock(double value);

	/**
	 * Returns the value of the '<em><b>Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cost</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cost</em>' attribute.
	 * @see #setCost(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getProcessor_Cost()
	 * @model
	 * @generated
	 */
	double getCost();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Processor#getCost <em>Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cost</em>' attribute.
	 * @see #getCost()
	 * @generated
	 */
	void setCost(double value);

	/**
	 * Returns the value of the '<em><b>Internal Bus Bandwidth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Bus Bandwidth</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Bus Bandwidth</em>' attribute.
	 * @see #setInternalBusBandwidth(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getProcessor_InternalBusBandwidth()
	 * @model
	 * @generated
	 */
	double getInternalBusBandwidth();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Processor#getInternalBusBandwidth <em>Internal Bus Bandwidth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal Bus Bandwidth</em>' attribute.
	 * @see #getInternalBusBandwidth()
	 * @generated
	 */
	void setInternalBusBandwidth(double value);

	/**
	 * Returns the value of the '<em><b>Internal Bus Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Bus Delay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Bus Delay</em>' attribute.
	 * @see #setInternalBusDelay(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getProcessor_InternalBusDelay()
	 * @model
	 * @generated
	 */
	double getInternalBusDelay();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Processor#getInternalBusDelay <em>Internal Bus Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal Bus Delay</em>' attribute.
	 * @see #getInternalBusDelay()
	 * @generated
	 */
	void setInternalBusDelay(double value);

	/**
	 * Returns the value of the '<em><b>Lower Fail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lower Fail</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lower Fail</em>' attribute.
	 * @see #setLowerFail(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getProcessor_LowerFail()
	 * @model
	 * @generated
	 */
	double getLowerFail();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Processor#getLowerFail <em>Lower Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower Fail</em>' attribute.
	 * @see #getLowerFail()
	 * @generated
	 */
	void setLowerFail(double value);

	/**
	 * Returns the value of the '<em><b>Upper Fail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Upper Fail</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upper Fail</em>' attribute.
	 * @see #setUpperFail(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getProcessor_UpperFail()
	 * @model
	 * @generated
	 */
	double getUpperFail();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Processor#getUpperFail <em>Upper Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper Fail</em>' attribute.
	 * @see #getUpperFail()
	 * @generated
	 */
	void setUpperFail(double value);

} // Processor
