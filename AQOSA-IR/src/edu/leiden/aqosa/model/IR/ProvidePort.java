/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provide Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.ProvidePort#getConnects <em>Connects</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getProvidePort()
 * @model
 * @generated
 */
public interface ProvidePort extends EObject {
	/**
	 * Returns the value of the '<em><b>Connects</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connects</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connects</em>' reference.
	 * @see #setConnects(OutPort)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getProvidePort_Connects()
	 * @model required="true"
	 * @generated
	 */
	OutPort getConnects();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.ProvidePort#getConnects <em>Connects</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connects</em>' reference.
	 * @see #getConnects()
	 * @generated
	 */
	void setConnects(OutPort value);

} // ProvidePort
