/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Repository</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.Repository#getComponentinstance <em>Componentinstance</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Repository#getProcessor <em>Processor</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Repository#getBus <em>Bus</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Repository#getExternalport <em>Externalport</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getRepository()
 * @model
 * @generated
 */
public interface Repository extends EObject {
	/**
	 * Returns the value of the '<em><b>Componentinstance</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.ComponentInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Componentinstance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Componentinstance</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getRepository_Componentinstance()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComponentInstance> getComponentinstance();

	/**
	 * Returns the value of the '<em><b>Processor</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.Processor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processor</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processor</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getRepository_Processor()
	 * @model containment="true"
	 * @generated
	 */
	EList<Processor> getProcessor();

	/**
	 * Returns the value of the '<em><b>Bus</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.Bus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bus</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bus</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getRepository_Bus()
	 * @model containment="true"
	 * @generated
	 */
	EList<Bus> getBus();

	/**
	 * Returns the value of the '<em><b>Externalport</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.ExternalPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Externalport</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Externalport</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getRepository_Externalport()
	 * @model containment="true"
	 * @generated
	 */
	EList<ExternalPort> getExternalport();

} // Repository
