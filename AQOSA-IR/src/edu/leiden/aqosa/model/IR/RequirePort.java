/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Require Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.RequirePort#getInternal <em>Internal</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.RequirePort#getExternal <em>External</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getRequirePort()
 * @model
 * @generated
 */
public interface RequirePort extends EObject {
	/**
	 * Returns the value of the '<em><b>Internal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal</em>' reference.
	 * @see #setInternal(InPort)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getRequirePort_Internal()
	 * @model
	 * @generated
	 */
	InPort getInternal();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.RequirePort#getInternal <em>Internal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal</em>' reference.
	 * @see #getInternal()
	 * @generated
	 */
	void setInternal(InPort value);

	/**
	 * Returns the value of the '<em><b>External</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External</em>' reference.
	 * @see #setExternal(ExternalPort)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getRequirePort_External()
	 * @model
	 * @generated
	 */
	ExternalPort getExternal();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.RequirePort#getExternal <em>External</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External</em>' reference.
	 * @see #getExternal()
	 * @generated
	 */
	void setExternal(ExternalPort value);

} // RequirePort
