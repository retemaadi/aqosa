/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scenarios</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.Scenarios#getFlowset <em>Flowset</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getScenarios()
 * @model
 * @generated
 */
public interface Scenarios extends EObject {
	/**
	 * Returns the value of the '<em><b>Flowset</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.FlowSet}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Flowset</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Flowset</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getScenarios_Flowset()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<FlowSet> getFlowset();

} // Scenarios
