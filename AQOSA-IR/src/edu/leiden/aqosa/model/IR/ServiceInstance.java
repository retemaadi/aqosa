/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.ServiceInstance#getInstance <em>Instance</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.ServiceInstance#getCycles <em>Cycles</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.ServiceInstance#getMemoryUsage <em>Memory Usage</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.ServiceInstance#getNetworkUsage <em>Network Usage</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.ServiceInstance#getProvide <em>Provide</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.ServiceInstance#getDepend <em>Depend</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getServiceInstance()
 * @model
 * @generated
 */
public interface ServiceInstance extends EObject {
	/**
	 * Returns the value of the '<em><b>Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance</em>' reference.
	 * @see #setInstance(Service)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getServiceInstance_Instance()
	 * @model required="true"
	 * @generated
	 */
	Service getInstance();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.ServiceInstance#getInstance <em>Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance</em>' reference.
	 * @see #getInstance()
	 * @generated
	 */
	void setInstance(Service value);

	/**
	 * Returns the value of the '<em><b>Cycles</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cycles</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cycles</em>' attribute.
	 * @see #setCycles(int)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getServiceInstance_Cycles()
	 * @model
	 * @generated
	 */
	int getCycles();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.ServiceInstance#getCycles <em>Cycles</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cycles</em>' attribute.
	 * @see #getCycles()
	 * @generated
	 */
	void setCycles(int value);

	/**
	 * Returns the value of the '<em><b>Memory Usage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memory Usage</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memory Usage</em>' attribute.
	 * @see #setMemoryUsage(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getServiceInstance_MemoryUsage()
	 * @model
	 * @generated
	 */
	double getMemoryUsage();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.ServiceInstance#getMemoryUsage <em>Memory Usage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Memory Usage</em>' attribute.
	 * @see #getMemoryUsage()
	 * @generated
	 */
	void setMemoryUsage(double value);

	/**
	 * Returns the value of the '<em><b>Network Usage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Network Usage</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Network Usage</em>' attribute.
	 * @see #setNetworkUsage(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getServiceInstance_NetworkUsage()
	 * @model
	 * @generated
	 */
	double getNetworkUsage();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.ServiceInstance#getNetworkUsage <em>Network Usage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Network Usage</em>' attribute.
	 * @see #getNetworkUsage()
	 * @generated
	 */
	void setNetworkUsage(double value);

	/**
	 * Returns the value of the '<em><b>Provide</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.ProvidePort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provide</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provide</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getServiceInstance_Provide()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProvidePort> getProvide();

	/**
	 * Returns the value of the '<em><b>Depend</b></em>' containment reference list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.Dependence}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Depend</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Depend</em>' containment reference list.
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getServiceInstance_Depend()
	 * @model containment="true"
	 * @generated
	 */
	EList<Dependence> getDepend();

} // ServiceInstance
