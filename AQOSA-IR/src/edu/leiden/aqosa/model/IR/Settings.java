/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.Settings#getEvaluations <em>Evaluations</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Settings#getNoRun <em>No Run</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Settings#getNoSampling <em>No Sampling</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Settings#getNoDuplicate <em>No Duplicate</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Settings#getMinCost <em>Min Cost</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.Settings#getMaxCost <em>Max Cost</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getSettings()
 * @model
 * @generated
 */
public interface Settings extends EObject {
	/**
	 * Returns the value of the '<em><b>Evaluations</b></em>' attribute list.
	 * The list contents are of type {@link edu.leiden.aqosa.model.IR.Evaluations}.
	 * The literals are from the enumeration {@link edu.leiden.aqosa.model.IR.Evaluations}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluations</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluations</em>' attribute list.
	 * @see edu.leiden.aqosa.model.IR.Evaluations
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getSettings_Evaluations()
	 * @model
	 * @generated
	 */
	EList<Evaluations> getEvaluations();

	/**
	 * Returns the value of the '<em><b>No Run</b></em>' attribute.
	 * The default value is <code>"10"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Run</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Run</em>' attribute.
	 * @see #setNoRun(int)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getSettings_NoRun()
	 * @model default="10"
	 * @generated
	 */
	int getNoRun();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Settings#getNoRun <em>No Run</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Run</em>' attribute.
	 * @see #getNoRun()
	 * @generated
	 */
	void setNoRun(int value);

	/**
	 * Returns the value of the '<em><b>No Sampling</b></em>' attribute.
	 * The default value is <code>"100"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Sampling</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Sampling</em>' attribute.
	 * @see #setNoSampling(int)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getSettings_NoSampling()
	 * @model default="100"
	 * @generated
	 */
	int getNoSampling();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Settings#getNoSampling <em>No Sampling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Sampling</em>' attribute.
	 * @see #getNoSampling()
	 * @generated
	 */
	void setNoSampling(int value);

	/**
	 * Returns the value of the '<em><b>No Duplicate</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Duplicate</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Duplicate</em>' attribute.
	 * @see #setNoDuplicate(int)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getSettings_NoDuplicate()
	 * @model default="1"
	 * @generated
	 */
	int getNoDuplicate();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Settings#getNoDuplicate <em>No Duplicate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Duplicate</em>' attribute.
	 * @see #getNoDuplicate()
	 * @generated
	 */
	void setNoDuplicate(int value);

	/**
	 * Returns the value of the '<em><b>Min Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Cost</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Cost</em>' attribute.
	 * @see #setMinCost(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getSettings_MinCost()
	 * @model
	 * @generated
	 */
	double getMinCost();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Settings#getMinCost <em>Min Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Cost</em>' attribute.
	 * @see #getMinCost()
	 * @generated
	 */
	void setMinCost(double value);

	/**
	 * Returns the value of the '<em><b>Max Cost</b></em>' attribute.
	 * The default value is <code>"1000"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Cost</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Cost</em>' attribute.
	 * @see #setMaxCost(double)
	 * @see edu.leiden.aqosa.model.IR.AQOSAPackage#getSettings_MaxCost()
	 * @model default="1000"
	 * @generated
	 */
	double getMaxCost();

	/**
	 * Sets the value of the '{@link edu.leiden.aqosa.model.IR.Settings#getMaxCost <em>Max Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Cost</em>' attribute.
	 * @see #getMaxCost()
	 * @generated
	 */
	void setMaxCost(double value);

} // Settings
