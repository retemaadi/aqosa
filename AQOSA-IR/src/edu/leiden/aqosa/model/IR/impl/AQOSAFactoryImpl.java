/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import edu.leiden.aqosa.model.IR.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AQOSAFactoryImpl extends EFactoryImpl implements AQOSAFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AQOSAFactory init() {
		try {
			AQOSAFactory theAQOSAFactory = (AQOSAFactory)EPackage.Registry.INSTANCE.getEFactory(AQOSAPackage.eNS_URI);
			if (theAQOSAFactory != null) {
				return theAQOSAFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AQOSAFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AQOSAFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case AQOSAPackage.AQOSA_MODEL: return createAQOSAModel();
			case AQOSAPackage.ASSEMBLY: return createAssembly();
			case AQOSAPackage.COMPONENT: return createComponent();
			case AQOSAPackage.SERVICE: return createService();
			case AQOSAPackage.FLOW: return createFlow();
			case AQOSAPackage.IN_PORT: return createInPort();
			case AQOSAPackage.OUT_PORT: return createOutPort();
			case AQOSAPackage.COMPUTE_ACTION: return createComputeAction();
			case AQOSAPackage.COMMUNICATE_ACTION: return createCommunicateAction();
			case AQOSAPackage.SCENARIOS: return createScenarios();
			case AQOSAPackage.FLOW_SET: return createFlowSet();
			case AQOSAPackage.FLOW_INSTANCE: return createFlowInstance();
			case AQOSAPackage.REPOSITORY: return createRepository();
			case AQOSAPackage.COMPONENT_INSTANCE: return createComponentInstance();
			case AQOSAPackage.SERVICE_INSTANCE: return createServiceInstance();
			case AQOSAPackage.PROVIDE_PORT: return createProvidePort();
			case AQOSAPackage.REQUIRE_PORT: return createRequirePort();
			case AQOSAPackage.DEPENDENCE: return createDependence();
			case AQOSAPackage.PROCESSOR: return createProcessor();
			case AQOSAPackage.BUS: return createBus();
			case AQOSAPackage.EXTERNAL_PORT: return createExternalPort();
			case AQOSAPackage.OBJECTIVES: return createObjectives();
			case AQOSAPackage.SETTINGS: return createSettings();
			case AQOSAPackage.CONSTRAINTS: return createConstraints();
			case AQOSAPackage.CONFLICT_COMPONENTS: return createConflictComponents();
			case AQOSAPackage.COUPLE_COMPONENTS: return createCoupleComponents();
			case AQOSAPackage.FEATURE_MODEL: return createFeatureModel();
			case AQOSAPackage.FEATURE: return createFeature();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case AQOSAPackage.DEPENDENCY_TYPE:
				return createDependencyTypeFromString(eDataType, initialValue);
			case AQOSAPackage.EVALUATIONS:
				return createEvaluationsFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case AQOSAPackage.DEPENDENCY_TYPE:
				return convertDependencyTypeToString(eDataType, instanceValue);
			case AQOSAPackage.EVALUATIONS:
				return convertEvaluationsToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AQOSAModel createAQOSAModel() {
		AQOSAModelImpl aqosaModel = new AQOSAModelImpl();
		return aqosaModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assembly createAssembly() {
		AssemblyImpl assembly = new AssemblyImpl();
		return assembly;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component createComponent() {
		ComponentImpl component = new ComponentImpl();
		return component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Service createService() {
		ServiceImpl service = new ServiceImpl();
		return service;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Flow createFlow() {
		FlowImpl flow = new FlowImpl();
		return flow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InPort createInPort() {
		InPortImpl inPort = new InPortImpl();
		return inPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutPort createOutPort() {
		OutPortImpl outPort = new OutPortImpl();
		return outPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputeAction createComputeAction() {
		ComputeActionImpl computeAction = new ComputeActionImpl();
		return computeAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicateAction createCommunicateAction() {
		CommunicateActionImpl communicateAction = new CommunicateActionImpl();
		return communicateAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scenarios createScenarios() {
		ScenariosImpl scenarios = new ScenariosImpl();
		return scenarios;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlowSet createFlowSet() {
		FlowSetImpl flowSet = new FlowSetImpl();
		return flowSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlowInstance createFlowInstance() {
		FlowInstanceImpl flowInstance = new FlowInstanceImpl();
		return flowInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Repository createRepository() {
		RepositoryImpl repository = new RepositoryImpl();
		return repository;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance createComponentInstance() {
		ComponentInstanceImpl componentInstance = new ComponentInstanceImpl();
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceInstance createServiceInstance() {
		ServiceInstanceImpl serviceInstance = new ServiceInstanceImpl();
		return serviceInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidePort createProvidePort() {
		ProvidePortImpl providePort = new ProvidePortImpl();
		return providePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirePort createRequirePort() {
		RequirePortImpl requirePort = new RequirePortImpl();
		return requirePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dependence createDependence() {
		DependenceImpl dependence = new DependenceImpl();
		return dependence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Processor createProcessor() {
		ProcessorImpl processor = new ProcessorImpl();
		return processor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Bus createBus() {
		BusImpl bus = new BusImpl();
		return bus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalPort createExternalPort() {
		ExternalPortImpl externalPort = new ExternalPortImpl();
		return externalPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Objectives createObjectives() {
		ObjectivesImpl objectives = new ObjectivesImpl();
		return objectives;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Settings createSettings() {
		SettingsImpl settings = new SettingsImpl();
		return settings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraints createConstraints() {
		ConstraintsImpl constraints = new ConstraintsImpl();
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConflictComponents createConflictComponents() {
		ConflictComponentsImpl conflictComponents = new ConflictComponentsImpl();
		return conflictComponents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoupleComponents createCoupleComponents() {
		CoupleComponentsImpl coupleComponents = new CoupleComponentsImpl();
		return coupleComponents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModel createFeatureModel() {
		FeatureModelImpl featureModel = new FeatureModelImpl();
		return featureModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature createFeature() {
		FeatureImpl feature = new FeatureImpl();
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DependencyType createDependencyTypeFromString(EDataType eDataType, String initialValue) {
		DependencyType result = DependencyType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDependencyTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Evaluations createEvaluationsFromString(EDataType eDataType, String initialValue) {
		Evaluations result = Evaluations.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEvaluationsToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AQOSAPackage getAQOSAPackage() {
		return (AQOSAPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AQOSAPackage getPackage() {
		return AQOSAPackage.eINSTANCE;
	}

} //AQOSAFactoryImpl
