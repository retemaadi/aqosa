/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import edu.leiden.aqosa.model.IR.AQOSAModel;
import edu.leiden.aqosa.model.IR.AQOSAPackage;
import edu.leiden.aqosa.model.IR.Assembly;
import edu.leiden.aqosa.model.IR.Constraints;
import edu.leiden.aqosa.model.IR.FeatureModel;
import edu.leiden.aqosa.model.IR.Objectives;
import edu.leiden.aqosa.model.IR.Repository;
import edu.leiden.aqosa.model.IR.Scenarios;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.AQOSAModelImpl#getAssembly <em>Assembly</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.AQOSAModelImpl#getScenarios <em>Scenarios</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.AQOSAModelImpl#getRepository <em>Repository</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.AQOSAModelImpl#getObjectives <em>Objectives</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.AQOSAModelImpl#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.AQOSAModelImpl#getFeatureModel <em>Feature Model</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AQOSAModelImpl extends EObjectImpl implements AQOSAModel {
	/**
	 * The cached value of the '{@link #getAssembly() <em>Assembly</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssembly()
	 * @generated
	 * @ordered
	 */
	protected Assembly assembly;

	/**
	 * The cached value of the '{@link #getScenarios() <em>Scenarios</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScenarios()
	 * @generated
	 * @ordered
	 */
	protected Scenarios scenarios;

	/**
	 * The cached value of the '{@link #getRepository() <em>Repository</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepository()
	 * @generated
	 * @ordered
	 */
	protected Repository repository;

	/**
	 * The cached value of the '{@link #getObjectives() <em>Objectives</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectives()
	 * @generated
	 * @ordered
	 */
	protected Objectives objectives;

	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected Constraints constraints;

	/**
	 * The cached value of the '{@link #getFeatureModel() <em>Feature Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureModel()
	 * @generated
	 * @ordered
	 */
	protected FeatureModel featureModel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AQOSAModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AQOSAPackage.Literals.AQOSA_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assembly getAssembly() {
		return assembly;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssembly(Assembly newAssembly, NotificationChain msgs) {
		Assembly oldAssembly = assembly;
		assembly = newAssembly;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AQOSAPackage.AQOSA_MODEL__ASSEMBLY, oldAssembly, newAssembly);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssembly(Assembly newAssembly) {
		if (newAssembly != assembly) {
			NotificationChain msgs = null;
			if (assembly != null)
				msgs = ((InternalEObject)assembly).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AQOSAPackage.AQOSA_MODEL__ASSEMBLY, null, msgs);
			if (newAssembly != null)
				msgs = ((InternalEObject)newAssembly).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AQOSAPackage.AQOSA_MODEL__ASSEMBLY, null, msgs);
			msgs = basicSetAssembly(newAssembly, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.AQOSA_MODEL__ASSEMBLY, newAssembly, newAssembly));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scenarios getScenarios() {
		return scenarios;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScenarios(Scenarios newScenarios, NotificationChain msgs) {
		Scenarios oldScenarios = scenarios;
		scenarios = newScenarios;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AQOSAPackage.AQOSA_MODEL__SCENARIOS, oldScenarios, newScenarios);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScenarios(Scenarios newScenarios) {
		if (newScenarios != scenarios) {
			NotificationChain msgs = null;
			if (scenarios != null)
				msgs = ((InternalEObject)scenarios).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AQOSAPackage.AQOSA_MODEL__SCENARIOS, null, msgs);
			if (newScenarios != null)
				msgs = ((InternalEObject)newScenarios).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AQOSAPackage.AQOSA_MODEL__SCENARIOS, null, msgs);
			msgs = basicSetScenarios(newScenarios, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.AQOSA_MODEL__SCENARIOS, newScenarios, newScenarios));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Repository getRepository() {
		return repository;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRepository(Repository newRepository, NotificationChain msgs) {
		Repository oldRepository = repository;
		repository = newRepository;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AQOSAPackage.AQOSA_MODEL__REPOSITORY, oldRepository, newRepository);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepository(Repository newRepository) {
		if (newRepository != repository) {
			NotificationChain msgs = null;
			if (repository != null)
				msgs = ((InternalEObject)repository).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AQOSAPackage.AQOSA_MODEL__REPOSITORY, null, msgs);
			if (newRepository != null)
				msgs = ((InternalEObject)newRepository).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AQOSAPackage.AQOSA_MODEL__REPOSITORY, null, msgs);
			msgs = basicSetRepository(newRepository, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.AQOSA_MODEL__REPOSITORY, newRepository, newRepository));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Objectives getObjectives() {
		return objectives;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectives(Objectives newObjectives, NotificationChain msgs) {
		Objectives oldObjectives = objectives;
		objectives = newObjectives;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AQOSAPackage.AQOSA_MODEL__OBJECTIVES, oldObjectives, newObjectives);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectives(Objectives newObjectives) {
		if (newObjectives != objectives) {
			NotificationChain msgs = null;
			if (objectives != null)
				msgs = ((InternalEObject)objectives).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AQOSAPackage.AQOSA_MODEL__OBJECTIVES, null, msgs);
			if (newObjectives != null)
				msgs = ((InternalEObject)newObjectives).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AQOSAPackage.AQOSA_MODEL__OBJECTIVES, null, msgs);
			msgs = basicSetObjectives(newObjectives, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.AQOSA_MODEL__OBJECTIVES, newObjectives, newObjectives));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraints getConstraints() {
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstraints(Constraints newConstraints, NotificationChain msgs) {
		Constraints oldConstraints = constraints;
		constraints = newConstraints;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AQOSAPackage.AQOSA_MODEL__CONSTRAINTS, oldConstraints, newConstraints);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstraints(Constraints newConstraints) {
		if (newConstraints != constraints) {
			NotificationChain msgs = null;
			if (constraints != null)
				msgs = ((InternalEObject)constraints).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AQOSAPackage.AQOSA_MODEL__CONSTRAINTS, null, msgs);
			if (newConstraints != null)
				msgs = ((InternalEObject)newConstraints).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AQOSAPackage.AQOSA_MODEL__CONSTRAINTS, null, msgs);
			msgs = basicSetConstraints(newConstraints, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.AQOSA_MODEL__CONSTRAINTS, newConstraints, newConstraints));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModel getFeatureModel() {
		return featureModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureModel(FeatureModel newFeatureModel, NotificationChain msgs) {
		FeatureModel oldFeatureModel = featureModel;
		featureModel = newFeatureModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AQOSAPackage.AQOSA_MODEL__FEATURE_MODEL, oldFeatureModel, newFeatureModel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeatureModel(FeatureModel newFeatureModel) {
		if (newFeatureModel != featureModel) {
			NotificationChain msgs = null;
			if (featureModel != null)
				msgs = ((InternalEObject)featureModel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AQOSAPackage.AQOSA_MODEL__FEATURE_MODEL, null, msgs);
			if (newFeatureModel != null)
				msgs = ((InternalEObject)newFeatureModel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AQOSAPackage.AQOSA_MODEL__FEATURE_MODEL, null, msgs);
			msgs = basicSetFeatureModel(newFeatureModel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.AQOSA_MODEL__FEATURE_MODEL, newFeatureModel, newFeatureModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AQOSAPackage.AQOSA_MODEL__ASSEMBLY:
				return basicSetAssembly(null, msgs);
			case AQOSAPackage.AQOSA_MODEL__SCENARIOS:
				return basicSetScenarios(null, msgs);
			case AQOSAPackage.AQOSA_MODEL__REPOSITORY:
				return basicSetRepository(null, msgs);
			case AQOSAPackage.AQOSA_MODEL__OBJECTIVES:
				return basicSetObjectives(null, msgs);
			case AQOSAPackage.AQOSA_MODEL__CONSTRAINTS:
				return basicSetConstraints(null, msgs);
			case AQOSAPackage.AQOSA_MODEL__FEATURE_MODEL:
				return basicSetFeatureModel(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AQOSAPackage.AQOSA_MODEL__ASSEMBLY:
				return getAssembly();
			case AQOSAPackage.AQOSA_MODEL__SCENARIOS:
				return getScenarios();
			case AQOSAPackage.AQOSA_MODEL__REPOSITORY:
				return getRepository();
			case AQOSAPackage.AQOSA_MODEL__OBJECTIVES:
				return getObjectives();
			case AQOSAPackage.AQOSA_MODEL__CONSTRAINTS:
				return getConstraints();
			case AQOSAPackage.AQOSA_MODEL__FEATURE_MODEL:
				return getFeatureModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AQOSAPackage.AQOSA_MODEL__ASSEMBLY:
				setAssembly((Assembly)newValue);
				return;
			case AQOSAPackage.AQOSA_MODEL__SCENARIOS:
				setScenarios((Scenarios)newValue);
				return;
			case AQOSAPackage.AQOSA_MODEL__REPOSITORY:
				setRepository((Repository)newValue);
				return;
			case AQOSAPackage.AQOSA_MODEL__OBJECTIVES:
				setObjectives((Objectives)newValue);
				return;
			case AQOSAPackage.AQOSA_MODEL__CONSTRAINTS:
				setConstraints((Constraints)newValue);
				return;
			case AQOSAPackage.AQOSA_MODEL__FEATURE_MODEL:
				setFeatureModel((FeatureModel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AQOSAPackage.AQOSA_MODEL__ASSEMBLY:
				setAssembly((Assembly)null);
				return;
			case AQOSAPackage.AQOSA_MODEL__SCENARIOS:
				setScenarios((Scenarios)null);
				return;
			case AQOSAPackage.AQOSA_MODEL__REPOSITORY:
				setRepository((Repository)null);
				return;
			case AQOSAPackage.AQOSA_MODEL__OBJECTIVES:
				setObjectives((Objectives)null);
				return;
			case AQOSAPackage.AQOSA_MODEL__CONSTRAINTS:
				setConstraints((Constraints)null);
				return;
			case AQOSAPackage.AQOSA_MODEL__FEATURE_MODEL:
				setFeatureModel((FeatureModel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AQOSAPackage.AQOSA_MODEL__ASSEMBLY:
				return assembly != null;
			case AQOSAPackage.AQOSA_MODEL__SCENARIOS:
				return scenarios != null;
			case AQOSAPackage.AQOSA_MODEL__REPOSITORY:
				return repository != null;
			case AQOSAPackage.AQOSA_MODEL__OBJECTIVES:
				return objectives != null;
			case AQOSAPackage.AQOSA_MODEL__CONSTRAINTS:
				return constraints != null;
			case AQOSAPackage.AQOSA_MODEL__FEATURE_MODEL:
				return featureModel != null;
		}
		return super.eIsSet(featureID);
	}

} //AQOSAModelImpl
