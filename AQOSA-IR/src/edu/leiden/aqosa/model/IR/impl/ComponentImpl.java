/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import edu.leiden.aqosa.model.IR.AQOSAPackage;
import edu.leiden.aqosa.model.IR.Component;
import edu.leiden.aqosa.model.IR.InPort;
import edu.leiden.aqosa.model.IR.OutPort;
import edu.leiden.aqosa.model.IR.Service;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ComponentImpl#getName <em>Name</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ComponentImpl#getService <em>Service</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ComponentImpl#getInport <em>Inport</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ComponentImpl#getOutport <em>Outport</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComponentImpl extends EObjectImpl implements Component {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getService() <em>Service</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getService()
	 * @generated
	 * @ordered
	 */
	protected EList<Service> service;

	/**
	 * The cached value of the '{@link #getInport() <em>Inport</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInport()
	 * @generated
	 * @ordered
	 */
	protected EList<InPort> inport;

	/**
	 * The cached value of the '{@link #getOutport() <em>Outport</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutport()
	 * @generated
	 * @ordered
	 */
	protected EList<OutPort> outport;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AQOSAPackage.Literals.COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.COMPONENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Service> getService() {
		if (service == null) {
			service = new EObjectContainmentEList<Service>(Service.class, this, AQOSAPackage.COMPONENT__SERVICE);
		}
		return service;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InPort> getInport() {
		if (inport == null) {
			inport = new EObjectContainmentEList<InPort>(InPort.class, this, AQOSAPackage.COMPONENT__INPORT);
		}
		return inport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutPort> getOutport() {
		if (outport == null) {
			outport = new EObjectContainmentEList<OutPort>(OutPort.class, this, AQOSAPackage.COMPONENT__OUTPORT);
		}
		return outport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AQOSAPackage.COMPONENT__SERVICE:
				return ((InternalEList<?>)getService()).basicRemove(otherEnd, msgs);
			case AQOSAPackage.COMPONENT__INPORT:
				return ((InternalEList<?>)getInport()).basicRemove(otherEnd, msgs);
			case AQOSAPackage.COMPONENT__OUTPORT:
				return ((InternalEList<?>)getOutport()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AQOSAPackage.COMPONENT__NAME:
				return getName();
			case AQOSAPackage.COMPONENT__SERVICE:
				return getService();
			case AQOSAPackage.COMPONENT__INPORT:
				return getInport();
			case AQOSAPackage.COMPONENT__OUTPORT:
				return getOutport();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AQOSAPackage.COMPONENT__NAME:
				setName((String)newValue);
				return;
			case AQOSAPackage.COMPONENT__SERVICE:
				getService().clear();
				getService().addAll((Collection<? extends Service>)newValue);
				return;
			case AQOSAPackage.COMPONENT__INPORT:
				getInport().clear();
				getInport().addAll((Collection<? extends InPort>)newValue);
				return;
			case AQOSAPackage.COMPONENT__OUTPORT:
				getOutport().clear();
				getOutport().addAll((Collection<? extends OutPort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AQOSAPackage.COMPONENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case AQOSAPackage.COMPONENT__SERVICE:
				getService().clear();
				return;
			case AQOSAPackage.COMPONENT__INPORT:
				getInport().clear();
				return;
			case AQOSAPackage.COMPONENT__OUTPORT:
				getOutport().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AQOSAPackage.COMPONENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case AQOSAPackage.COMPONENT__SERVICE:
				return service != null && !service.isEmpty();
			case AQOSAPackage.COMPONENT__INPORT:
				return inport != null && !inport.isEmpty();
			case AQOSAPackage.COMPONENT__OUTPORT:
				return outport != null && !outport.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ComponentImpl
