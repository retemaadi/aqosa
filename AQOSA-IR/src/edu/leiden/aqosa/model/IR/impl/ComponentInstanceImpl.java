/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import edu.leiden.aqosa.model.IR.AQOSAPackage;
import edu.leiden.aqosa.model.IR.Component;
import edu.leiden.aqosa.model.IR.ComponentInstance;
import edu.leiden.aqosa.model.IR.ServiceInstance;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ComponentInstanceImpl#getId <em>Id</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ComponentInstanceImpl#getCost <em>Cost</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ComponentInstanceImpl#getCompatible <em>Compatible</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ComponentInstanceImpl#getService <em>Service</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ComponentInstanceImpl#getVariancePercentage <em>Variance Percentage</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComponentInstanceImpl extends EObjectImpl implements ComponentInstance {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getCost() <em>Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCost()
	 * @generated
	 * @ordered
	 */
	protected static final double COST_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCost() <em>Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCost()
	 * @generated
	 * @ordered
	 */
	protected double cost = COST_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCompatible() <em>Compatible</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompatible()
	 * @generated
	 * @ordered
	 */
	protected Component compatible;

	/**
	 * The cached value of the '{@link #getService() <em>Service</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getService()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceInstance> service;

	/**
	 * The default value of the '{@link #getVariancePercentage() <em>Variance Percentage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariancePercentage()
	 * @generated
	 * @ordered
	 */
	protected static final double VARIANCE_PERCENTAGE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getVariancePercentage() <em>Variance Percentage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariancePercentage()
	 * @generated
	 * @ordered
	 */
	protected double variancePercentage = VARIANCE_PERCENTAGE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AQOSAPackage.Literals.COMPONENT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.COMPONENT_INSTANCE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCost() {
		return cost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCost(double newCost) {
		double oldCost = cost;
		cost = newCost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.COMPONENT_INSTANCE__COST, oldCost, cost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component getCompatible() {
		if (compatible != null && compatible.eIsProxy()) {
			InternalEObject oldCompatible = (InternalEObject)compatible;
			compatible = (Component)eResolveProxy(oldCompatible);
			if (compatible != oldCompatible) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AQOSAPackage.COMPONENT_INSTANCE__COMPATIBLE, oldCompatible, compatible));
			}
		}
		return compatible;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component basicGetCompatible() {
		return compatible;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCompatible(Component newCompatible) {
		Component oldCompatible = compatible;
		compatible = newCompatible;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.COMPONENT_INSTANCE__COMPATIBLE, oldCompatible, compatible));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceInstance> getService() {
		if (service == null) {
			service = new EObjectContainmentEList<ServiceInstance>(ServiceInstance.class, this, AQOSAPackage.COMPONENT_INSTANCE__SERVICE);
		}
		return service;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getVariancePercentage() {
		return variancePercentage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariancePercentage(double newVariancePercentage) {
		double oldVariancePercentage = variancePercentage;
		variancePercentage = newVariancePercentage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.COMPONENT_INSTANCE__VARIANCE_PERCENTAGE, oldVariancePercentage, variancePercentage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AQOSAPackage.COMPONENT_INSTANCE__SERVICE:
				return ((InternalEList<?>)getService()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AQOSAPackage.COMPONENT_INSTANCE__ID:
				return getId();
			case AQOSAPackage.COMPONENT_INSTANCE__COST:
				return getCost();
			case AQOSAPackage.COMPONENT_INSTANCE__COMPATIBLE:
				if (resolve) return getCompatible();
				return basicGetCompatible();
			case AQOSAPackage.COMPONENT_INSTANCE__SERVICE:
				return getService();
			case AQOSAPackage.COMPONENT_INSTANCE__VARIANCE_PERCENTAGE:
				return getVariancePercentage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AQOSAPackage.COMPONENT_INSTANCE__ID:
				setId((String)newValue);
				return;
			case AQOSAPackage.COMPONENT_INSTANCE__COST:
				setCost((Double)newValue);
				return;
			case AQOSAPackage.COMPONENT_INSTANCE__COMPATIBLE:
				setCompatible((Component)newValue);
				return;
			case AQOSAPackage.COMPONENT_INSTANCE__SERVICE:
				getService().clear();
				getService().addAll((Collection<? extends ServiceInstance>)newValue);
				return;
			case AQOSAPackage.COMPONENT_INSTANCE__VARIANCE_PERCENTAGE:
				setVariancePercentage((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AQOSAPackage.COMPONENT_INSTANCE__ID:
				setId(ID_EDEFAULT);
				return;
			case AQOSAPackage.COMPONENT_INSTANCE__COST:
				setCost(COST_EDEFAULT);
				return;
			case AQOSAPackage.COMPONENT_INSTANCE__COMPATIBLE:
				setCompatible((Component)null);
				return;
			case AQOSAPackage.COMPONENT_INSTANCE__SERVICE:
				getService().clear();
				return;
			case AQOSAPackage.COMPONENT_INSTANCE__VARIANCE_PERCENTAGE:
				setVariancePercentage(VARIANCE_PERCENTAGE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AQOSAPackage.COMPONENT_INSTANCE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case AQOSAPackage.COMPONENT_INSTANCE__COST:
				return cost != COST_EDEFAULT;
			case AQOSAPackage.COMPONENT_INSTANCE__COMPATIBLE:
				return compatible != null;
			case AQOSAPackage.COMPONENT_INSTANCE__SERVICE:
				return service != null && !service.isEmpty();
			case AQOSAPackage.COMPONENT_INSTANCE__VARIANCE_PERCENTAGE:
				return variancePercentage != VARIANCE_PERCENTAGE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", cost: ");
		result.append(cost);
		result.append(", variancePercentage: ");
		result.append(variancePercentage);
		result.append(')');
		return result.toString();
	}

} //ComponentInstanceImpl
