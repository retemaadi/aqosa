/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import edu.leiden.aqosa.model.IR.AQOSAPackage;
import edu.leiden.aqosa.model.IR.ComponentDeployment;
import edu.leiden.aqosa.model.IR.Constraints;
import edu.leiden.aqosa.model.IR.HardwareConstraints;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constraints</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ConstraintsImpl#getDeployment <em>Deployment</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ConstraintsImpl#getReplacement <em>Replacement</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConstraintsImpl extends EObjectImpl implements Constraints {
	/**
	 * The cached value of the '{@link #getDeployment() <em>Deployment</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeployment()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentDeployment> deployment;

	/**
	 * The cached value of the '{@link #getReplacement() <em>Replacement</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReplacement()
	 * @generated
	 * @ordered
	 */
	protected EList<HardwareConstraints> replacement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstraintsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AQOSAPackage.Literals.CONSTRAINTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentDeployment> getDeployment() {
		if (deployment == null) {
			deployment = new EObjectContainmentEList<ComponentDeployment>(ComponentDeployment.class, this, AQOSAPackage.CONSTRAINTS__DEPLOYMENT);
		}
		return deployment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HardwareConstraints> getReplacement() {
		if (replacement == null) {
			replacement = new EObjectContainmentEList<HardwareConstraints>(HardwareConstraints.class, this, AQOSAPackage.CONSTRAINTS__REPLACEMENT);
		}
		return replacement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AQOSAPackage.CONSTRAINTS__DEPLOYMENT:
				return ((InternalEList<?>)getDeployment()).basicRemove(otherEnd, msgs);
			case AQOSAPackage.CONSTRAINTS__REPLACEMENT:
				return ((InternalEList<?>)getReplacement()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AQOSAPackage.CONSTRAINTS__DEPLOYMENT:
				return getDeployment();
			case AQOSAPackage.CONSTRAINTS__REPLACEMENT:
				return getReplacement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AQOSAPackage.CONSTRAINTS__DEPLOYMENT:
				getDeployment().clear();
				getDeployment().addAll((Collection<? extends ComponentDeployment>)newValue);
				return;
			case AQOSAPackage.CONSTRAINTS__REPLACEMENT:
				getReplacement().clear();
				getReplacement().addAll((Collection<? extends HardwareConstraints>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AQOSAPackage.CONSTRAINTS__DEPLOYMENT:
				getDeployment().clear();
				return;
			case AQOSAPackage.CONSTRAINTS__REPLACEMENT:
				getReplacement().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AQOSAPackage.CONSTRAINTS__DEPLOYMENT:
				return deployment != null && !deployment.isEmpty();
			case AQOSAPackage.CONSTRAINTS__REPLACEMENT:
				return replacement != null && !replacement.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ConstraintsImpl
