/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import edu.leiden.aqosa.model.IR.AQOSAPackage;
import edu.leiden.aqosa.model.IR.Component;
import edu.leiden.aqosa.model.IR.CoupleComponents;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Couple Components</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.CoupleComponentsImpl#getComponent1 <em>Component1</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.CoupleComponentsImpl#getComponent2 <em>Component2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CoupleComponentsImpl extends ComponentDeploymentImpl implements CoupleComponents {
	/**
	 * The cached value of the '{@link #getComponent1() <em>Component1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponent1()
	 * @generated
	 * @ordered
	 */
	protected Component component1;

	/**
	 * The cached value of the '{@link #getComponent2() <em>Component2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponent2()
	 * @generated
	 * @ordered
	 */
	protected Component component2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CoupleComponentsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AQOSAPackage.Literals.COUPLE_COMPONENTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component getComponent1() {
		if (component1 != null && component1.eIsProxy()) {
			InternalEObject oldComponent1 = (InternalEObject)component1;
			component1 = (Component)eResolveProxy(oldComponent1);
			if (component1 != oldComponent1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AQOSAPackage.COUPLE_COMPONENTS__COMPONENT1, oldComponent1, component1));
			}
		}
		return component1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component basicGetComponent1() {
		return component1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponent1(Component newComponent1) {
		Component oldComponent1 = component1;
		component1 = newComponent1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.COUPLE_COMPONENTS__COMPONENT1, oldComponent1, component1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component getComponent2() {
		if (component2 != null && component2.eIsProxy()) {
			InternalEObject oldComponent2 = (InternalEObject)component2;
			component2 = (Component)eResolveProxy(oldComponent2);
			if (component2 != oldComponent2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AQOSAPackage.COUPLE_COMPONENTS__COMPONENT2, oldComponent2, component2));
			}
		}
		return component2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component basicGetComponent2() {
		return component2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponent2(Component newComponent2) {
		Component oldComponent2 = component2;
		component2 = newComponent2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.COUPLE_COMPONENTS__COMPONENT2, oldComponent2, component2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AQOSAPackage.COUPLE_COMPONENTS__COMPONENT1:
				if (resolve) return getComponent1();
				return basicGetComponent1();
			case AQOSAPackage.COUPLE_COMPONENTS__COMPONENT2:
				if (resolve) return getComponent2();
				return basicGetComponent2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AQOSAPackage.COUPLE_COMPONENTS__COMPONENT1:
				setComponent1((Component)newValue);
				return;
			case AQOSAPackage.COUPLE_COMPONENTS__COMPONENT2:
				setComponent2((Component)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AQOSAPackage.COUPLE_COMPONENTS__COMPONENT1:
				setComponent1((Component)null);
				return;
			case AQOSAPackage.COUPLE_COMPONENTS__COMPONENT2:
				setComponent2((Component)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AQOSAPackage.COUPLE_COMPONENTS__COMPONENT1:
				return component1 != null;
			case AQOSAPackage.COUPLE_COMPONENTS__COMPONENT2:
				return component2 != null;
		}
		return super.eIsSet(featureID);
	}

} //CoupleComponentsImpl
