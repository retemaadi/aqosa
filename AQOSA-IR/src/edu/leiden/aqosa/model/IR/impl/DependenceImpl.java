/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import edu.leiden.aqosa.model.IR.AQOSAPackage;
import edu.leiden.aqosa.model.IR.Dependence;
import edu.leiden.aqosa.model.IR.DependencyType;
import edu.leiden.aqosa.model.IR.RequirePort;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dependence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.DependenceImpl#getType <em>Type</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.DependenceImpl#getRequire <em>Require</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.DependenceImpl#getDepend <em>Depend</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DependenceImpl extends EObjectImpl implements Dependence {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final DependencyType TYPE_EDEFAULT = DependencyType.AND;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected DependencyType type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRequire() <em>Require</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequire()
	 * @generated
	 * @ordered
	 */
	protected EList<RequirePort> require;

	/**
	 * The cached value of the '{@link #getDepend() <em>Depend</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepend()
	 * @generated
	 * @ordered
	 */
	protected EList<Dependence> depend;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DependenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AQOSAPackage.Literals.DEPENDENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DependencyType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(DependencyType newType) {
		DependencyType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.DEPENDENCE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RequirePort> getRequire() {
		if (require == null) {
			require = new EObjectContainmentEList<RequirePort>(RequirePort.class, this, AQOSAPackage.DEPENDENCE__REQUIRE);
		}
		return require;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Dependence> getDepend() {
		if (depend == null) {
			depend = new EObjectContainmentEList<Dependence>(Dependence.class, this, AQOSAPackage.DEPENDENCE__DEPEND);
		}
		return depend;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AQOSAPackage.DEPENDENCE__REQUIRE:
				return ((InternalEList<?>)getRequire()).basicRemove(otherEnd, msgs);
			case AQOSAPackage.DEPENDENCE__DEPEND:
				return ((InternalEList<?>)getDepend()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AQOSAPackage.DEPENDENCE__TYPE:
				return getType();
			case AQOSAPackage.DEPENDENCE__REQUIRE:
				return getRequire();
			case AQOSAPackage.DEPENDENCE__DEPEND:
				return getDepend();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AQOSAPackage.DEPENDENCE__TYPE:
				setType((DependencyType)newValue);
				return;
			case AQOSAPackage.DEPENDENCE__REQUIRE:
				getRequire().clear();
				getRequire().addAll((Collection<? extends RequirePort>)newValue);
				return;
			case AQOSAPackage.DEPENDENCE__DEPEND:
				getDepend().clear();
				getDepend().addAll((Collection<? extends Dependence>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AQOSAPackage.DEPENDENCE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case AQOSAPackage.DEPENDENCE__REQUIRE:
				getRequire().clear();
				return;
			case AQOSAPackage.DEPENDENCE__DEPEND:
				getDepend().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AQOSAPackage.DEPENDENCE__TYPE:
				return type != TYPE_EDEFAULT;
			case AQOSAPackage.DEPENDENCE__REQUIRE:
				return require != null && !require.isEmpty();
			case AQOSAPackage.DEPENDENCE__DEPEND:
				return depend != null && !depend.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //DependenceImpl
