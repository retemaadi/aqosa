/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import edu.leiden.aqosa.model.IR.AQOSAPackage;
import edu.leiden.aqosa.model.IR.ExternalPort;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>External Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ExternalPortImpl#getId <em>Id</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ExternalPortImpl#getLowerFail <em>Lower Fail</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ExternalPortImpl#getUpperFail <em>Upper Fail</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ExternalPortImpl extends EObjectImpl implements ExternalPort {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getLowerFail() <em>Lower Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowerFail()
	 * @generated
	 * @ordered
	 */
	protected static final double LOWER_FAIL_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getLowerFail() <em>Lower Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowerFail()
	 * @generated
	 * @ordered
	 */
	protected double lowerFail = LOWER_FAIL_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpperFail() <em>Upper Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpperFail()
	 * @generated
	 * @ordered
	 */
	protected static final double UPPER_FAIL_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getUpperFail() <em>Upper Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpperFail()
	 * @generated
	 * @ordered
	 */
	protected double upperFail = UPPER_FAIL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExternalPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AQOSAPackage.Literals.EXTERNAL_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.EXTERNAL_PORT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLowerFail() {
		return lowerFail;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLowerFail(double newLowerFail) {
		double oldLowerFail = lowerFail;
		lowerFail = newLowerFail;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.EXTERNAL_PORT__LOWER_FAIL, oldLowerFail, lowerFail));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getUpperFail() {
		return upperFail;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpperFail(double newUpperFail) {
		double oldUpperFail = upperFail;
		upperFail = newUpperFail;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.EXTERNAL_PORT__UPPER_FAIL, oldUpperFail, upperFail));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AQOSAPackage.EXTERNAL_PORT__ID:
				return getId();
			case AQOSAPackage.EXTERNAL_PORT__LOWER_FAIL:
				return getLowerFail();
			case AQOSAPackage.EXTERNAL_PORT__UPPER_FAIL:
				return getUpperFail();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AQOSAPackage.EXTERNAL_PORT__ID:
				setId((String)newValue);
				return;
			case AQOSAPackage.EXTERNAL_PORT__LOWER_FAIL:
				setLowerFail((Double)newValue);
				return;
			case AQOSAPackage.EXTERNAL_PORT__UPPER_FAIL:
				setUpperFail((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AQOSAPackage.EXTERNAL_PORT__ID:
				setId(ID_EDEFAULT);
				return;
			case AQOSAPackage.EXTERNAL_PORT__LOWER_FAIL:
				setLowerFail(LOWER_FAIL_EDEFAULT);
				return;
			case AQOSAPackage.EXTERNAL_PORT__UPPER_FAIL:
				setUpperFail(UPPER_FAIL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AQOSAPackage.EXTERNAL_PORT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case AQOSAPackage.EXTERNAL_PORT__LOWER_FAIL:
				return lowerFail != LOWER_FAIL_EDEFAULT;
			case AQOSAPackage.EXTERNAL_PORT__UPPER_FAIL:
				return upperFail != UPPER_FAIL_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", lowerFail: ");
		result.append(lowerFail);
		result.append(", upperFail: ");
		result.append(upperFail);
		result.append(')');
		return result.toString();
	}

} //ExternalPortImpl
