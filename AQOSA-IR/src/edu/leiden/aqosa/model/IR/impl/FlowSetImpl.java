/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import edu.leiden.aqosa.model.IR.AQOSAPackage;
import edu.leiden.aqosa.model.IR.FlowInstance;
import edu.leiden.aqosa.model.IR.FlowSet;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Flow Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.FlowSetImpl#getName <em>Name</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.FlowSetImpl#getFlowinstance <em>Flowinstance</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.FlowSetImpl#getCompletionTime <em>Completion Time</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.FlowSetImpl#getMissedPercentage <em>Missed Percentage</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FlowSetImpl extends EObjectImpl implements FlowSet {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFlowinstance() <em>Flowinstance</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlowinstance()
	 * @generated
	 * @ordered
	 */
	protected EList<FlowInstance> flowinstance;

	/**
	 * The default value of the '{@link #getCompletionTime() <em>Completion Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompletionTime()
	 * @generated
	 * @ordered
	 */
	protected static final double COMPLETION_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCompletionTime() <em>Completion Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompletionTime()
	 * @generated
	 * @ordered
	 */
	protected double completionTime = COMPLETION_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMissedPercentage() <em>Missed Percentage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMissedPercentage()
	 * @generated
	 * @ordered
	 */
	protected static final double MISSED_PERCENTAGE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMissedPercentage() <em>Missed Percentage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMissedPercentage()
	 * @generated
	 * @ordered
	 */
	protected double missedPercentage = MISSED_PERCENTAGE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FlowSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AQOSAPackage.Literals.FLOW_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.FLOW_SET__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FlowInstance> getFlowinstance() {
		if (flowinstance == null) {
			flowinstance = new EObjectContainmentEList<FlowInstance>(FlowInstance.class, this, AQOSAPackage.FLOW_SET__FLOWINSTANCE);
		}
		return flowinstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCompletionTime() {
		return completionTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCompletionTime(double newCompletionTime) {
		double oldCompletionTime = completionTime;
		completionTime = newCompletionTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.FLOW_SET__COMPLETION_TIME, oldCompletionTime, completionTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMissedPercentage() {
		return missedPercentage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMissedPercentage(double newMissedPercentage) {
		double oldMissedPercentage = missedPercentage;
		missedPercentage = newMissedPercentage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.FLOW_SET__MISSED_PERCENTAGE, oldMissedPercentage, missedPercentage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AQOSAPackage.FLOW_SET__FLOWINSTANCE:
				return ((InternalEList<?>)getFlowinstance()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AQOSAPackage.FLOW_SET__NAME:
				return getName();
			case AQOSAPackage.FLOW_SET__FLOWINSTANCE:
				return getFlowinstance();
			case AQOSAPackage.FLOW_SET__COMPLETION_TIME:
				return getCompletionTime();
			case AQOSAPackage.FLOW_SET__MISSED_PERCENTAGE:
				return getMissedPercentage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AQOSAPackage.FLOW_SET__NAME:
				setName((String)newValue);
				return;
			case AQOSAPackage.FLOW_SET__FLOWINSTANCE:
				getFlowinstance().clear();
				getFlowinstance().addAll((Collection<? extends FlowInstance>)newValue);
				return;
			case AQOSAPackage.FLOW_SET__COMPLETION_TIME:
				setCompletionTime((Double)newValue);
				return;
			case AQOSAPackage.FLOW_SET__MISSED_PERCENTAGE:
				setMissedPercentage((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AQOSAPackage.FLOW_SET__NAME:
				setName(NAME_EDEFAULT);
				return;
			case AQOSAPackage.FLOW_SET__FLOWINSTANCE:
				getFlowinstance().clear();
				return;
			case AQOSAPackage.FLOW_SET__COMPLETION_TIME:
				setCompletionTime(COMPLETION_TIME_EDEFAULT);
				return;
			case AQOSAPackage.FLOW_SET__MISSED_PERCENTAGE:
				setMissedPercentage(MISSED_PERCENTAGE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AQOSAPackage.FLOW_SET__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case AQOSAPackage.FLOW_SET__FLOWINSTANCE:
				return flowinstance != null && !flowinstance.isEmpty();
			case AQOSAPackage.FLOW_SET__COMPLETION_TIME:
				return completionTime != COMPLETION_TIME_EDEFAULT;
			case AQOSAPackage.FLOW_SET__MISSED_PERCENTAGE:
				return missedPercentage != MISSED_PERCENTAGE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", completionTime: ");
		result.append(completionTime);
		result.append(", missedPercentage: ");
		result.append(missedPercentage);
		result.append(')');
		return result.toString();
	}

} //FlowSetImpl
