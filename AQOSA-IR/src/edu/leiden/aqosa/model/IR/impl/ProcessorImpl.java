/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import java.io.Serializable;

import edu.leiden.aqosa.model.IR.AQOSAPackage;
import edu.leiden.aqosa.model.IR.Processor;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Processor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ProcessorImpl#getId <em>Id</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ProcessorImpl#getClock <em>Clock</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ProcessorImpl#getCost <em>Cost</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ProcessorImpl#getInternalBusBandwidth <em>Internal Bus Bandwidth</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ProcessorImpl#getInternalBusDelay <em>Internal Bus Delay</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ProcessorImpl#getLowerFail <em>Lower Fail</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ProcessorImpl#getUpperFail <em>Upper Fail</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ProcessorImpl extends EObjectImpl implements Processor, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2932876720067930982L;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getClock() <em>Clock</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClock()
	 * @generated
	 * @ordered
	 */
	protected static final double CLOCK_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getClock() <em>Clock</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClock()
	 * @generated
	 * @ordered
	 */
	protected double clock = CLOCK_EDEFAULT;

	/**
	 * The default value of the '{@link #getCost() <em>Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCost()
	 * @generated
	 * @ordered
	 */
	protected static final double COST_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCost() <em>Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCost()
	 * @generated
	 * @ordered
	 */
	protected double cost = COST_EDEFAULT;

	/**
	 * The default value of the '{@link #getInternalBusBandwidth() <em>Internal Bus Bandwidth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalBusBandwidth()
	 * @generated
	 * @ordered
	 */
	protected static final double INTERNAL_BUS_BANDWIDTH_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getInternalBusBandwidth() <em>Internal Bus Bandwidth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalBusBandwidth()
	 * @generated
	 * @ordered
	 */
	protected double internalBusBandwidth = INTERNAL_BUS_BANDWIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getInternalBusDelay() <em>Internal Bus Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalBusDelay()
	 * @generated
	 * @ordered
	 */
	protected static final double INTERNAL_BUS_DELAY_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getInternalBusDelay() <em>Internal Bus Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalBusDelay()
	 * @generated
	 * @ordered
	 */
	protected double internalBusDelay = INTERNAL_BUS_DELAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getLowerFail() <em>Lower Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowerFail()
	 * @generated
	 * @ordered
	 */
	protected static final double LOWER_FAIL_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getLowerFail() <em>Lower Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowerFail()
	 * @generated
	 * @ordered
	 */
	protected double lowerFail = LOWER_FAIL_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpperFail() <em>Upper Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpperFail()
	 * @generated
	 * @ordered
	 */
	protected static final double UPPER_FAIL_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getUpperFail() <em>Upper Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpperFail()
	 * @generated
	 * @ordered
	 */
	protected double upperFail = UPPER_FAIL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AQOSAPackage.Literals.PROCESSOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.PROCESSOR__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getClock() {
		return clock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClock(double newClock) {
		double oldClock = clock;
		clock = newClock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.PROCESSOR__CLOCK, oldClock, clock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCost() {
		return cost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCost(double newCost) {
		double oldCost = cost;
		cost = newCost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.PROCESSOR__COST, oldCost, cost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getInternalBusBandwidth() {
		return internalBusBandwidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalBusBandwidth(double newInternalBusBandwidth) {
		double oldInternalBusBandwidth = internalBusBandwidth;
		internalBusBandwidth = newInternalBusBandwidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.PROCESSOR__INTERNAL_BUS_BANDWIDTH, oldInternalBusBandwidth, internalBusBandwidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getInternalBusDelay() {
		return internalBusDelay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalBusDelay(double newInternalBusDelay) {
		double oldInternalBusDelay = internalBusDelay;
		internalBusDelay = newInternalBusDelay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.PROCESSOR__INTERNAL_BUS_DELAY, oldInternalBusDelay, internalBusDelay));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLowerFail() {
		return lowerFail;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLowerFail(double newLowerFail) {
		double oldLowerFail = lowerFail;
		lowerFail = newLowerFail;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.PROCESSOR__LOWER_FAIL, oldLowerFail, lowerFail));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getUpperFail() {
		return upperFail;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpperFail(double newUpperFail) {
		double oldUpperFail = upperFail;
		upperFail = newUpperFail;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.PROCESSOR__UPPER_FAIL, oldUpperFail, upperFail));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AQOSAPackage.PROCESSOR__ID:
				return getId();
			case AQOSAPackage.PROCESSOR__CLOCK:
				return getClock();
			case AQOSAPackage.PROCESSOR__COST:
				return getCost();
			case AQOSAPackage.PROCESSOR__INTERNAL_BUS_BANDWIDTH:
				return getInternalBusBandwidth();
			case AQOSAPackage.PROCESSOR__INTERNAL_BUS_DELAY:
				return getInternalBusDelay();
			case AQOSAPackage.PROCESSOR__LOWER_FAIL:
				return getLowerFail();
			case AQOSAPackage.PROCESSOR__UPPER_FAIL:
				return getUpperFail();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AQOSAPackage.PROCESSOR__ID:
				setId((String)newValue);
				return;
			case AQOSAPackage.PROCESSOR__CLOCK:
				setClock((Double)newValue);
				return;
			case AQOSAPackage.PROCESSOR__COST:
				setCost((Double)newValue);
				return;
			case AQOSAPackage.PROCESSOR__INTERNAL_BUS_BANDWIDTH:
				setInternalBusBandwidth((Double)newValue);
				return;
			case AQOSAPackage.PROCESSOR__INTERNAL_BUS_DELAY:
				setInternalBusDelay((Double)newValue);
				return;
			case AQOSAPackage.PROCESSOR__LOWER_FAIL:
				setLowerFail((Double)newValue);
				return;
			case AQOSAPackage.PROCESSOR__UPPER_FAIL:
				setUpperFail((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AQOSAPackage.PROCESSOR__ID:
				setId(ID_EDEFAULT);
				return;
			case AQOSAPackage.PROCESSOR__CLOCK:
				setClock(CLOCK_EDEFAULT);
				return;
			case AQOSAPackage.PROCESSOR__COST:
				setCost(COST_EDEFAULT);
				return;
			case AQOSAPackage.PROCESSOR__INTERNAL_BUS_BANDWIDTH:
				setInternalBusBandwidth(INTERNAL_BUS_BANDWIDTH_EDEFAULT);
				return;
			case AQOSAPackage.PROCESSOR__INTERNAL_BUS_DELAY:
				setInternalBusDelay(INTERNAL_BUS_DELAY_EDEFAULT);
				return;
			case AQOSAPackage.PROCESSOR__LOWER_FAIL:
				setLowerFail(LOWER_FAIL_EDEFAULT);
				return;
			case AQOSAPackage.PROCESSOR__UPPER_FAIL:
				setUpperFail(UPPER_FAIL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AQOSAPackage.PROCESSOR__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case AQOSAPackage.PROCESSOR__CLOCK:
				return clock != CLOCK_EDEFAULT;
			case AQOSAPackage.PROCESSOR__COST:
				return cost != COST_EDEFAULT;
			case AQOSAPackage.PROCESSOR__INTERNAL_BUS_BANDWIDTH:
				return internalBusBandwidth != INTERNAL_BUS_BANDWIDTH_EDEFAULT;
			case AQOSAPackage.PROCESSOR__INTERNAL_BUS_DELAY:
				return internalBusDelay != INTERNAL_BUS_DELAY_EDEFAULT;
			case AQOSAPackage.PROCESSOR__LOWER_FAIL:
				return lowerFail != LOWER_FAIL_EDEFAULT;
			case AQOSAPackage.PROCESSOR__UPPER_FAIL:
				return upperFail != UPPER_FAIL_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", clock: ");
		result.append(clock);
		result.append(", cost: ");
		result.append(cost);
		result.append(", internalBusBandwidth: ");
		result.append(internalBusBandwidth);
		result.append(", internalBusDelay: ");
		result.append(internalBusDelay);
		result.append(", lowerFail: ");
		result.append(lowerFail);
		result.append(", upperFail: ");
		result.append(upperFail);
		result.append(')');
		return result.toString();
	}

} //ProcessorImpl
