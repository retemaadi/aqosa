/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import edu.leiden.aqosa.model.IR.AQOSAPackage;
import edu.leiden.aqosa.model.IR.Bus;
import edu.leiden.aqosa.model.IR.ComponentInstance;
import edu.leiden.aqosa.model.IR.ExternalPort;
import edu.leiden.aqosa.model.IR.Processor;
import edu.leiden.aqosa.model.IR.Repository;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Repository</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.RepositoryImpl#getComponentinstance <em>Componentinstance</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.RepositoryImpl#getProcessor <em>Processor</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.RepositoryImpl#getBus <em>Bus</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.RepositoryImpl#getExternalport <em>Externalport</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RepositoryImpl extends EObjectImpl implements Repository {
	/**
	 * The cached value of the '{@link #getComponentinstance() <em>Componentinstance</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentinstance()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentInstance> componentinstance;

	/**
	 * The cached value of the '{@link #getProcessor() <em>Processor</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessor()
	 * @generated
	 * @ordered
	 */
	protected EList<Processor> processor;

	/**
	 * The cached value of the '{@link #getBus() <em>Bus</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBus()
	 * @generated
	 * @ordered
	 */
	protected EList<Bus> bus;

	/**
	 * The cached value of the '{@link #getExternalport() <em>Externalport</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalport()
	 * @generated
	 * @ordered
	 */
	protected EList<ExternalPort> externalport;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RepositoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AQOSAPackage.Literals.REPOSITORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentInstance> getComponentinstance() {
		if (componentinstance == null) {
			componentinstance = new EObjectContainmentEList<ComponentInstance>(ComponentInstance.class, this, AQOSAPackage.REPOSITORY__COMPONENTINSTANCE);
		}
		return componentinstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Processor> getProcessor() {
		if (processor == null) {
			processor = new EObjectContainmentEList<Processor>(Processor.class, this, AQOSAPackage.REPOSITORY__PROCESSOR);
		}
		return processor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Bus> getBus() {
		if (bus == null) {
			bus = new EObjectContainmentEList<Bus>(Bus.class, this, AQOSAPackage.REPOSITORY__BUS);
		}
		return bus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExternalPort> getExternalport() {
		if (externalport == null) {
			externalport = new EObjectContainmentEList<ExternalPort>(ExternalPort.class, this, AQOSAPackage.REPOSITORY__EXTERNALPORT);
		}
		return externalport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AQOSAPackage.REPOSITORY__COMPONENTINSTANCE:
				return ((InternalEList<?>)getComponentinstance()).basicRemove(otherEnd, msgs);
			case AQOSAPackage.REPOSITORY__PROCESSOR:
				return ((InternalEList<?>)getProcessor()).basicRemove(otherEnd, msgs);
			case AQOSAPackage.REPOSITORY__BUS:
				return ((InternalEList<?>)getBus()).basicRemove(otherEnd, msgs);
			case AQOSAPackage.REPOSITORY__EXTERNALPORT:
				return ((InternalEList<?>)getExternalport()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AQOSAPackage.REPOSITORY__COMPONENTINSTANCE:
				return getComponentinstance();
			case AQOSAPackage.REPOSITORY__PROCESSOR:
				return getProcessor();
			case AQOSAPackage.REPOSITORY__BUS:
				return getBus();
			case AQOSAPackage.REPOSITORY__EXTERNALPORT:
				return getExternalport();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AQOSAPackage.REPOSITORY__COMPONENTINSTANCE:
				getComponentinstance().clear();
				getComponentinstance().addAll((Collection<? extends ComponentInstance>)newValue);
				return;
			case AQOSAPackage.REPOSITORY__PROCESSOR:
				getProcessor().clear();
				getProcessor().addAll((Collection<? extends Processor>)newValue);
				return;
			case AQOSAPackage.REPOSITORY__BUS:
				getBus().clear();
				getBus().addAll((Collection<? extends Bus>)newValue);
				return;
			case AQOSAPackage.REPOSITORY__EXTERNALPORT:
				getExternalport().clear();
				getExternalport().addAll((Collection<? extends ExternalPort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AQOSAPackage.REPOSITORY__COMPONENTINSTANCE:
				getComponentinstance().clear();
				return;
			case AQOSAPackage.REPOSITORY__PROCESSOR:
				getProcessor().clear();
				return;
			case AQOSAPackage.REPOSITORY__BUS:
				getBus().clear();
				return;
			case AQOSAPackage.REPOSITORY__EXTERNALPORT:
				getExternalport().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AQOSAPackage.REPOSITORY__COMPONENTINSTANCE:
				return componentinstance != null && !componentinstance.isEmpty();
			case AQOSAPackage.REPOSITORY__PROCESSOR:
				return processor != null && !processor.isEmpty();
			case AQOSAPackage.REPOSITORY__BUS:
				return bus != null && !bus.isEmpty();
			case AQOSAPackage.REPOSITORY__EXTERNALPORT:
				return externalport != null && !externalport.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RepositoryImpl
