/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import edu.leiden.aqosa.model.IR.AQOSAPackage;
import edu.leiden.aqosa.model.IR.ExternalPort;
import edu.leiden.aqosa.model.IR.InPort;
import edu.leiden.aqosa.model.IR.RequirePort;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Require Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.RequirePortImpl#getInternal <em>Internal</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.RequirePortImpl#getExternal <em>External</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RequirePortImpl extends EObjectImpl implements RequirePort {
	/**
	 * The cached value of the '{@link #getInternal() <em>Internal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternal()
	 * @generated
	 * @ordered
	 */
	protected InPort internal;

	/**
	 * The cached value of the '{@link #getExternal() <em>External</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternal()
	 * @generated
	 * @ordered
	 */
	protected ExternalPort external;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequirePortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AQOSAPackage.Literals.REQUIRE_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InPort getInternal() {
		if (internal != null && internal.eIsProxy()) {
			InternalEObject oldInternal = (InternalEObject)internal;
			internal = (InPort)eResolveProxy(oldInternal);
			if (internal != oldInternal) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AQOSAPackage.REQUIRE_PORT__INTERNAL, oldInternal, internal));
			}
		}
		return internal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InPort basicGetInternal() {
		return internal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternal(InPort newInternal) {
		InPort oldInternal = internal;
		internal = newInternal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.REQUIRE_PORT__INTERNAL, oldInternal, internal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalPort getExternal() {
		if (external != null && external.eIsProxy()) {
			InternalEObject oldExternal = (InternalEObject)external;
			external = (ExternalPort)eResolveProxy(oldExternal);
			if (external != oldExternal) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AQOSAPackage.REQUIRE_PORT__EXTERNAL, oldExternal, external));
			}
		}
		return external;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalPort basicGetExternal() {
		return external;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternal(ExternalPort newExternal) {
		ExternalPort oldExternal = external;
		external = newExternal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.REQUIRE_PORT__EXTERNAL, oldExternal, external));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AQOSAPackage.REQUIRE_PORT__INTERNAL:
				if (resolve) return getInternal();
				return basicGetInternal();
			case AQOSAPackage.REQUIRE_PORT__EXTERNAL:
				if (resolve) return getExternal();
				return basicGetExternal();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AQOSAPackage.REQUIRE_PORT__INTERNAL:
				setInternal((InPort)newValue);
				return;
			case AQOSAPackage.REQUIRE_PORT__EXTERNAL:
				setExternal((ExternalPort)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AQOSAPackage.REQUIRE_PORT__INTERNAL:
				setInternal((InPort)null);
				return;
			case AQOSAPackage.REQUIRE_PORT__EXTERNAL:
				setExternal((ExternalPort)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AQOSAPackage.REQUIRE_PORT__INTERNAL:
				return internal != null;
			case AQOSAPackage.REQUIRE_PORT__EXTERNAL:
				return external != null;
		}
		return super.eIsSet(featureID);
	}

} //RequirePortImpl
