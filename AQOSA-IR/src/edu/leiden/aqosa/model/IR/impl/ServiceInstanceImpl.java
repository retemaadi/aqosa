/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import edu.leiden.aqosa.model.IR.AQOSAPackage;
import edu.leiden.aqosa.model.IR.Dependence;
import edu.leiden.aqosa.model.IR.ProvidePort;
import edu.leiden.aqosa.model.IR.Service;
import edu.leiden.aqosa.model.IR.ServiceInstance;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ServiceInstanceImpl#getInstance <em>Instance</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ServiceInstanceImpl#getCycles <em>Cycles</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ServiceInstanceImpl#getMemoryUsage <em>Memory Usage</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ServiceInstanceImpl#getNetworkUsage <em>Network Usage</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ServiceInstanceImpl#getProvide <em>Provide</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.ServiceInstanceImpl#getDepend <em>Depend</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ServiceInstanceImpl extends EObjectImpl implements ServiceInstance {
	/**
	 * The cached value of the '{@link #getInstance() <em>Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstance()
	 * @generated
	 * @ordered
	 */
	protected Service instance;

	/**
	 * The default value of the '{@link #getCycles() <em>Cycles</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCycles()
	 * @generated
	 * @ordered
	 */
	protected static final int CYCLES_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCycles() <em>Cycles</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCycles()
	 * @generated
	 * @ordered
	 */
	protected int cycles = CYCLES_EDEFAULT;

	/**
	 * The default value of the '{@link #getMemoryUsage() <em>Memory Usage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemoryUsage()
	 * @generated
	 * @ordered
	 */
	protected static final double MEMORY_USAGE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMemoryUsage() <em>Memory Usage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemoryUsage()
	 * @generated
	 * @ordered
	 */
	protected double memoryUsage = MEMORY_USAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getNetworkUsage() <em>Network Usage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNetworkUsage()
	 * @generated
	 * @ordered
	 */
	protected static final double NETWORK_USAGE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getNetworkUsage() <em>Network Usage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNetworkUsage()
	 * @generated
	 * @ordered
	 */
	protected double networkUsage = NETWORK_USAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProvide() <em>Provide</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvide()
	 * @generated
	 * @ordered
	 */
	protected EList<ProvidePort> provide;

	/**
	 * The cached value of the '{@link #getDepend() <em>Depend</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepend()
	 * @generated
	 * @ordered
	 */
	protected EList<Dependence> depend;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AQOSAPackage.Literals.SERVICE_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Service getInstance() {
		if (instance != null && instance.eIsProxy()) {
			InternalEObject oldInstance = (InternalEObject)instance;
			instance = (Service)eResolveProxy(oldInstance);
			if (instance != oldInstance) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AQOSAPackage.SERVICE_INSTANCE__INSTANCE, oldInstance, instance));
			}
		}
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Service basicGetInstance() {
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstance(Service newInstance) {
		Service oldInstance = instance;
		instance = newInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.SERVICE_INSTANCE__INSTANCE, oldInstance, instance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCycles() {
		return cycles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCycles(int newCycles) {
		int oldCycles = cycles;
		cycles = newCycles;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.SERVICE_INSTANCE__CYCLES, oldCycles, cycles));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMemoryUsage() {
		return memoryUsage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMemoryUsage(double newMemoryUsage) {
		double oldMemoryUsage = memoryUsage;
		memoryUsage = newMemoryUsage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.SERVICE_INSTANCE__MEMORY_USAGE, oldMemoryUsage, memoryUsage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getNetworkUsage() {
		return networkUsage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNetworkUsage(double newNetworkUsage) {
		double oldNetworkUsage = networkUsage;
		networkUsage = newNetworkUsage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.SERVICE_INSTANCE__NETWORK_USAGE, oldNetworkUsage, networkUsage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProvidePort> getProvide() {
		if (provide == null) {
			provide = new EObjectContainmentEList<ProvidePort>(ProvidePort.class, this, AQOSAPackage.SERVICE_INSTANCE__PROVIDE);
		}
		return provide;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Dependence> getDepend() {
		if (depend == null) {
			depend = new EObjectContainmentEList<Dependence>(Dependence.class, this, AQOSAPackage.SERVICE_INSTANCE__DEPEND);
		}
		return depend;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AQOSAPackage.SERVICE_INSTANCE__PROVIDE:
				return ((InternalEList<?>)getProvide()).basicRemove(otherEnd, msgs);
			case AQOSAPackage.SERVICE_INSTANCE__DEPEND:
				return ((InternalEList<?>)getDepend()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AQOSAPackage.SERVICE_INSTANCE__INSTANCE:
				if (resolve) return getInstance();
				return basicGetInstance();
			case AQOSAPackage.SERVICE_INSTANCE__CYCLES:
				return getCycles();
			case AQOSAPackage.SERVICE_INSTANCE__MEMORY_USAGE:
				return getMemoryUsage();
			case AQOSAPackage.SERVICE_INSTANCE__NETWORK_USAGE:
				return getNetworkUsage();
			case AQOSAPackage.SERVICE_INSTANCE__PROVIDE:
				return getProvide();
			case AQOSAPackage.SERVICE_INSTANCE__DEPEND:
				return getDepend();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AQOSAPackage.SERVICE_INSTANCE__INSTANCE:
				setInstance((Service)newValue);
				return;
			case AQOSAPackage.SERVICE_INSTANCE__CYCLES:
				setCycles((Integer)newValue);
				return;
			case AQOSAPackage.SERVICE_INSTANCE__MEMORY_USAGE:
				setMemoryUsage((Double)newValue);
				return;
			case AQOSAPackage.SERVICE_INSTANCE__NETWORK_USAGE:
				setNetworkUsage((Double)newValue);
				return;
			case AQOSAPackage.SERVICE_INSTANCE__PROVIDE:
				getProvide().clear();
				getProvide().addAll((Collection<? extends ProvidePort>)newValue);
				return;
			case AQOSAPackage.SERVICE_INSTANCE__DEPEND:
				getDepend().clear();
				getDepend().addAll((Collection<? extends Dependence>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AQOSAPackage.SERVICE_INSTANCE__INSTANCE:
				setInstance((Service)null);
				return;
			case AQOSAPackage.SERVICE_INSTANCE__CYCLES:
				setCycles(CYCLES_EDEFAULT);
				return;
			case AQOSAPackage.SERVICE_INSTANCE__MEMORY_USAGE:
				setMemoryUsage(MEMORY_USAGE_EDEFAULT);
				return;
			case AQOSAPackage.SERVICE_INSTANCE__NETWORK_USAGE:
				setNetworkUsage(NETWORK_USAGE_EDEFAULT);
				return;
			case AQOSAPackage.SERVICE_INSTANCE__PROVIDE:
				getProvide().clear();
				return;
			case AQOSAPackage.SERVICE_INSTANCE__DEPEND:
				getDepend().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AQOSAPackage.SERVICE_INSTANCE__INSTANCE:
				return instance != null;
			case AQOSAPackage.SERVICE_INSTANCE__CYCLES:
				return cycles != CYCLES_EDEFAULT;
			case AQOSAPackage.SERVICE_INSTANCE__MEMORY_USAGE:
				return memoryUsage != MEMORY_USAGE_EDEFAULT;
			case AQOSAPackage.SERVICE_INSTANCE__NETWORK_USAGE:
				return networkUsage != NETWORK_USAGE_EDEFAULT;
			case AQOSAPackage.SERVICE_INSTANCE__PROVIDE:
				return provide != null && !provide.isEmpty();
			case AQOSAPackage.SERVICE_INSTANCE__DEPEND:
				return depend != null && !depend.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (cycles: ");
		result.append(cycles);
		result.append(", memoryUsage: ");
		result.append(memoryUsage);
		result.append(", networkUsage: ");
		result.append(networkUsage);
		result.append(')');
		return result.toString();
	}

} //ServiceInstanceImpl
