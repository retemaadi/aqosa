/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package edu.leiden.aqosa.model.IR.impl;

import edu.leiden.aqosa.model.IR.AQOSAPackage;
import edu.leiden.aqosa.model.IR.Evaluations;
import edu.leiden.aqosa.model.IR.Settings;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.SettingsImpl#getEvaluations <em>Evaluations</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.SettingsImpl#getNoRun <em>No Run</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.SettingsImpl#getNoSampling <em>No Sampling</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.SettingsImpl#getNoDuplicate <em>No Duplicate</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.SettingsImpl#getMinCost <em>Min Cost</em>}</li>
 *   <li>{@link edu.leiden.aqosa.model.IR.impl.SettingsImpl#getMaxCost <em>Max Cost</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SettingsImpl extends EObjectImpl implements Settings {
	/**
	 * The cached value of the '{@link #getEvaluations() <em>Evaluations</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluations()
	 * @generated
	 * @ordered
	 */
	protected EList<Evaluations> evaluations;

	/**
	 * The default value of the '{@link #getNoRun() <em>No Run</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoRun()
	 * @generated
	 * @ordered
	 */
	protected static final int NO_RUN_EDEFAULT = 10;

	/**
	 * The cached value of the '{@link #getNoRun() <em>No Run</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoRun()
	 * @generated
	 * @ordered
	 */
	protected int noRun = NO_RUN_EDEFAULT;

	/**
	 * The default value of the '{@link #getNoSampling() <em>No Sampling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoSampling()
	 * @generated
	 * @ordered
	 */
	protected static final int NO_SAMPLING_EDEFAULT = 100;

	/**
	 * The cached value of the '{@link #getNoSampling() <em>No Sampling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoSampling()
	 * @generated
	 * @ordered
	 */
	protected int noSampling = NO_SAMPLING_EDEFAULT;

	/**
	 * The default value of the '{@link #getNoDuplicate() <em>No Duplicate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoDuplicate()
	 * @generated
	 * @ordered
	 */
	protected static final int NO_DUPLICATE_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getNoDuplicate() <em>No Duplicate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoDuplicate()
	 * @generated
	 * @ordered
	 */
	protected int noDuplicate = NO_DUPLICATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinCost() <em>Min Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinCost()
	 * @generated
	 * @ordered
	 */
	protected static final double MIN_COST_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMinCost() <em>Min Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinCost()
	 * @generated
	 * @ordered
	 */
	protected double minCost = MIN_COST_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxCost() <em>Max Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxCost()
	 * @generated
	 * @ordered
	 */
	protected static final double MAX_COST_EDEFAULT = 1000.0;

	/**
	 * The cached value of the '{@link #getMaxCost() <em>Max Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxCost()
	 * @generated
	 * @ordered
	 */
	protected double maxCost = MAX_COST_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SettingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AQOSAPackage.Literals.SETTINGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Evaluations> getEvaluations() {
		if (evaluations == null) {
			evaluations = new EDataTypeUniqueEList<Evaluations>(Evaluations.class, this, AQOSAPackage.SETTINGS__EVALUATIONS);
		}
		return evaluations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNoRun() {
		return noRun;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoRun(int newNoRun) {
		int oldNoRun = noRun;
		noRun = newNoRun;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.SETTINGS__NO_RUN, oldNoRun, noRun));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNoSampling() {
		return noSampling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoSampling(int newNoSampling) {
		int oldNoSampling = noSampling;
		noSampling = newNoSampling;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.SETTINGS__NO_SAMPLING, oldNoSampling, noSampling));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNoDuplicate() {
		return noDuplicate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoDuplicate(int newNoDuplicate) {
		int oldNoDuplicate = noDuplicate;
		noDuplicate = newNoDuplicate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.SETTINGS__NO_DUPLICATE, oldNoDuplicate, noDuplicate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMinCost() {
		return minCost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinCost(double newMinCost) {
		double oldMinCost = minCost;
		minCost = newMinCost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.SETTINGS__MIN_COST, oldMinCost, minCost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMaxCost() {
		return maxCost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxCost(double newMaxCost) {
		double oldMaxCost = maxCost;
		maxCost = newMaxCost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AQOSAPackage.SETTINGS__MAX_COST, oldMaxCost, maxCost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AQOSAPackage.SETTINGS__EVALUATIONS:
				return getEvaluations();
			case AQOSAPackage.SETTINGS__NO_RUN:
				return getNoRun();
			case AQOSAPackage.SETTINGS__NO_SAMPLING:
				return getNoSampling();
			case AQOSAPackage.SETTINGS__NO_DUPLICATE:
				return getNoDuplicate();
			case AQOSAPackage.SETTINGS__MIN_COST:
				return getMinCost();
			case AQOSAPackage.SETTINGS__MAX_COST:
				return getMaxCost();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AQOSAPackage.SETTINGS__EVALUATIONS:
				getEvaluations().clear();
				getEvaluations().addAll((Collection<? extends Evaluations>)newValue);
				return;
			case AQOSAPackage.SETTINGS__NO_RUN:
				setNoRun((Integer)newValue);
				return;
			case AQOSAPackage.SETTINGS__NO_SAMPLING:
				setNoSampling((Integer)newValue);
				return;
			case AQOSAPackage.SETTINGS__NO_DUPLICATE:
				setNoDuplicate((Integer)newValue);
				return;
			case AQOSAPackage.SETTINGS__MIN_COST:
				setMinCost((Double)newValue);
				return;
			case AQOSAPackage.SETTINGS__MAX_COST:
				setMaxCost((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AQOSAPackage.SETTINGS__EVALUATIONS:
				getEvaluations().clear();
				return;
			case AQOSAPackage.SETTINGS__NO_RUN:
				setNoRun(NO_RUN_EDEFAULT);
				return;
			case AQOSAPackage.SETTINGS__NO_SAMPLING:
				setNoSampling(NO_SAMPLING_EDEFAULT);
				return;
			case AQOSAPackage.SETTINGS__NO_DUPLICATE:
				setNoDuplicate(NO_DUPLICATE_EDEFAULT);
				return;
			case AQOSAPackage.SETTINGS__MIN_COST:
				setMinCost(MIN_COST_EDEFAULT);
				return;
			case AQOSAPackage.SETTINGS__MAX_COST:
				setMaxCost(MAX_COST_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AQOSAPackage.SETTINGS__EVALUATIONS:
				return evaluations != null && !evaluations.isEmpty();
			case AQOSAPackage.SETTINGS__NO_RUN:
				return noRun != NO_RUN_EDEFAULT;
			case AQOSAPackage.SETTINGS__NO_SAMPLING:
				return noSampling != NO_SAMPLING_EDEFAULT;
			case AQOSAPackage.SETTINGS__NO_DUPLICATE:
				return noDuplicate != NO_DUPLICATE_EDEFAULT;
			case AQOSAPackage.SETTINGS__MIN_COST:
				return minCost != MIN_COST_EDEFAULT;
			case AQOSAPackage.SETTINGS__MAX_COST:
				return maxCost != MAX_COST_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (evaluations: ");
		result.append(evaluations);
		result.append(", noRun: ");
		result.append(noRun);
		result.append(", noSampling: ");
		result.append(noSampling);
		result.append(", noDuplicate: ");
		result.append(noDuplicate);
		result.append(", minCost: ");
		result.append(minCost);
		result.append(", maxCost: ");
		result.append(maxCost);
		result.append(')');
		return result.toString();
	}

} //SettingsImpl
