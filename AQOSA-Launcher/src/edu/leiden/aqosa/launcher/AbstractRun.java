package edu.leiden.aqosa.launcher;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.opt4j.core.IndividualStateListener;
import org.opt4j.core.common.archive.ArchiveModule;
import org.opt4j.core.common.completer.IndividualCompleterModule;
import org.opt4j.core.common.completer.IndividualCompleterModule.Type;
import org.opt4j.core.optimizer.OptimizerIterationListener;
import org.opt4j.core.optimizer.OptimizerModule;
import org.opt4j.core.optimizer.OptimizerStateListener;
import org.opt4j.core.problem.ProblemModule;
import org.opt4j.core.start.Opt4JModule;
import org.opt4j.core.start.Opt4JTask;
import org.opt4j.operators.OperatorModule;
import org.opt4j.optimizers.ea.EvolutionaryAlgorithmModule;
import org.opt4j.optimizers.ea.Nsga2Module;
import org.opt4j.optimizers.ea.SMSModule;
import org.opt4j.optimizers.ea.SelectorModule;
import org.opt4j.optimizers.ea.Spea2Module;
import org.opt4j.viewer.ViewerModule;

import com.google.inject.Module;

import edu.leiden.aqosa.akka.AkkaCompleterModule;
import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.hadoop.completer.HadoopCompleterModule;
import edu.leiden.aqosa.hadoop.hv.DistributedSMSModule;
import edu.leiden.aqosa.logger.AbstractArchWriter;
import edu.leiden.aqosa.logger.ArchLoggerModule;
import edu.leiden.aqosa.logger.EvaluationLogger.DetailLevel;
import edu.leiden.aqosa.mating.AbstractArchMating;
import edu.leiden.aqosa.mating.ArchMatingModule;
import edu.leiden.aqosa.mating.CrossoverMutateMating;
import edu.leiden.aqosa.mating.MutateMating;
import edu.leiden.aqosa.mating.RandomMating;
import edu.leiden.aqosa.mating.RandomMutateMating;
import edu.leiden.aqosa.mating.RandomSequentialMating;
import edu.leiden.aqosa.mating.RandomSequentialMutateMating;
import edu.leiden.aqosa.mating.SequentialMating;
import edu.leiden.aqosa.mating.SequentialMutateMating;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.model.Configuration;
import edu.leiden.aqosa.model.ModelResource;
import edu.leiden.aqosa.model.IR.AQOSAModel;
import edu.leiden.aqosa.optimizer.ArchProblem;
import edu.leiden.aqosa.viewer.AQOSAViewerModule;

abstract class AbstractRun implements Callable<Void> {
	
	private final Experiment experiment;
	private final List<String> results = new ArrayList<String>();
	
	protected AbstractRun(InputStream inputModelStream) throws IOException {
		experiment = new Experiment(inputModelStream);
	}
	
	protected void oneRun() throws Exception {
		Opt4JTask task = new Opt4JTask(false);
		task.init(experiment.getModules());
		
		try {
			task.execute();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			task.close();
		}
		
		String archiveFilename = experiment.getArchiveFilename();
		if (!archiveFilename.equals(""))
			results.add(archiveFilename);
		
		String optimumFilename = experiment.getOptimumFilename();
		if (!optimumFilename.equals(""))
			results.add(optimumFilename);
		
	}
	
	@Override
	public Void call() throws Exception {
		try {
			run();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return null;		
	}
	
	abstract protected void run() throws Exception;

	protected ArchModel getArchModel() {
		return experiment.getArchModel();
	}
	
	public void setGenotype(Class<? extends ArchGenotype> genotypeClass) {
		experiment.configGenotype(genotypeClass);
	}
	
	public void setConfiguration(Configuration configuration) {
		experiment.configConfiguration(configuration);
	}
	
	public void setEA(int generations, int alpha, int mu, int lambda) {
		experiment.configGenerations(generations);
		experiment.configAlpha(alpha);
		experiment.configMu(mu);
		experiment.configLambda(lambda);
	}
	
	public void setCrossoverRate(double crossoverRate) {
		experiment.configCrossoverRate(crossoverRate);
	}
	
	public void setHeuristicRate(double heuristicRate) {
		experiment.configHeuristicRate(heuristicRate);
	}
	
	public void setArchiveSize(int archiveSize) {
		experiment.configArchiveSize(archiveSize);
	}
	
	protected void setAlgorithm(EAAlgorithm algo) {
		experiment.configAlgorithm(algo);
	}
	
	protected void setCompleter(CompleterType type) {
		experiment.configCompleter(type);
	}
	
	protected void setMatingAppraoch(MatingApproach appr) {
		experiment.configMatingApproach(appr);
	}
	
	public void addOptimizerStateListener(Class<? extends OptimizerStateListener> stateListenerClass) {
		experiment.addOptimizerStateListener(stateListenerClass);
	}
	
	public void addIndividualStateListener(Class<? extends IndividualStateListener> stateListenerClass) {
		experiment.addIndividualStateListener(stateListenerClass);
	}
	
	public void addOptimizerIterationListener(Class<? extends OptimizerIterationListener> iterationListenerClass) {
		experiment.addOptimizerIterationListener(iterationListenerClass);
	}
	
	public void setLoggerLevel(DetailLevel logLevel) {
		experiment.configLoggerLevel(logLevel);
	}
	
	public void setSerializedLog(boolean serializedLog) {
		experiment.configSerializedLog(serializedLog);
	}
	
	public void setIterationLog(boolean iterationLog) {
		experiment.configIterationLog(iterationLog);
	}
	
	public void setTimeLog(boolean timeLog) {
		experiment.configTimeLog(timeLog);
	}
	
	public void setEvaluationLogger(PrintStream stream) {
		experiment.configEvaluationLogger(stream);
	}
	
	public void setLogWriter(Class<? extends AbstractArchWriter> writerClass) {
		experiment.configLogWriter(writerClass);
	}
	
	protected void setArchiveLogger(String archiveFilename) {
		experiment.configArchiveLogger(archiveFilename);
	}
	
	protected void setOptimumLogger(String optimumFilename) {
		experiment.configOptimumLogger(optimumFilename);
	}
	
	public void setViewer() {
		experiment.configViewer();
	}
	
	public List<String> getResults() {
		return results;
	}
	
}

class Experiment {
	
	protected final ModelResource modelResource;
	protected final AQOSAModel aqosaModel;
	protected final Map<Class<? extends Opt4JModule>, Module> modules = new HashMap<Class<? extends Opt4JModule>, Module>();
	
	public Experiment(InputStream inputModelStream) {
		modelResource = new ModelResource();
		
		aqosaModel = modelResource.readModel(inputModelStream);
		ArchProblem problem = new ArchProblem();
		problem.setModel(aqosaModel);
		
		IndividualCompleterModule completer = new IndividualCompleterModule();
		completer.setType(Type.PARALLEL);
		
		modules.put(ProblemModule.class, problem);
		modules.put(IndividualCompleterModule.class, completer);
		modules.put(OptimizerModule.class, new EvolutionaryAlgorithmModule());
		modules.put(SelectorModule.class, new Nsga2Module());
		modules.put(OperatorModule.class, new ArchMatingModule());
		modules.put(ArchiveModule.class, new ArchiveModule());
	}
	
	protected ArchModel getArchModel() {
		ArchProblem problem = (ArchProblem) modules.get(ProblemModule.class);
		return problem.getModel();
	}
	
	protected void configGenotype(Class<? extends ArchGenotype> genotypeClass) {
		ArchProblem problem = (ArchProblem) modules.get(ProblemModule.class);
		problem.setGenotypeClass(genotypeClass);
	}
	
	protected void configConfiguration(Configuration configuration) {
		ArchProblem problem = (ArchProblem) modules.get(ProblemModule.class);
		problem.setModel(aqosaModel, configuration);
	}
	
	protected void configGenerations(int generations) {
		EvolutionaryAlgorithmModule ea = (EvolutionaryAlgorithmModule) modules.get(OptimizerModule.class);
		ea.setGenerations(generations);
	}
	
	protected void configAlpha(int alpha) {
		EvolutionaryAlgorithmModule ea = (EvolutionaryAlgorithmModule) modules.get(OptimizerModule.class);
		ea.setAlpha(alpha);
	}
	
	protected void configMu(int mu) {
		EvolutionaryAlgorithmModule ea = (EvolutionaryAlgorithmModule) modules.get(OptimizerModule.class);
		ea.setMu(mu);
	}
	
	protected void configLambda(int lambda) {
		EvolutionaryAlgorithmModule ea = (EvolutionaryAlgorithmModule) modules.get(OptimizerModule.class);
		ea.setLambda(lambda);
	}
	
	protected void configCrossoverRate(double crossoverRate) {
		EvolutionaryAlgorithmModule ea = (EvolutionaryAlgorithmModule) modules.get(OptimizerModule.class);
		ea.setCrossoverRate(crossoverRate);
	}
	
	protected void configAlgorithm(EAAlgorithm algo) {
		switch (algo) {
		default:
		case NSGA2:
			modules.put(SelectorModule.class, new Nsga2Module());
			break;
		case SPEA2:
			modules.put(SelectorModule.class, new Spea2Module());
			break;
		case SMS:
			modules.put(SelectorModule.class, new SMSModule());
			break;
		case DistributedSMS:
			modules.put(SelectorModule.class, new DistributedSMSModule());
			break;
		}
	}
	
	protected void configCompleter(CompleterType type) {
		Opt4JModule module;
		switch (type) {
		case SEQUENTIAL:
			IndividualCompleterModule seqCompleter = new IndividualCompleterModule();
			seqCompleter.setType(Type.SEQUENTIAL);
			module = seqCompleter;
			break;
		default:
		case MULTITHREAD:
			IndividualCompleterModule multiCompleter = new IndividualCompleterModule();
			multiCompleter.setType(Type.PARALLEL);
			module = multiCompleter;
			break;
		case HADOOP:
			HadoopCompleterModule hadoopCompleter = new HadoopCompleterModule(modelResource);
			module = hadoopCompleter;
			break;
		case AKKA:
			AkkaCompleterModule akkaCompleter = new AkkaCompleterModule();
			module = akkaCompleter;
			break;
		}
		
		modules.put(IndividualCompleterModule.class, module);
	}
	
	protected void configMatingApproach(MatingApproach appr) {
		ArchMatingModule mating = (ArchMatingModule) modules.get(OperatorModule.class);
		
		Class<? extends AbstractArchMating> matingClass = null;
		switch (appr) {
		case ONLY_MUTATE:
			matingClass = MutateMating.class;
			break;			
		case CROSSOVER_MUTATE:
			matingClass = CrossoverMutateMating.class;
			break;			
		case RANDOM:
			matingClass = RandomMating.class;
			break;
		default:
		case SEQUENTIAL:
			matingClass = SequentialMating.class;
			break;
		case HALFRANDOM_HALFSEQUENTIAL:
			matingClass = RandomSequentialMating.class;
			break;
		case HALFRANDOM_HALFMUTATE:
			matingClass = RandomMutateMating.class;
			break;
		case HALFSEQUENTIAL_HALFMUTATE:
			matingClass = SequentialMutateMating.class;
			break;
		case HALFRANDOMSEQUENTIAL_HALFMUTATE:
			matingClass = RandomSequentialMutateMating.class;
			break;
		}
		
		mating.setApproach(matingClass);
	}
	
	protected void configHeuristicRate(double heuristicRate) {
		ArchMatingModule mating = (ArchMatingModule) modules.get(OperatorModule.class);
		mating.setHeuristicRate(heuristicRate);
	}
	
	protected void configArchiveSize(int archiveSize) {
		ArchiveModule archive = (ArchiveModule) modules.get(ArchiveModule.class);
		archive.setCapacity(archiveSize);
	}
	
	public void addOptimizerStateListener(Class<? extends OptimizerStateListener> stateListenerClass) {
		ArchLoggerModule logger = (ArchLoggerModule) modules.get(ArchLoggerModule.class);
		if (logger == null) {
			logger = new ArchLoggerModule();
			modules.put(ArchLoggerModule.class, logger);
		}
		logger.addOptimizerStateListenerClass(stateListenerClass);
	}
	
	public void addIndividualStateListener(Class<? extends IndividualStateListener> stateListenerClass) {
		ArchLoggerModule logger = (ArchLoggerModule) modules.get(ArchLoggerModule.class);
		if (logger == null) {
			logger = new ArchLoggerModule();
			modules.put(ArchLoggerModule.class, logger);
		}
		logger.addIndividualStateListenerClass(stateListenerClass);
	}
	
	public void addOptimizerIterationListener(Class<? extends OptimizerIterationListener> iterationListenerClass) {
		ArchLoggerModule logger = (ArchLoggerModule) modules.get(ArchLoggerModule.class);
		if (logger == null) {
			logger = new ArchLoggerModule();
			modules.put(ArchLoggerModule.class, logger);
		}
		logger.addOptimizerIterationListenerClass(iterationListenerClass);
	}
	
	protected void configLoggerLevel(DetailLevel logLevel) {
		ArchLoggerModule logger = (ArchLoggerModule) modules.get(ArchLoggerModule.class);
		if (logger == null) {
			logger = new ArchLoggerModule();
			modules.put(ArchLoggerModule.class, logger);
		}
		logger.setLoggerLevel(logLevel);
	}
	
	protected void configSerializedLog(boolean serializedLog) {
		ArchLoggerModule logger = (ArchLoggerModule) modules.get(ArchLoggerModule.class);
		if (logger == null) {
			logger = new ArchLoggerModule();
			modules.put(ArchLoggerModule.class, logger);
		}
		logger.setSerializedLog(serializedLog);
	}
	
	protected void configIterationLog(boolean iterationLog) {
		ArchLoggerModule logger = (ArchLoggerModule) modules.get(ArchLoggerModule.class);
		if (logger == null) {
			logger = new ArchLoggerModule();
			modules.put(ArchLoggerModule.class, logger);
		}
		logger.setIterationLog(iterationLog);
	}
	
	protected void configTimeLog(boolean timeLog) {
		ArchLoggerModule logger = (ArchLoggerModule) modules.get(ArchLoggerModule.class);
		if (logger == null) {
			logger = new ArchLoggerModule();
			modules.put(ArchLoggerModule.class, logger);
		}
		logger.setTimeLog(timeLog);
	}
	
	protected void configEvaluationLogger(PrintStream stream) {
		ArchLoggerModule logger = (ArchLoggerModule) modules.get(ArchLoggerModule.class);
		if (logger == null) {
			logger = new ArchLoggerModule();
			modules.put(ArchLoggerModule.class, logger);
		}
		logger.setStream(stream);
	}
	
	protected void configLogWriter(Class<? extends AbstractArchWriter> writerClass) {
		ArchLoggerModule logger = (ArchLoggerModule) modules.get(ArchLoggerModule.class);
		if (logger == null) {
			logger = new ArchLoggerModule();
			modules.put(ArchLoggerModule.class, logger);
		}
		logger.setWriter(writerClass);
	}
	
	protected void configArchiveLogger(String filename) {
		ArchLoggerModule logger = (ArchLoggerModule) modules.get(ArchLoggerModule.class);
		if (logger == null) {
			logger = new ArchLoggerModule();
			modules.put(ArchLoggerModule.class, logger);
		}
		logger.setArchiveFilename(filename);
	}

	protected String getArchiveFilename() {
		ArchLoggerModule logger = (ArchLoggerModule) modules.get(ArchLoggerModule.class);
		if (logger == null) {
			logger = new ArchLoggerModule();
			modules.put(ArchLoggerModule.class, logger);
		}
		return logger.getArchiveFilename();
	}
	
	protected void configOptimumLogger(String filename) {
		ArchLoggerModule logger = (ArchLoggerModule) modules.get(ArchLoggerModule.class);
		if (logger == null) {
			logger = new ArchLoggerModule();
			modules.put(ArchLoggerModule.class, logger);
		}
		logger.setOptimumFilename(filename);
	}
	
	protected String getOptimumFilename() {
		ArchLoggerModule logger = (ArchLoggerModule) modules.get(ArchLoggerModule.class);
		if (logger == null) {
			logger = new ArchLoggerModule();
			modules.put(ArchLoggerModule.class, logger);
		}
		return logger.getOptimumFilename();
	}
	
	protected void configViewer() {
		configViewer("AQOSA Optimization Viewer");
	}
	
	protected void configViewer(String title) {
		AQOSAViewerModule viewer = (AQOSAViewerModule) modules.get(ViewerModule.class);
		if (viewer == null) {
			viewer =  new AQOSAViewerModule();
			modules.put(ViewerModule.class, viewer);
		}
		viewer.setTitle(title);
	}

	public Collection<Module> getModules() {
		return modules.values();
	}

}
