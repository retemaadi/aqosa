package edu.leiden.aqosa.launcher;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public abstract class BulkRun extends AbstractRun {

	protected List<Integer> runNumbers;
	protected final String logDir;
	protected final String archiveFilename;
	protected final String optimumFilename;
	
	protected static List<Integer> createSimpleList(int noRuns) {
		List<Integer> runNumbers = new ArrayList<Integer>();
		for (int i=1; i<=noRuns; i++) {
			runNumbers.add(i);
		}
		
		return runNumbers;
	}
	
	public BulkRun(int noRuns, InputStream inputModelStream, String logDir, String archiveFilename, String optimumFilename) throws IOException {
		this(createSimpleList(noRuns), inputModelStream, logDir, archiveFilename, optimumFilename);
	}
	
	protected BulkRun(List<Integer> runNumbers, InputStream inputModelStream, String logDir, String archiveFilename, String optimumFilename) throws IOException {
		super(inputModelStream);
		this.runNumbers = runNumbers;
		this.logDir = logDir;
		this.archiveFilename = archiveFilename;
		this.optimumFilename = optimumFilename;
	}
	
	@Override
	public void setCompleter(CompleterType type) {
		super.setCompleter(type);
	}
	
	protected void setArchiveLogger(Integer runNo) {
		super.setArchiveLogger(logDir + File.separator + archiveFilename + runNo + ".tsv");
	}
	
	protected void setArchiveLogger(String folder, Integer runNo) {
		super.setArchiveLogger(logDir + File.separator + folder + File.separator + archiveFilename + runNo + ".tsv");
	}
	
	protected void setOptimumLogger(Integer runNo) {
		super.setOptimumLogger(logDir + File.separator + optimumFilename + runNo + ".tsv");
	}
	
	protected void setOptimumLogger(String folder, Integer runNo) {
		super.setOptimumLogger(logDir + File.separator + folder + File.separator + optimumFilename + runNo + ".tsv");
	}
	
}
