package edu.leiden.aqosa.launcher;

public enum CompleterType {
	SEQUENTIAL,
	
	MULTITHREAD,
	
	HADOOP,
	
	AKKA
}
