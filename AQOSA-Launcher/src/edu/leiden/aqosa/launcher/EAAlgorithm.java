package edu.leiden.aqosa.launcher;

public enum EAAlgorithm {
	NSGA2,
	
	SPEA2,
	
	SMS,
	
	DistributedSMS
}
