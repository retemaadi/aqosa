package edu.leiden.aqosa.launcher;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.opt4j.core.optimizer.OptimizerStateListener;

import edu.leiden.aqosa.commonality.CommonalityAnalyzer;
import edu.leiden.aqosa.logger.FrontCollector;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.model.Configuration;
import edu.leiden.aqosa.model.FeatureRoot;
import edu.leiden.aqosa.model.Features;
import edu.leiden.aqosa.solution.Individuals;

public class FeatureRun extends BulkRun {

	protected final Features features;
	protected final List<Integer> featureNumbers;
//	protected int delta = 9;		// DEFAULT DELTA
	protected int omega = 3;		// DEFAULT OMEGA
	
	protected int deltaLower = 2;
	protected int deltaUpper = 7;
	
//	public FeatureRun(String featureFile, InputStream inputModelStream, String logDir, String archiveFilename, String optimumFilename) throws IOException {
//		this(null, featureFile, inputModelStream, logDir, archiveFilename, optimumFilename);
//		runNumbers = createSimpleList(features.getConfigurations().size());
//	}
	
//	public FeatureRun(int noRuns, String featureFile, InputStream inputModelStream, String logDir, String archiveFilename, String optimumFilename) throws IOException {
//		this(createSimpleList(noRuns), featureFile, inputModelStream, logDir, archiveFilename, optimumFilename);
//	}
	
	public FeatureRun(int noRuns, List<Integer> featureNumbers, String featureFile, InputStream inputModelStream, String logDir, String archiveFilename, String optimumFilename) throws IOException {
		super(createSimpleList(noRuns), inputModelStream, logDir, archiveFilename, optimumFilename);
		features = new Features(new FeatureRoot(featureFile), getArchModel().getAQOSAModel(), getArchModel().getRepository());
		this.featureNumbers = featureNumbers;
		Class<? extends OptimizerStateListener> stateListenerClass = FrontCollector.class;
		super.addOptimizerStateListener(stateListenerClass);
		
	}
	
//	public FeatureRun(List<Configuration> confs, int noRuns, String featureFile, InputStream inputModelStream, String logDir, String archiveFilename, String optimumFilename) throws IOException {
//		super(0, inputModelStream, logDir, archiveFilename, optimumFilename);
//		//this(configs2Run(confs), featureFile, inputModelStream, logDir, archiveFilename, optimumFilename);
//		init(featureFile, configs2Run(confs) );
//	}

	/*
	private void init(String featureFile, List<Integer> runNumbers) {
		this.archModel 		= getArchModel();
		FeatureRoot root	= new FeatureRoot(featureFile);
		features 		 	= new Features(root, archModel.getAQOSAModel(), archModel.getRepository());
		this.runNumbers  	= runNumbers;
	}
	*/
	
	/*
	public static List<Integer> configs2Run(List<Configuration> configurations) {
		List<Integer> ret = new ArrayList<Integer>();
		
		for (Configuration c : configurations) {
			for (int i = 0; i < features.getConfigurations().size(); i++ ) {
				Configuration allConf = features.getConfigurations().get(i);
				if (c.equals(allConf)) {
					ret.add(i);
					break;
				}
			}
		}
		return ret;
	}
	*/
	
	@Override
	protected void run() throws Exception {
		for (Integer run : runNumbers) {
			FrontCollector.reset();
			
			String foldername = "run" + run;
			File folder = new File(logDir + File.separator + foldername);
			if (!folder.exists()) {
				folder.mkdir();
			}
			
			System.out.println(" ***** " + foldername + " ***** ");
			
			ArrayList<Individuals> fronts = new ArrayList<Individuals>();
			
	//		final String filename = "fronts.ser";
	//		File file = new File(filename);
	//		if (file.exists()) {
	//			fronts = BinaryIO.read(filename);
	//		} else {
	//			fronts = (ArrayList<Individuals>) FrontCollector.getFronts();
	//			BinaryIO.write(fronts, filename);
	//		}
			
			for (Integer feature : featureNumbers) {
				if (archiveFilename != null)
					super.setArchiveLogger(foldername, feature);
				if (optimumFilename != null)
					super.setOptimumLogger(foldername, feature);
				
				Configuration configuration = features.getConfigurations().get(feature);
				super.setConfiguration(configuration);
				
				oneRun();
			}
				
			System.out.println("=================================================");
			
			fronts = (ArrayList<Individuals>) FrontCollector.getFronts();
			
			CommonalityAnalyzer analyzer = new CommonalityAnalyzer(fronts);
			//System.out.println(analyzer.analyze(0, delta, omega, false));

			PrintWriter writer = new PrintWriter(logDir + File.separator + foldername + File.separator + "delta.tsv");
			for (int fr=0; fr<fronts.size(); fr++)
				for (int del=deltaLower; del<=deltaUpper; del++) {
					int count = analyzer.analyze(fr, del, omega, false);
					System.out.println("Front " + fr + ", Delta " + del + ": " + count);
					writer.println(fr + "\t" + del + "\t" + count);
			}
			writer.flush();
			writer.close();
			
			System.out.println("=================================================");
		}
	}

	@Override
	public void setAlgorithm(EAAlgorithm algo) {
		super.setAlgorithm(algo);
	}
	
	@Override
	public void setMatingAppraoch(MatingApproach appr) {
		super.setMatingAppraoch(appr);
	}
	
	@Override
	public ArchModel getArchModel() {
		return super.getArchModel();
	}
	
	public Features getFeatures() {
		return features;
	}
	
}

/*
class BinaryIO {
	
	public static void write(ArrayList<Individuals> fronts, String filename) {
		
		PrintWriter pw;
		try {
			pw = new PrintWriter(filename);
			
			byte[] data = (byte[]) SerializationUtils.serialize(fronts);
			
			pw.print(data);
			pw.flush();
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<Individuals> read(String filename) {
		ArrayList<Individuals> archive = new ArrayList<Individuals>();
		FileReader fr = null;
		try { 
			File file = new File(filename);
			int len = (int) file.length();
			
			char[] cbuf = new char[len];
			fr = new FileReader(file);
			fr.read(cbuf);
			
			byte[] data = new byte[cbuf.length];
			for (int i=0; i<cbuf.length; i++) {
				data[i] = (byte) cbuf[i];
			}
			
			
			archive = (ArrayList<Individuals>) SerializationUtils.deserialize(data);
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fr != null) fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return archive;
	}
}
*/