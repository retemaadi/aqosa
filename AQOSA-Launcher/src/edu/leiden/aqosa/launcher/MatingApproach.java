package edu.leiden.aqosa.launcher;

import org.opt4j.core.config.annotations.Info;

/** Mating approach. */
public enum MatingApproach {		
	/**
	 * Mating without any heuristic-based operator and with only Mutate.
	 * 
	 * @see edu.leiden.aqosa.mating.MutateMating
	 */
	@Info("Mating without any heuristic-based operator and with only Mutate.")
	ONLY_MUTATE("onlymutate"),

	/**
	 * Mating without any heuristic-based operator and with Crossover and Mutate.
	 * 
	 * @see edu.leiden.aqosa.mating.CrossoverMutateMating
	 */
	@Info("Mating without any heuristic-based operator and with Crossover and Mutate.")
	CROSSOVER_MUTATE("crossovermutate"),

	/**
	 * Mating with random order of heuristic-based operators.
	 * 
	 * @see edu.leiden.aqosa.mating.RandomMating
	 */
	@Info("Mating with random order of heuristic-based operators.")
	RANDOM("random"),

	/**
	 * Mating with sequential order of heuristic-based operators.
	 * 
	 * @see edu.leiden.aqosa.mating.SequentialMating
	 */
	@Info("Mating with sequential order of heuristic-based operators.")
	SEQUENTIAL("sequential"),

	/**
	 * Mating with one parent random and one parent sequential order of heuristic-based operators.
	 * 
	 * @see edu.leiden.aqosa.mating.RandomSequentialMating
	 */
	@Info("Mating with one parent random and one parent sequential order of heuristic-based operators.")
	HALFRANDOM_HALFSEQUENTIAL("randomsequential"),

	/**
	 * Mating with one parent random order of heuristic-based operators and one parent mutate only.
	 * 
	 * @see edu.leiden.aqosa.mating.RandomMutateMating
	 */
	@Info("Mating with one parent random order of heuristic-based operators and one parent mutate only.")
	HALFRANDOM_HALFMUTATE("halfrandom"),

	/**
	 * Mating with one parent sequential order of heuristic-based operators and one parent mutate only.
	 * 
	 * @see edu.leiden.aqosa.mating.SequentialMutateMating
	 */
	@Info("Mating with one parent sequential order of heuristic-based operators and one parent mutate only.")
	HALFSEQUENTIAL_HALFMUTATE("halfsequential"),

	/**
	 * Mating with one parent mix of random/sequential order of heuristic-based operators and one parent mutate only.
	 * 
	 * @see edu.leiden.aqosa.mating.RandomSequentialMutateMating
	 */
	@Info("Mating with one parent mix of random/sequential order of heuristic-based operators and one parent mutate only.")
	HALFRANDOMSEQUENTIAL_HALFMUTATE("halfrandomsequential"),

	/**
	 * Default mating approach.
	 * 
	 */
	@Info("Default mating approach.")
	DEFAULT("default");
	
	protected final String text;
	
	MatingApproach(String text) {
		this.text = text;
	}
	
	@Override
	public String toString() {
		return this.text;
	}
}
