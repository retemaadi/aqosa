package edu.leiden.aqosa.launcher;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MultipleAlgoRun extends BulkRun {

	public MultipleAlgoRun(int noRuns, InputStream inputModelStream, String logDir, String archiveFilename, String optimumFilename) throws IOException {
		super(noRuns, inputModelStream, logDir, archiveFilename, optimumFilename);
	}
	
	public MultipleAlgoRun(List<Integer> runNumbers, InputStream inputModelStream, String logDir, String archiveFilename, String optimumFilename) throws IOException {
		super(runNumbers, inputModelStream, logDir, archiveFilename, optimumFilename);
	}
	
	@Override
	protected void run() throws Exception {
		for (EAAlgorithm algo : EAAlgorithm.values()) {
			for (Integer run : runNumbers) {
				super.setAlgorithm(algo);
				if (archiveFilename != null)
					super.setArchiveLogger(algo.toString(), run);
				if (optimumFilename != null)
					super.setOptimumLogger(algo.toString(), run);
				
				oneRun();
			}
		}
	}
	
	@Override
	public void setMatingAppraoch(MatingApproach appr) {
		super.setMatingAppraoch(appr);
	}
}
