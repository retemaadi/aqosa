package edu.leiden.aqosa.launcher;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MultipleApprRun extends BulkRun {

	public MultipleApprRun(int noRuns, InputStream inputModelStream, String logDir, String archiveFilename, String optimumFilename) throws IOException {
		super(noRuns, inputModelStream, logDir, archiveFilename, optimumFilename);
	}
	
	public MultipleApprRun(List<Integer> runNumbers, InputStream inputModelStream, String logDir, String archiveFilename, String optimumFilename) throws IOException {
		super(runNumbers, inputModelStream, logDir, archiveFilename, optimumFilename);
	}
	
	@Override
	protected void run() throws Exception {
		for (MatingApproach appr : MatingApproach.values()) {
			if (appr == MatingApproach.DEFAULT)
				continue;
			
			for (Integer run : runNumbers) {
				super.setMatingAppraoch(appr);
				if (archiveFilename != null)
					super.setArchiveLogger(appr.toString(), run);
				if (optimumFilename != null)
					super.setOptimumLogger(appr.toString(), run);
				
				oneRun();
			}
		}
	}
	
	@Override
	public void setAlgorithm(EAAlgorithm algo) {
		super.setAlgorithm(algo);
	}
	
}
