package edu.leiden.aqosa.launcher;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MultipleRun extends BulkRun {

	public MultipleRun(int noRuns, InputStream inputModelStream, String logDir, String archiveFilename, String optimumFilename) throws IOException {
		super(noRuns, inputModelStream, logDir, archiveFilename, optimumFilename);
	}
	
	public MultipleRun(List<Integer> runNumbers, InputStream inputModelStream, String logDir, String archiveFilename, String optimumFilename) throws IOException {
		super(runNumbers, inputModelStream, logDir, archiveFilename, optimumFilename);
	}
	
	@Override
	protected void run() throws Exception {
		for (Integer run : runNumbers) {
			if (archiveFilename != null)
				super.setArchiveLogger(run);
			if (optimumFilename != null)
				super.setOptimumLogger(run);
			
			oneRun();
		}
	}
	
	@Override
	public void setAlgorithm(EAAlgorithm algo) {
		super.setAlgorithm(algo);
	}
	
	@Override
	public void setMatingAppraoch(MatingApproach appr) {
		super.setMatingAppraoch(appr);
	}
	
}
