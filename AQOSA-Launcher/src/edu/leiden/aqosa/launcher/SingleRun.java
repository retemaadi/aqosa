package edu.leiden.aqosa.launcher;

import java.io.IOException;
import java.io.InputStream;


public class SingleRun extends AbstractRun {

	public SingleRun(InputStream inputModelStream) throws IOException {
		super(inputModelStream);
	}
	
	@Override
	protected void run() throws Exception {
		oneRun();
	}
	
	@Override
	public void setAlgorithm(EAAlgorithm algo) {
		super.setAlgorithm(algo);
	}
	
	@Override
	public void setCompleter(CompleterType type) {
		super.setCompleter(type);
	}
	
	@Override
	public void setMatingAppraoch(MatingApproach appr) {
		super.setMatingAppraoch(appr);
	}
	
	@Override
	public void setArchiveLogger(String archiveFilename) {
		super.setArchiveLogger(archiveFilename);
	}
	
	@Override
	public void setOptimumLogger(String optimumFilename) {
		super.setOptimumLogger(optimumFilename);
	}
	
}
