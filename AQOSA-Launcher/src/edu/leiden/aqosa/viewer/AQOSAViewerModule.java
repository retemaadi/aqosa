package edu.leiden.aqosa.viewer;

import org.opt4j.viewer.ViewerModule;

public class AQOSAViewerModule extends ViewerModule {

	@Override
	public void config() {
		super.config();
		addIndividualMouseListener(ArchVisualizer.class);
	}
}
