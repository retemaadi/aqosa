package edu.leiden.aqosa.viewer;

import java.awt.Component;
import java.awt.Point;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.opt4j.core.Individual;
import org.opt4j.viewer.IndividualMouseListener;
import org.opt4j.viewer.Viewport;

import com.google.inject.Inject;

import edu.leiden.aqosa.solution.ArchSolution;

public class ArchVisualizer implements IndividualMouseListener {

	protected final Viewport viewport;
	
	@Inject
	public ArchVisualizer(Viewport viewport) {
		this.viewport = viewport;
	}
	
	@Override
	public void onPopup(Individual individual, Component component, Point p, JPopupMenu menu) {
		JMenuItem menuItem = new JMenuItem("View Architecture");
		menuItem.addActionListener(new GraphShow((ArchSolution) individual.getPhenotype(), viewport));
		menu.add(menuItem);
	}

	@Override
	public void onDoubleClick(Individual individual, Component component, Point p) {
		GraphShow graphShow = new GraphShow((ArchSolution) individual.getPhenotype(), viewport);
		graphShow.show();
	}

}
