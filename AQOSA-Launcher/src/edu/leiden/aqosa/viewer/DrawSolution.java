package edu.leiden.aqosa.viewer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.util.List;

import javax.swing.JFrame;

import org.apache.commons.collections15.Transformer;
import org.eclipse.emf.ecore.EObject;

import edu.leiden.aqosa.model.IR.Bus;
import edu.leiden.aqosa.model.IR.Processor;
import edu.leiden.aqosa.solution.ArchSolution;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.PickableVertexPaintTransformer;
import edu.uci.ics.jung.visualization.picking.MultiPickedState;
import edu.uci.ics.jung.visualization.picking.PickedState;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;

public class DrawSolution {

	public static void draw(ArchSolution solution) {
		Graph<EObject, String> graph = new UndirectedSparseGraph<EObject, String>();
		List<Processor> processors = solution.getProcessors();
		List<Bus> buses = solution.getBuses();
		
		for (Processor proc : processors) {
			graph.addVertex(proc);
		}
		
		for (Bus bus : buses) {
			graph.addVertex(bus);
		}

		int j = 0;
		for (List<Boolean> bus2node : solution.getBus2Nodes()) {
			int i = 0;
			for (Boolean conn : bus2node) {
				if (conn) {
					Bus b = buses.get(j);
					Processor p = processors.get(i);
					graph.addEdge(j + ": " + b.getId() + " ---> " + i + " (" + p.getId() + ")", b, p);
				}
				i++;
			}
			j++;
		}
		
		// The Layout<V, E> is parameterized by the vertex and edge types
		Layout<EObject, String> layout = new CircleLayout<EObject, String>(graph);
		layout.setSize(new Dimension(1024, 768)); // sets the initial size of the space
		
		// The BasicVisualizationServer<V,E> is parameterized by the edge types
		VisualizationViewer<EObject, String> vv = new VisualizationViewer<EObject, String>(layout);
		vv.setPreferredSize(new Dimension(1280, 960)); // Sets the viewing area size
		Transformer<EObject, String> transformer = new Transformer<EObject, String>() {
			@Override
			public String transform(EObject vertex) {
				if (vertex instanceof Bus) {
					Bus b = (Bus) vertex;
					return b.getId();
				} else if (vertex instanceof Processor) {
					Processor p = (Processor) vertex;
					return p.getId();
				}
				return vertex.toString();
			}
		};
		
		DefaultModalGraphMouse<EObject, String> graphMouse = new DefaultModalGraphMouse<EObject, String>();
		vv.setGraphMouse(graphMouse);
		graphMouse.setMode(ModalGraphMouse.Mode.PICKING);

		PickedState<EObject> pvs = new MultiPickedState<EObject>();
		vv.setPickedVertexState(pvs);
		
		vv.getRenderContext().setVertexLabelTransformer(transformer);
		vv.getRenderer().getVertexLabelRenderer().setPosition(Position.CNTR);
		
		vv.getRenderContext().setVertexFillPaintTransformer(new PickableVertexPaintTransformer<EObject>(pvs, Color.BLACK, Color.YELLOW) {
			@Override
			public Paint transform(EObject vertex) {
				if (super.pi.isPicked(vertex))
					return super.transform(vertex);
				
				if (vertex instanceof Bus) {
					return new Color(224, 96, 224);
				} else {
					return new Color(96, 224, 224);
				}
			}
		});
		
		vv.getRenderContext().setVertexShapeTransformer(new Transformer<EObject, Shape>() {
			@Override
			public Shape transform(EObject vertex) {
				if (vertex instanceof Bus) {
					return new Ellipse2D.Double(-30, -30, 120, 120);
				} else {
					return new Rectangle(-25, -25, 100, 100);
				}
			}
		});
		
		vv.getRenderContext().setEdgeDrawPaintTransformer(new Transformer<String, Paint>() {
			@Override
			public Paint transform(String edge) {
				return Color.BLUE;
			}
		});
		
		JFrame frame = new JFrame("AQOSA Solutuion Representation for Genotype: " + solution.toString());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocation(200, 100);
		frame.getContentPane().add(vv);
		frame.pack();
		frame.setVisible(true);
	}
}
