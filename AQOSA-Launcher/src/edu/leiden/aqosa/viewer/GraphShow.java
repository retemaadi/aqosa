package edu.leiden.aqosa.viewer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.util.List;

import javax.swing.JPanel;

import org.apache.commons.collections15.Transformer;
import org.eclipse.emf.ecore.EObject;
import org.opt4j.viewer.Viewport;
import org.opt4j.viewer.Widget;

import edu.leiden.aqosa.model.IR.Bus;
import edu.leiden.aqosa.model.IR.Processor;
import edu.leiden.aqosa.solution.ArchSolution;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.PickableVertexPaintTransformer;
import edu.uci.ics.jung.visualization.picking.MultiPickedState;
import edu.uci.ics.jung.visualization.picking.PickedState;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;

public class GraphShow implements ActionListener, Widget {

	protected final ArchSolution solution;
	protected final Viewport viewport;
	protected final JPanel panel;

	public GraphShow(ArchSolution solution, Viewport viewport) {
		this.solution = solution;
		this.viewport = viewport;
		panel = new JPanel();
	}
	
	@Override
	public JPanel getPanel() {
		return panel;
	}

	@Override
	public synchronized void init(Viewport viewport) {
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		show();
	}
	
	protected void show() {
		Graph<String, String> graph = new UndirectedSparseGraph<String, String>();
		List<Processor> processors = solution.getProcessors();
		List<Bus> buses = solution.getBuses();
		
		int n = 0;
		for (Processor proc : processors) {
			graph.addVertex("Node" + (++n) + ": " + proc.getId());
		}
		
		int b = 0;
		for (Bus bus : buses) {
			graph.addVertex("Bus" + (++b) + ": " + bus.getId());
		}

		int j = 0;
		for (List<Boolean> bus2node : solution.getBus2Nodes()) {
			int i = 0;
			for (Boolean conn : bus2node) {
				if (conn) {
					//Bus bus = buses.get(j);
					//Processor proc = processors.get(i);
					//graph.addEdge(j + ": " + bus.getId() + " ---> " + i + " (" + proc.getId() + ")", bus.getId(), proc.getId());
					String nodeTitle = "Node" + (i+1) + ": " + processors.get(i).getId();
					String busTitle = "Bus" + (j+1) + ": " + buses.get(j).getId();
					graph.addEdge("From " + nodeTitle + " --> To " + busTitle, nodeTitle, busTitle);
				}
				i++;
			}
			j++;
		}
		
		// The Layout<V, E> is parameterized by the vertex and edge types
		Layout<String, String> layout = new CircleLayout<String, String>(graph);
		layout.setSize(new Dimension(1024, 768)); // sets the initial size of the space
		
		// The BasicVisualizationServer<V,E> is parameterized by the edge types
		VisualizationViewer<String, String> vv = new VisualizationViewer<String, String>(layout);
		vv.setPreferredSize(new Dimension(1280, 960)); // Sets the viewing area size
		Transformer<String, String> transformer = new Transformer<String, String>() {
			@Override
			public String transform(String vertex) {
				return vertex;
				/*
				if (vertex.startsWith("Bus")) {
					return vertex.substring(0, 6);
				} else {
					return vertex.substring(0, 7);
				}
				*/
			}
		};
		
		DefaultModalGraphMouse<EObject, String> graphMouse = new DefaultModalGraphMouse<EObject, String>();
		vv.setGraphMouse(graphMouse);
		graphMouse.setMode(ModalGraphMouse.Mode.PICKING);

		PickedState<String> pvs = new MultiPickedState<String>();
		vv.setPickedVertexState(pvs);
		
		vv.getRenderContext().setVertexLabelTransformer(transformer);
		vv.getRenderer().getVertexLabelRenderer().setPosition(Position.CNTR);
		
		vv.getRenderContext().setVertexFillPaintTransformer(new PickableVertexPaintTransformer<String>(pvs, Color.BLACK, Color.YELLOW) {
			@Override
			public Paint transform(String vertex) {
				if (super.pi.isPicked(vertex))
					return super.transform(vertex);
				
				//if (vertex instanceof Bus) {
				if (vertex.startsWith("Bus")) {
					return new Color(224, 96, 224);
				} else {
					return new Color(96, 224, 224);
				}
			}
		});
		
		vv.getRenderContext().setVertexShapeTransformer(new Transformer<String, Shape>() {
			@Override
			public Shape transform(String vertex) {
				//if (vertex instanceof Bus) {
				if (vertex.startsWith("Bus")) {
					return new Ellipse2D.Double(-30, -30, 120, 120);
				} else {
					return new Rectangle(-25, -25, 100, 100);
				}
			}
		});
		
		vv.getRenderContext().setEdgeDrawPaintTransformer(new Transformer<String, Paint>() {
			@Override
			public Paint transform(String edge) {
				return Color.BLUE;
			}
		});

		panel.add(vv, BorderLayout.CENTER);
		viewport.addWidget(this);
	}
	
}
