package edu.leiden.aqosa.featureModel;

import java.util.ArrayList;
import java.util.HashSet;

import edu.mit.csail.sdg.alloy4.A4Reporter;
import edu.mit.csail.sdg.alloy4.Computer;
import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4.ErrorWarning;
import edu.mit.csail.sdg.alloy4compiler.ast.Command;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprVar;
import edu.mit.csail.sdg.alloy4compiler.ast.Module;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.parser.CompUtil;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Options;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Solution;
import edu.mit.csail.sdg.alloy4compiler.translator.TranslateAlloyToKodkod;
import edu.mit.csail.sdg.alloy4viz.VizGUI;

public class FMConfigs {

	protected final HashSet<String> featuresIDs = new HashSet<String>();						// IDs of all Alloy Features
	protected final ArrayList<ArrayList<String>> configs = new ArrayList<ArrayList<String>>();	// All lists of feature configurations

	public FMConfigs(String filename) {
		parseAlloyFile(filename, false);
	}

	public HashSet<String> getFeaturesIDs() {
		return featuresIDs;
	}
	
	public ArrayList<ArrayList<String>> getConfigs() {
		return configs;
	}
	
	/**
	 * This method parses the specified Alloy file, which should contain the
	 * Feature Model (FM). This file should be preferably created by the Clafer
	 * compiler, after compiling a clafer file containing the FM. <br>
	 * The method also "solves" the file by finding all the different solutions
	 * / configurations of the FM and stores the IDs of all the Features found
	 * and all configurations.
	 * 
	 * @param filepath
	 *            : The path to the the specified Alloy file.
	 * @param visualizeLastSolution
	 *            : a flag indicating whether to open (true) the Alloy
	 *            visualizer for the last configuration or not (false)
	 */
	private void parseAlloyFile(String filename, boolean visualizeLastSolution) {

		// For parsing alloy files we need an A4Reporter object
		// and an A4Options object, which holds the solver for our Feature
		// Model.
		A4Reporter reporter = new A4Reporter() {
			public void warning(ErrorWarning msg) {
				System.out.print("Relevance Warning:\n"	+ (msg.toString().trim()) + "\n\n");
				System.out.flush();
			}
		};
		A4Options options = new A4Options();
		options.solver = A4Options.SatSolver.SAT4J;

		try {
			// Parse Alloy file
			Module world = CompUtil.parseEverything_fromFile(reporter, null, filename);

			// Populate the allAlloyFeaturesIDs set
			gatherAllFeatsIDs(world);

			// Solve Alloy file
			for (Command command : world.getAllCommands()) {
				// Execute the command in the file
				System.out.println("============ Command " + command + ": ============");
				A4Solution ans = TranslateAlloyToKodkod.execute_command(reporter, world.getAllReachableSigs(), command, options);

				// Iterate over all satisfiable solutions
				while (ans != null && ans.satisfiable()) {
					// System.out.println("-- Printing all atoms:");

					// Store the current feature model configuration
					ArrayList<String> configFeaturesIDs = new ArrayList<String>();

					for (ExprVar var : ans.getAllAtoms()) {
						// Remove unnecessary chars
						String id = var.toString();
						id = id.substring(id.indexOf('_') + 1,id.lastIndexOf('$'));
						configFeaturesIDs.add(id); // Store the ID of feature in the configuration list
					}

					// Add current configuration list to the all configurations list
					configs.add(configFeaturesIDs);

					//System.out.println("-- Printing solution: ");
					//System.out.println(ans.toString());
					//System.out.println("Solution satisfiable? : " + ans.satisfiable());

					// Store current solution to a temporary xml file
					ans.writeXML("temp/@@tempA4Solution.xml"); // may speed up code if removed but needed for visualization.
					ans = ans.next();
				}
			} // end for loop

			if (visualizeLastSolution) {
				new VizGUI(false, "temp/@@tempA4Solution.xml", null, enumerator, null);
			}
		} catch (Err e) {
			e.printStackTrace();
		}

		// ExampleUsingTheCompiler.main(arg);
		// SimpleCLI.main(arg);
		// SimpleGUI.main(args);
	}
	
	/**
	 * Stores the IDs of all the Alloy features, in the allFeatures list. If
	 * error occurs then the function stops and returns.
	 * 
	 * @param world
	 *            : A Module object that comes from parsing the Alloy file.
	 * @return false if an error has occurred, otherwise true.
	 */
	private boolean gatherAllFeatsIDs(Module world) {
		boolean ret = true;

		for (Sig s : world.getAllSigs()) {
			try {
				System.out.println("Sig: " + s);

				String id = s.toString();
				id = id.substring(id.indexOf('_') + 1);
				featuresIDs.add(id);
			} catch (Exception e) {
				e.printStackTrace();
				System.out
						.println("Problem while gathering IDs of all features. Terminating function.");
				ret = false;
				break;
			}
		}

		return ret;
	}

	/** This object performs solution enumeration. */
	private final Computer enumerator = new Computer() {
		public String compute(Object input) {
			final String arg = (String) input;
			// SimpleCallback1 cb = new SimpleCallback1(SimpleGUI.this, viz,
			// log, Verbosity.get().ordinal(), latestAlloyVersionName,
			// latestAlloyVersion);
			// SimpleTask2 task = new SimpleTask2();
			// task.filename = arg;
			// try {
			// WorkerEngine.run(task, SubMemory.get(), SubStack.get(),
			// alloyHome() + fs + "binary", "", cb);
			// // task.run(cb);
			// } catch(Throwable ex) {
			// WorkerEngine.stop();
			// log.logBold("Fatal Error: Solver failed due to unknown reason.\n"
			// +
			// "One possible cause is that, in the Options menu, your specified\n"
			// +
			// "memory size is larger than the amount allowed by your OS.\n" +
			// "Also, please make sure \"java\" is in your program path.\n");
			// log.logDivider();
			// log.flush();
			// doStop(2);
			// return arg;
			// }
			return arg;
		}
	};

}
