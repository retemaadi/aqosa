/**
 * Last Update: 1/7/2013
 */
package edu.leiden.aqosa.featureModel;

import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.BasicEList;

import edu.leiden.aqosa.model.IR.AQOSAModel;
import edu.leiden.aqosa.model.IR.Component;
import edu.leiden.aqosa.model.IR.Feature;

/**
 * @author Vasilis Karagiorgis
 */
public class FeatureMapper {
	
	protected final AQOSAModel model;
	protected final FMConfigs configs;
	
	protected final EList<Feature> aqosaFeatures;						  // Stores all the features found in the Aqosa file.
	protected final EList<Feature> aqosaLeafFeatures;					  // Stores the leaf features of the feature tree. These are the ones that actually map to SW components.
	protected final ArrayList<String> aqosaFeaturesIDs;					  // Stores the IDs of the leaf features, found in the Aqosa file.
	protected final HashMap<String, String> alloy2AqosaMap;				  // Format: <Alloy feature ID - Aqosa Feature ID>
	protected final HashMap<String, ArrayList<String>> feats2Components;  // Format: <Alloy feature ID - List of Components' ID>
	
	// Constructor
	public FeatureMapper(AQOSAModel model, FMConfigs configs) {
		this.model 			   = model;
		this.configs 		   = configs;
		this.aqosaFeatures     = getAllAqosaFeatures(model);
		System.out.println("Printing All Features: ("+aqosaFeatures.size()+") "+aqosaFeatures);
		this.aqosaLeafFeatures = getAqosaLeafFeatures(model);
		this.aqosaFeaturesIDs  = getAqosaFeatsIDs();
		System.out.println("Printing Aqosa Leaf Features: "+aqosaFeaturesIDs.size()+" "+aqosaFeaturesIDs.toString());
		this.alloy2AqosaMap    = mapFeatsAlloy2Aqosa(); 
		this.feats2Components  = mapFeats2Components();
	}
	
	// Getters
	public HashMap<String, String> getAlloy2AqosaMap() {
		return alloy2AqosaMap;
	}
	
	public HashMap<String, ArrayList<String>> getFeats2Components() {
		return feats2Components;
	}
	
	// Methods
	private EList<Feature> getAqosaLeafFeatures(AQOSAModel model) {
		EList<Feature> ret = new BasicEList<Feature>();
		
		for(Feature f : model.getFeatureModel().getFeatures()) {
			getLeafFeaturesPreOrder(f, ret);
		}
		return ret;
	}

	private EList<Feature> getAllAqosaFeatures(AQOSAModel model) {
		EList<Feature> ret = new BasicEList<Feature>();
		
		for(Feature f : model.getFeatureModel().getFeatures()) {
			getFeaturesPreOrder(f, ret);
		}
		return ret;
	}
	
	/*
	  preorder(node)
		if node == null then return
		visit(node)
		preorder(node.left) 
		preorder(node.right) 
	 */
	private EList<Feature> getFeaturesPreOrder(Feature f, EList<Feature> list) {
		list.add(f);							// Add feature to list
		for (Feature child : f.getFeatures()) {	// Visit Children
			getFeaturesPreOrder(child, list);
		}
		return list;
	}
	
	private EList<Feature> getLeafFeaturesPreOrder(Feature f, EList<Feature> list) {
		if (f.getFeatures().size() == 0) {	// If it's leaf feature (has no children), add to list
			list.add(f);
		}
		else {
			for (Feature child : f.getFeatures()) {	// Visit the children
				getLeafFeaturesPreOrder(child, list);
			}
		}
		
		return list;
	}
	
	/**
	 * Helper method to gather all the leaf features' IDs from the AQOSA model.
	 * @return A list with the string IDs of the features.
	 */
	private ArrayList<String> getAqosaFeatsIDs() {
		
		ArrayList<String> l = new ArrayList<String>();
		
		for (Feature f : aqosaLeafFeatures) {
			l.add(f.getName());
		}
		
		return l;
	}
	
	/**
	 * This method maps the features, which are found in the Alloy file for feature model 
	 * and enumerated by the FMConfigs class, with the features of the AQOSA file. The two 
	 * set of features should match by number and name. Ideally the AQOSA features should 
	 * be named in uppercase.
	 * 
	 * @return A hashmap of the mapped features. Alloy features as Keys and AQOSA features as Values.
	 * @throws Exception :<ol>1) If an Alloy feature don't correspond to an AQOSA feature.</ol>
	 * 				      <ol>2) If Alloy features have not been gathered yet.</ol>
	 */
	private HashMap<String, String> mapFeatsAlloy2Aqosa() {
		
		HashMap<String, String> map = new HashMap<String, String>();
		
		// Check if alloy features have been gathered. Implies that the alloy file parsing has already been done.
		if (configs.getFeaturesIDs() == null || configs.getFeaturesIDs().isEmpty()) {
			throw new IllegalStateException("Alloy features have not been gathered, yet.");
		}
		
		// Map alloy features to aqosa features
		boolean featureFound = false;
		for (String alloyF : configs.getFeaturesIDs()) {
			
			// Find corresponding Aqosa feature
			for (Feature aqosaF : aqosaFeatures) {
				if (alloyF.compareToIgnoreCase(aqosaF.getName().trim()) == 0) {
					featureFound = true;
					map.put(alloyF, aqosaF.getName().trim());
					break;
				}
			}
			
			if (!featureFound) {
				throw new IllegalStateException("Unmatched features between alloy and aqosa files: " + alloyF);
			}
			featureFound = false;
			
		}
		
		return map;
	}
	
	/**
	 * This method creates a map between an Alloy feature name and a list of corresponding components.
	 * For every Alloy feature, it searches the corresponding Aqosa Feature and takes its list of components.
	 * 
	 * @return The map between alloy feature names and corresponding list of component names.
	 */
	private HashMap<String, ArrayList<String>> mapFeats2Components() {
		
		HashMap<String, ArrayList<String>> ret = new HashMap<String, ArrayList<String>>();
		
		for (String alloyFeat : configs.getFeaturesIDs()) {
			
			// if not leaf feature then continue
			if (!aqosaFeaturesIDs.contains(alloyFeat.toUpperCase())) { continue; }
			
			String aqF = alloy2AqosaMap.get(alloyFeat);
			
			// find alloyFeat in aqosa feature ELIST and get the corresponding related components
			for (Feature aqosaFeat : aqosaLeafFeatures) {
				if (aqosaFeat.getName().equalsIgnoreCase(aqF)) {
					ret.put(alloyFeat, getAqosaFeatComponents(aqosaFeat));
				}
			}
		}
		
		return ret;
	}
	
	/**
	 * A method to return a list of the Components' IDs that are related to the given feature EObject.
	 * @param f
	 * 			: The feature object
	 * @return An array list containing the IDs of the components related to the feature.
	 */
	public ArrayList<String> getAqosaFeatComponents(Feature f) {
		ArrayList<String> ret = new ArrayList<String>();
		
		for (Component comp : f.getComponentRelation()){
			ret.add(comp.getName());
		}
		
		return ret;
	}
	
	/**
	 * A method to return a list of the Components' IDs that are related to the given feature ID.
	 * @param feature
	 * 			: The feature ID
	 * @return An array list containing the IDs of the components related to the feature.
	 */
	public ArrayList<String> getComponents(String feature) {
		return feats2Components.get(feature);
	}
	
	/**
	 * From a given feature configuration, return a list of corresponding features' IDs.
	 * @param features
	 * 			: An ArrayList of Strings containing the feature IDs of a feature configuration.
	 * @return An ArrayList of Strings containing the leaf features IDs, corresponding to the given configuration.
	 */
	public ArrayList<String> filterLeafs(ArrayList<String> features) {
		ArrayList<String> leafs = new ArrayList<String>();
		for (String feat : features) {
			String id = alloy2AqosaMap.get(feat);
			if (aqosaFeaturesIDs.contains(id))
				leafs.add(feat);
		}
		
		return leafs;
	}
	
	/**
	 * From a given feature configuration, return a list of corresponding features.
	 * @param config
	 * 			: An ArrayList of Strings containing the feature IDs of a feature configuration.
	 * @return An EList of the actual leaf features EObjects corresponding to the given configuration.
	 */
	public EList<Feature> getAqosaFeaturesFromConfig(ArrayList<String> config) {
		EList<Feature> ret = new BasicEList<Feature>();
		
		for (String id : config) {
			String aqosaFeat = alloy2AqosaMap.get(id);
			for (Feature f : aqosaLeafFeatures) {
				String featName = f.getName().trim();
				if (featName.equalsIgnoreCase(aqosaFeat)) {
					ret.add(f);
				}
			}
		}
		
		return ret;
	}
	
	/**
	 * From a give configuration return all the unique related software components' IDs.
	 * @param config
	 * 			: An ArrayList of Strings containing the feature IDs of a feature configuration.
	 * @return An ArrayList of Strings containing the IDs of the software components.
	 */
	public ArrayList<String> componentsFromConfiguration(ArrayList<String> config) {
		EList<Feature> aqFeatList = getAqosaFeaturesFromConfig(config);
		ArrayList<String> ret     = new ArrayList<String>();
		ArrayList<String> compsFromFeat;
		
		for (Feature f : aqFeatList) {
			compsFromFeat = getAqosaFeatComponents(f);
			for (String comp : compsFromFeat) {
				if (ret.contains(comp)) continue;
				ret.add(comp);
			}
		}
		
		return ret;
	}

}
