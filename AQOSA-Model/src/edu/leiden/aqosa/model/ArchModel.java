package edu.leiden.aqosa.model;

import edu.leiden.aqosa.model.IR.AQOSAModel;

public class ArchModel {

	protected final AQOSAModel model;
	protected final ArchRepository repo;
	protected final ArchScenario scen;
	protected final ArchObjectives objs;
	
	protected Configuration configuration = new NullConfiguration();
	
	public ArchModel(AQOSAModel model) {
		this.model = model;
		repo = new ArchRepository(model);
		scen = new ArchScenario(model);
		objs = new ArchObjectives(model);
	}
	
	public AQOSAModel getAQOSAModel() {
		return model;
	}
	
	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
	
	public Configuration getConfiguration() {
		return configuration;
	}
	
	public ArchScenario getScenario() {
		return scen;
	}
	
	public ArchRepository getRepository() {
		return repo;
	}
	
	public ArchObjectives getObjectives() {
		return objs;
	}
	
}
