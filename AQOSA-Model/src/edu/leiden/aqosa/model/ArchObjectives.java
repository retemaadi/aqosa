package edu.leiden.aqosa.model;

import edu.leiden.aqosa.model.IR.AQOSAModel;
import edu.leiden.aqosa.model.IR.Evaluations;
import edu.leiden.aqosa.model.IR.Settings;

public class ArchObjectives {

	protected Settings settings;
	
	public ArchObjectives(AQOSAModel model) {
		settings = model.getObjectives().getSettings();
	}
	
	public boolean hasPerformance() {
		return (settings.getEvaluations().contains(Evaluations.RESPONSE_TIME) |
				settings.getEvaluations().contains(Evaluations.CPU_UTILIZATION) |
				settings.getEvaluations().contains(Evaluations.BUS_UTILIZATION));
	}
	
	public boolean hasResponseTime() {
		return (settings.getEvaluations().contains(Evaluations.RESPONSE_TIME));
	}
	
	public boolean hasCPUUtilization() {
		return (settings.getEvaluations().contains(Evaluations.CPU_UTILIZATION));
	}
	
	public boolean hasBusUtilization() {
		return (settings.getEvaluations().contains(Evaluations.BUS_UTILIZATION));
	}
	
	public boolean hasSafety() {
		return (settings.getEvaluations().contains(Evaluations.SAFETY));
	}
	
	public boolean hasCost() {
		return (settings.getEvaluations().contains(Evaluations.COST));
	}

	public int getNoRun() {
		return settings.getNoRun();
	}

	public int getNoSampling() {
		return settings.getNoSampling();
	}

	public int getNoDuplicate() {
		return settings.getNoDuplicate();
	}

	public double getMinCost() {
		return settings.getMinCost();
	}
	
	public double getMaxCost() {
		return settings.getMaxCost();
	}
}
