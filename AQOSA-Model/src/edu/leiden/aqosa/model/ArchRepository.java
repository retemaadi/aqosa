package edu.leiden.aqosa.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import edu.leiden.aqosa.model.IR.AQOSAModel;
import edu.leiden.aqosa.model.IR.Bus;
import edu.leiden.aqosa.model.IR.Component;
import edu.leiden.aqosa.model.IR.ComponentInstance;
import edu.leiden.aqosa.model.IR.ExternalPort;
import edu.leiden.aqosa.model.IR.Processor;
import edu.leiden.aqosa.model.IR.Repository;
import edu.leiden.aqosa.model.IR.Service;

public class ArchRepository {

	private Repository repo;
	protected final EList<Component> components;
	protected final Map<Integer, List<String>> instances;
	protected final List<String> cpus;
	protected final Map<String, Processor> procMap;
	protected final List<String> buses;
	protected final Map<String, Bus> busMap;
	protected final Map<String, Double[]> extports;
	
	public ArchRepository(AQOSAModel model) {
		this.repo = model.getRepository();
		this.components = model.getAssembly().getComponent();
		this.instances = readInstances();
		this.cpus = readCPUs();
		this.procMap = createProcMap();
		this.buses = readBuses();
		this.busMap = createBusMap();
		this.extports = readExtports();
	}
	
	protected Map<Integer, List<String>> readInstances() {
		Map<Integer, List<String>> instances = new HashMap<Integer, List<String>>();
		for (int i=0; i<components.size(); i++) {
			Component c = components.get(i);
			instances.put(i, readCompatibleInstances(c));
		}
		return instances;
	}
	
	protected List<String> readCompatibleInstances(Component component) {
		List<String> instances = new ArrayList<String>();
		for (ComponentInstance ci : repo.getComponentinstance()) {
			if (ci.getCompatible().equals(component))
				instances.add(ci.getId());
		}
		
		return instances;
	}
	
	public Component getParent(Service service) {
		for (Component comp : components) {
			for (Service ser : comp.getService()) {
				if (service.equals(ser))
					return comp;
			}
		}
		
		return null;
	}
	
	public List<String> getInstances(int comp) {
		return instances.get(comp);
	}
	
	public ComponentInstance getInstance(String instanceId) {
		for (ComponentInstance ci : repo.getComponentinstance()) {
			if (ci.getId().equals(instanceId))
				return ci;
		}
		
		return null;
	}
	
	protected List<String> readCPUs() {
		List<String> cpus = new ArrayList<String>();
		for (Processor p : repo.getProcessor()) {
			cpus.add(p.getId());
		}
		
		return cpus;
	}
	
	private Map<String, Processor> createProcMap() {
		Map<String, Processor> map = new HashMap<String, Processor>();
		for (Processor p : repo.getProcessor()) {
			map.put(p.getId(), p);
		}
		
		return map;
	}

	public List<String> getCPUs() {
		return cpus;
	}
	
	public Processor getProcessor(String id) {
		return procMap.get(id);
	}
	
	protected List<String> readBuses() {
		List<String> buses = new ArrayList<String>();
		for (Bus b : repo.getBus()) {
			buses.add(b.getId());
		}
		
		return buses;
	}
	
	private Map<String, Bus> createBusMap() {
		Map<String, Bus> map = new HashMap<String, Bus>();
		for (Bus b : repo.getBus()) {
			map.put(b.getId(), b);
		}
		
		return map;
	}

	public List<String> getBuses() {
		return buses;
	}
	
	public Bus getBus(String id) {
		return busMap.get(id);
	}
	
	/*
	public List<Double> getCPUClocks(List<String> processors) {
		List<Double> clocks = new ArrayList<Double>();
		for (String id : processors) {
			for (Processor p : repo.getProcessor()) {
				if (p.getId().equals(id))
					clocks.add(p.getClock());
			}
		}
		
		return clocks;
	}
	
	public List<Double> getCPUPowers(List<String> processors) {
		List<Double> powers = new ArrayList<Double>();
		for (String id : processors) {
			for (Processor p : repo.getProcessor()) {
				if (p.getId().equals(id))
					powers.add(120.0);
			}
		}
		
		return powers;
	}
	
	public List<Double> getCPUCosts(List<String> processors) {
		List<Double> costs = new ArrayList<Double>();
		for (String id : processors) {
			for (Processor p : repo.getProcessor()) {
				if (p.getId().equals(id))
					costs.add(p.getCost());
			}
		}
		
		return costs;
	}
	
	public List<Double[]> getCPUFailures(List<String> processors) {
		List<Double[]> failures = new ArrayList<Double[]>();
		for (String id : processors) {
			for (Processor p : repo.getProcessor()) {
				if (p.getId().equals(id)) {
					Double[] val = new Double[2];
					val[0] = p.getLowerFail();
					val[1] = p.getUpperFail();
					failures.add(val);
				}
			}
		}
		
		return failures;
	}
	
	public List<Double> getBusBandwidths(List<String> buses) {
		List<Double> bandwidths = new ArrayList<Double>();
		for (String id : buses) {
			for (Bus b : repo.getBus()) {
				if (b.getId().equals(id))
					bandwidths.add(b.getBandwidth());
			}
		}
		
		return bandwidths;
	}
	
	public List<Double> getBusDelays(List<String> buses) {
		List<Double> delays = new ArrayList<Double>();
		for (String id : buses) {
			for (Bus b : repo.getBus()) {
				if (b.getId().equals(id))
					delays.add(b.getDelay());
			}
		}
		
		return delays;
	}
	
	public List<Double> getBusCosts(List<String> buses) {
		List<Double> costs = new ArrayList<Double>();
		for (String id : buses) {
			for (Bus b : repo.getBus()) {
				if (b.getId().equals(id))
					costs.add(b.getCost());
			}
		}
		
		return costs;
	}
	*/
	
	protected Map<String, Double[]> readExtports() {
		HashMap<String, Double[]> extports = new HashMap<String, Double[]>();
		for(ExternalPort port : repo.getExternalport()) {
			String id = port.getId();
			Double[] val = new Double[2];
			val[0] = port.getLowerFail();
			val[1] = port.getUpperFail();
			
			extports.put(id, val);
		}
		
		return extports;
	}
	
	public Map<String, Double[]> getExtports() {
		return extports;
	}
	
}
