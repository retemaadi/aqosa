package edu.leiden.aqosa.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import edu.leiden.aqosa.model.IR.AQOSAModel;
import edu.leiden.aqosa.model.IR.Action;
import edu.leiden.aqosa.model.IR.CommunicateAction;
import edu.leiden.aqosa.model.IR.Component;
import edu.leiden.aqosa.model.IR.ComponentInstance;
import edu.leiden.aqosa.model.IR.ComputeAction;
import edu.leiden.aqosa.model.IR.Flow;
import edu.leiden.aqosa.model.IR.FlowSet;
import edu.leiden.aqosa.model.IR.InPort;
import edu.leiden.aqosa.model.IR.OutPort;
import edu.leiden.aqosa.model.IR.ProvidePort;
import edu.leiden.aqosa.model.IR.Service;
import edu.leiden.aqosa.model.IR.ServiceInstance;

public class ArchScenario {

	protected final AQOSAModel model;
	protected final int noComp;
	protected final List<List<Boolean>> callMatrix;
	
	
	public ArchScenario(AQOSAModel model) {
		this.model = model;
		this.noComp = model.getAssembly().getComponent().size();
		this.callMatrix = calcCallMatrix();
	}
	
	public int getNoComponents() {
		return noComp;
	}
	
	public int getComponentIndex(Component comp) {
		for (int i=0; i<noComp; i++) {
			Component c = model.getAssembly().getComponent().get(i);
			if (comp.equals(c))
				return i;
		}
		
		return -1;
	}
	
	public EList<FlowSet> getFlowSet() {
		return model.getScenarios().getFlowset();
	}
	
	public ComponentInstance getComponentInstance(String compId) {
		for (ComponentInstance ci : model.getRepository().getComponentinstance()) {
			if (ci.getId().equals(compId))
				return ci;
		}
		
		return null;
	}
	
	public ServiceInstance getServiceInstance(String compId, Service service) {
		for (ComponentInstance ci : model.getRepository().getComponentinstance()) {
			if (ci.getId().equals(compId)) {
				for (ServiceInstance si : ci.getService()) {
					if (si.getInstance().equals(service))
						return si;
				}
			}
		}
		
		return null;
	}
	
	public List<OutPort> getSourcePort(InPort inPort) {
		List<OutPort> outPorts = new ArrayList<OutPort>();
		
		for (Flow flow : model.getAssembly().getFlow()) {
			for (Action action : flow.getAction()) {
				if (action instanceof ComputeAction) continue;
				CommunicateAction mAct = (CommunicateAction) action;
				InPort dest = mAct.getDestination();
				if (dest.equals(inPort)) {
					OutPort source = mAct.getSource();
					outPorts.add(source);
				}
			}
		}
		
		return outPorts;
	}
	
	public ServiceInstance getProviderService(String compId, OutPort outPort) {
		for (ComponentInstance ci : model.getRepository().getComponentinstance()) {
			if (! ci.getId().equals(compId)) continue;
			for (ServiceInstance si : ci.getService()) {
				for (ProvidePort port : si.getProvide()) {
					if (port.getConnects().equals(outPort))
						return si;
				}
			}
		}
		
		return null;
	}
	
	private List<List<Boolean>> convertCallMatrix(boolean[][] callMatrix) {
		List<List<Boolean>> matrix = new ArrayList<List<Boolean>>();
		for (int i=0; i<callMatrix.length; i++) {
			List<Boolean> matrixRow = new ArrayList<Boolean>();
			for (int j=0; j<callMatrix[i].length; j++) {
				matrixRow.add(callMatrix[i][j]);
			}
			matrix.add(matrixRow);
		}
		return matrix;
	}
	
	private List<List<Boolean>> calcCallMatrix() {
		boolean[][] callMatrix = new boolean[noComp][noComp];
				
		for (int i=0; i<noComp; i++) {
			for (int j=0; j<noComp; j++) {
				callMatrix[i][j] = false;
			}
		}
		
		for (Flow flow : model.getAssembly().getFlow()) {
			for (Action action : flow.getAction()) {
				if (action instanceof ComputeAction) continue;
				CommunicateAction mAct = (CommunicateAction) action;
				Component src = (Component) mAct.getSource().eContainer();
				Component dst = (Component) mAct.getDestination().eContainer();
				int s = getComponentIndex(src);
				int d = getComponentIndex(dst);
				callMatrix[s][d] = true;
				callMatrix[d][s] = true;
			}
		}
		
		return convertCallMatrix(callMatrix);
	}
	
	public List<List<Boolean>> getCallMatrix() {
		return callMatrix;
	}
	
}
