package edu.leiden.aqosa.model;

import java.util.ArrayList;
import java.util.List;

import edu.leiden.aqosa.model.IR.Action;
import edu.leiden.aqosa.model.IR.CommunicateAction;
import edu.leiden.aqosa.model.IR.Component;
import edu.leiden.aqosa.model.IR.ComputeAction;
import edu.leiden.aqosa.model.IR.Feature;
import edu.leiden.aqosa.model.IR.Flow;

public class Configuration {

	protected final List<Feature> features;
	protected final List<Component> components;
	protected final List<Flow> flows;
	
	protected Configuration() {
		features = null;
		components = null;
		flows = null;
	}
	
	public Configuration(List<String> ids, List<Feature> leafFeatures, List<Flow> allFlows, ArchRepository repo) {
		features = new ArrayList<Feature>();
		components = new ArrayList<Component>();
		flows = new ArrayList<Flow>();
		for (String id : ids) {
			for (Feature feature : leafFeatures) {
				if (feature.getName().equalsIgnoreCase(id)) {
					features.add(feature);
					for (Component comp : feature.getComponentRelation())
						components.add(comp);
					break;
				}
			}
		}
		
		for (Flow flow : allFlows) {
			boolean rejectedFlow = false;
			for (Action act : flow.getAction()) {
				if (act instanceof CommunicateAction)
					continue;
				
				ComputeAction action = (ComputeAction) act;
				Component comp = repo.getParent(action.getService());
				if (!components.contains(comp)) {
					rejectedFlow = true;
					break;
				}
			}
			
			if (!rejectedFlow) {
				flows.add(flow);
			}
		}
	}
	
	public List<Feature> getFeatures() {
		return features;
	}

	public List<Component> getComponents() {
		return components;
	}
	
	public List<Flow> getFlows() {
		return flows;
	}
	
	public boolean isValidComponent(Component c) {
		return components.contains(c);
	}
	
	public boolean isValidFlow(Flow f) {
		return flows.contains(f);
	}
	
}
