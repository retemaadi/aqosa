package edu.leiden.aqosa.model;

import java.io.InputStream;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.parser.CompModule;
import edu.mit.csail.sdg.alloy4compiler.parser.CompUtil;

public class FeatureRoot {

	private CompModule module = null;
	
	public FeatureRoot(String filename) {
		if (filename == null) {
			return;
		}
		
		try {
			module = CompUtil.parseEverything_fromFile(null, null, filename);
		} catch (Err e) {
			e.printStackTrace();
		}
	}
	
	public FeatureRoot(InputStream inputFeaturesStream) {
		throw new IllegalStateException("InputStream for features is not supported yet!");
		/*
		try {
			String content = null;
			int r;
			while ((r = inputFeaturesStream.read()) != -1) {
				content += (char) r;
			}
			
			module = CompUtil.parseOneModule(content);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Err e) {
			e.printStackTrace();
		}
		*/
	}
	
	public CompModule getModule() {
		return module;
	}
}
