package edu.leiden.aqosa.model;

import java.util.ArrayList;
import java.util.List;

import edu.leiden.aqosa.model.IR.AQOSAModel;
import edu.leiden.aqosa.model.IR.Component;
import edu.leiden.aqosa.model.IR.Feature;
import edu.leiden.aqosa.model.IR.Flow;
import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.Command;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprVar;
import edu.mit.csail.sdg.alloy4compiler.parser.CompModule;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Options;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Solution;
import edu.mit.csail.sdg.alloy4compiler.translator.TranslateAlloyToKodkod;

public class Features {

	//protected final HashSet<String> featuresIDs = new HashSet<String>();						// IDs of all Alloy Features
	protected final List<Feature> leafFeatures;													// All lists of leaf features
	protected final List<Configuration> configurations = new ArrayList<Configuration>();		// All lists of feature configurations
	
	public Features(FeatureRoot featureRoot, AQOSAModel model, ArchRepository repo) {
		CompModule rootModule = featureRoot.getModule();
		if (rootModule == null) {
			configurations.add(new NullConfiguration());
			leafFeatures = new ArrayList<Feature>();
			return;
		}
		
		A4Options options = new A4Options();
		options.solver = A4Options.SatSolver.SAT4J;
		
		leafFeatures = getLeafFeatures(model.getFeatureModel().getFeatures().get(0), new ArrayList<Feature>());
		try {
			// Populate the allAlloyFeaturesIDs set
			//gatherAllFeatureIDs(rootModule);
			
			// Solve Alloy file
			for (Command command : rootModule.getAllCommands()) {
				// Execute the command in the file
				A4Solution ans = TranslateAlloyToKodkod.execute_command(null, rootModule.getAllReachableSigs(), command, options);

				// Iterate over all satisfiable solutions
				while (ans != null && ans.satisfiable()) {
					// Store the current feature model configuration
					ArrayList<String> configurationFeaturesIDs = new ArrayList<String>();

					for (ExprVar var : ans.getAllAtoms()) {
						// Remove unnecessary chars
						String id = var.toString();
						id = id.substring(id.indexOf('_') + 1, id.lastIndexOf('$'));
						configurationFeaturesIDs.add(id); // Store the ID of feature in the configuration list
					}
					
					Configuration configuration = new Configuration(configurationFeaturesIDs, leafFeatures, model.getAssembly().getFlow(), repo);
					// Add current configuration list to the all configurations list
					configurations.add(configuration);
					ans = ans.next();
				}
			}
		} catch (Err e) {
			e.printStackTrace();
		}
	}

	private List<Feature> getLeafFeatures(Feature feature, List<Feature> list) {
		if (feature.getFeatures().size() == 0) {	// If it's leaf feature (has no children), add to list
			list.add(feature);
		} else {
			for (Feature child : feature.getFeatures()) {	// Visit the children
				getLeafFeatures(child, list);
			}
		}
		
		return list;
	}
	
	/*
	private boolean gatherAllFeatureIDs(Module node) {
		boolean ret = true;
		for (Sig s : node.getAllSigs()) {
			try {
				String id = s.toString();
				id = id.substring(id.indexOf('_') + 1);
				featuresIDs.add(id);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Problem while gathering IDs of all features. Terminating function.");
				ret = false;
				break;
			}
		}
		return ret;
	}
	
	public HashSet<String> getFeaturesIDs() {
		return featuresIDs;
	}
	*/
	
	public List<Configuration> getConfigurations() {
		return configurations;
	}
	
}

class NullConfiguration extends Configuration {
	public NullConfiguration() {
		super();
	}
	
	@Override
	public List<Feature> getFeatures() {
		throw new IllegalStateException("Invalid call for NullConfiguration");
	}
	
	@Override
	public List<Component> getComponents() {
		throw new IllegalStateException("Invalid call for NullConfiguration");
	}
	
	@Override
	public List<Flow> getFlows() {
		throw new IllegalStateException("Invalid call for NullConfiguration");
	}
	
	@Override
	public boolean isValidComponent(Component c) {
		return true;
	}
	
	@Override
	public boolean isValidFlow(Flow f) {
		return true;
	}
}