package edu.leiden.aqosa.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

import edu.leiden.aqosa.model.IR.AQOSAModel;
import edu.leiden.aqosa.model.IR.AQOSAPackage;

public class ModelResource {

	protected final AQOSAPackage aqosaPackage;
	protected final ResourceSet resourceSet;
	protected final XMLResourceImpl resource;
	
	public ModelResource() {
		aqosaPackage = AQOSAPackage.eINSTANCE;
		aqosaPackage.setName("IR");
		aqosaPackage.setNsPrefix("aqosa.ir");
		aqosaPackage.setNsURI("http://se.liacs.nl/aqosa/ir");
		
		resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());
		resourceSet.getPackageRegistry().put("http://se.liacs.nl/aqosa/ir", aqosaPackage);
		
		resource = (XMLResourceImpl) resourceSet.createResource(URI.createURI("ArchModel"));
	}
	
	public AQOSAModel readModel(InputStream inputModelStream) {
		try {
			resource.load(inputModelStream, null);
			return (AQOSAModel) resource.getContents().get(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void writeModel(OutputStream outputModelStream) {
		try {
			Map<String, Object> options = new HashMap<String, Object>();
			options.put(XMLResource.OPTION_EXTENDED_META_DATA, Boolean.TRUE);
			options.put(XMLResource.OPTION_RECORD_UNKNOWN_FEATURE, Boolean.TRUE);
			options.put(XMLResource.OPTION_ENCODING, "UTF-8");
			
			resource.save(outputModelStream, options);
			outputModelStream.flush();
			outputModelStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
}
