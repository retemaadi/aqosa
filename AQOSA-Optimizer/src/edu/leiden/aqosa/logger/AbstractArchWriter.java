package edu.leiden.aqosa.logger;

import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintWriter;

public abstract class AbstractArchWriter extends PrintWriter implements ArchWriter {

	public AbstractArchWriter(OutputStream out) {
		super(out);
	}

	public AbstractArchWriter(String fileName) throws FileNotFoundException {
		super(fileName);
	}
	
}
