package edu.leiden.aqosa.logger;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.opt4j.core.IndividualStateListener;
import org.opt4j.core.config.annotations.Info;
import org.opt4j.core.optimizer.OptimizerIterationListener;
import org.opt4j.core.optimizer.OptimizerStateListener;
import org.opt4j.core.start.Constant;
import org.opt4j.core.start.Opt4JModule;

import edu.leiden.aqosa.logger.EvaluationLogger.DetailLevel;

public class ArchLoggerModule extends Opt4JModule {
	
	@Info("The writer for result logging.")
	protected Class<? extends AbstractArchWriter> writerClass;
	protected ArchWriterProvider provider;

	protected List<Class<? extends OptimizerStateListener>> optimizerStateListeners = new ArrayList<Class<? extends OptimizerStateListener>>();
	protected List<Class<? extends IndividualStateListener>> individualStateListeners = new ArrayList<Class<? extends IndividualStateListener>>();
	protected List<Class<? extends OptimizerIterationListener>> optimizerIterationListeners = new ArrayList<Class<? extends OptimizerIterationListener>>();
	
	@Info("The stream for each evaluation logging.")
	protected PrintStream stream = System.out;
	
	@Info("The name of the output file for Archive.")
	@Constant(value = "filename", namespace = ArchiveLogger.class)
	protected String archiveFilename = "";

	@Info("The name of the output file for Optimums.")
	@Constant(value = "filename", namespace = OptimumLogger.class)
	protected String optimumFilename = "";

	@Info("The level of the details info for output.")
	@Constant(value = "logLevel", namespace = EvaluationLogger.class)
	protected DetailLevel logLevel = DetailLevel.FULL;
	
	@Info("The switch for storing the serialized data.")
	@Constant(value = "serializedLog", namespace = ArchiveLogger.class)
	protected boolean serializedLog = false;
	
	@Info("The switch for storing archive per iteration.")
	@Constant(value = "iterationLog", namespace = ArchiveLogger.class)
	protected boolean iterationLog = false;
	
	@Info("The switch for storing experiment time.")
	@Constant(value = "timeLog", namespace = OptimumLogger.class)
	protected boolean timeLog = false;
	
	public void setWriter(Class<? extends AbstractArchWriter> writerClass) {
		this.writerClass = writerClass;
	}
	
	public void setStream(PrintStream stream) {
		this.stream = stream;
	}
	
	public void setArchiveFilename(String archiveFilename) {
		this.archiveFilename = archiveFilename;
	}
	
	public String getArchiveFilename() {
		return archiveFilename;
	}
	
	public void setOptimumFilename(String optimumFilename) {
		this.optimumFilename = optimumFilename;
	}
	
	public String getOptimumFilename() {
		return optimumFilename;
	}
	
	public void setLoggerLevel(DetailLevel logLevel) {
		this.logLevel = logLevel;
	}
	
	public DetailLevel getLoggerLevel() {
		return logLevel;
	}
	
	public void setSerializedLog(boolean serializedLog) {
		this.serializedLog = serializedLog;
	}
	
	public boolean getSerializedLog() {
		return serializedLog;
	}
	
	public void setIterationLog(boolean iterationLog) {
		this.iterationLog = iterationLog;
	}
	
	public boolean getIterationLog() {
		return iterationLog;
	}

	public void setTimeLog(boolean timeLog) {
		this.timeLog = timeLog;
	}
	
	public boolean getTimeLog() {
		return timeLog;
	}

	public void addOptimizerStateListenerClass(Class<? extends OptimizerStateListener> stateListenerClass) {
		optimizerStateListeners.add(stateListenerClass);
	}
	
	public void addIndividualStateListenerClass(Class<? extends IndividualStateListener> stateListenerClass) {
		individualStateListeners.add(stateListenerClass);
	}
	
	public void addOptimizerIterationListenerClass(Class<? extends OptimizerIterationListener> iterationListenerClass) {
		optimizerIterationListeners.add(iterationListenerClass);
	}
	
	@Override
	public void config() {
		provider = new ArchWriterProvider(writerClass);
		bind(ArchWriterProvider.class).toInstance(provider);
		
		for (Class<? extends OptimizerStateListener> listener : optimizerStateListeners) {
			addOptimizerStateListener(listener);
		}
		for (Class<? extends IndividualStateListener> listener : individualStateListeners) {
			addIndividualStateListener(listener);
		}
		for (Class<? extends OptimizerIterationListener> listener : optimizerIterationListeners) {
			addOptimizerIterationListener(listener);
		}
		
		EvaluationLogger logger = new EvaluationLogger(stream, logLevel);
		bind(EvaluationLogger.class).toInstance(logger);
		addIndividualStateListener(EvaluationLogger.class);
		addOptimizerStateListener(EvaluationLogger.class);
		addOptimizerIterationListener(EvaluationLogger.class);
		
		if (!archiveFilename.equals("")) {
			bind(ArchiveLogger.class).in(SINGLETON);
			addOptimizerStateListener(ArchiveLogger.class);
			addOptimizerIterationListener(ArchiveLogger.class);
		}
		
		if (!optimumFilename.equals("")) {
			bind(OptimumLogger.class).in(SINGLETON);
			addOptimizerStateListener(OptimumLogger.class);
			addOptimizerIterationListener(OptimumLogger.class);
		}		
	}

}
