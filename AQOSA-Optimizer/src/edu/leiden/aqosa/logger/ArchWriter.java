package edu.leiden.aqosa.logger;

import java.io.Closeable;
import java.io.Flushable;

public interface ArchWriter extends Flushable, Closeable {

	public void print(byte[] bytes);
	public void println(String x);
	public void flush();
	public void close();
	
}
