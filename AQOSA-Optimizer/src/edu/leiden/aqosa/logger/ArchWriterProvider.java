package edu.leiden.aqosa.logger;

import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;

public class ArchWriterProvider {
	
	protected final Class<? extends AbstractArchWriter> writerClass;
	
	public ArchWriterProvider(Class<? extends AbstractArchWriter> writerClass) {
		this.writerClass = writerClass;
	}
	
	private DefaultArchWriter provideDefaultWriter(String filename) {
		try {
			return new DefaultArchWriter(filename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}		
		return null;
	}
	
	public ArchWriter provideWriter(String filename) {
		ArchWriter pw;
		
		try {
			if (writerClass == null) {
				pw = provideDefaultWriter(filename);
			} else {
				Constructor<?> constructor = writerClass.getConstructor(String.class);
				pw = (ArchWriter) constructor.newInstance(filename);
			}
		} catch (Exception e) {
			pw = provideDefaultWriter(filename);
		}
		
		return pw;
	}

}
