package edu.leiden.aqosa.logger;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang.SerializationUtils;
import org.opt4j.core.Individual;
import org.opt4j.core.Objective;
import org.opt4j.core.Objectives;
import org.opt4j.core.Value;
import org.opt4j.core.common.logger.Logger;
import org.opt4j.core.optimizer.Archive;
import org.opt4j.core.optimizer.Optimizer;
import org.opt4j.core.optimizer.OptimizerIterationListener;
import org.opt4j.core.optimizer.OptimizerStateListener;
import org.opt4j.core.start.Constant;

import com.google.inject.Inject;

import edu.leiden.aqosa.solution.Individuals;


/**
 * @author Ramin
 * 
 */
public class ArchiveLogger implements Logger, OptimizerStateListener, OptimizerIterationListener {

	protected final String columnDelimiter = "\t";
	
	protected final String filename;
	protected final boolean serializedLog;
	protected final boolean iterationLog;
	
	protected final Archive archive;
	protected final ArchWriterProvider provider;
	private AtomicInteger solutionNumber;
	
	@Inject
	public ArchiveLogger(Archive archive, ArchWriterProvider provider,
			@Constant(value = "filename", namespace = ArchiveLogger.class) String filename,
			@Constant(value = "serializedLog", namespace = ArchiveLogger.class) boolean serializedLog,
			@Constant(value = "iterationLog", namespace = ArchiveLogger.class) boolean iterationLog) {
		this.filename = filename;
		this.serializedLog = serializedLog;
		this.iterationLog = iterationLog;
		
		this.archive = archive;
		this.provider = provider;
	}
	
	protected String getIndividual(Individual individual) {
		solutionNumber.incrementAndGet();
		
		String solution = null;
		if (individual.getPhenotype() == null)
			solution = " INVALID ";
		else
			solution = individual.getPhenotype().toString();
		
		return (String.valueOf(solutionNumber) + columnDelimiter + "\"" + solution + "\"" + getIndividualObjectives(individual));
	}

	private String getIndividualObjectives(Individual individual) {
		String output = "";
		Objectives objectives = individual.getObjectives();

		for (Objective objective : objectives.getKeys()) {
			Value<?> value = objectives.get(objective);
			assert value != null : "Objective " + objective.getName() + " not set for individual " + individual;

			output += columnDelimiter + value.getValue().toString();
		}
		
		return output;
	}

	@Override
	public void optimizationStarted(Optimizer optimizer) {
		solutionNumber = new AtomicInteger(0);		
	}

	@Override
	public void optimizationStopped(Optimizer optimizer) {
		solutionNumber.set(0);
		ArchWriter writer = provider.provideWriter(filename);
		for (Individual individual : archive) {
			writer.println(getIndividual(individual));
		}
		writer.flush();
		writer.close();
		
		if (serializedLog) {
			Individuals individuals = new Individuals(archive);
			byte[] data = (byte[]) SerializationUtils.serialize(individuals);
			ArchWriter writerBinary = provider.provideWriter(filename + ".ser");
			writerBinary.print(data);
			writerBinary.flush();
			writerBinary.close();
		}
	}

	@Override
	public void iterationComplete(int iteration) {
		if (iterationLog && iteration > 1) {
			if (serializedLog) {
				Individuals individuals = new Individuals(archive);
				byte[] data = (byte[]) SerializationUtils.serialize(individuals);
				ArchWriter writerBinary = provider.provideWriter(filename + "_i" + iteration + ".ser");
				writerBinary.print(data);
				writerBinary.flush();
				writerBinary.close();
			} else {
				solutionNumber.set(0);
				ArchWriter writerBinary = provider.provideWriter(filename + "_i" + iteration);
				for (Individual individual : archive) {
					writerBinary.println(getIndividual(individual));
				}
				writerBinary.flush();
				writerBinary.close();
			}
		}
	}
	
}