package edu.leiden.aqosa.logger;

import java.io.FileNotFoundException;

public class DefaultArchWriter extends AbstractArchWriter {

	public DefaultArchWriter(String fileName) throws FileNotFoundException {
		super(fileName);
	}

	@Override
	public void print(byte[] bytes) {
		char[] data = new char[bytes.length];
		for (int i=0; i < bytes.length; i++) {
			data[i] += (char) bytes[i];
		}
		super.print(data);
	}
	
}
