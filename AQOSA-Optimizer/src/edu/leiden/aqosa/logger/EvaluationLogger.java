package edu.leiden.aqosa.logger;

import java.io.PrintStream;
import java.util.concurrent.atomic.AtomicInteger;

import org.opt4j.core.Individual;
import org.opt4j.core.Individual.State;
import org.opt4j.core.IndividualStateListener;
import org.opt4j.core.Value;
import org.opt4j.core.common.logger.Logger;
import org.opt4j.core.optimizer.Optimizer;
import org.opt4j.core.optimizer.OptimizerIterationListener;
import org.opt4j.core.optimizer.OptimizerStateListener;

import edu.leiden.aqosa.solution.ArchSolution;

public class EvaluationLogger implements Logger, OptimizerStateListener, OptimizerIterationListener, IndividualStateListener {
	
	public enum DetailLevel {
		OFF,
		HALF,
		FULL
	}
	
	protected final PrintStream stream;
	protected final DetailLevel logLevel;
	private AtomicInteger latestEvaluationNumber;
	
	public EvaluationLogger(PrintStream stream, DetailLevel logLevel) {
		this.stream = stream;
		this.logLevel = logLevel;
	}
	
	public PrintStream getStream() {
		return stream;
	}
	
	public DetailLevel getLogLevel() {
		return logLevel;
	}
	
	@Override
	public void inidividualStateChanged(Individual individual) {
		if (logLevel == DetailLevel.FULL && individual.getState() == State.EVALUATED) {
			latestEvaluationNumber.incrementAndGet();
			
			String logger = latestEvaluationNumber + ") ";
			ArchSolution solution = (ArchSolution) individual.getPhenotype();
			
			if (solution == null) {
				logger += "[ INVALID ]: ";
			} else {
				logger += solution.toString() + ": ";

			}
			
			
			logger += "[";
			for (Value<?> o : individual.getObjectives().getValues())
				logger += o.getDouble() + ", ";
			logger = logger.substring(0, logger.length() - 2);
			logger += "]";
			
			stream.println(logger);
		}
	}

	@Override
	public void optimizationStarted(Optimizer optimizer) {
		latestEvaluationNumber = new AtomicInteger(0);
		if(logLevel != DetailLevel.OFF)
			stream.println("========== OPTIMIZTION STARTED ==========");
	}

	@Override
	public void optimizationStopped(Optimizer optimizer) {
		if(logLevel != DetailLevel.OFF)
			stream.println("========== OPTIMIZTION STOPPED ==========");
	}

	@Override
	public void iterationComplete(int iteration) {
		if(logLevel != DetailLevel.OFF)
			stream.println("--- Iteration: " + iteration);
		
	}

}
