package edu.leiden.aqosa.logger;

import java.util.ArrayList;
import java.util.List;

import org.opt4j.core.optimizer.Archive;
import org.opt4j.core.optimizer.Optimizer;
import org.opt4j.core.optimizer.OptimizerStateListener;

import com.google.inject.Inject;

import edu.leiden.aqosa.solution.Individuals;

public class FrontCollector implements OptimizerStateListener {

	protected static List<Individuals> fronts = new ArrayList<Individuals>();
	protected Archive ar;
	
	@Inject
	public FrontCollector(Archive archive) {
		this.ar = archive;
	}

	@Override
	public void optimizationStarted(Optimizer optimizer) {
		// Do Nothing!
	}

	@Override
	public void optimizationStopped(Optimizer optimizer) {
		Individuals individuals = new Individuals(ar);
		fronts.add(individuals);
	}
	
	public static void reset() {
		fronts = new ArrayList<Individuals>();
	}
	
	public static List<Individuals> getFronts() {
		return fronts;
	}
}
