package edu.leiden.aqosa.logger;

import java.util.Date;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.opt4j.core.Individual;
import org.opt4j.core.Objective;
import org.opt4j.core.common.logger.Logger;
import org.opt4j.core.optimizer.Archive;
import org.opt4j.core.optimizer.Optimizer;
import org.opt4j.core.optimizer.OptimizerIterationListener;
import org.opt4j.core.optimizer.OptimizerStateListener;
import org.opt4j.core.start.Constant;

import com.google.inject.Inject;


/**
 * @author Ramin
 * 
 */
public class OptimumLogger implements Logger, OptimizerStateListener, OptimizerIterationListener {

	protected final String columnDelimiter = "\t";
	
	protected final String filename;
	protected final boolean timeLog;
	protected final Archive archive;
	protected final ArchWriterProvider provider;
	
	protected Collection<Objective> objectives;
	protected Map<Objective, Double> optimums = new HashMap<Objective, Double>();
	protected Map<Objective, Integer> iterations = new HashMap<Objective, Integer>();
	
	protected long startTime;
	protected long endTime;
	
	@Inject
	public OptimumLogger(Archive archive, ArchWriterProvider provider,
			@Constant(value = "filename", namespace = OptimumLogger.class) String filename,
			@Constant(value = "timeLog", namespace = OptimumLogger.class) boolean timeLog) {
		this.filename = filename;
		this.timeLog = timeLog;
		this.archive = archive;
		this.provider = provider;
	}
	
	@Override
	public void optimizationStarted(Optimizer optimizer) {
		startTime = (new Date()).getTime();
	}

	@Override
	public void optimizationStopped(Optimizer optimizer) {
		endTime = (new Date()).getTime();
		ArchWriter writer  = provider.provideWriter(filename);
		
		if (objectives != null) {
			for (Objective obj : objectives) {
				writer.println(obj.getName() + columnDelimiter + optimums.get(obj) + columnDelimiter + iterations.get(obj));
			}
		}
		
		if (timeLog) {
			writer.println("");
			writer.println("StartTime" + columnDelimiter + startTime);
			writer.println("EndTime" + columnDelimiter + endTime);
			writer.println("ExecTime" + columnDelimiter + (endTime - startTime));
		}
		
		writer.flush();
		writer.close();
	}

	@Override
	public void iterationComplete(int iteration) {
		if (iteration == 2) {
			init();
		} else if (iteration > 2) {
			for (Objective obj : objectives) {
				Double optimum = findOptimum(obj);
				if (optimum < optimums.get(obj)) {
					iterations.put(obj, iteration);
					optimums.put(obj, optimum);
				}
			}
		}
	}
	
	private void init() {
		Individual ind = (archive.toArray(new Individual[0]))[0];
		objectives = ind.getObjectives().getKeys();
		
		for (Objective obj : objectives) {
			optimums.put(obj, 1.0);
			iterations.put(obj, 0);
		}
	}

	private Double findOptimum(Objective obj) {
		double min = 1.0;
		for (Individual individual : archive) {
			Double value = individual.getObjectives().get(obj).getDouble(); 
			if (value < min)
				min = value;
		}
		
		return min;
	}

}