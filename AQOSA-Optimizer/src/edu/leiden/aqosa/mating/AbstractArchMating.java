package edu.leiden.aqosa.mating;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.opt4j.core.Genotype;
import org.opt4j.core.Individual;
import org.opt4j.core.IndividualFactory;
import org.opt4j.core.common.random.Rand;
import org.opt4j.core.start.Constant;
import org.opt4j.operators.AbstractGenericOperator.OperatorClassPredicate;
import org.opt4j.operators.copy.Copy;
import org.opt4j.operators.crossover.Crossover;
import org.opt4j.operators.crossover.CrossoverGenericImplementation;
import org.opt4j.operators.crossover.Pair;
import org.opt4j.operators.mutate.Mutate;
import org.opt4j.operators.mutate.MutateGenericImplementation;
import org.opt4j.operators.mutate.MutationRate;
import org.opt4j.optimizers.ea.Coupler;
import org.opt4j.optimizers.ea.CrossoverRate;
import org.opt4j.optimizers.ea.MatingCrossoverMutate;

import com.google.inject.Inject;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.genotype.ArrayIntegerGenotype;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.operator.ArchOperator;
import edu.leiden.aqosa.operator.CrossoverArrayIntegerRate;
import edu.leiden.aqosa.operator.MutateArrayIntegerRandom;
import edu.leiden.aqosa.operator.cps.CPUChangeCost;
import edu.leiden.aqosa.operator.cps.CPUChangePerformance;
import edu.leiden.aqosa.operator.cps.CPUChangeSafety;
import edu.leiden.aqosa.operator.cps.ComponentMove;
import edu.leiden.aqosa.operator.lb.LoadBalancer;
import edu.leiden.aqosa.solution.ArchSolution;

public abstract class AbstractArchMating extends MatingCrossoverMutate {
	
	protected List<ArchOperator<ArchGenotype>> operators = new ArrayList<ArchOperator<ArchGenotype>>();
	
	class OperatorComparator implements Comparator<ArchOperator<ArchGenotype>> {
		@Override
		public int compare(ArchOperator<ArchGenotype> opr1, ArchOperator<ArchGenotype> opr2) {
			Integer count1 = opr1.getCounter();
			return count1.compareTo(opr2.getCounter());
		}	
	}
	
	protected final double heuristicRate;
	protected final boolean cpsSwitch;
	protected final boolean lbSwitch;
	
	/**
	 * Constructs a {@link MatingCrossoverMutate} with a given {@link Crossover}
	 * , {@link Mutate}, {@link Copy}, {@link Coupler}, {@link CrossoverRate},
	 * {@link Rand}, and {@link IndividualFactory}.
	 * 
	 * @param crossover
	 *            the crossover operator
	 * @param mutate
	 *            the mutate operator
	 * @param copy
	 *            the copy operator
	 * @param coupler
	 *            the coupler
	 * @param crossoverRate
	 *            the used crossover rate
	 * @param mutationRate
	 *            the mutation rate
	 * @param random
	 *            the random number generator
	 * @param individualFactory
	 *            the individual factory
	 */
	@Inject
	public AbstractArchMating(Crossover<Genotype> crossover, Mutate<Genotype> mutate,
			Copy<Genotype> copy, Coupler coupler, CrossoverRate crossoverRate, MutationRate mutationRate,
			Rand random, IndividualFactory individualFactory, ArchModel model,
			@Constant(value = "heuristicRate", namespace = AbstractArchMating.class) double heuristicRate,
			@Constant(value = "cpsSwitch", namespace = AbstractArchMating.class) boolean cpsSwitch,
			@Constant(value = "lbSwitch", namespace = AbstractArchMating.class) boolean lbSwitch) {
		super(crossover, mutate, copy, coupler, crossoverRate, mutationRate, random, individualFactory);
		
		this.heuristicRate = heuristicRate;
		this.cpsSwitch = cpsSwitch;
		this.lbSwitch = lbSwitch;
		
		OperatorClassPredicate predicate = new OperatorClassPredicate(ArrayIntegerGenotype.class);
		
		MutateGenericImplementation genericMutate = (MutateGenericImplementation) mutate;
		Mutate<Genotype> operatorMutate = new MutateArrayIntegerRandom(random);
		genericMutate.addOperator(predicate, operatorMutate);
		
		CrossoverGenericImplementation genericCrossover = (CrossoverGenericImplementation) crossover;
		Crossover<Genotype> operatorCrossover = new CrossoverArrayIntegerRate(crossoverRate.get(), random);
		genericCrossover.addOperator(predicate, operatorCrossover);
		
		if (cpsSwitch) {
			operators.add(new ComponentMove<ArchGenotype>(model));
			operators.add(new CPUChangePerformance<ArchGenotype>(model));
			operators.add(new CPUChangeCost<ArchGenotype>(model));
			operators.add(new CPUChangeSafety<ArchGenotype>(model));
		}
		if (lbSwitch) {
			operators.add(new LoadBalancer<ArchGenotype>(model));			
		}
	}

	@Override
	protected Pair<Individual> mate(Individual parent1, Individual parent2, boolean doCrossover) {
		ArchSolution p1 = (ArchSolution) parent1.getPhenotype();
		ArchSolution p2 = (ArchSolution) parent2.getPhenotype();
		ArchGenotype o1 = (ArchGenotype) copy.copy(parent1.getGenotype());
		ArchGenotype o2 = (ArchGenotype) copy.copy(parent2.getGenotype());
		
		if (p1 == null || p2 == null) {
			if (doCrossover) {
				Pair<Genotype> crossed = crossover.crossover(o1, o2);
				o1 = (ArchGenotype) crossed.getFirst();
				o2 = (ArchGenotype) crossed.getSecond();
				mutate.mutate(o1, mutationRate.get());
				mutate.mutate(o2, mutationRate.get());
			} else {
				mutate.mutate(o1, mutationRate.get());
				mutate.mutate(o2, mutationRate.get());
			}
			
		} else {
			if (this instanceof MutateMating) {
				mutate.mutate(o1, mutationRate.get());
				mutate.mutate(o2, mutationRate.get());
			} else if (this instanceof CrossoverMutateMating) {
				if (doCrossover) {
					Pair<Genotype> crossed = crossover.crossover(o1, o2);
					o1 = (ArchGenotype) crossed.getFirst();
					o2 = (ArchGenotype) crossed.getSecond();
					mutate.mutate(o1, mutationRate.get());
					mutate.mutate(o2, mutationRate.get());
				} else {
					mutate.mutate(o1, mutationRate.get());
					mutate.mutate(o2, mutationRate.get());
				}
			} else {
				boolean doHeuristic = random.nextDouble() <= heuristicRate;
				if (doHeuristic) {
					Pair<ArchGenotype> offspring = mate(p1, p2, o1, o2);
					o1 = offspring.getFirst();
					o2 = offspring.getSecond();
				} else if (doCrossover) {
					Pair<Genotype> crossed = crossover.crossover(o1, o2);
					o1 = (ArchGenotype) crossed.getFirst();
					o2 = (ArchGenotype) crossed.getSecond();
					mutate.mutate(o1, mutationRate.get());
					mutate.mutate(o2, mutationRate.get());
				} else {
					mutate.mutate(o1, mutationRate.get());
					mutate.mutate(o2, mutationRate.get());
				}
			}
		}
		
		return new Pair<Individual>(individualFactory.create(o1), individualFactory.create(o2));
	}
	
	protected abstract Pair<ArchGenotype> mate(ArchSolution p1, ArchSolution p2, ArchGenotype o1, ArchGenotype o2);
	
	/**
	 * This method returns a random order of operator to define a random turn for them
	 * 
	 * @return List<Integer>
	 */
	protected List<ArchOperator<ArchGenotype>> getRandomTurn() {
		List<ArchOperator<ArchGenotype>> randomOrder = operators;
		Collections.shuffle(randomOrder);
		return randomOrder;
	}
	
	/**
	 * This method sorts the turn for operators in a round robin manner
	 * 
	 * @return List<Integer>
	 */
	protected List<ArchOperator<ArchGenotype>> getRoundRobinTurn() {
		List<ArchOperator<ArchGenotype>> roundrobinOrder = operators;
		Collections.sort(roundrobinOrder, new OperatorComparator());
		return roundrobinOrder;
	}
	
}
