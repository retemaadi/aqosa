package edu.leiden.aqosa.mating;

import org.opt4j.core.config.Icons;
import org.opt4j.core.config.annotations.Icon;
import org.opt4j.core.config.annotations.Info;
import org.opt4j.core.start.Constant;
import org.opt4j.core.start.Opt4JModule;
import org.opt4j.optimizers.ea.Mating;

@Icon(Icons.OPERATOR)
public class ArchMatingModule extends Opt4JModule {

	@Info("Mating approach")
	protected Class<? extends AbstractArchMating> mating;

	@Info("Heuristic Rate (like Crossover rate) ")
	@Constant(value = "heuristicRate", namespace = AbstractArchMating.class)
	protected double heuristicRate = 0.95;
	
	@Info("ConcurrentProcessingSystems operators switch")
	@Constant(value = "cpsSwitch", namespace = AbstractArchMating.class)
	protected boolean cpsSwitch = true;
	
	@Info("LoadBalancer operator switch")
	@Constant(value = "lbSwitch", namespace = AbstractArchMating.class)
	protected boolean lbSwitch = false;
	
	public void setApproach(Class<? extends AbstractArchMating> mating) {
		this.mating = mating;
	}
	
	public void setHeuristicRate(double heuristicRate) {
		this.heuristicRate = heuristicRate;
	}
	
	public double getHeuristicRate() {
		return heuristicRate;
	}
	
	public void setCpsSwitch(boolean cpsSwitch) {
		this.cpsSwitch = cpsSwitch;
	}
	
	public boolean getCpsSwitch() {
		return cpsSwitch;
	}
	
	public void setLbSwitch(boolean lbSwitch) {
		this.lbSwitch = lbSwitch;
	}
	
	public boolean getLbSwitch() {
		return lbSwitch;
	}
	
	@Override
	protected void config() {
		bind(Mating.class).to(mating).in(SINGLETON);
	}
}
