package edu.leiden.aqosa.mating;

import org.opt4j.core.Genotype;
import org.opt4j.core.IndividualFactory;
import org.opt4j.core.common.random.Rand;
import org.opt4j.core.start.Constant;
import org.opt4j.operators.copy.Copy;
import org.opt4j.operators.crossover.Crossover;
import org.opt4j.operators.crossover.Pair;
import org.opt4j.operators.mutate.Mutate;
import org.opt4j.operators.mutate.MutationRate;
import org.opt4j.optimizers.ea.ConstantCrossoverRate;
import org.opt4j.optimizers.ea.Coupler;
import org.opt4j.optimizers.ea.CrossoverRate;

import com.google.inject.Inject;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.solution.ArchSolution;

public class MutateMating extends AbstractArchMating {

	@Inject
	public MutateMating(Crossover<Genotype> crossover, Mutate<Genotype> mutate,
			Copy<Genotype> copy, Coupler coupler, CrossoverRate crossoverRate, MutationRate mutationRate,
			Rand random, IndividualFactory individualFactory, ArchModel model,
			@Constant(value = "heuristicRate", namespace = AbstractArchMating.class) double heuristicRate,
			@Constant(value = "cpsSwitch", namespace = AbstractArchMating.class) boolean cpsSwitch,
			@Constant(value = "lbSwitch", namespace = AbstractArchMating.class) boolean lbSwitch) {
		super(crossover, mutate, copy, coupler, new ConstantCrossoverRate(0.0), mutationRate, random, individualFactory, model, 0.0, false, false);
	}

	@Override
	protected Pair<ArchGenotype> mate(ArchSolution p1, ArchSolution p2,	ArchGenotype o1, ArchGenotype o2) {
		mutate.mutate(o1, mutationRate.get());
		mutate.mutate(o2, mutationRate.get());
		
		return new Pair<ArchGenotype>(o1, o2);
	}
	
}
