package edu.leiden.aqosa.operator;

import edu.leiden.aqosa.genes.ArchGenotype;

public abstract class AbstractArchOperator<G extends ArchGenotype> implements ArchOperator<G> {

	private int counter;
	
	protected void count() {
		counter++;
	}
	
	public int getCounter() {
		return counter;
	}
	
}
