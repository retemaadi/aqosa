package edu.leiden.aqosa.operator;

import org.opt4j.core.optimizer.Operator;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.solution.ArchSolution;

public interface ArchOperator<G extends ArchGenotype> extends Operator<G> {

	/**
	 * detects any bottleneck in the architecture
	 * 
	 * @param genotype
	 * @return true if there is a bottleneck
	 */
	public Boolean diagnose(ArchSolution phenotype);
	
	/**
	 * heals the bottleneck by applying the anti pattern
	 * 
	 * @param phenotype
	 * @param offspring which is a well-balanced genotype
	 */
	public void heal(ArchSolution phenotype, G offspring);

	/**
	 * counts the number of times that operator applies (for doing round robin turn)
	 * 
	 */
	public int getCounter();
	
}
