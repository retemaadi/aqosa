package edu.leiden.aqosa.operator;

import org.opt4j.core.Genotype;
import org.opt4j.operators.crossover.Crossover;

import com.google.inject.ImplementedBy;

@ImplementedBy(CrossoverArrayIntegerRate.class)
public interface CrossoverArrayInteger extends Crossover<Genotype> {

}
