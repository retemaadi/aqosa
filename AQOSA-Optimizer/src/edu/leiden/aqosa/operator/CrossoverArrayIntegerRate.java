package edu.leiden.aqosa.operator;

import java.util.Random;

import org.opt4j.core.Genotype;
import org.opt4j.core.common.random.Rand;
import org.opt4j.operators.Apply;
import org.opt4j.operators.crossover.Pair;

import com.google.inject.Inject;

import edu.leiden.aqosa.genotype.ArrayIntegerGenotype;

@Apply(ArrayIntegerGenotype.class)
public class CrossoverArrayIntegerRate implements CrossoverArrayInteger {

	protected final Random random;

	protected final double rate;

	@Inject
	public CrossoverArrayIntegerRate(double rate, Rand random) {
		this.rate = rate;
		this.random = random;
	}

	@Override
	public Pair<Genotype> crossover(Genotype p1, Genotype p2) {
		ArrayIntegerGenotype g1 = (ArrayIntegerGenotype) p1;
		ArrayIntegerGenotype g2 = (ArrayIntegerGenotype) p2;
		
		ArrayIntegerGenotype o1 = g1.newInstance();
		ArrayIntegerGenotype o2 = g2.newInstance();
		
		int size = p1.size();
		
		boolean select = random.nextBoolean();
		for (int i = 0; i < size; i++) {
			if (random.nextDouble() < rate) {
				select = !select;
			}

			if (select) {
				o1.add(g1.get(i));
				o2.add(g2.get(i));
			} else {
				o1.add(g2.get(i));
				o2.add(g1.get(i));
			}
		}
		
		Pair<Genotype> offspring = new Pair<Genotype>((ArrayIntegerGenotype) o1, (ArrayIntegerGenotype) o2);
		return offspring;
	}

}
