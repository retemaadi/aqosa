package edu.leiden.aqosa.operator;

import org.opt4j.core.Genotype;
import org.opt4j.operators.mutate.Mutate;

import com.google.inject.ImplementedBy;

@ImplementedBy(MutateArrayIntegerRandom.class)
public interface MutateArrayInteger extends Mutate<Genotype> {

}
