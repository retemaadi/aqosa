package edu.leiden.aqosa.operator;

import java.util.ArrayList;
import java.util.Random;

import org.opt4j.core.common.random.Rand;
import org.opt4j.core.Genotype;
import org.opt4j.operators.Apply;

import com.google.inject.Inject;

import edu.leiden.aqosa.genotype.ArrayIntegerGenotype;

@Apply(ArrayIntegerGenotype.class)
public class MutateArrayIntegerRandom implements MutateArrayInteger  {

	protected final Random random;

	@Inject
	public MutateArrayIntegerRandom(Rand random) {
		this.random = random;
	}

	public void mutate(Genotype geno, double p) {
		ArrayIntegerGenotype genotype = (ArrayIntegerGenotype) geno; 
		
		int size = genotype.size();
		
		for (int i = 0; i < size; i++) {
			if (random.nextDouble() < p) {
				int lb = genotype.getLowerBound(i);
				int ub = genotype.getUpperBound(i);
				int arrSize = genotype.getMaximumArraySize();
				
				ArrayList<Integer> array = new ArrayList<Integer>();				
				for (int j = 0; j < arrSize; j++) {
					int value = lb + random.nextInt(ub - lb + 1);
					array.add(value);
				}
				
				genotype.set(i, array);
			}
		}
	}
}
