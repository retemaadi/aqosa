package edu.leiden.aqosa.operator.cps;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.model.IR.Processor;
import edu.leiden.aqosa.solution.ArchSolution;

/**
 * 
 * @author Ramin
 */
public class CPUChangePerformance<G extends ArchGenotype> extends ConcurrentProcessingSystems<G> {

	private final List<Processor> processors;
	private List<Integer> sortedIndex;
	
	class ClockComparator implements Comparator<Integer> {
		private final List<Processor> processors;
		
		public ClockComparator(List<Processor> processors) {
			this.processors = processors;
		}
		
		@Override
		public int compare(Integer idx1, Integer idx2) {
			int comp = Double.compare(processors.get(idx1).getClock(), processors.get(idx2).getClock());
			if (comp == 0) {
				comp = Double.compare(processors.get(idx1).getCost(), processors.get(idx2).getCost());
			}
			
	        return comp;
		}	
	}
		
	/**
	 * @param model
	 */
	public CPUChangePerformance(ArchModel model) {
		processors = model.getAQOSAModel().getRepository().getProcessor();
		sortedIndex = new ArrayList<Integer>();
		for (int i=0; i<processors.size(); i++)
			sortedIndex.add(i);
		Collections.sort(sortedIndex, new ClockComparator(processors));
	}

	/**
	 * this method detects if more powerful cpu is possible
	 * 
	 * @param genotype
	 * @param phenotype
	 * @return true if there it is possible
	 */
	public Boolean diagnose(ArchSolution phenotype) {
		count();
		for (Processor p : phenotype.getProcessors()) {
			int powerfulIdx = sortedIndex.get(sortedIndex.size() - 1);
			if (p.getClock() < processors.get(powerfulIdx).getClock())
				return true;
		}
			
		return false;
	}

	/**
	 * this method replace the highest utilization cpu in arch with the closest more powerful in repo
	 * 
	 * @param phenotype
	 * @param offspring
	 */
	public void heal(ArchSolution phenotype, G offspring) {
		final List<Processor> archProcessors = phenotype.getProcessors();
		final List<Double> archUtils = phenotype.getCpuUtilizationList();
		if (archUtils == null)
			return;
		
		List<Integer> archSortedIndex = new ArrayList<Integer>();
		for (int i=0; i<archUtils.size(); i++)
			archSortedIndex.add(i);
		Collections.sort(archSortedIndex, Collections.reverseOrder(new UtilComparator(archUtils)));
				
		for (Integer archIdx : archSortedIndex) {
			Processor archP = archProcessors.get(archIdx);
			for (Integer repoIdx : sortedIndex) {
				Processor repoP = processors.get(repoIdx);
				if (archP.getCost() > repoP.getCost()) {
					offspring.getNodes().set(archIdx, repoIdx);
					return;
				}
			}
		}
	}
	
}
