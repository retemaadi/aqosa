package edu.leiden.aqosa.operator.cps;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.model.IR.Processor;
import edu.leiden.aqosa.solution.ArchSolution;

/**
 * 
 * @author Ramin
 */
public class CPUChangeSafety<G extends ArchGenotype> extends ConcurrentProcessingSystems<G> {

	private final List<Processor> processors;
	private List<Integer> sortedIndex;
	
	class FailureComparator implements Comparator<Integer> {
		private final List<Processor> processors;
		
		public FailureComparator(List<Processor> processors) {
			this.processors = processors;
		}
		
		@Override
		public int compare(Integer idx1, Integer idx2) {
			double avg1 = (processors.get(idx1).getLowerFail() + processors.get(idx1).getUpperFail()) / 2;
			double avg2 = (processors.get(idx2).getLowerFail() + processors.get(idx2).getUpperFail()) / 2;
			
			int comp = Double.compare(avg1, avg2);
			if (comp == 0) {
				comp = Double.compare(processors.get(idx1).getUpperFail(), processors.get(idx2).getUpperFail());
			}
			
	        return comp;
		}	
	}
	
	/**
	 * @param model
	 */
	public CPUChangeSafety(ArchModel model) {
		processors = model.getAQOSAModel().getRepository().getProcessor();
		sortedIndex = new ArrayList<Integer>();
		for (int i=0; i<processors.size(); i++)
			sortedIndex.add(i);
		Collections.sort(sortedIndex, Collections.reverseOrder(new FailureComparator(processors)));
	}

	/**
	 * this method detects if a safer cpu is possible
	 * 
	 * @param phenotype
	 * @return true if it is possible
	 */
	public Boolean diagnose(ArchSolution phenotype) {
		count();
		
		for (Processor p : phenotype.getProcessors()) {
			int safestIdx = sortedIndex.get(sortedIndex.size() - 1);
			if (p.getUpperFail() > processors.get(safestIdx).getUpperFail())
				return true;
		}
			
		return false;
	}

	/**
	 * this method replace the lowest safe cpu in arch with the closest safer in repo
	 * 
	 * @param phenotype
	 * @param offspring
	 */
	public void heal(ArchSolution phenotype, G offspring) {
		final List<Processor> archProcessors = phenotype.getProcessors();
		List<Integer> archSortedIndex = new ArrayList<Integer>();
		for (int i=0; i<archProcessors.size(); i++)
			archSortedIndex.add(i);
		Collections.sort(archSortedIndex, Collections.reverseOrder(new FailureComparator(archProcessors)));
		
		for (Integer archIdx : archSortedIndex) {
			Processor archP = archProcessors.get(archIdx);
			double avgArch = (archP.getLowerFail() + archP.getUpperFail()) / 2;
			for (Integer repoIdx : sortedIndex) {
				Processor repoP = processors.get(repoIdx);
				double avgRepo = (repoP.getLowerFail() + repoP.getUpperFail()) / 2;
				if (avgArch > avgRepo) {
					offspring.getNodes().set(archIdx, repoIdx);
					return;
				}
			}
		}
	}

}