package edu.leiden.aqosa.operator.cps;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.model.ArchRepository;
import edu.leiden.aqosa.model.IR.ComponentInstance;
import edu.leiden.aqosa.solution.ArchSolution;

/**
 * @author Ramin
 */
public class ComponentMove<G extends ArchGenotype> extends ConcurrentProcessingSystems<G> {

	private final ArchRepository repository;
	
	class CycleComparator implements Comparator<String> {
		@Override
		public int compare(String idx1, String idx2) {
			ComponentInstance ci1 = repository.getInstance(idx1);
			ComponentInstance ci2 = repository.getInstance(idx2);
	        return Double.compare(ci1.getService().get(0).getCycles(), ci2.getService().get(0).getCycles());
		}	
	}
	
	/**
	 * @param model
	 */
	public ComponentMove(ArchModel model) {
		repository = new ArchRepository(model.getAQOSAModel());
	}

	/**
	 * this method detects 2 cpus in that one is with a low utilization and the
	 * other is with a high utilization
	 * 
	 * @param genotype
	 * @param phenotype
	 * @return true if there is any bottleneck
	 */
	public Boolean diagnose(ArchSolution phenotype) {
		count();
		// if number of nodes in a genotype is less than 2, pattern will not be applicable
		if (phenotype.getNodes() < 2)
			return false;

		return true;
	}

	/**
	 * this method moves the components from cpu with high utilization to the
	 * cpu with low utilization if the whole required time to complete jobs on
	 * node with low utilization does not become greater than the average of
	 * both stated nodes
	 * 
	 * @param phenotype
	 * @param offspring
	 */
	public void heal(ArchSolution phenotype, G offspring) {
		final List<Double> archUtils = phenotype.getCpuUtilizationList();
		if (archUtils == null)
			return;
		int last = archUtils.size() - 1;
		List<Integer> archSortedIndex = new ArrayList<Integer>();
		
		for (int i=0; i < archUtils.size(); i++)
			archSortedIndex.add(i);
		Collections.sort(archSortedIndex, new UtilComparator(archUtils));
		
		int minProc = archSortedIndex.get(0);
		int maxProc = archSortedIndex.get(last);
		
		List<Integer> componentsOnMaxNode = findComponents(maxProc, phenotype.getDeploy());
		List<String> componentInstances = new ArrayList<String>();
		for (Integer comp : componentsOnMaxNode) {
			componentInstances.addAll(repository.getInstances(comp));
		}
		Collections.sort(componentInstances, Collections.reverseOrder(new CycleComparator()));
		
		int component = 0;
		String ci = componentInstances.get(0);
		for (Integer comp : componentsOnMaxNode) {
			if (repository.getInstances(comp).contains(ci)) {
				component = comp;
				break;
			}
		}
		ArrayList<Integer> deploy = new ArrayList<Integer>();
		deploy.add(minProc+1);		// node numbers start from 1
		
		offspring.getDeploy().set(component, deploy);
	}

	/**
	 * this method returns a list of component numbers which are deployed on a
	 * node
	 * 
	 * @param nodeNumber
	 * @param genotype
	 * @return List<Integer>
	 */
	private List<Integer> findComponents(int nodeNumber, List<List<Integer>> deployList) {
		List<Integer> compNums = new ArrayList<Integer>();
		int comp = 0;
		for (List<Integer> nodes : deployList) {
			for (Integer node : nodes) {
				// since node is in [1-n] we need to subtract it by 1 to be
				// comparable with the node numbers in [0-n]
				if ((node - 1) == nodeNumber)
					compNums.add(comp);
			}
			comp++;
		}
		
		return compNums;
	}

}
