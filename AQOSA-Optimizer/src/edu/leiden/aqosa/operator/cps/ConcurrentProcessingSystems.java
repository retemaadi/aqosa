package edu.leiden.aqosa.operator.cps;

import java.util.Comparator;
import java.util.List;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.operator.AbstractArchOperator;
import edu.leiden.aqosa.operator.ArchOperator;

public abstract class ConcurrentProcessingSystems<G extends ArchGenotype> extends AbstractArchOperator<G> implements ArchOperator<G> {

}

class UtilComparator implements Comparator<Integer> {
	private final List<Double> utilizations;
	
	public UtilComparator(List<Double> utilizations) {
		this.utilizations = utilizations;
	}
	
	@Override
	public int compare(Integer idx1, Integer idx2) {
        return Double.compare(utilizations.get(idx1), utilizations.get(idx2));
	}	
}
