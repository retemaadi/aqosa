package edu.leiden.aqosa.operator.lb;

import java.util.ArrayList;
import java.util.List;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.operator.AbstractArchOperator;
import edu.leiden.aqosa.solution.ArchSolution;

public class LoadBalancer<G extends ArchGenotype> extends
		AbstractArchOperator<G> {

	private static final double[] MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY = {
			6.0, 5.0, 5.0, 4.0, 28.0, 29.0, 5.0, 1.0,
			25.0, 5.0, 10.0, 27.0, 15.0, 1.0, 24.0,
			1.0, 1.0, 3.0 };

	protected ArchModel model;

	public static int counter = 0;

	private int componentMaxExecTimeIndex;
	private int nodeNumber;
	
	/**
	 * @param model
	 */
	public LoadBalancer(ArchModel model) {
		this.model = model;
	}

	/**
	 * this method detects 2 cpus in that one is with a low utilization and the
	 * other is with a high utilization
	 * 
	 * @param genotype
	 * @param phenotype
	 * @return true if there is any bottleneck
	 */
	public Boolean diagnose(ArchSolution phenotype) {
		counter++;
		System.out.println("Diagnose....");
		List<List<Integer>> deployList = phenotype.getDeploy();
		System.out.println("deployList: " + deployList);
		for (int i = 0; i < deployList.size(); i++) {
			List<Integer> nodeList = deployList.get(i);
			for (int j = 0; j < nodeList.size(); j++) {
				nodeNumber = nodeList.get(j);
				double executionTime = calcExcTime(i, nodeNumber, phenotype);
				switch(i) {
				case 0: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[0]) {
						System.out.println("bottleneck is detected, component 0");
						componentMaxExecTimeIndex = i;
						return true;
					}
					break;
				case 1: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[1]) {
						System.out.println("bottleneck is detected, component 1");
						componentMaxExecTimeIndex = i;						
						return true;
					}
					break;
				case 2: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[2]) {
						System.out.println("bottleneck is detected, component 2");
						componentMaxExecTimeIndex = i;						
						return true;
					}
					break;
				case 3: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[3]) {
						System.out.println("bottleneck is detected, component 3");
						componentMaxExecTimeIndex = i;						
						return true;
					}
					break;
				case 4: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[4]) {
						System.out.println("bottleneck is detected, component 4");
						componentMaxExecTimeIndex = i;						
						return true;
					}
					break;
				case 5: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[5]) {
						System.out.println("bottleneck is detected, component 5");
						componentMaxExecTimeIndex = i;						
						return true;
					}
					break;
				case 6: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[6]) {
						System.out.println("bottleneck is detected, component 6");
						componentMaxExecTimeIndex = i;						
						return true;
					}
					break;
				case 7: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[7]) {
						System.out.println("bottleneck is detected, component 7");
						componentMaxExecTimeIndex = i;						
						return true;
					}
					break;
				case 8: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[8]) {
						System.out.println("bottleneck is detected, component 8");
						componentMaxExecTimeIndex = i;						
						return true;
					}
					break;
				case 9: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[9]) {
						System.out.println("bottleneck is detected, component 9");
						componentMaxExecTimeIndex = i;						
						return true;
					}
					break;
				case 10: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[10]) {
						System.out.println("bottleneck is detected, component 10");
						componentMaxExecTimeIndex = i;						
						return true;
					}
					break;
				case 11: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[11]) {
						System.out.println("bottleneck is detected, component 11");
						componentMaxExecTimeIndex = i;						
						return true;
					}
					break;
				case 12: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[12]) {
						System.out.println("bottleneck is detected, component 12");
						componentMaxExecTimeIndex = i;						
						return true;
					}	
					break;
				case 13: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[13]) {
						System.out.println("bottleneck is detected, component 13");
						componentMaxExecTimeIndex = i;						
						return true;
					}
					break;
				case 14: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[14]) {
						System.out.println("bottleneck is detected, component 14");
						componentMaxExecTimeIndex = i;						
						return true;
					}	
					break;
				case 15: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[15]) {
						System.out.println("bottleneck is detected, component 15");
						componentMaxExecTimeIndex = i;						
						return true;
					}	
					break;
				case 16: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[16]) {
						System.out.println("bottleneck is detected, component 16");
						componentMaxExecTimeIndex = i;						
						return true;
					}	
					break;
				case 17: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[17]) {
						System.out.println("bottleneck is detected, component 17");
						componentMaxExecTimeIndex = i;						
						return true;
					}	
					break;
				case 18: 
					if (executionTime >= MAXIMUM_THRESHOLD_LOAD_BALANCE_TIME_ARRAY[18]) {
						System.out.println("bottleneck is detected, component 18");
						componentMaxExecTimeIndex = i;						
						return true;
					}	
					break;
				}				
			}
		}

		return false;
	}

	private double calcExcTime(int componentNumber, int nodeNumber,
			ArchSolution phenotype) {
		System.out.println("nodeNumber: " + nodeNumber);
		System.out.println("componentNumber: " + componentNumber);
		int componentCycle = model.getAQOSAModel().getRepository()
				.getComponentinstance().get(componentNumber).getService()
				.get(0).getCycles();
		double cpuClock = phenotype.getProcessors().get(nodeNumber - 1)
				.getClock();		
		double executionTime = componentCycle / cpuClock;
		System.out.println("executionTime: " + executionTime);
		return executionTime;
	}

	/**
	 * this method moves the components from cpu with high utilization to the
	 * cpu with low utilization if the whole required time to complete jobs on
	 * node with low utilization does not become greater than the average of
	 * both stated nodes
	 * 
	 * @param phenotype
	 * @param offspring
	 */
	public void heal(ArchSolution phenotype, G offspring) {
		System.out.println("Heal...");
		int minIndex = calcMinIndex(phenotype.getCpuUtilizationList());
		System.out.println("minIndex: " + minIndex);
		ArrayList<Integer> deployList = (ArrayList<Integer>) phenotype.getDeploy().get(componentMaxExecTimeIndex);
		// If the component has been deployed on another node once, load balancer has done the heal and does not need to do it again
		if(deployList.size() == 2)
			return;
		System.out.println("deployList before: " + deployList);
		deployList.add(minIndex+1);
		System.out.println("deployList after:" + deployList);		
		offspring.getDeploy().set(componentMaxExecTimeIndex, deployList);
	}
	
	private int calcMinIndex(List<Double> utils) {
		double minUtil = utils.get(0);
		int minIndex = 0;
		for(int i = 1 ; i < utils.size() ; i++) {
			if(utils.get(i) < minUtil) {
				minUtil = utils.get(i);
				minIndex = i;
			}
		}
		return minIndex;
	}
}
