package edu.leiden.aqosa.optimizer;

import java.util.Random;

import org.opt4j.core.problem.Creator;

import com.google.inject.Inject;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.genes.GenotypeProvider;
import edu.leiden.aqosa.model.ArchModel;


/**
 * 
 * @author Ramin
 */
public class ArchCreator implements Creator<ArchGenotype> {
	
	protected final ArchModel model;
	protected final GenotypeProvider provider;
	
	@Inject
	public ArchCreator(ArchModel model, GenotypeProvider provider) {
		this.model = model;
		this.provider = provider;
	}
	
	public ArchGenotype create() {
		Random random = new Random();
		ArchGenotype genotype = null;
		do {
			//genotype = new ArchGenotype(model);
			genotype = provider.provideGenotype(model);
			genotype.init(random);
		} while (!genotype.validate());

		return genotype;
	}
	
}
