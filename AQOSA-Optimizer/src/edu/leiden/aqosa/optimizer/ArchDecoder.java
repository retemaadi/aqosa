package edu.leiden.aqosa.optimizer;

import org.opt4j.core.problem.Decoder;

import com.google.inject.Inject;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.model.ArchRepository;
import edu.leiden.aqosa.solution.ArchSolution;

/**
 * 
 * @author Ramin
 */
public class ArchDecoder implements Decoder<ArchGenotype, ArchSolution> {

	protected final ArchModel model;
	protected final ArchRepository repo;
	
	@Inject
	public ArchDecoder(ArchModel model) {
		this.model = model;
		this.repo = model.getRepository();
	}
	
	public ArchSolution decode(ArchGenotype genotype) {
		ArchSolution solution = null;
		
		if(genotype.validate()) { 
			solution = new ArchSolution(model, genotype);
		}
		
		return solution;
	}
	
}
