package edu.leiden.aqosa.optimizer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.opt4j.core.Objectives;
import org.opt4j.core.problem.Evaluator;

import com.google.inject.Inject;

import edu.leiden.aqosa.evaluation.CostEval;
import edu.leiden.aqosa.evaluation.EvaluationThread;
import edu.leiden.aqosa.evaluation.PerformanceEval;
import edu.leiden.aqosa.evaluation.SafetyEval;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.objectives.BusUtilizationObjective;
import edu.leiden.aqosa.objectives.CostObjective;
import edu.leiden.aqosa.objectives.CpuUtilizationObjective;
import edu.leiden.aqosa.objectives.EvalObjective;
import edu.leiden.aqosa.objectives.ResponseTimeObjective;
import edu.leiden.aqosa.objectives.SafetyObjective;
import edu.leiden.aqosa.solution.ArchSolution;

/**
 * 
 * @author Ramin
 */
public class ArchEvaluator implements Evaluator<ArchSolution> {

	protected final ArchModel model;
	protected final List<EvalObjective> invalidFitness;

	@Inject
	public ArchEvaluator(ArchModel model) {
		this.model = model;
		this.invalidFitness = new ArrayList<EvalObjective>();
		
		if (model.getObjectives().hasResponseTime())		// Response Time
			invalidFitness.add(new ResponseTimeObjective(ResponseTimeObjective.MAX));
		if (model.getObjectives().hasCPUUtilization())		// CPU Utilization
			invalidFitness.add(new CpuUtilizationObjective(CpuUtilizationObjective.MAX));
		if (model.getObjectives().hasBusUtilization())		// Bus Utilization
			invalidFitness.add(new BusUtilizationObjective(BusUtilizationObjective.MAX));
		if (model.getObjectives().hasSafety())				// Safety
			invalidFitness.add(new SafetyObjective(SafetyObjective.MAX));
		if (model.getObjectives().hasCost())				// Cost
			invalidFitness.add(new CostObjective(CostObjective.MAX));
	}

	public Objectives evaluate(ArchSolution solution) {
		final List<EvalObjective> results = evaluation(solution);
		
		Objectives objs = new Objectives();
		for (EvalObjective eval : results) {
			objs.add(eval.getObjective(), eval.getValue());
		}
		return objs;
	}

	protected List<EvalObjective> evaluation(ArchSolution solution) {
		if (solution == null)
			return invalidFitness;
			
		List<EvaluationThread> threads = new ArrayList<EvaluationThread>();
		if (model.getObjectives().hasPerformance())
			threads.add(new PerformanceEval(model, solution));
		if (model.getObjectives().hasSafety())
			threads.add(new SafetyEval(model, solution));
		if (model.getObjectives().hasCost())
			threads.add(new CostEval(model, solution));
		
		ExecutorService executor = Executors.newFixedThreadPool(threads.size());
		try {
			List<Future<Collection<EvalObjective>>> returns = new ArrayList<Future<Collection<EvalObjective>>>();
			for (EvaluationThread thr : threads) {
				returns.add(executor.submit(thr));
			}
			
			List<EvalObjective> fitness = new ArrayList<EvalObjective>();
			for (Future<Collection<EvalObjective>> future : returns) {
				Collection<EvalObjective> result = future.get();
				// TODO check for consistency of results with objectives in the model 
				fitness.addAll(result);
			}
			
			executor.shutdown();
			return fitness;
			
		} catch (InterruptedException ex) {
			executor.shutdownNow();
			return invalidFitness;
			
		} catch (ExecutionException ex) {
			executor.shutdownNow();
			if (ex.getCause() instanceof InterruptedException)
				return invalidFitness;
			
			throw new RuntimeException(ex);
		}
		
	}
}
