package edu.leiden.aqosa.optimizer;

import org.opt4j.core.config.Icons;
import org.opt4j.core.config.annotations.Icon;
import org.opt4j.core.problem.ProblemModule;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.genes.GenotypeProvider;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.model.Configuration;
import edu.leiden.aqosa.model.IR.AQOSAModel;

/**
 * 
 * @author Ramin
 */
@Icon(Icons.PROBLEM)
public class ArchProblem extends ProblemModule {

	protected ArchModel model;
	protected Class<? extends ArchGenotype> genotypeClass;
	
	public void setModel(AQOSAModel model) {
		this.model = new ArchModel(model);
	}
	
	public void setModel(AQOSAModel model, Configuration configuration) {
		this.model = new ArchModel(model);
		this.model.setConfiguration(configuration);
	}
	
	public void setConfiguration(Configuration configuration) {
		if (model == null)
			return;
		model.setConfiguration(configuration);
	}
	
	public void setGenotypeClass(Class<? extends ArchGenotype> genotypeClass) {
		this.genotypeClass = genotypeClass;
	}
	
	public ArchModel getModel() {
		return model;
	}
	
	@Override
	protected void config() {
		bind(ArchModel.class).toInstance(model);
		
		GenotypeProvider provider = new GenotypeProvider(genotypeClass);
		bind(GenotypeProvider.class).toInstance(provider);
		
		bindProblem(ArchCreator.class, ArchDecoder.class, ArchEvaluator.class);
	}
	
}
