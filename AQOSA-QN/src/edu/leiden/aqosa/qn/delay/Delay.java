package edu.leiden.aqosa.qn.delay;

import edu.leiden.aqosa.qn.network.Customer;

public class Delay {
	protected DistributionSampler sampler;

	public Delay() {
	}

	public Delay(DistributionSampler s) {
		sampler = s;
	}

	public double sample() {
		return sampler.next();
	}

	public double sample(Customer c) {
		return sampler.next();
	}
}
