package edu.leiden.aqosa.qn.delay;

public class Deterministic implements DistributionSampler {
	private double time;

	public Deterministic(double t) {
		time = t;
	}

	public double next() {
		return time;
	}
}
