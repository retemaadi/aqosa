package edu.leiden.aqosa.qn.delay;

public interface DistributionSampler {
	public double next();
}
