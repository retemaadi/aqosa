package edu.leiden.aqosa.qn.network;

import edu.leiden.aqosa.qn.simulator.Simulator;

public class Customer {
	
	protected String type;
	private int priority;
	private double deadline;
	private double arrivalTime;
	private double serviceDemand;
	private double queueInsertionTime;
	private Node location = null;

	protected final Simulator simulator;
	
	public Customer(Simulator simulator) {
		this.simulator = simulator;
		priority = 0;
		deadline = 0;
		arrivalTime = simulator.now();
	}

	public Customer(Simulator simulator, String type) {
		this(simulator);
		this.type = type;
	}

	public Customer(Simulator simulator, String type, double deadline) {
		this(simulator, type);
		assert deadline >= 0 : "ERROR: Customer deadline out of bounds";
		this.deadline = deadline;
	}

	public Customer(Simulator simulator, String type, int priority) {
		this(simulator, type);
		assert priority >= 0 : "ERROR: Customer priority out of bounds"; 
		this.priority = priority;
	}

	public Customer(Simulator simulator, String type, int priority, double deadline) {
		this(simulator, type, priority);
		assert deadline >= 0 : "ERROR: Customer deadline out of bounds";
		this.deadline = deadline;
	}

	public String toString() {
		return ("Customer (class " + type + ", priority " + priority + ")");
	}

	public double getArrivalTime() {
		return arrivalTime;
	}

	public void setServiceDemand(double d) {
		serviceDemand = d;
	}

	public double getServiceDemand() {
		return serviceDemand;
	}

	public void setQueueInsertionTime(double d) {
		queueInsertionTime = d;
	}

	public void setQueueInsertionTime() {
		setQueueInsertionTime(simulator.now());
	}

	public double getQueueInsertionTime() {
		return queueInsertionTime;
	}

	public Node getLocation() {
		return location;
	}

	public void setLocation(Node n) {
		location = n;
	}

	public String getType() {
		return type;
	}

	public int getPriority() {
		return priority;
	}

	public double getDeadline() {
		return deadline;
	}
	
//	public boolean smallerThan(Customer e) {
//		return type <= e.getType();
//	}

}
