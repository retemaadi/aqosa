package edu.leiden.aqosa.qn.network;

import edu.leiden.aqosa.qn.delay.Delay;
import edu.leiden.aqosa.qn.simulator.Simulator;

public class DelayNode extends Node {
	protected Delay serviceTime;
	protected EndServiceEvent lastEndServiceEvent;

	public DelayNode(Simulator simulator, Delay d) {
		this(simulator, "Delay Node", d);
	}

	public DelayNode(Simulator simulator, String s, Delay d) {
		super(simulator, s);
		serviceTime = d;
	}

	//
	// Invokes a service delay when called. After the delay, the method
	// forward is called, which can be overridden to effect
	// special behaviors.
	//
	protected final void invokeService(Customer c) {
		double serveTime = c.getServiceDemand();
		lastEndServiceEvent = new EndServiceEvent(c, serveTime);
		simulator.schedule(lastEndServiceEvent);
	}

	class EndServiceEvent extends Event {
		Customer customer;

		public EndServiceEvent(Customer c, double t) {
			super(simulator, t, false);
			customer = c;
		}

		public void invoke() throws InterruptedException {
			forward(customer);
		}

		public Customer getCustomer() {
			return customer;
		}
	}

	//
	// The service demand is set on entry as the customer may be preempted
	// by a subclass. In preemptive-resume strategies, this case the
	// demand needs to be reduced to reflect the residual service time.
	// This method is finalized - subclasses should modify behavior via
	// accept().
	//
	public final void enter(Customer c) throws InterruptedException {
		c.setServiceDemand(serviceTime.sample(c));
		super.enter(c);
	}

	//
	// Overrides superclass method. An arriving customer is now subject to
	// delay.
	//
	protected void accept(Customer c) throws InterruptedException {
		invokeService(c);
	}

}
