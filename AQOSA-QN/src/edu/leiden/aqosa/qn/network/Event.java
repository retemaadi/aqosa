package edu.leiden.aqosa.qn.network;

import edu.leiden.aqosa.qn.simulator.Simulator;

public abstract class Event {
	protected double invokeTime;

	public Event(Simulator simulator, double time, boolean absolute) {
		if (absolute)
			invokeTime = time;
		invokeTime = simulator.now() + time;
	}

	public abstract void invoke() throws InterruptedException;
	
	public double getInvokeTime() {
		return invokeTime;
	}
}
