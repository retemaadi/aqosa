package edu.leiden.aqosa.qn.network;

public class Link {
	protected Node node;

	public Link() {
		this.node = null;
	}
	
	public Link(Node n) {
		this.node = n;
	}

	//
	// Can be called by subclasses that have more than one target node
	//
	protected void send(Customer c, Node n) throws InterruptedException {
		n.enter(c);
	}

	//
	// Can be overridden in subclasses that have more than one target node
	//
	protected void move(Customer c) throws InterruptedException {
		send(c, node);
	}

}
