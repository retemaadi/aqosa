package edu.leiden.aqosa.qn.network;

import edu.leiden.aqosa.qn.simulator.LogType;
import edu.leiden.aqosa.qn.simulator.Simulator;

public class Node {
	protected String name;
	protected Link link = null;
	protected int arrivals = 0;

	protected final Simulator simulator;
	
	public Node(Simulator simulator) {
		this(simulator, "BASE Node");
	}

	public Node(Simulator simulator, String s) {
		this.simulator = simulator; 
		name = s;
		simulator.addNode(this);
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return name;
	}

	public void setLink(Link r) {
		link = r;
	}

	public void logResults() {
		// Simulator.logResult(name + " arrivals", arrivals);
		simulator.logResult(LogType.NODE_ARRIVALS, name, arrivals);
	}

	//
	// Customers enter from the outside, are then processed by accept
	// and then forwarded to the next node by forward.
	// These methods can variously be overridden to effect different
	// Behavior.
	//
	public void enter(Customer c) throws InterruptedException {
		arrivals++;
		c.setLocation(this);
		accept(c);
	}

	protected void accept(Customer c) throws InterruptedException {
		forward(c);
	}

	protected void forward(Customer c) throws InterruptedException {
		link.move(c);
	}

}
