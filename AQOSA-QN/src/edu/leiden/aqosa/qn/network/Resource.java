package edu.leiden.aqosa.qn.network;

import edu.leiden.aqosa.qn.simulator.Simulator;
import edu.leiden.aqosa.qn.simulator.TimebasedMeasure;

public class Resource {
	boolean resourceAvailable;
	TimebasedMeasure resourceCount;
	int resources;
	int nresources;

	public Resource(Simulator simulator) {
		this(simulator, 1);
	}

	public Resource(Simulator simulator, int n) {
		resourceCount = new TimebasedMeasure(simulator);
		resources = n;
		nresources = n;
	}

	public void claim() {
		assert resourceIsAvailable() : "Attempt to claim unavailable resource";
		resources--;
		resourceCount.add((float) (nresources - resources));
	}

	public void release() {
		assert resources < nresources : "Attempt to release non-existent resource " + resources	+ " " + nresources;
		resources++;
		resourceCount.add((float) (nresources - resources));
	}

	public int numberOfAvailableResources() {
		return resources;
	}

	public boolean resourceIsAvailable() {
		return resources > 0;
	}

	public double utilisation() {
		return resourceCount.mean() / nresources;
	}

}
