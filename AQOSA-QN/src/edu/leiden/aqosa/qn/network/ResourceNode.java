package edu.leiden.aqosa.qn.network;

import edu.leiden.aqosa.qn.delay.Delay;
import edu.leiden.aqosa.qn.queue.FIFOQueue;
import edu.leiden.aqosa.qn.queue.Queue;
import edu.leiden.aqosa.qn.simulator.LogType;
import edu.leiden.aqosa.qn.simulator.Simulator;

public class ResourceNode extends DelayNode {
	protected int noOfResources;
	protected Queue queue;
	protected Resource resources;
	protected int losses = 0;
	protected Node lossNode = null;

	public ResourceNode(Simulator simulator, String s, Delay d, int n) {
		this(simulator, s, d, n, new FIFOQueue(simulator));
	}

	public ResourceNode(Simulator simulator, String s, Delay d, int n, Queue q) {
		super(simulator, s, d);
		noOfResources = n;
		queue = q;
		resources = new Resource(simulator, noOfResources);
	}

	public void setLossNode(Node n) {
		lossNode = n;
	}

	@Override
	public String toString() {
		return name + ", resources = " + resources.numberOfAvailableResources()	+ ", queue length = " + queue.queueLength();
	}

	//
	// Overrides superclass method.
	// Customers queue for resources, if there are none available.
	// If the queue is full customers are routed to the loss node.
	//
	@Override
	protected void accept(Customer c) throws InterruptedException {
		if (resources.resourceIsAvailable()) {
			resources.claim();
			invokeService(c);
		} else {
			if (queue.canAccept(c)) {
				queue.enqueue(c);
			} else {
				losses++;
				lossNode.enter(c);
			}
		}
	}

	@Override
	protected void forward(Customer c) throws InterruptedException {
		super.forward(c);
		releaseResource();
	}
	
	//
	// A released resource is allocated to the next queued customer, if
	// there is one.
	//
	public void releaseResource() {
		if (!queue.isEmpty()) {
			Customer c = queue.dequeue();
			invokeService(c);
		} else {
			resources.release();
		}
	}

	public int queueLength() {
		return queue.queueLength();
	}

	@Override
	public void logResults() {
		super.logResults();
		// Simulator.logResult(name + ", Server utilisation", resources.utilisation());
		simulator.logResult(LogType.NODE_UTILIZATION, name, resources.utilisation());
		// Simulator.logResult(name + ", Mean number of customers in queue", queue.meanQueueLength());
		simulator.logResult(LogType.NODE_QUEUELENGTH, name, queue.meanQueueLength());
		// Simulator.logResult(name + ", Variance of number of customers in queue", queue.varQueueLength());
		// Simulator.logResult(name + ", Conditional mean queueing time", queue.meanTimeInQueue());
		simulator.logResult(LogType.NODE_QUEUETIME, name, queue.meanTimeInQueue());
		// Simulator.logResult(name + ", Conditional variance of queueing time", queue.varTimeInQueue());
		// Simulator.logResult(name + ", Losses", losses);
		simulator.logResult(LogType.NODE_LOSSES, name, losses);
		// Simulator.logResult(name + ", Proportion of customers lost", (double) losses / (double) arrivals);
		simulator.logResult(LogType.NODE_LOSS_PROBABILITY, name, (double) losses / (double) arrivals);
	}
}
