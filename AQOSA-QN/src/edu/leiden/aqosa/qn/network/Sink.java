package edu.leiden.aqosa.qn.network;

import edu.leiden.aqosa.qn.simulator.Simulator;

/**
 * A Sink node absrobs customers from a queueing network. Departing customers
 * are registered with the {@link Network} class, which records the customer's
 * sojourn time.
 * 
 * @param name
 *            The name of the source node
 * @param d
 *            The {@link DistributionSampler} used to generate the inter-arrival
 *            times
 * @param b
 *            The {@link DistributionSampler} used to generate the batch sizes
 */
public class Sink extends Node {
	
	public Sink(Simulator simulator) {
		this(simulator, "Sink");
	}

	public Sink(Simulator simulator, String name) {
		super(simulator, name);
	}

	//
	// Do nothing here - customer is absorbed...
	//
	protected void accept(Customer c) {
		simulator.registerCompletion(c);
	}

}
