package edu.leiden.aqosa.qn.network;

import edu.leiden.aqosa.qn.delay.Deterministic;
import edu.leiden.aqosa.qn.delay.DistributionSampler;
import edu.leiden.aqosa.qn.simulator.Simulator;

/**
 * A Source node injects customers into a queueing network. The inter-arrival
 * time (a {@link DistributionSampler}) must be specified. Arrivals may
 * optionally be batched, with the batch size specified by a second
 * {@link DistributionSampler}.
 */
public class Source extends Node {
	protected DistributionSampler delay;
	protected DistributionSampler batchsize;

	/**
	 * @param name
	 *            The name of the source node
	 * @param d
	 *            The {@link DistributionSampler} used to generate the
	 *            inter-arrival times
	 */
	public Source(Simulator simulator, String name, DistributionSampler d) {
		this(simulator, name, d, new Deterministic(1));
	}

	/**
	 * @param name
	 *            The name of the source node
	 * @param d
	 *            The {@link DistributionSampler} used to generate the
	 *            inter-arrival times
	 * @param b
	 *            The {@link DistributionSampler} used to generate the batch
	 *            sizes
	 */
	public Source(Simulator simulator, String name, DistributionSampler d, DistributionSampler b) {
		super(simulator, name);
		delay = d;
		batchsize = b;
		simulator.schedule(new Arrival(delay.next()));
	}

	/**
	 * Builds a new customer. This can be overridden to support specialised
	 * {@link Customer} subclasses.
	 * 
	 * @return a customer
	 * 
	 */
	protected Customer buildCustomer() {
		return new Customer(simulator);
	}

	/**
	 * Injects customers into the network using forward. The initial location of
	 * the customer is the source node.
	 * @throws InterruptedException 
	 */
	protected void injectCustomer() throws InterruptedException {
		Customer c = buildCustomer();
		c.setLocation(this);
		forward(c);
	}

	protected void injectCustomers() throws InterruptedException {
		int nArrivals = (int) batchsize.next();
		for (int i = 0; i < nArrivals; i++) {
			injectCustomer();
		}
	}

	/**
	 * One arrival prompts the next...
	 */
	class Arrival extends Event {
		public Arrival(double time) {
			super(simulator, time, false);
		}

		public void invoke() throws InterruptedException {
			injectCustomers();
			simulator.schedule(new Arrival(delay.next()));
		}
	}

}
