package edu.leiden.aqosa.qn.queue;

import edu.leiden.aqosa.qn.network.Event;

public class Diary extends List {
	
	public boolean before(Object x, Object y) {
		return (((Event) x).getInvokeTime() <= ((Event) y).getInvokeTime());
	}

	public void insertInOrder(Event e) {
		ListIterator it = this.getIterator();
		while (it.canAdvance() && before(it.getValue(), e)) {
			it.advance();
		}
		it.add(e);
	}

	public void remove(Event e) {
		super.remove(e);
	}

	public Event removeFromFront() {
		Event e = (Event) super.removeFromFront();
		return e;
	}
}
