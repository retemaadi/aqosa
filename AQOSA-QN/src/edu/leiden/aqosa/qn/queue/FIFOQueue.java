package edu.leiden.aqosa.qn.queue;

import edu.leiden.aqosa.qn.network.Customer;
import edu.leiden.aqosa.qn.simulator.Simulator;

public class FIFOQueue extends Queue {
	private List q = new List("FIFO Queue");

	public FIFOQueue(Simulator simulator) {
		super(simulator);
	}

	public FIFOQueue(Simulator simulator, int cap) {
		super(simulator, cap);
	}

	protected void insertIntoQueue(Customer e) {
		q.insertAtBack(e);
	}

	protected void insertAtHeadOfQueue(Customer e) {
		q.insertAtFront(e);
	}

	protected Customer headOfQueue() {
		return (Customer) q.first();
	}

	protected Customer removeFromQueue() {
		return (Customer) q.removeFromFront();
	}

}
