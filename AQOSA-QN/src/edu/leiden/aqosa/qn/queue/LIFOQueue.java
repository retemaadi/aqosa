package edu.leiden.aqosa.qn.queue;

import edu.leiden.aqosa.qn.network.Customer;
import edu.leiden.aqosa.qn.simulator.Simulator;

public class LIFOQueue extends Queue {
	private List q = new List("LIFO Queue");

	public LIFOQueue(Simulator simulator) {
		super(simulator);
	}

	public LIFOQueue(Simulator simulator, int cap) {
		super(simulator, cap);
	}

	protected void insertIntoQueue(Customer e) {
		q.insertAtFront(e);
	}

	protected void insertAtHeadOfQueue(Customer e) {
		q.insertAtFront(e);
	}

	protected Customer headOfQueue() {
		return (Customer) q.first();
	}

	protected Customer removeFromQueue() {
		return (Customer) q.removeFromFront();
	}

}
