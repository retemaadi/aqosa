package edu.leiden.aqosa.qn.queue;

import edu.leiden.aqosa.qn.network.Customer;
import edu.leiden.aqosa.qn.simulator.Simulator;

public class PriorityQueue extends Queue {
	protected int nqueues;
	protected Queue[] qs;

	//
	// Customer priorities must be 0, 1, .., nqueues-1
	// Priority 0 is the highest priority
	//
	public PriorityQueue(Simulator simulator, int n) {
		super(simulator);
		nqueues = n;
		buildPriorityQueue(simulator);
	}

	//
	// By default each individual queue is FIFO...
	//
	protected Queue buildOneQueue(Simulator simulator) {
		return new FIFOQueue(simulator);
	}

	void buildPriorityQueue(Simulator simulator) {
		qs = new Queue[nqueues];
		for (int i = 0; i < nqueues; i++) {
			qs[i] = buildOneQueue(simulator);
		}
	}

	//
	// Overrides superclass method.
	//
	public boolean canAccept(Customer c) {
		return qs[c.getPriority()].canAccept(c);
	}

	//
	// Define superclass abstract methods...
	//

	protected void insertIntoQueue(Customer e) {
		int priority = e.getPriority();
		qs[priority].enqueue(e);
	}

	protected void insertAtHeadOfQueue(Customer e) {
		int priority = e.getPriority();
		qs[priority].enqueueAtHead(e);
	}

	protected Customer headOfQueue() {
		for (int i = 0; i < nqueues; i++) {
			if (qs[i].queueLength() > 0) {
				return qs[i].head();
			}
		}
		assert false : "Priority queue - all queues empty during head\n (This cannot happen!)";
		return null;
	}

	protected Customer removeFromQueue() {
		for (int i = 0; i < nqueues; i++) {
			if (qs[i].queueLength() > 0) {
				return qs[i].dequeue();
			}
		}
		assert false : "Priority queue - all queues empty during remove\n (This cannot happen!)";
		return null;
	}

}
