package edu.leiden.aqosa.qn.queue;

import edu.leiden.aqosa.qn.network.Customer;
import edu.leiden.aqosa.qn.simulator.SimpleMeasure;
import edu.leiden.aqosa.qn.simulator.Simulator;
import edu.leiden.aqosa.qn.simulator.TimebasedMeasure;

public abstract class Queue {
	protected int pop = 0;
	private SimpleMeasure queueingTime = new SimpleMeasure();
	private TimebasedMeasure popMeasure;
	private int capacity;

	protected final Simulator simulator;
	
	public Queue(Simulator simulator) {
		this.simulator = simulator;
		capacity = Integer.MAX_VALUE;
		popMeasure = new TimebasedMeasure(simulator);
	}

	public Queue(Simulator simulator, int cap) {
		this.simulator = simulator;
		capacity = cap;
		popMeasure = new TimebasedMeasure(simulator);
	}

	public int getCapacity() {
		return capacity;
	}

	public boolean isInfinite() {
		return (capacity == Integer.MAX_VALUE);
	}

	public boolean isEmpty() {
		return (pop == 0);
	}

	public boolean canAccept(Customer c) {
		return pop < capacity;
	}

	public int queueLength() {
		return pop;
	}

	public void enqueue(Customer c) {
		assert canAccept(c) : "Attempt to add to a full queue"; 
		c.setQueueInsertionTime();
		insertIntoQueue(c);
		pop++;
		popMeasure.add((float) pop);
	}

	//
	// The check isn't necessary as this can only be called after preemption
	// i.e. after an arrival; the arrival will have checked the queue
	// for spare capacity
	//
	public void enqueueAtHead(Customer c) {
		assert canAccept(c) : "Attempt to add to a full queue"; 
		c.setQueueInsertionTime();
		insertAtHeadOfQueue(c);
		pop++;
		popMeasure.add((float) pop);
	}

	public Customer head() {
		assert pop > 0 : "Attempt to take the head of an empty queue"; 
		Customer c = headOfQueue();
		return c;
	}

	public Customer dequeue() {
		assert pop > 0 : "Attempt to dequeue an empty queue!"; 
		Customer c = removeFromQueue();
		pop--;
		popMeasure.add((float) pop);
		queueingTime.add(simulator.now() - c.getQueueInsertionTime());
		return c;
	}

	/**
	 * These abstract methods allow different queueing disciplines to be
	 * supported - see the various subclasses
	 */

	protected abstract void insertIntoQueue(Customer o);

	protected abstract void insertAtHeadOfQueue(Customer o);

	protected abstract Customer headOfQueue();

	protected abstract Customer removeFromQueue();

	/**
	 * Generic measures
	 */
	public double meanQueueLength() {
		return popMeasure.mean();
	}

	public double varQueueLength() {
		return popMeasure.variance();
	}

	public double meanTimeInQueue() {
		return queueingTime.mean();
	}

	public double varTimeInQueue() {
		return queueingTime.variance();
	}
}
