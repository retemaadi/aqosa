package edu.leiden.aqosa.qn.simulator;

public enum LogType {
	COMPLETION_TIME("Completion Time"),
	COMPLETED_CUTOMERS("Completed Customers"),
	MEANTIME_NETWORK("Mean Time in Network"),
	VARIANCETIME_NETWORK("Vari. Time in Network"),
	
	NODE_ARRIVALS("_ID_ Num. of Arrivals"),
	NODE_UTILIZATION("_ID_ Utilisation"),
	NODE_QUEUELENGTH("_ID_ Queue Length"),
	NODE_QUEUETIME("_ID_ Queue Time"),
	NODE_LOSSES("_ID_ Num. of Losses"),
	NODE_LOSS_PROBABILITY("_ID_ Chance of Loss"),
	
	CUSTOMERTYPE_COMPLETED("_ID_ Completed"),
	CUSTOMERTYPE_MISSED("_ID_ Missed Deadline"),
	CUSTOMERTYPE_MEANTIME("_ID_ Mean Time"),
	CUSTOMERTYPE_VARIANCETIME("_ID_ Vari. Time"),
	;
	
	private String title;
	private LogType(String title) {
		this.title = title;
	}
	
	@Override
	public String toString() {
		return title;
	}
	
	public String toString(String id) {
		return title.replaceAll("_ID_", id);
	}
}
