package edu.leiden.aqosa.qn.simulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Logger {

	private final double CONFIDENCE_LEVEL = 0.05;
	private static class ConfidenceLevel {
		static double[] t01 = { 1000000.0, 31.82, 6.92, 4.54, 3.75, 3.36, 3.14,
				3.00, 2.90, 2.82, 2.76, 2.72, 2.68, 2.65, 2.62, 2.60, 2.58,
				2.57, 2.55, 2.54, 2.53 };
		static double[] t025 = { 1000000.0, 12.71, 4.3, 3.18, 2.78, 2.57, 2.45,
				2.36, 2.31, 2.26, 2.23, 2.20, 2.18, 2.16, 2.14, 2.13, 2.12,
				2.11, 2.10, 2.09, 2.09 };
		static double[] t05 = { 1000000.0, 6.31, 2.92, 2.35, 2.13, 2.02, 1.94,
				1.90, 1.86, 1.83, 1.81, 1.80, 1.78, 1.77, 1.76, 1.75, 1.75,
				1.74, 1.73, 1.73, 1.72 };
		static double[] t10 = { 1000000.0, 3.08, 1.89, 1.64, 1.53, 1.48, 1.44,
				1.42, 1.40, 1.38, 1.37, 1.36, 1.36, 1.35, 1.34, 1.34, 1.34,
				1.33, 1.33, 1.33, 1.32 };

		static double table(int dof, double alpha) {
			if (Math.abs(alpha - 0.01) < 0.00001) {
				return t01[dof];
			} else if (Math.abs(alpha - 0.025) < 0.00001) {
				return t025[dof];
			} else if (Math.abs(alpha - 0.05) < 0.00001) {
				return t05[dof];
			} else if (Math.abs(alpha - 0.1) < 0.00001) {
				return t10[dof];
			} else {
				return 0.0;
			}
		}
	}

	private Map<String, List<Double>> values = new HashMap<String, List<Double>>();

	public void logResult(LogType log, double value) {
		logResult(log.toString(), value);
	}
	
	public void logResult(LogType log, String id, double value) {
		logResult(log.toString(id), value);
	}
	
	private void logResult(String id, double value) {
		List<Double> valueList = values.get(id);
		if (valueList == null)
			valueList = new ArrayList<Double>();
		valueList.add(value);
		values.put(id, valueList);
	}

	public double sampleMean(List<Double> valueList) {
		double acc = 0.0;
		if (valueList == null)
			return acc;
		for (int i = 0; i < valueList.size(); i++) {
			acc += valueList.get(i);
		}
		return acc / valueList.size();
	}

	public double sampleVariance(List<Double> valueList, double mean) {
		double acc = 0.0;
		if (valueList == null)
			return acc;
		for (int i = 0; i < valueList.size(); i++) {
			double diff = valueList.get(i) - mean;
			acc += diff * diff;
		}
		return acc / (valueList.size() - 1);
	}

	public double getResult(LogType log) {
		return getResult(log.toString());
	}
	
	public double getResult(LogType log, String id) {
		return getResult(log.toString(id));
	}
	
	private double getResult(String id) {
		List<Double> valueList = values.get(id);
		return sampleMean(valueList);
	}
	
	public void displayResults() {
		displayResults(false);
	}
	
	public void displayResults(boolean detailed) {
		if (values.size() > 0) {
			System.out.println("\nSUMMARY OF STATISTICS");
			System.out.println("========================\n");
			
			if (detailed)
				displayResultsDetailed();
			else
				displayResultsSimple();
		} else {
			System.out.println("No results were logged");
		}
	}
	
	private void displayResultsSimple() {
		for (String id : values.keySet())
			System.out.println(id + ": \t" + getResult(id));
	}
	
	private void displayResultsDetailed() {
		for (String id : values.keySet()) {
			List<Double> valueList = values.get(id);
			double mean = sampleMean(valueList);
			System.out.println(id);
			System.out.println("\t Mean value: \t" + mean);
			int n = valueList.size();
			if (n > 1) {
				System.out.println("\t Degrees of freedom: \t" + (n - 1));

				double s = Math.sqrt(sampleVariance(valueList, mean));
				double z = ConfidenceLevel.table(n - 1, CONFIDENCE_LEVEL);
				if (z > 0.0)
					System.out.println("\t C.I. half width: \t" + z * s / Math.sqrt((double) n));
			}

			System.out.println();
		}
	}
}
