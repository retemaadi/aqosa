package edu.leiden.aqosa.qn.simulator;

public class SimpleMeasure {

	private int n = 0;
	private double sum = 0.0;
	private double sumSquare = 0.0;
	
	public void add(double x) {
		n++;
		sum += x;
		sumSquare += x * x;
	}

	public int count() {
		return n;
	}
	
	public double mean() {
		return sum / n;
	}

	public double variance() {
		double mean = this.mean();
		return (sumSquare - n * mean * mean) / (n - 1);
	}
}
