package edu.leiden.aqosa.qn.simulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.leiden.aqosa.qn.network.Customer;
import edu.leiden.aqosa.qn.network.Event;
import edu.leiden.aqosa.qn.network.Node;
import edu.leiden.aqosa.qn.queue.Diary;

public class Simulator {
	
	public enum StopType {
		StopByTime,
		StopByCustomer,
	}
	
	private List<Node> nodes = new ArrayList<Node>();
	private Diary diary;
	private Logger logger;
	private SimpleMeasure networkMeasure;
	private Map<String, SimpleMeasure> customerMeasures;
	private Map<String, Integer> missedDeadline;
	
	private double time;
	private StopType stopType;
	private double stopParam;
	
	public Simulator(StopType sType, double stopCounter) {
		diary = new Diary();
		logger = new Logger();
		networkMeasure = new SimpleMeasure();
		customerMeasures = new HashMap<String, SimpleMeasure>();
		missedDeadline = new HashMap<String, Integer>();
		
		time = 0.0;
		stopType = sType;
		stopParam = stopCounter;
	}
	
	public void go() throws InterruptedException {
		while (!diary.isEmpty() && !stop()) {
			Event e = (Event) diary.removeFromFront();
			time = e.getInvokeTime();
			if (!stop())
				e.invoke();
		}
	}
	
	public void insertEvent(Event e) {
		diary.insertInOrder(e);
	}
	
	public boolean stop() {
		switch (stopType) {
		case StopByTime:
			return time > stopParam;
		case StopByCustomer:
			return networkMeasure.count() > stopParam;
		}
		return true;
	}
	
	public void simulate() throws InterruptedException {
		go();
		
		logResult(LogType.COMPLETION_TIME, now());
		logResult(LogType.COMPLETED_CUTOMERS, networkMeasure.count());
		logResult(LogType.MEANTIME_NETWORK, networkMeasure.mean());
		logResult(LogType.VARIANCETIME_NETWORK, networkMeasure.variance());
		for (Node n : nodes) {
			n.logResults();
		}
		for (String key : customerMeasures.keySet()) {
			SimpleMeasure measure = customerMeasures.get(key);
			
			Integer missed = missedDeadline.get(key);
			if (missed == null)
				missed = new Integer(0);
			
			logResult(LogType.CUSTOMERTYPE_COMPLETED, key, measure.count());
			logResult(LogType.CUSTOMERTYPE_MISSED, key, missed);
			logResult(LogType.CUSTOMERTYPE_MEANTIME, key, measure.mean());
			logResult(LogType.CUSTOMERTYPE_VARIANCETIME, key, measure.variance());
		}
	}

	public double now() {
		return time;
	}
	
	public void schedule(Event e) {
		insertEvent(e);
	}
	
	public void addNode(Node n) {
		nodes.add(n);
	}
	
	public void registerCompletion(Customer c) {
		// Total Network
		double time = now() - c.getArrivalTime();
		networkMeasure.add(time);
		
		// CustomerType
		String key = c.getType();
		SimpleMeasure measure = customerMeasures.get(key);
		if (measure == null)
			measure = new SimpleMeasure();
		
		measure.add(time);
		customerMeasures.put(key, measure);
		
		// Missed deadlines
		double deadline = c.getDeadline();
		if (deadline > 0 && time > deadline) {
			Integer missed = missedDeadline.get(key);
			if (missed == null)
				missed = new Integer(0);
			
			missed++;
			missedDeadline.put(key, missed);
		}
	}
	
	protected void logResult(LogType log, double value) {
		logger.logResult(log, value);
	}
	
	public void logResult(LogType log, String id, double value) {
		logger.logResult(log, id, value);
	}
	
	public double getResult(LogType log) {
		return logger.getResult(log);
	}
	
	public double getResult(LogType log, String id) {
		return logger.getResult(log, id);
	}
	
	public void displayResults() {
		logger.displayResults(false);
	}
	
	public void displayResults(boolean detailed) {
		logger.displayResults(detailed);
	}
	
}
