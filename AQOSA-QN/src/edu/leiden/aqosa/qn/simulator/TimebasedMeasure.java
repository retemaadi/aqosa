package edu.leiden.aqosa.qn.simulator;

public class TimebasedMeasure {
	
	private double lastChange = 0.0;
	private double current = 0.0;	
	private double sum = 0.0;
	private double sumSquare = 0.0;

	private final Simulator simulator;
	
	public TimebasedMeasure(Simulator simulator) {
		this.simulator = simulator;
	}
	
	public void add(double x) {
		sum += current * (simulator.now() - lastChange);
		sumSquare += (current * current) * (simulator.now() - lastChange);
		current = x;
		lastChange = simulator.now();
	}

	public double mean() {
		return sum / simulator.now();
	}

	public double variance() {
		double mean = this.mean();
		return sumSquare / simulator.now() - mean * mean;
	}
}
