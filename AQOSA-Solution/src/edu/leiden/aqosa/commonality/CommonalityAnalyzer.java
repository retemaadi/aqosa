package edu.leiden.aqosa.commonality;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import edu.leiden.aqosa.solution.ArchSolution;
import edu.leiden.aqosa.solution.Individuals;
import edu.leiden.aqosa.solution.Quality;

public class CommonalityAnalyzer {

	protected final List<Individuals> fronts;
	
	public CommonalityAnalyzer(List<Individuals> fronts) {
		this.fronts = fronts;
	}
	
	public int analyze(int frontNo, int delta, int omega, boolean considerBus) {
		Individuals mainFront = fronts.get(frontNo);
		List<Individuals> otherFronts = new ArrayList<Individuals>(fronts);
		otherFronts.remove(frontNo);
		
		int counter = 0;
		
		nextSolution:
		for (Entry<ArchSolution, Quality> ind : mainFront) {
			ArchSolution solution = ind.getKey();
			
			nextFront:
			for (Individuals nextFront : otherFronts) {
				
				for (Entry<ArchSolution, Quality> ind2 : nextFront) {
					ArchSolution solution2 = ind2.getKey();
					
					if (distance(solution, solution2, omega, considerBus) < delta) {
						break nextFront;
					}
				}
				
				break nextSolution;
			}
			
			// If reach here, it is a common solution!
			counter++;
//			System.out.println(solution);
		}
		
		return counter;
	}
	
	protected int distance(ArchSolution s1, ArchSolution s2, int omega, boolean considerBus) {
		if (s1 == null || s2 == null) {
			return -1;
		}
		
		int distanceProc = distanceProcessor(s1, s2, omega);
		return (considerBus ? distanceProc + distanceBus(s1, s2, omega) : distanceProc);
	}
	
	protected int distanceProcessor(ArchSolution s1, ArchSolution s2, int omega) {
		return calcDistance(s1.getSortedProcessorsClocks(), s2.getSortedProcessorsClocks(), omega);
	}
	
	protected int distanceBus(ArchSolution s1, ArchSolution s2, int omega) {
		return calcDistance(s1.getSortedBusesBandwidths(), s2.getSortedBusesBandwidths(), omega);
	}
	
	protected int calcDistance(List<Double> l1, List<Double> l2, int omega) {
		List<Double> longerList = new ArrayList<Double>(l1);		
		if (l1.size() < l2.size()) {
			longerList = new ArrayList<Double>(l2);
			l2 = new ArrayList<Double>(l1);
			l1 = new ArrayList<Double>(longerList);
		}
			
		for (Double d : longerList) {
			if ( l2.contains(d) ) {
				l1.remove(d);
				l2.remove(d);
			}
		}
		
		int distanceReplace = l2.size();
		int distanceAdd = l1.size() - l2.size();
		return distanceReplace + distanceAdd * omega;
	}
}
