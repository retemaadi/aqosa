package edu.leiden.aqosa.genes;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.opt4j.core.Genotype;
import org.opt4j.core.genotype.CompositeGenotype;

import edu.leiden.aqosa.model.ArchModel;

/**
 * 
 * @author Ramin
 */

public class ArchGenotype extends CompositeGenotype<Integer, ArchGenome> {

	protected final ArchModel model;
	protected final int noComponents;
	protected final int noDuplicates;
	

	public ArchGenotype(ArchModel model) {
		this.model = model;
		this.noComponents = model.getScenario().getNoComponents();
		this.noDuplicates = model.getObjectives().getNoDuplicate();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <G extends Genotype> G newInstance() {
		try {
			Constructor<? extends ArchGenotype> cstr = this.getClass().getConstructor(ArchModel.class);
			ArchGenotype gene = (ArchGenotype) cstr.newInstance(model);
			
			return (G) gene;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void init(Random random) {
		for (int i = 0; i < noComponents; i++) {
			List<String> instances = model.getRepository().getInstances(i);
			ComponentGenotype compGenotype = new ComponentGenotype(instances);
			compGenotype.init(random);
			put(i, compGenotype);
		}
		
		DeployGenotype deployGenotype = new DeployGenotype(noComponents, noDuplicates);
		deployGenotype.init(random);
		put(noComponents, deployGenotype);

		NodesGenotype nodesGenotype = new NodesGenotype(model.getRepository().getCPUs());
		nodesGenotype.init(random, noComponents);
		put(noComponents+1, nodesGenotype);

		EdgesGenotype edgesGenotype = new EdgesGenotype(model.getRepository().getBuses());
		edgesGenotype.init(random, noComponents);
		put(noComponents+2, edgesGenotype);
		
		ConnectionGenotype connectionGenotype = new ConnectionGenotype();
		connectionGenotype.init(random, noComponents);
		put(noComponents+3, connectionGenotype);
	}
	
	public int getNoComponents() {
		return noComponents;
	}
	
	public ComponentGenotype getComponent(int comp) {
		assert comp < noComponents;
		return get(comp);
	}

	public DeployGenotype getDeploy() {
		return get(noComponents);
	}

	public NodesGenotype getNodes() {
		return get(noComponents+1);
	}

	public EdgesGenotype getEdges() {
		return get(noComponents+2);
	}

	public ConnectionGenotype getConnections() {
		return get(noComponents+3);
	}

	/*
	 * for bus2bus usage
	 */
	/*
	private boolean isConnected2Bus(int dst, int busno, boolean start) {
		Integer n = getDeploy().getNodes();
		ConnectionGenotype connections = getConnections();
		List<Boolean> bus = connections.getBus2Nodes(n).get(busno);
		if (bus.get(dst))
			return true;

		List<Boolean> busLink = connections.getBus2Buses().get(busno);
		for (int i = (start ? 0 : busno + 1); i < busLink.size(); i++)
			if (busLink.get(i))
				if (isConnected2Bus(dst, i, false))
					return true;
		
		return false;
	}
	*/

	private boolean isConnected(int src, int dst) throws Exception {
		Integer nodes = getDeploy().getNodes();
		ConnectionGenotype connections = getConnections();
		List<List<Boolean>> bus2nodes = connections.getBus2Nodes(nodes);
		
		for (int i = 0; i < bus2nodes.size(); i++) {
			List<Boolean> busConn = bus2nodes.get(i);
			
			/*
			 * for bus2bus usage
			 */
			/*
			if (busConn.get(src))
				if (isConnected2Bus(dst, i, true))
					return true;
			*/
			
			try {
				if (busConn.get(src))
					if (busConn.get(dst))
						return true;
			} catch (Exception e) {
				System.err.println("Src: " + src);
				System.err.println("Dst: " + dst);
				System.err.println("BusSize: " + busConn.size());
				System.err.print("Bus: ");
				for (Boolean b : busConn)
					System.err.print(b + " ");
				System.err.println("");
				throw e;
			}
				
		}
		
		return false;
	}

	private boolean isConnected(List<Integer> deployNode1, List<Integer> deployNode2) throws Exception {
		for (Integer src : deployNode1) {
			src -= 1;
			for (Integer dst: deployNode2) {
				dst -= 1;
				if (src == dst)
					continue;
				if (!isConnected(src, dst))
					return false;
			}
		}
		
		return true;
	}
	
	public boolean validate() {
		DeployGenotype deployGene = getDeploy();
		ConnectionGenotype connectionsGene = getConnections();
		
		Integer nodes = deployGene.getNodes();
		Integer edges = connectionsGene.getEdges();
		List<List<Integer>> deploy = deployGene.list();
		
		try {
			// Check the compatibility between 4 genes
			Integer maxEdges = nodes * (nodes - 1) / 2;
			if (edges > maxEdges)
				return false;

			// Check if deploy genotype contains fills all the nodes
			List<Integer> allDeploy = new ArrayList<Integer>();
			for (List<Integer> d : deploy)
				allDeploy.addAll(d);
			for (int i = 1; i <= nodes; i++)
				if (!allDeploy.contains(i))
					return false;
			
			// Check if deploy arrays contains unique nodes
			for (List<Integer> d : deploy) {
				for (int i = 0; i < d.size(); i++) {
					List<Integer> de = new ArrayList<Integer>();
					de.addAll(d);
					Integer uni = de.get(i);
					de.remove(i);
					
					if (de.contains(uni))
						return false;
				}
			}
			
			// Check if each bus is proper connected
			List<List<Boolean>> bus2nodes = connectionsGene.getBus2Nodes(nodes);
			List<List<Boolean>> bus2buses = connectionsGene.getBus2Buses();
			for (int i = 0; i < edges; i++) {
				int connectedPorts = 0;
				for (int j = 0; j < nodes; j++)
					if (bus2nodes.get(i).get(j))
						connectedPorts++;
				for (int j = 0; j < edges; j++)
					if (bus2buses.get(i).get(j))
						connectedPorts++;

				if (connectedPorts < 2)
					return false;
			}

			// Check if the needed connections between components exist.
			List<List<Boolean>> callMatrix = model.getScenario().getCallMatrix();
			for (int i = 0; i < nodes; i++) {
				for (int j = 0; j < nodes; j++) {
					if (callMatrix.get(i).get(j)) {
						List<Integer> deployNode1 = deploy.get(i);
						List<Integer> deployNode2 = deploy.get(j);
						if (!isConnected(deployNode1, deployNode2)) {
							return false;
						}
					}
				}
			}
			
			return true;
			
		} catch (Exception e) {
			System.err.println("Nodes: " + nodes);
			System.err.println("Edges: " + edges);
			for (List<Integer> d : deploy) {
				System.err.print("{");
				for (Integer i : d) {
					System.err.print("[");
					System.err.print(i + ",");
					System.err.print("]");
				}
				System.err.print("}");
			}
			System.err.println("");
			System.err.println("");
			
			e.printStackTrace();
			System.exit(0);
			return false;
		}
	}
	
}
