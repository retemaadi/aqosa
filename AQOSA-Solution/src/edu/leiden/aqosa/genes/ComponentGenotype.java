package edu.leiden.aqosa.genes;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Random;

import org.opt4j.core.Genotype;
import org.opt4j.core.genotype.SelectGenotype;


/**
 * 
 * @author Ramin
 */

@SuppressWarnings("serial")
public class ComponentGenotype extends SelectGenotype<String> implements ArchGenome {

	public ComponentGenotype(List<String> instances) {
		super(instances);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <G extends Genotype> G newInstance() {
		try {
			Constructor<? extends ComponentGenotype> cstr = this.getClass().getConstructor(List.class);
			return (G) cstr.newInstance(values);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public void init(Random random) {
		init(random, 1);
	}
	
	public String getInstance() {
		return getValue(0);
	}
}
