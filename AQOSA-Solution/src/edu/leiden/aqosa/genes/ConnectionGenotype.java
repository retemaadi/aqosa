package edu.leiden.aqosa.genes;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.opt4j.core.Genotype;
import org.opt4j.core.genotype.BooleanGenotype;


/**
 * 
 * @author Ramin
 */

@SuppressWarnings("serial")
public class ConnectionGenotype extends BooleanGenotype implements ArchGenome {
	
	protected Integer edges;
	
	public ConnectionGenotype() {
		this(null);
	}

	public ConnectionGenotype(Integer edges) {
		this.edges = edges;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <G extends Genotype> G newInstance() {
		try {
			Constructor<? extends ConnectionGenotype> cstr = this.getClass().getConstructor(Integer.class);
			return (G) cstr.newInstance(edges);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void init(Random random, int maxNodes) {
		//int maxEdges = maxNodes * (maxNodes - 1) / 2
		int maxEdges = EdgesGenotype.maxEdges(maxNodes);
		super.init(random, maxEdges * maxNodes);
		
		//int maxEd = maxNodes * (maxNodes - 1) / 2;
		int maxEd = EdgesGenotype.maxEdges(maxNodes);
		this.edges = ((maxEd == 0) ? 0 : 1 + random.nextInt(maxEd));		
	}
	
	protected Boolean getPositionN(int row, int col) {
		int index = (row * col) + col;
		return get(index);
	}
	
	protected Boolean getPositionB(int row, int col) {
		//return bus2buses.get(row).get(col);
		return false;
	}
	
	public Integer getEdges() {
		return edges;
	}
	
	public List<List<Boolean>> getBus2Nodes(Integer nodes) {
		ArrayList<List<Boolean>> matrix = new ArrayList<List<Boolean>>();
		for (int i = 0; i < edges; i++) {
			ArrayList<Boolean> row = new ArrayList<Boolean>();
			for (int j = 0; j < nodes; j++)
				row.add(j, getPositionN(i, j));
			matrix.add(i, row);
		}
		return matrix;
	}
	
	public List<List<Boolean>> getBus2Buses() {
		ArrayList<List<Boolean>> matrix = new ArrayList<List<Boolean>>();
		for (int i = 0; i < edges; i++) {
			ArrayList<Boolean> row = new ArrayList<Boolean>();
			for (int j = 0; j < edges; j++)
				row.add(j, getPositionB(i, j));
			matrix.add(i, row);
		}
		return matrix;
	}
}
