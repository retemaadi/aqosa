package edu.leiden.aqosa.genes;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.opt4j.core.Genotype;

import edu.leiden.aqosa.genotype.ArrayIntegerGenotype;


/**
 * 
 * @author Ramin
 */

@SuppressWarnings("serial")
public class DeployGenotype extends ArrayIntegerGenotype implements ArchGenome {

	private final Integer components;
	private final Integer duplicates;
	
	public DeployGenotype(Integer components, Integer duplicates) {
		super(1, components, duplicates);
		this.components = components;
		this.duplicates = duplicates;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <G extends Genotype> G newInstance() {
		try {
			Constructor<? extends DeployGenotype> cstr = this.getClass().getConstructor(Integer.class, Integer.class);
			return (G) cstr.newInstance(components, duplicates);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private boolean checkDuplicates(List<Integer> list, int newElement) {
		int occur = 0;
		for (Integer elem : list) {
			if (elem == newElement)
				occur++;
			if (occur >= duplicates)
				return true;
		}
		
		return false;
	}
	
	private ArrayList<Integer> getDeployArray(List<Integer> list, int comp) {
		ArrayList<Integer> deployArray = new ArrayList<Integer>();
		for (int i=0; i<list.size(); i++) {
			if (comp == list.get(i))
				deployArray.add(i + 1);
		}
		
		return deployArray;
	}
	
	public void init(Random random) {
		int nodes = random.nextInt(components) + 1;
		List<Integer> places = new ArrayList<Integer>();
		
		for (int i=0; i<nodes; i++) {
			Integer place;
			do {
				place = random.nextInt(components);
			} while (checkDuplicates(places, place));
			places.add(place);
		}

		for (int i=0; i<components; i++) {
			ArrayList<Integer> deployArray = getDeployArray(places, i);
			if (deployArray.size() == 0) {
				int deploy = random.nextInt(nodes) + 1;
				deployArray.add(deploy);
			}
			add(deployArray);
		}
	}
	
	/*
	private boolean find(int pos, int compNo) {
		for (int j = 0; j < compNo; j++) {
			List<Integer> posList = getValue(j);
			for (int lastPos : posList)
				if (lastPos == pos)
					return true;
		}
		return false;
	}

	public Integer getNodes() {
		int nodes = 1;
		for (int i = 1; i < components; i++) {
			List<Integer> currentPositions = getValue(i);
			for (int pos : currentPositions) {
				if (!find(pos, i)) {
					nodes++;
				}
			}
		}
		
		return nodes;
	}
	*/
	
	public Integer getNodes() {
		// since only genotypes which covers from 1..nodes are valid, so maximum is the number of nodes.
		List<Integer> allDeploy = new ArrayList<Integer>();
		for (int i = 0; i < components; i++) {
			allDeploy.addAll(get(i));
		}
		
		int max = 1;
		for (Integer i : allDeploy) {
			if (i > max)
				max = i;
		}
		return max;
	}

	public List<List<Integer>> list() {
		ArrayList<List<Integer>> array = new ArrayList<List<Integer>>();
		for (int i = 0; i < size(); i++)
			array.add(i, get(i));
		return array;
	}
}
