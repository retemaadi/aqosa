package edu.leiden.aqosa.genes;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.opt4j.core.Genotype;
import org.opt4j.core.genotype.SelectGenotype;


/**
 * 
 * @author Ramin
 */

@SuppressWarnings("serial")
public class EdgesGenotype extends SelectGenotype<String> implements ArchGenome {

	public EdgesGenotype(List<String> buses) {
		super(buses);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <G extends Genotype> G newInstance() {
		try {
			Constructor<? extends EdgesGenotype> cstr = this.getClass().getConstructor(List.class);
			return (G) cstr.newInstance(values);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public void init(Random random, int maxNodes) {
		//init(random, maxNodes * (maxNodes - 1) / 2);
		super.init(random, EdgesGenotype.maxEdges(maxNodes));
	}
	
	public List<String> getBuses(Integer edges) {
		List<String> array = new ArrayList<String>();
		for (int i = 0; i < edges; i++)
			array.add(i, getValue(i));
		return array;
	}
	
	public static int maxEdges(int maxNodes) {
		int maxEdges = 5;
		if ((maxNodes / 8) > maxEdges)
			maxEdges = (maxNodes / 8);
		
		return maxEdges;
	}
}
