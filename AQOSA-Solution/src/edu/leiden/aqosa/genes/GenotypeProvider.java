package edu.leiden.aqosa.genes;

import java.lang.reflect.Constructor;

import edu.leiden.aqosa.model.ArchModel;

public class GenotypeProvider {
	
	protected final Class<? extends ArchGenotype> genotypeClass;
	
	public GenotypeProvider(Class<? extends ArchGenotype> genotypeClass) {
		this.genotypeClass = genotypeClass;
	}
	
	public ArchGenotype provideGenotype(ArchModel model) {
		ArchGenotype genotype;
		
		try {
			if (genotypeClass == null) {
				genotype = new ArchGenotype(model);
			} else {
				Constructor<?> constructor = genotypeClass.getConstructor(ArchModel.class);
				genotype = (ArchGenotype) constructor.newInstance(model);
			}
		} catch (Exception e) {
			genotype = new ArchGenotype(model);
		}
		
		return genotype;
	}

}
