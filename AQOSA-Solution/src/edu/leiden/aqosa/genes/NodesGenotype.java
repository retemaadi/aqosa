package edu.leiden.aqosa.genes;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.opt4j.core.Genotype;
import org.opt4j.core.genotype.SelectGenotype;


/**
 * 
 * @author Ramin
 */

@SuppressWarnings("serial")
public class NodesGenotype extends SelectGenotype<String> implements ArchGenome {

	public NodesGenotype(List<String> processors) {
		super(processors);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <G extends Genotype> G newInstance() {
		try {
			Constructor<? extends NodesGenotype> cstr = this.getClass().getConstructor(List.class);
			return (G) cstr.newInstance(values);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public void init(Random random, int maxNodes) {
		super.init(random, maxNodes);
	}
	
	public List<String> getProcessors(int nodes) {
		List<String> array = new ArrayList<String>();
		for (int i = 0; i < nodes; i++)
			array.add(i, getValue(i));
		return array;
	}
}
