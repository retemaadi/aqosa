package edu.leiden.aqosa.genotype;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Random;

import org.opt4j.core.Genotype;
import org.opt4j.core.genotype.Bounds;
import org.opt4j.core.genotype.ListGenotype;

public class ArrayIntegerGenotype extends ArrayList<ArrayList<Integer>> implements ListGenotype<ArrayList<Integer>> {

	protected final static int DEFAULT_MAXIMUM_ARRAY_SIZE = 10;
	
	protected final int maximumArraySize;
	protected final Bounds<Integer> bounds;
	
	public ArrayIntegerGenotype(int lowerBound, int upperBound) {
		this(new FixedBounds<Integer>(lowerBound, upperBound));
	}

	public ArrayIntegerGenotype(int lowerBound, int upperBound, int maximumArraySize) {
		this(new FixedBounds<Integer>(lowerBound, upperBound), maximumArraySize);
	}

	private ArrayIntegerGenotype(Bounds<Integer> bounds) {
		this(bounds, DEFAULT_MAXIMUM_ARRAY_SIZE);
	}

	private ArrayIntegerGenotype(Bounds<Integer> bounds, int maximumArraySize) {
		this.bounds = bounds;
		this.maximumArraySize = maximumArraySize;
	}

	public int getLowerBound(int index) {
		return bounds.getLowerBound(index);
	}

	public int getUpperBound(int index) {
		return bounds.getUpperBound(index);
	}
	
	public Bounds<Integer> getBounds() {
		return bounds;
	}
	
	public int getMaximumArraySize() {
		return maximumArraySize;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <G extends Genotype> G newInstance() {
		try {
			Constructor<? extends ArrayIntegerGenotype> cstr = this.getClass().getConstructor(Bounds.class, Integer.class);
			return (G) cstr.newInstance(bounds, maximumArraySize);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Initialize this genotype with {@code n} random values.
	 * 
	 * @param random
	 *            the random number generator
	 * @param n
	 *            the number of elements in the resulting genotype
	 * @param arrSize
	 *            the default size of Array, -1 means random assign
	 */
	public void init(Random random, int n, int arrSize) {
		for (int i = 0; i < n; i++) {
			int lo = getLowerBound(i);
			int hi = getUpperBound(i);
			
			ArrayList<Integer> array = new ArrayList<Integer>();
			if (arrSize == -1)
				arrSize = 1 + random.nextInt(maximumArraySize);
			
			for (int j = 0; j < arrSize; j++) {
				int value = lo + random.nextInt(hi - lo + 1);
				array.add(value);
			}
			
			if (i >= size()) {
				add(array);
			} else {
				set(i, array);
			}
		}
	}
	
	private static final long serialVersionUID = 1L;
}

/**
 * The {@link FixedBounds} are {@link Bounds} that return a fixed lower and
 * upper bound for each index.
 * 
 * @author lukasiewycz
 * 
 * @param <E>
 */
class FixedBounds<E extends Number> implements Bounds<E> {

	protected final E lowerBound;
	protected final E upperBound;

	/**
	 * Construct a {@link FixedBounds}.
	 * 
	 * @param lowerBound
	 *            the lower bound
	 * @param upperBound
	 *            the upper bound
	 */
	public FixedBounds(E lowerBound, E upperBound) {
		super();
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	@Override
	public E getLowerBound(int index) {
		return lowerBound;
	}

	@Override
	public E getUpperBound(int index) {
		return upperBound;
	}

}