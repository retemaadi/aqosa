package edu.leiden.aqosa.solution;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.genes.ComponentGenotype;
import edu.leiden.aqosa.genes.ConnectionGenotype;
import edu.leiden.aqosa.genes.DeployGenotype;
import edu.leiden.aqosa.genes.EdgesGenotype;
import edu.leiden.aqosa.genes.NodesGenotype;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.model.ArchRepository;
import edu.leiden.aqosa.model.IR.Bus;
import edu.leiden.aqosa.model.IR.Processor;

public class ArchSolution implements Serializable {

	private static final long serialVersionUID = -329905766750186850L;
	
	protected final int nComp;
	protected final int nodes;
	protected final int edges;
	protected final List<String> components;
	protected final List<List<Integer>> deploy;
	protected final List<String> plist;
	protected final List<Processor> processors;
	protected final List<String> blist;
	protected final List<Bus> buses;
	protected final List<List<Boolean>> bus2nodes;
	protected final List<List<Boolean>> bus2buses;
	
	private List<Double> cpuUtilization;
	private List<Double> busUtilization;
	
	public ArchSolution(ArchModel model, ArchGenotype genotype) {
		this.nComp = genotype.getNoComponents();
		this.components = new ArrayList<String>();
		for (int i = 0; i < nComp; i++) {
			ComponentGenotype components = genotype.getComponent(i);
			this.components.add(components.getInstance());
		}

		DeployGenotype deploy = genotype.getDeploy();
		NodesGenotype nodes = genotype.getNodes();
		ConnectionGenotype connections = genotype.getConnections();
		EdgesGenotype edges = genotype.getEdges();

		Integer n = deploy.getNodes();
		Integer e = connections.getEdges();
		this.plist = nodes.getProcessors(n);
		this.blist = edges.getBuses(e);

		this.nodes = n;
		this.edges = e;
		this.deploy = deploy.list();

		this.processors = fillProcessors(model.getRepository(), plist);
		this.buses 		= fillBuses(model.getRepository(), blist);

		this.bus2nodes = connections.getBus2Nodes(n);
		this.bus2buses = connections.getBus2Buses();
	}

	public List<Double> getSortedProcessorsClocks() {
		List<Double> listClocks = new ArrayList<Double>(this.processors.size());
		
		for (Processor p : this.processors) {
			listClocks.add(p.getClock());
		}
		Collections.sort(listClocks);
		
		return listClocks;
	}

	public List<Double> getSortedBusesBandwidths() {
		List<Double> listBandwidths = new ArrayList<Double>(this.buses.size());
		
		for(Bus b : this.buses) {
			listBandwidths.add(b.getBandwidth());
		}
		Collections.sort(listBandwidths);
		
		return listBandwidths;
	}

	private List<Processor> fillProcessors(ArchRepository repo, List<String> plist) {
		List<Processor> processors = new ArrayList<Processor>();
		for (String p : plist) {
			processors.add(repo.getProcessor(p));
		}

		return processors;
	}

	private List<Bus> fillBuses(ArchRepository repo, List<String> blist) {
		List<Bus> buses = new ArrayList<Bus>();
		for (String b : blist) {
			buses.add(repo.getBus(b));
		}

		return buses;
	}

	public int getNodes() {
		return nodes;
	}

	public int getEdges() {
		return edges;
	}

	public List<String> getComponentInstances() {
		return components;
	}

	public String getComponentInstance(int comp) {
		return components.get(comp);
	}

	public List<List<Integer>> getDeploy() {
		return deploy;
	}

	public List<Processor> getProcessors() {
		return processors;
	}
	
	public List<Bus> getBuses() {
		return buses;
	}
	
	public List<List<Boolean>> getBus2Nodes() {
		return bus2nodes;
	}

	public List<List<Boolean>> getBus2Buses() {
		return bus2buses;
	}

	public void setCpuUtilizationList(List<Double> cpuUtilList) {
		cpuUtilization = cpuUtilList;
	}
	
	public List<Double> getCpuUtilizationList() {
		return cpuUtilization;
	}
	
	public void setBusUtilizationList(List<Double> busUtilList) {
		busUtilization = busUtilList;
	}
	
	public List<Double> getBusUtilizationList() {
		return busUtilization;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ArchSolution) {
			ArchSolution solution = (ArchSolution) obj;
			return (components.equals(solution.components) &&
					deploy.equals(solution.deploy) &&
					plist.equals(solution.plist) &&
					blist.equals(solution.blist) &&
					bus2nodes.equals(solution.bus2nodes) &&
					bus2buses.equals(solution.bus2buses));
		}
		
		return false;
	};
	
	@Override
	public String toString() {
		String output = "[";

		int nodes = getNodes();
		int edges = getEdges();

		for (List<Integer> ld : getDeploy()) {
			output += "[";
			for (Integer d : ld)
				output += d + ", ";
			output = output.substring(0, output.length() - 2);
			output += "]";
		}
		output += "] [" + nodes + "|";
		
		for (Processor p : processors) {
			output += "(";
			output += p.getClock() + ", " + p.getCost();
			output += ")";
		}
		output += "]";

		if (edges > 0) {
			output += " [" + edges + "|";

			for (Bus b : buses) {
				output += "(";
				output += b.getBandwidth() + ", " + b.getDelay() + ", " + b.getCost();
				output += ")";
			}

			output += "] [";
			for (List<Boolean> b2n : getBus2Nodes()) {
				for (Boolean c : b2n)
					output += (c ? 1 : 0) + ", ";
				output = output.substring(0, output.length() - 2);
				output += "; ";
			}
			output = output.substring(0, output.length() - 2);
			/*
			output += "] [";

			for (List<Boolean> b2b : getBus2Buses()) {
				for (Boolean c : b2b) {
					output += (c ? 1 : 0) + ", ";
				}
				output = output.substring(0, output.length() - 2);
				output += "; ";
			}
			output = output.substring(0, output.length() - 2);
			*/
			output += "]";
		}

		return output;
	}

	/*
	 * Only useful for calling SCoPE So, I don't update this part anymore!
	 */
	// public String commandLine() {}

}
