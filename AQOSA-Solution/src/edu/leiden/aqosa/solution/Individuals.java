package edu.leiden.aqosa.solution;

import java.io.Serializable;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Map.Entry;

import org.opt4j.core.Individual;
import org.opt4j.core.optimizer.Archive;


public class Individuals extends ArrayList<Entry<ArchSolution, Quality>> implements Serializable {

	private static final long serialVersionUID = -15371677380472069L;

	public Individuals(Archive archive) {
		for (Individual i : archive) {
			ArchSolution solution = (ArchSolution) i.getPhenotype();
			Quality quality = new Quality( i.getObjectives() );
			
			Entry<ArchSolution, Quality> e = new SimpleEntry<ArchSolution, Quality>(solution, quality);
			this.add(e);
		}
		
	}
	
}
