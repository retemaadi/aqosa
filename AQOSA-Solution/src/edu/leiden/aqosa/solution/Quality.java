package edu.leiden.aqosa.solution;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.opt4j.core.Objective;
import org.opt4j.core.Objectives;
import org.opt4j.core.Value;

public class Quality implements Serializable {

	private static final long serialVersionUID = -6340208659984075664L;
	
	protected final TreeMap<String, Double> map;
	
	public Quality() {
		map = new TreeMap<String, Double>();
	}
	
	public Quality(Objectives objectives) {
		this();
		Iterator<Entry<Objective, Value<?>>> iter = objectives.iterator();
		while (iter.hasNext()) {
			Entry<Objective, Value<?>> entry = iter.next();
			this.add(entry.getKey().getName(), entry.getValue().getDouble());
		}
	}
	
	public void add(String objective, Double value) {
		map.put(objective, value);
	}

	public Objectives getObjectives() {
		Objectives objectives = new Objectives();
		for (Entry<String, Double> entry : map.entrySet()) {
			String name = entry.getKey();
			Double value = entry.getValue();
			
			Objective ob = new Objective(name);
			objectives.add(ob, value);
		}
		
		return objectives;
	}
	
}
