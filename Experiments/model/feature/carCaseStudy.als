/*
All clafers: 23 | Abstract: 0 | Concrete: 23 | References: 0
Constraints: 6
Goals: 0
Global scope: 1..1
All names unique: True
*/
open util/integer
pred show {}
run  show for 1 but 1 c10_extended_dashboard, 1 c1_car, 1 c4_interior_lights, 1 c5_ignition_switch, 1 c6_key_ignition, 1 c7_button_ignition, 1 c8_dashboard, 1 c9_simple_dashboard

one sig c1_car
{ r_c2_theft_alarm : lone c2_theft_alarm
, r_c3_park_assist : lone c3_park_assist
, r_c4_interior_lights : one c4_interior_lights
, r_c5_ignition_switch : one c5_ignition_switch
, r_c8_dashboard : one c8_dashboard
, r_c11_airbag_system : lone c11_airbag_system
, r_c15_antilock_braking_system : lone c15_antilock_braking_system
, r_c16_traction_control_system : lone c16_traction_control_system
, r_c17_stability_control_system : lone c17_stability_control_system
, r_c20_cruise_control : lone c20_cruise_control }

lone sig c2_theft_alarm
{}
{ one r_c2_theft_alarm }

lone sig c3_park_assist
{}
{ one r_c3_park_assist }

one sig c4_interior_lights
{}

one sig c5_ignition_switch
{ r_c6_key_ignition : lone c6_key_ignition
, r_c7_button_ignition : lone c7_button_ignition }
{ let children = (r_c6_key_ignition + r_c7_button_ignition) | one children }

lone sig c6_key_ignition
{}
{ one r_c6_key_ignition }

lone sig c7_button_ignition
{}
{ one r_c7_button_ignition }

one sig c8_dashboard
{ r_c9_simple_dashboard : lone c9_simple_dashboard
, r_c10_extended_dashboard : lone c10_extended_dashboard }
{ let children = (r_c9_simple_dashboard + r_c10_extended_dashboard) | one children }

lone sig c9_simple_dashboard
{}
{ one r_c9_simple_dashboard }

lone sig c10_extended_dashboard
{}
{ one r_c10_extended_dashboard }

lone sig c11_airbag_system
{ r_c12_front_airbag : one c12_front_airbag
, r_c13_sides_airbag : lone c13_sides_airbag
, r_c14_passengers_airbag : lone c14_passengers_airbag }
{ one r_c11_airbag_system }

lone sig c12_front_airbag
{}
{ one r_c12_front_airbag }

lone sig c13_sides_airbag
{}
{ one r_c13_sides_airbag }

lone sig c14_passengers_airbag
{}
{ one r_c14_passengers_airbag }

lone sig c15_antilock_braking_system
{}
{ one r_c15_antilock_braking_system }

lone sig c16_traction_control_system
{}
{ one r_c16_traction_control_system }

lone sig c17_stability_control_system
{ r_c18_basic_skid_control : lone c18_basic_skid_control
, r_c19_extended_skid_control : lone c19_extended_skid_control }
{ one r_c17_stability_control_system
  let children = (r_c18_basic_skid_control + r_c19_extended_skid_control) | one children }

lone sig c18_basic_skid_control
{}
{ one r_c18_basic_skid_control }

lone sig c19_extended_skid_control
{}
{ one r_c19_extended_skid_control }

lone sig c20_cruise_control
{ r_c21_basic_cruise_control : lone c21_basic_cruise_control
, r_c22_adaptive_cruise_control : lone c22_adaptive_cruise_control
, r_c23_fullyAdaptive_cruise_control : lone c23_fullyAdaptive_cruise_control }
{ one r_c20_cruise_control
  let children = (r_c21_basic_cruise_control + r_c22_adaptive_cruise_control + r_c23_fullyAdaptive_cruise_control) | one children }

lone sig c21_basic_cruise_control
{}
{ one r_c21_basic_cruise_control }

lone sig c22_adaptive_cruise_control
{}
{ one r_c22_adaptive_cruise_control }

lone sig c23_fullyAdaptive_cruise_control
{}
{ one r_c23_fullyAdaptive_cruise_control }

fact { (some c1_car.@r_c17_stability_control_system) => (some c1_car.@r_c16_traction_control_system) }
fact { (some c1_car.@r_c16_traction_control_system) => (some c1_car.@r_c15_antilock_braking_system) }
fact { (some (c1_car.@r_c20_cruise_control).@r_c21_basic_cruise_control) => (some (c1_car.@r_c17_stability_control_system).@r_c18_basic_skid_control) }
fact { (some (c1_car.@r_c20_cruise_control).@r_c22_adaptive_cruise_control) => (some (c1_car.@r_c17_stability_control_system).@r_c19_extended_skid_control) }
fact { (some (c1_car.@r_c20_cruise_control).@r_c23_fullyAdaptive_cruise_control) => (some (c1_car.@r_c17_stability_control_system).@r_c19_extended_skid_control) }
fact { (some (c1_car.@r_c5_ignition_switch).@r_c7_button_ignition) => (no (c1_car.@r_c8_dashboard).@r_c9_simple_dashboard) }
