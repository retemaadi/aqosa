package experiments.feature;

import java.io.FileInputStream;

import edu.leiden.aqosa.featureModel.FMConfigs;
import edu.leiden.aqosa.featureModel.FeatureMapper;
import edu.leiden.aqosa.model.ModelResource;
import edu.leiden.aqosa.model.IR.AQOSAModel;

public class ReadConfigs {
	
	public static void main(String[] args) throws Exception {
		
		//String files[] = new String[] {
		//	"D:\\Eggrafa\\Masters\\master thesis\\Feature model tools\\clafer-tools-0.3.2\\examples\\mycar.als",
		// 	"D:\\Eggrafa\\Masters\\master thesis\\Feature model tools\\clafer-tools-0.3.2\\examples\\carCaseStudy.als"
		//};
		String file = "model/feature/carCaseStudy.als";
		FMConfigs configs = new FMConfigs(file);
		
		// Load Aqosa file
		//final FileInputStream modelFileIS = new FileInputStream("C:\\Users\\Vasilis\\runtime-EclipseApplication\\test\\automotiveCaseStudy_Vasilis.aqosa");
		final FileInputStream modelFileIS = new FileInputStream("model/feature/automotiveCaseStudy_Vasilis.aqosa");
		
		ModelResource modelResource = new ModelResource();
		AQOSAModel model = modelResource.readModel(modelFileIS);
		
		//debug
		System.out.println("Total Alloy Features: " + configs.getFeaturesIDs().size()  + " -- " + configs.getFeaturesIDs().toString());
		System.out.println("Total Configurations: " + configs.getConfigs().size());
		
		FeatureMapper fm = new FeatureMapper(model, configs);
		System.out.println("\nFeature Mapper");
		System.out.println("Alloy to Aqosa feature map: " + fm.getAlloy2AqosaMap().toString());
		System.out.println("Alloy to Components map: " + fm.getFeats2Components().toString());
		
	}
	
}
