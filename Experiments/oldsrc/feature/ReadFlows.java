package experiments.feature;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Random;

import edu.leiden.aqosa.featureModel.FMConfigs;
import edu.leiden.aqosa.featureModel.FeatureMapper;
import edu.leiden.aqosa.model.ModelResource;
import edu.leiden.aqosa.model.IR.Action;
import edu.leiden.aqosa.model.IR.AQOSAModel;
import edu.leiden.aqosa.model.IR.CommunicateAction;
import edu.leiden.aqosa.model.IR.Component;
import edu.leiden.aqosa.model.IR.ComputeAction;
import edu.leiden.aqosa.model.IR.Flow;
import edu.leiden.aqosa.model.IR.Service;

public class ReadFlows {
	
	public static void main(String[] args) throws Exception {
		final FileInputStream modelFileIS = new FileInputStream("model/feature/automotiveCaseStudy_Vasilis.aqosa");
		AQOSAModel model = (new ModelResource()).readModel(modelFileIS);
		//ArchModel arch = new ArchModel(model);
		
		String configFile = "model/feature/carCaseStudy.als";
		FMConfigs configs = new FMConfigs(configFile);
		FeatureMapper mapper = new FeatureMapper(model, configs);
		
		int size = configs.getConfigs().size();
		Random random = new Random();
		int r = random.nextInt(size);
		
		ArrayList<String> randConfig = configs.getConfigs().get(r);
		ArrayList<String> filteredConfig = mapper.filterLeafs(randConfig);
		System.out.println("Random Configurations: " + filteredConfig.size() + " " + filteredConfig);
		System.out.println("Random Configurations2: " +mapper.getAqosaFeaturesFromConfig(randConfig).size()+ " " + mapper.getAqosaFeaturesFromConfig(randConfig));
		System.out.println("Components from Config: "+mapper.componentsFromConfiguration(randConfig).size()+" "+mapper.componentsFromConfiguration(randConfig));
		
		ArrayList<String> components = new ArrayList<String>(); // we can use the function "componentsFromConfiguration" in FeatureMapper, instead.
		for (String feat : filteredConfig) {
			System.out.println(feat);
			ArrayList<String> comps = mapper.getComponents(feat);
			components.addAll(comps);
			for (String c : comps) {
				System.out.println("	### : " + c);
			}
		}
		
		System.out.println();
		System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		System.out.println();
		
		ArrayList<String> acceptableFlows = new ArrayList<String>();
		boolean rejectFlow;
		for (Flow flow : model.getAssembly().getFlow()) {
			rejectFlow = false;
			System.out.println(flow.getName());
			for (Action act : flow.getAction()) {
				if (act instanceof CommunicateAction)
					continue;
				
				ComputeAction action = (ComputeAction) act;
				Component comp = fetchComponent(action.getService(), model);
				
				if (components.contains(comp.getName())) {
					System.out.println("	$$$: [Accept Comp] " + comp.getName());
				} else {
					rejectFlow = true;
					System.out.println("	!!!: [Reject Comp] " + comp.getName());
				}
			}
			if (rejectFlow == false) {
				acceptableFlows.add(flow.getName());
			}
		}
		System.out.println();
		System.out.println("Acceptable Flows: "+acceptableFlows.size()+" "+acceptableFlows);
	}
	
	static private Component fetchComponent(Service service, AQOSAModel model) {
		for (Component comp : model.getAssembly().getComponent()) {
			for (Service ser : comp.getService()) {
				if (service.equals(ser))
					return comp;
			}
		}
		
		return null;
	}
}
