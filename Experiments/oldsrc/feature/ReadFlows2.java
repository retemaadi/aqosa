package experiments.feature;

import java.io.FileInputStream;
import java.util.Random;

import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.model.Configuration;
import edu.leiden.aqosa.model.FeatureRoot;
import edu.leiden.aqosa.model.Features;
import edu.leiden.aqosa.model.ModelResource;
import edu.leiden.aqosa.model.IR.AQOSAModel;
import edu.leiden.aqosa.model.IR.Component;
import edu.leiden.aqosa.model.IR.Feature;
import edu.leiden.aqosa.model.IR.Flow;

public class ReadFlows2 {
	
	public static void main(String[] args) throws Exception {
		ModelResource mr = new ModelResource();
		
		final FileInputStream modelFileIS = new FileInputStream("model/feature/automotiveCaseStudy_Vasilis.aqosa");
		AQOSAModel model = mr.readModel(modelFileIS);
		ArchModel arch = new ArchModel(model);
		
		FeatureRoot root = new FeatureRoot("model/feature/carCaseStudy.als");
		Features features = new Features(root, model, arch.getRepository());
		
		int size = features.getConfigurations().size();
		Random random = new Random();
		int r = random.nextInt(size);
		
		Configuration randConfiguration = features.getConfigurations().get(r);
		System.out.println("Random Configurations: " + randConfiguration);
		for(Feature f : randConfiguration.getFeatures()) {
			System.out.println("		Feat: " + f.getName());
		}
		for(Component c : randConfiguration.getComponents()) {
			System.out.println("		Comp: " + c.getName());
		}
		for(Flow f : randConfiguration.getFlows()) {
			System.out.println("		Flow: " + f.getName());
		}
	}
	
}
