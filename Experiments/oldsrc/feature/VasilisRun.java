package experiments.feature;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import edu.leiden.aqosa.launcher.FeatureRun;
import edu.leiden.aqosa.launcher.MatingApproach;
import edu.leiden.aqosa.launcher.MultipleRun;
import edu.leiden.aqosa.launcher.SingleRun;

@SuppressWarnings("unused")
public class VasilisRun {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		final String featureFile = new String("model/feature/carCaseStudy.als");
		final FileInputStream modelFile = new FileInputStream("model/feature/automotiveCaseStudy_Vasilis.aqosa");
		final int generations = 10;
		final int alpha = 50;
		final int mu = 5;
		final int lambda = 5;
		final int archiveSize = 20;
		
		FeatureRun run = new FeatureRun(3, featureFile, modelFile, "temp", "archive", "optimum");
		run.setEA(generations, alpha, mu, lambda);
		run.setArchiveSize(archiveSize);
		run.setMatingAppraoch(MatingApproach.CROSSOVER_MUTATE);
		//run.setViewer();
		run.call();
		
		for (String result : run.getResults()) {
			System.out.println(result);
		}
	}
}
