package experiments.jss;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.genes.ComponentGenotype;
import edu.leiden.aqosa.genes.ConnectionGenotype;
import edu.leiden.aqosa.genes.DeployGenotype;
import edu.leiden.aqosa.genes.EdgesGenotype;
import edu.leiden.aqosa.genes.NodesGenotype;
import edu.leiden.aqosa.model.ArchModel;
import edu.leiden.aqosa.optimizer.ArchEvaluator;
import edu.leiden.aqosa.solution.ArchSolution;
import experiments.LoadModel;

/**
 * 
 * @author Ramin
 */
public class Baseline extends ArchEvaluator {
	
	public Baseline(ArchModel model) {
		super(model);
	}

	private static final String MODEL_FILENAME = "/home/ramin/workspaces/runtime-workspace/IR-Editor/Saab-IPC/Saab-IPC_v32__baseline.aqosa";
	
	public static void main(String[] args) throws Exception {
		ArchModel model = new ArchModel( LoadModel.loadModel( new FileInputStream(MODEL_FILENAME) ));

		BaselineGenotype genotype = new BaselineGenotype(model);
		//do {
			genotype.init(new Random());
		//} while(!genotype.validate());
		
		ArchSolution baseline = new ArchSolution(model, genotype);		
		// System.out.println(baseline.toString());
		
		Baseline evaluator = new Baseline(model);
		for (int i=0; i<25; i++) {
			evaluator.evaluate(baseline);
		}
	}
		
}

class BaselineGenotype extends ArchGenotype
{

	public BaselineGenotype(ArchModel model) {
		super(model);
	}

	public void init(Random random) {
		for (int i=0; i<noComp; i++) {
			List<String> instances = model.getRepository().getInstances(i);
			ComponentGenotype compGenotype = new ComponentGenotype(instances);
			compGenotype.init(random);
			put(i, compGenotype);
		}
		
		MyDeploy deployGenotype = new MyDeploy(noComp);
		put(noComp, deployGenotype);

		MyNodes nodesGenotype = new MyNodes(noComp, model.getRepository().getCPUs());
		put(noComp+1, nodesGenotype);

		MyEdges edgesGenotype = new MyEdges(noComp, model.getRepository().getBuses());
//		System.out.println(edgesGenotype);
		put(noComp+2, edgesGenotype);
		
		MyConn connectionGenotype = new MyConn(noComp, null);
		put(noComp+3, connectionGenotype);
	}	
}

@SuppressWarnings("serial")
class MyDeploy extends DeployGenotype {

	ArrayList<Integer> list1 = new ArrayList<Integer>();
	ArrayList<Integer> list2 = new ArrayList<Integer>();
	ArrayList<Integer> list3 = new ArrayList<Integer>();
	ArrayList<Integer> list4 = new ArrayList<Integer>();
	ArrayList<Integer> list5 = new ArrayList<Integer>();
	ArrayList<Integer> list6 = new ArrayList<Integer>();
	
	public MyDeploy(int components) {		
		super(components);
		
		list1.add(1);
		list2.add(2);
		list3.add(3);
		list4.add(4);
		list5.add(5);
		list6.add(6);
		
		super.add(list5);
		super.add(list5);
		super.add(list4);
		super.add(list2);
		super.add(list1);
		super.add(list1);
		super.add(list1);
		super.add(list3);
		super.add(list1);
		super.add(list1);
		super.add(list1);
		super.add(list1);
		super.add(list1);
		super.add(list6);
		super.add(list1);
		super.add(list1);
		super.add(list1);
		super.add(list1);
		super.add(list2);
		super.add(list2);
		super.add(list2);
		super.add(list2);
		super.add(list2);
	}
	
}

@SuppressWarnings("serial")
class MyNodes extends NodesGenotype {

	public MyNodes(Integer maxNodes, List<String> processors) {
		super(maxNodes, processors);
		super.add(0);
		super.add(0);
		super.add(4);
		super.add(3);
		super.add(2);
		super.add(1);
	}
	
}

@SuppressWarnings("serial")
class MyEdges extends EdgesGenotype {

	public MyEdges(Integer maxNodes, List<String> buses) {
		super(maxNodes, buses);
		super.add(2);
		super.add(0);
		super.add(3);
	}
	
}

@SuppressWarnings("serial")
class MyConn extends ConnectionGenotype {

	public MyConn(Integer maxNodes, Integer edges) {
		super(maxNodes, edges);
	}

	/*
	public MyConn(Integer maxNodes, Integer edges,	List<List<Boolean>> bus2nodes, List<List<Boolean>> bus2buses) {
		super(maxNodes, edges, bus2nodes, bus2buses);
		super.edges = 3;
		
		super.bus2nodes = new ArrayList<List<Boolean>>();
		
		ArrayList<Boolean> bus1 = new ArrayList<Boolean>();
		bus1.add(true);
		bus1.add(true);
		bus1.add(false);
		bus1.add(false);
		bus1.add(false);
		bus1.add(false);
		
		ArrayList<Boolean> bus2 = new ArrayList<Boolean>();
		bus2.add(false);
		bus2.add(true);
		bus2.add(true);
		bus2.add(true);
		bus2.add(true);
		bus2.add(false);
		
		ArrayList<Boolean> bus3 = new ArrayList<Boolean>();
		bus3.add(false);
		bus3.add(true);
		bus3.add(false);
		bus3.add(false);
		bus3.add(false);
		bus3.add(true);
		
		super.bus2nodes.add(bus1);
		super.bus2nodes.add(bus2);
		super.bus2nodes.add(bus3);
		
		super.bus2buses = new ArrayList<List<Boolean>>();
		
		ArrayList<Boolean> nobus = new ArrayList<Boolean>();
		nobus.add(false);
		nobus.add(false);
		nobus.add(false);
		
		super.bus2buses.add(nobus);
		super.bus2buses.add(nobus);
		super.bus2buses.add(nobus);		
	}
	*/
}