package experiments.jss;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import org.opt4j.common.archive.ArchiveModule;
import org.opt4j.common.random.RandomModule;
import org.opt4j.common.random.RandomModule.RandType;
import org.opt4j.config.Icons;
import org.opt4j.config.annotations.Icon;
import org.opt4j.optimizer.ea.EvolutionaryAlgorithmModule;
import org.opt4j.optimizer.ea.Nsga2Module;
import org.opt4j.optimizer.ea.Spea2Module;
import org.opt4j.start.Opt4JTask;
import org.opt4j.viewer.ViewerModule;

import com.google.inject.Module;

import edu.leiden.aqosa.logger.ArchLoggerModule;
import edu.leiden.aqosa.optimizer.ArchProblem;
import experiments.LoadModel;

/**
 * 
 * @author Ramin
 */
@Icon(Icons.PROBLEM)
public class SAAB_IPC {
	
	private static final int NO_EXP = 30;
//	private static final String MODEL_FILENAME = "/home/ramin/workspaces/runtime-workspace/IR-Editor/Saab-IPC/Saab-IPC_v21.aqosa";
	private static final String MODEL_FILENAME = "model/Saab-IPC_v22.aqosa";
	private static final String LOG_FILENAME = "logger/jss/saab-ipc__exp___ID_.tsv";
	
	public static void main(String[] args) throws Exception {
		final int generations = 5000;
		final int alpha = 2000;
		final int mu = 100;
		final int lambda = 50;
		final int solutions = 50;
		final double crossover = 0.75;
		
		ArchProblem problem = new ArchProblem();
		problem.setModel(LoadModel.loadModel( new FileInputStream(MODEL_FILENAME) ));
		
		EvolutionaryAlgorithmModule ea = new EvolutionaryAlgorithmModule();
		ea.setGenerations(generations);
		ea.setAlpha(alpha);
		ea.setMu(mu);
		ea.setLambda(lambda);
		ea.setCrossoverRate(crossover);

		Spea2Module spea2 = new Spea2Module();
		Nsga2Module nsga2 = new Nsga2Module();

		RandomModule random = new RandomModule();
		random.setType(RandType.JAVA);
		
		ArchiveModule archive = new ArchiveModule();
		archive.setCapacity(solutions);

		ViewerModule viewer = new ViewerModule();
		viewer.setTitle("AQOSA Optimizer: SAAB IPC");
		
		ArchLoggerModule logger = new ArchLoggerModule();
		
		Random rand = new Random();
		for (int i = 0; i < NO_EXP; i++) {
			random.setSeed(rand.nextLong());
			String logname = LOG_FILENAME;
			logname = logname.replaceAll("_ID_", (new Integer(i+1)).toString());
			logname = logname.replaceAll("_exp_", (i%2 == 1) ? "spea2" : "nsga2");
//			logname = logname.replaceAll("_exp_", "spea2");
			logger.setArchiveFilename(logname);
			
			Collection<Module> modules = new ArrayList<Module>();
			modules.add(problem);
			modules.add(ea);
			modules.add((i%2 == 1) ? spea2 : nsga2);
//			modules.add(spea2);
			modules.add(random);
			modules.add(archive);
//			modules.add(viewer);
			modules.add(logger);
			
			// Optimization
			doExperiment(modules);
		}
	}
	
	private static void doExperiment(Collection<Module> modules) {
		Opt4JTask task = new Opt4JTask(false);
		task.init(modules);

		try {
			task.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			task.close();
		}
	}
	
}
