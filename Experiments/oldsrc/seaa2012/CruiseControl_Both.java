package experiments.seaa2012;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import org.opt4j.common.archive.ArchiveModule;
import org.opt4j.common.archive.ArchiveModule.Type;
import org.opt4j.common.random.RandomModule;
import org.opt4j.common.random.RandomModule.RandType;
import org.opt4j.optimizer.ea.EvolutionaryAlgorithmModule;
import org.opt4j.optimizer.ea.Nsga2Module;
import org.opt4j.optimizer.ea.Spea2Module;
import org.opt4j.start.Opt4JTask;

import com.google.inject.Module;

import edu.leiden.aqosa.logger.ArchLoggerModule;
import edu.leiden.aqosa.optimizer.ArchProblem;
import experiments.LoadModel;
import experiments.fixtopo.FixProblem;

public class CruiseControl_Both {
	
	private static final int NO_EXP = 25;
	private static final String MODEL_FILENAME = "model/CruiseControl.aqosa";
	private static final String LOG_FILENAME = "logger/temp/cruise__exp___ID_.tsv";
	private static final String FIXLOG_FILENAME = "logger/temp/fixcruise__exp___ID_.tsv";
	
	public static void main(String[] args) throws Exception {
		EvolutionaryAlgorithmModule ea = new EvolutionaryAlgorithmModule();
		ea.setGenerations(10000);
		ea.setAlpha(100);
		ea.setMu(50);
		ea.setLambda(50);
		ea.setCrossoverRate(0.95);
		
		Spea2Module spea2 = new Spea2Module();
		Nsga2Module nsga2 = new Nsga2Module();
		
		ArchiveModule archive = new ArchiveModule();
		archive.setType(Type.CROWDING);
		archive.setCapacity(20);

		RandomModule random = new RandomModule();
		random.setType(RandType.JAVA);
		
		ArchLoggerModule logger = new ArchLoggerModule();
		
		ArchProblem cruise = new ArchProblem();
		FileInputStream modelInputStream = new FileInputStream(MODEL_FILENAME);
		cruise.setModel(LoadModel.loadModel( modelInputStream ));
		
		Random rand = new Random();
		for (int i = 1; i <= NO_EXP; i++) {
			random.setSeed(rand.nextLong());
			String logname = LOG_FILENAME;
			logname = logname.replaceAll("_ID_", (new Integer(i)).toString());
			logname = logname.replaceAll("_exp_", (i%2 == 1) ? "spea2" : "nsga-ii");
			logger.setArchiveFilename(logname);
			
			Collection<Module> modules = new ArrayList<Module>();
			modules.add(ea);
			modules.add((i%2 == 1) ? spea2 : nsga2);
			modules.add(archive);
			modules.add(random);
			modules.add(cruise);
			modules.add(logger);
			
			doExperiment(modules);
		}
		
		FixProblem fixcruise = new FixProblem();
		fixcruise.setModel(LoadModel.loadModel( modelInputStream ));
		
		for (int i = 1; i <= NO_EXP; i++) {
			random.setSeed(rand.nextLong());
			String logname = FIXLOG_FILENAME;
			logname = logname.replaceAll("_ID_", (new Integer(i)).toString());
			logname = logname.replaceAll("_exp_", (i%2 == 1) ? "spea2" : "nsga-ii");
			logger.setArchiveFilename(logname);
			
			Collection<Module> modules = new ArrayList<Module>();
			modules.add(ea);
			modules.add((i%2 == 1) ? spea2 : nsga2);
			modules.add(archive);
			modules.add(random);
			modules.add(fixcruise);
			modules.add(logger);
			
			doExperiment(modules);
		}
	}

	private static void doExperiment(Collection<Module> modules) {
		Opt4JTask task = new Opt4JTask(false);
		task.init(modules);

		try {
			task.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			task.close();
		}
	}
		
}
