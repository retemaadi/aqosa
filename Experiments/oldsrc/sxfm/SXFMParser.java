package edu.leiden.aqosa.featureModel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

import sxfm.FeatureChoice;
import sxfm.FeatureModel;
import sxfm.FeatureTree;
import sxfm.SxfmPackage;
import sxfm.impl.ConstraintImpl;
import sxfm.impl.FeatureImpl;
import sxfm.impl.GroupImpl;
import sxfm.impl.GroupedFeatureImpl;
import sxfm.impl.MandatoryImpl;
import sxfm.impl.OptionalImpl;
import sxfm.impl.RootImpl;


public class SXFMParser {
	
	public void parseSXFM (InputStream inputModelStream) {
		/*
		 * AQOSAPackage aqosaPackage = AQOSAPackage.eINSTANCE;
		aqosaPackage.setName("IR");
		aqosaPackage.setNsPrefix("aqosa.ir");
		aqosaPackage.setNsURI("http://se.liacs.nl/aqosa/ir");
		
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());
		resourceSet.getPackageRegistry().put("http://se.liacs.nl/aqosa/ir", aqosaPackage);
		
		XMLResourceImpl resource = (XMLResourceImpl) resourceSet.createResource(URI.createURI("ArchModel"));
		resource.load(inputModelStream, null);

		AQOSAModel model = (AQOSAModel) resource.getContents().get(0);
		experiment = new Experiment(model);
		 */
		SxfmPackage sxfmPackage = SxfmPackage.eINSTANCE;
		sxfmPackage.setName("sxfm");
		sxfmPackage.setNsPrefix("sxfm");
		sxfmPackage.setNsURI("http://sxfm.com");
		
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());
		resourceSet.getPackageRegistry().put("http://sxfm.com", sxfmPackage);
		
		// why XMLResourceImpl and not XMIResourceImpl?
		XMLResourceImpl resource = (XMLResourceImpl) resourceSet.createResource(URI.createURI("FeatureModel"));
		try {
			resource.load(inputModelStream, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		FeatureModel fModel = (FeatureModel) resource.getContents().get(0);
		
		System.out.println("Feature Model = "+fModel.getName());
		System.out.println("\nFeature Tree elements:");
		
		/*
		 * FeatureModel contains the Constraints, the MetaData and the Feature Tree.
		 * Currently we are interested in the latter.
		 */
		FeatureTree fTree = fModel.getFeatureTree();
		
		/*
		 * Iterate over the tree to store all the IDs in the List of strings
		 */
		TreeIterator<EObject> iter = fTree.eAllContents();
		List<String> allElemsOfTree = new ArrayList<String>();
		while (iter.hasNext()) {
			EObject elem = iter.next();
			String elemId = "";
			
			if (elem instanceof RootImpl) {
				elemId = ((RootImpl)elem).getId();
				//System.out.println(elemId);
			}
			else if (elem instanceof ConstraintImpl) {
				int tmp = ((ConstraintImpl)elem).getId();
				elemId = Integer.toString(tmp);
			}
			else if (elem instanceof FeatureImpl) {
				elemId = ((FeatureImpl)elem).getId();
			}
			else if (elem instanceof GroupedFeatureImpl) {
				elemId = ((GroupedFeatureImpl)elem).getId();
			}
			else if (elem instanceof GroupImpl) {
				elemId = ((GroupImpl)elem).getId();
			}
			else if (elem instanceof MandatoryImpl) {
				elemId = ((MandatoryImpl)elem).getId();
			}
			else if (elem instanceof OptionalImpl) {
				elemId = ((OptionalImpl)elem).getId();
			}
			else {
				System.out.println("This elem doesn't have an id");
			}
			
			allElemsOfTree.add(elemId); // add ID to list
		}
		System.out.println(allElemsOfTree.toString());
		
		/*
		 * Find all the selected features of the model and store their IDs in a List
		 */
		// TODO: Fix case when list has no object (model has no configurations)
		ListIterator<FeatureChoice> it = fModel.getConfiguration().get(0).getFeatureChoice().listIterator();
		//EList<FeatureChoice> listOfFeatureChoices = 
		List<String> selectedFeaturesList = new ArrayList<String>();
		while (it.hasNext()) {
			FeatureChoice featureChoice = it.next();
			if (featureChoice.isSelected()) {
				selectedFeaturesList.add(featureChoice.getFeature().getId()); // add ID to list
			}
		}
		
		System.out.println(selectedFeaturesList.toString());
		
		
	} //end parseSXFM
	
	public static void main(String args[]) throws FileNotFoundException {
		//FileInputStream fis = new FileInputStream("src/edu/leiden/aqosa/featureModel/My.sxfm");
		FileInputStream fis = new FileInputStream("My.sxfm");
		new SXFMParser().parseSXFM(fis);
		
	} // end main
	
	

}
