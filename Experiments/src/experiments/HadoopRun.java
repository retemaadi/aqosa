package experiments;

import java.io.FileInputStream;

import edu.leiden.aqosa.launcher.CompleterType;
import edu.leiden.aqosa.launcher.MatingApproach;
import edu.leiden.aqosa.launcher.SingleRun;

/**
 * 
 * @author Ramin
 */
public class HadoopRun {
	
	public static void main(String[] args) throws Exception {
		final FileInputStream modelFile = new FileInputStream("model/CruiseControl.aqosa");
		final int generations = 4;
		final int alpha = 100;
		final int mu = 10;
		final int lambda = 5;
		final int archiveSize = 10;
		
		SingleRun run = new SingleRun(modelFile);
		run.setEA(generations, alpha, mu, lambda);
		run.setCompleter(CompleterType.HADOOP);
		run.setArchiveSize(archiveSize);
		run.setMatingAppraoch(MatingApproach.CROSSOVER_MUTATE);
		run.setArchiveLogger("logger/longrun-archive.tsv");
		run.setOptimumLogger("logger/longrun-optimum.tsv");
		run.call();
		
		for (String result : run.getResults()) {
			System.out.println(result);
		}
	}
	
}
