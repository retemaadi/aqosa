package experiments;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.commons.lang.SerializationUtils;
import org.opt4j.core.Objective;
import org.opt4j.core.Objectives;
import org.opt4j.core.Value;

import edu.leiden.aqosa.solution.ArchSolution;
import edu.leiden.aqosa.solution.Individuals;
import edu.leiden.aqosa.solution.Quality;

public class ReadArchive {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FileReader fr = null;
		try { 
			File file = new File("temp/mytest.tsv.ser");
			int len = (int) file.length();
			
			char[] cbuf = new char[len];
			fr = new FileReader(file);
			fr.read(cbuf);
			
			byte[] data = new byte[cbuf.length];
			for (int i=0; i<cbuf.length; i++) {
				data[i] = (byte) cbuf[i];
			}
			
			Individuals individuals = (Individuals) SerializationUtils.deserialize(data);
			System.out.println(individuals.size());
			for (Entry<ArchSolution, Quality> entry : individuals) {
				System.out.println(entry.getKey().toString());
				Objectives objectives = entry.getValue().getObjectives();
				Iterator<Entry<Objective, Value<?>>> iter = objectives.iterator();
				while(iter.hasNext()) {
					Entry<Objective, Value<?>> e = iter.next();
					System.out.println(e.getKey().getName() + ": " + e.getValue().getDouble());
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fr != null) fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
}
