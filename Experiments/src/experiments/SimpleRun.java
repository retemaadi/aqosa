package experiments;

import java.io.FileInputStream;

import edu.leiden.aqosa.launcher.CompleterType;
import edu.leiden.aqosa.launcher.MatingApproach;
import edu.leiden.aqosa.launcher.MultipleRun;
import edu.leiden.aqosa.launcher.SingleRun;
import edu.leiden.aqosa.logger.EvaluationLogger.DetailLevel;

/**
 * 
 * @author Ramin
 */
@SuppressWarnings("unused")
public class SimpleRun {
	
	public static void main(String[] args) throws Exception {
		final FileInputStream modelFile1 = new FileInputStream("model/CruiseControl.aqosa");
		final int generations = 10;
		final int alpha = 100;
		final int mu = 20;
		final int lambda = 20;
		final int archiveSize = 20;
		
		SingleRun run = new SingleRun(modelFile1);
		run.setEA(generations, alpha, mu, lambda);
		run.setArchiveSize(archiveSize);
		run.setMatingAppraoch(MatingApproach.CROSSOVER_MUTATE);
		//run.setIterationLog(true);
		run.setLoggerLevel(DetailLevel.FULL);
		run.setArchiveLogger("temp/mytest.tsv");
		run.setOptimumLogger("temp/myopt.tsv");
		//run.setViewer();
		run.call();
		
		for (String result : run.getResults()) {
			System.out.println(result);
		}
		
		/*
		final FileInputStream modelFile2 = new FileInputStream("model/CRN.aqosa");
		MultipleRun mrun = new MultipleRun(5, modelFile2, "temp", "archive", "optimums");
		mrun.setEA(generations, alpha, mu, lambda);
		mrun.setArchiveSize(archiveSize);
		mrun.setMatingAppraoch(MatingApproach.CROSSOVER_MUTATE);
		//mrun.setEvaluationLogger(false);
		mrun.call();
		
		for (String result : mrun.getResults()) {
			System.out.println(result);
		}
		*/
	}
		
}
