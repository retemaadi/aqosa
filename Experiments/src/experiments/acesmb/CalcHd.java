package experiments.acesmb;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang.SerializationUtils;
import org.opt4j.core.Objective;
import org.opt4j.core.Objectives;
import org.opt4j.core.Value;

import edu.leiden.aqosa.solution.ArchSolution;
import edu.leiden.aqosa.solution.Individuals;
import edu.leiden.aqosa.solution.Quality;

public class CalcHd {

	static String folder = "temp/approaches/run9/";
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		//rewriteAll();
		//System.exit(0);
		
		String[] filenames = {
				"crossovermutate/archive1.tsv",
				"crossovermutate/archive2.tsv",
				"crossovermutate/archive3.tsv",
				"crossovermutate/archive4.tsv",
				"crossovermutate/archive5.tsv",
				"random/archive1.tsv",
				"random/archive2.tsv",
				"random/archive3.tsv",
				"random/archive4.tsv",
				"random/archive5.tsv",
				"sequential/archive1.tsv",
				"sequential/archive2.tsv",
				"sequential/archive3.tsv",
				"sequential/archive4.tsv",
				"sequential/archive5.tsv"
		};
		//String[] filenames = {"sequential/archive4.tsv"};
		
		Individuals longrun = readFile(folder + "longrun-archive.tsv.ser");
		List<Objectives> bestpoints = (List<Objectives>) collectObjectives(longrun);
		
		for(Objective o : bestpoints.get(0).getKeys()) {
			System.out.println("Objective " + o.getName() + ": " + bestObjective(bestpoints, o) + ", " + worseObjective(bestpoints, o));
		}
		
		for (String filename : filenames) {
			System.out.println("=============================");
			System.out.println("distance (" + filename + "): ");
			for (int i=2; i<=21; i++) {
				String file = filename + "_i" + i + ".ser";
				Individuals shortrun = readFile(folder + file);
				Collection<Objectives> thisrun = collectObjectives(shortrun);
				System.out.println("		iteration " + i + ": " + Hausdorff.HDistance(bestpoints, thisrun, 1));
				if (i == 50)
					for(Objective o : bestpoints.get(0).getKeys()) {
						System.out.println("	Objective " + o.getName() + ": " + bestObjective(thisrun, o) + ", " + worseObjective(thisrun, o));
					}
			}
		}
	}
	
	private static AtomicInteger solutionNumber;
	@SuppressWarnings("unused")
	private static void rewriteAll() throws IOException {
		for (File dir : (new File(folder)).listFiles()) {
			if (dir.isDirectory()) {
				System.out.println(dir.getPath());
				FilenameFilter filter = new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						return (name.indexOf(".ser") > 0 && name.indexOf("_i") > 0);
					}
				};
				
				for (File file : dir.listFiles(filter)) {
					String fname = file.getName();
					String oname = fname.substring(0, fname.indexOf("."));
					String name = oname.replaceAll("archive", "arc");
					String after = fname.substring(fname.indexOf("."));
					String inx = after.substring(after.indexOf("_i") + 2, after.indexOf(".ser"));
					
					Individuals archive = readFile(file);
					
					String newname = name + "_i" + inx + ".tsv";
					File newfile = new File(dir, newname);
					
					PrintWriter writer = new PrintWriter(newfile);
					solutionNumber = new AtomicInteger(0);
					Iterator<Entry<ArchSolution, Quality>> iter = archive.iterator();
					while (iter.hasNext()) {
						Entry<ArchSolution, Quality> entry = iter.next();
						writer.println(getIndividual(entry));
					}
					
					writer.flush();
					writer.close();
				}
			}
		}
	}

	private static String getIndividual(Entry<ArchSolution, Quality> entry) {
		solutionNumber.incrementAndGet();
		ArchSolution arch = entry.getKey();
		String solution = null;
		if (arch == null)
			solution = " INVALID ";
		else
			solution = arch.toString();
		
		return (String.valueOf(solutionNumber) + "\t \"" + solution + "\"" + getIndividualObjectives(entry.getValue()));
	}

	private static String getIndividualObjectives(Quality quality) {
		String output = "";
		Objectives objectives = quality.getObjectives();
		for (Objective objective : objectives.getKeys()) {
			Value<?> value = objectives.get(objective);
			output += "\t" + value.getValue().toString();
		}
		
		return output;
	}
	
	private static Individuals readFile(File file) {
		FileReader fr = null;
		try {
			int len = (int) file.length();
			
			char[] cbuf = new char[len];
			fr = new FileReader(file);
			fr.read(cbuf);
			
			byte[] data = new byte[cbuf.length];
			for (int i=0; i<cbuf.length; i++) {
				data[i] = (byte) cbuf[i];
			}
			
			return (Individuals) SerializationUtils.deserialize(data);
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fr != null) fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return null;
	}
	
	private static Individuals readFile(String filename) {
		return readFile(new File(filename));
	}
	
	private static Collection<Objectives> collectObjectives(Individuals individuals) {
		ArrayList<Objectives> collection = new ArrayList<Objectives>();
		for (Entry<ArchSolution, Quality> entry : individuals) {
			collection.add(entry.getValue().getObjectives());
		}
		
		return collection;
	}
	
	private static double bestObjective(Collection<Objectives> points, Objective o) {
		double best = 1.0;
		for (Objectives p : points) {
			double value = p.get(o).getDouble();
			if (value < best)
				best = value;
		}
		
		return best;
	}
	
	private static double worseObjective(Collection<Objectives> points, Objective o) {
		double worse = 0;
		for (Objectives p : points) {
			double value = p.get(o).getDouble();
			if (value > worse)
				worse = value;
		}
		
		return worse;
	}
}
