package experiments.acesmb;

import java.io.FileInputStream;

import edu.leiden.aqosa.launcher.MatingApproach;
import edu.leiden.aqosa.launcher.SingleRun;
import edu.leiden.aqosa.logger.EvaluationLogger.DetailLevel;

/**
 * 
 * @author Ramin
 */
public class Exp1 {
	
	public static void main(String[] args) throws Exception {
		final FileInputStream modelFile1 = new FileInputStream("model/Saab-IPC.aqosa");
		final int generations = 200;
		final int alpha = 1000;
		final int mu = 250;
		final int lambda = 500;
		final int archiveSize = 50;
		
		SingleRun run = new SingleRun(modelFile1);
		run.setEA(generations, alpha, mu, lambda);
		run.setArchiveSize(archiveSize);
		run.setCrossoverRate(0.95);
		run.setMatingAppraoch(MatingApproach.CROSSOVER_MUTATE);
		run.setLoggerLevel(DetailLevel.HALF);
		run.setArchiveLogger("logger/longrun-archive.tsv");
		run.setOptimumLogger("logger/longrun-optimum.tsv");
		run.call();
		
		for (String result : run.getResults()) {
			System.out.println(result);
		}
	}
		
}
