package experiments.acesmb;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import edu.leiden.aqosa.launcher.MultipleApprRun;
import edu.leiden.aqosa.logger.EvaluationLogger.DetailLevel;

/**
 * 
 * @author Ramin
 */
public class Exp2 {
	
	public static void main(String[] args) throws Exception {
		final FileInputStream modelFile = new FileInputStream("model/Saab-IPC.aqosa");
		final int generations = 15;
		final int alpha = 100;
		final int mu = 25;
		final int lambda = 50;
		final int archiveSize = 20;
		
		List<Integer> runs = new ArrayList<Integer>();
		for (int r=1; r<=20; r++)
			runs.add(r);
		
		MultipleApprRun mrun = new MultipleApprRun(runs, modelFile, "logger/approaches", "archive", "optimums");
		mrun.setEA(generations, alpha, mu, lambda);
		mrun.setArchiveSize(archiveSize);
		mrun.setHeuristicRate(0.95);
		mrun.setCrossoverRate(0.95);
		mrun.setLoggerLevel(DetailLevel.HALF);
		mrun.call();
		
		for (String result : mrun.getResults()) {
			System.out.println(result);
		}
	}
	
}
