package experiments.acesmb;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import edu.leiden.aqosa.launcher.MatingApproach;
import edu.leiden.aqosa.launcher.MultipleApprRun;
import edu.leiden.aqosa.launcher.SingleRun;
import edu.leiden.aqosa.logger.EvaluationLogger.DetailLevel;

/**
 * 
 * @author Ramin
 */
public class Exp3 {
	
	public static void main(String[] args) throws Exception {
		final FileInputStream modelFile1 = new FileInputStream("model/CruiseControl.aqosa");
		final int generations1 = 200;
		final int alpha1 = 1000;
		final int mu1 = 250;
		final int lambda1 = 500;
		final int archiveSize1 = 50;
		
		SingleRun run = new SingleRun(modelFile1);
		run.setEA(generations1, alpha1, mu1, lambda1);
		run.setArchiveSize(archiveSize1);
		run.setCrossoverRate(0.95);
		run.setMatingAppraoch(MatingApproach.CROSSOVER_MUTATE);
		run.setLoggerLevel(DetailLevel.HALF);
		run.setArchiveLogger("logger/longrun-archive.tsv");
		run.setOptimumLogger("logger/longrun-optimum.tsv");
		run.call();
		for (String result : run.getResults()) {
			System.out.println(result);
		}
		
		final FileInputStream modelFile2 = new FileInputStream("model/CruiseControl.aqosa");
		final int generations2 = 15;
		final int alpha2 = 100;
		final int mu2 = 25;
		final int lambda2 = 50;
		final int archiveSize2 = 20;
		
		List<Integer> runs = new ArrayList<Integer>();
		for (int r=1; r<=30; r++)
			runs.add(r);
		
		MultipleApprRun mrun = new MultipleApprRun(runs, modelFile2, "logger/approaches", "archive", "optimums");
		mrun.setEA(generations2, alpha2, mu2, lambda2);
		mrun.setArchiveSize(archiveSize2);
		mrun.setHeuristicRate(0.95);
		mrun.setCrossoverRate(0.95);
		mrun.setLoggerLevel(DetailLevel.HALF);
		mrun.call();
		for (String result : mrun.getResults()) {
			System.out.println(result);
		}
	}
		
}
