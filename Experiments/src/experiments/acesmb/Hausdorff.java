package experiments.acesmb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.opt4j.core.Objective;
import org.opt4j.core.Objectives;

public class Hausdorff {

	public static double HDistance(Collection<Objectives> X, Collection<Objectives> Y, int p) {
		double first = dist(X, Y, p);
		double second = dist(Y, X, p);
		
		if (first > second)
			return first;
		else
			return second;
	}

	private static double dist(Collection<Objectives> X, Collection<Objectives> Y, int p) {
		double sum = 0;
		for (Objectives x : X) {
			sum += Math.pow(dist(x, Y), p); 
		}
		
		return (Math.pow(sum, 1/p) / X.size());
	}

	private static double dist(Objectives u, Collection<Objectives> X) {
		ArrayList<Double> distances = new ArrayList<Double>();
		for (Objectives v : X) {
			distances.add( dist(u, v) );
		}
		
		return Collections.min(distances);
	}
	
	private static double dist(Objectives u, Objectives v) {
		double sum = 0;
		if (u.getKeys().size() != v.getKeys().size())
			throw new IllegalStateException("Invalid objectives size in Hausdorff distance calculation!");
		
		for (Objective o : u.getKeys()) {
			double x = u.get(o).getDouble();
			double y = v.get(o).getDouble();
			
			sum += Math.pow(x-y, 2);
		}
		
		return Math.sqrt(sum);
	}

}
