package experiments.acesmb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.opt4j.core.Objective;
import org.opt4j.core.Objectives;

public class Hausdorff_orig {

	public static double HDistance(Collection<Objectives> a, Collection<Objectives> b) {
		double first = dist(a, b);
		double second = dist(b, a);
		
		if (first > second)
			return first;
		else
			return second;
	}

	private static double dist(Collection<Objectives> b, Collection<Objectives> a) {
		ArrayList<Double> distances = new ArrayList<Double>();
		for (Objectives o : b) {
			distances.add(dist(o, a));
		}
		
		return supremum(distances);
	}

	private static double dist(Objectives u, Collection<Objectives> a) {
		ArrayList<Double> distances = new ArrayList<Double>();
		for (Objectives v : a) {
			distances.add(dist(u, v));
		}
		
		return infimum(distances);
	}
	
	private static double dist(Objectives u, Objectives v) {
		double sum = 0;
		if (u.getKeys().size() != v.getKeys().size())
			throw new IllegalStateException("Invalid objectives size in Hausdorff distance calculation!");
		
		for (Objective o : u.getKeys()) {
			double x = u.get(o).getDouble();
			double y = v.get(o).getDouble();
			
			sum += Math.pow(x-y, 2);
		}
		
		return Math.sqrt(sum);
	}

	private static double infimum(Collection<Double> distances) {
		return Collections.min(distances);
	}

	private static double supremum(Collection<Double> distances) {
		return Collections.max(distances);
	}

}
