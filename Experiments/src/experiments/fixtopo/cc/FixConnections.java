package experiments.fixtopo.cc;

import java.util.Random;

import edu.leiden.aqosa.genes.ConnectionGenotype;

@SuppressWarnings("serial")
public class FixConnections extends ConnectionGenotype {

	public FixConnections() {
		this(null);
	}

	public FixConnections(Integer edges) {
		super(edges);
	}

	@Override
	public void init(Random random, int maxNodes) {
		super.init(random, maxNodes);
		this.edges = 2;
	}
	
	@Override
	protected Boolean getPositionN(int row, int col) {
		// Bus1 : T,T,F,F,F,F
		// Bus2 : F,T,T,T,T,T
		switch (row) {
		case 0:
			if (col < 2)
				return true;
			else
				return false;
		case 1:
			if (col < 1)
				return false;
			else
				return true;
		default:
			return false;
		}
	}
}
