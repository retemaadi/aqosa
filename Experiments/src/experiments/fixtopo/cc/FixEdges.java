package experiments.fixtopo.cc;

import java.util.List;
import java.util.Random;

import edu.leiden.aqosa.genes.EdgesGenotype;

@SuppressWarnings("serial")
public class FixEdges extends EdgesGenotype {

	public FixEdges(List<String> buses) {
		super(buses);
	}

	public void init(Random random) {
		super.init(random, 2);
	}
	
	@Override
	public void init(Random random, int n) {
		this.init(random);
	}
	
}
