package experiments.fixtopo.cc;

import java.util.Random;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.genes.ConnectionGenotype;
import edu.leiden.aqosa.genes.EdgesGenotype;
import edu.leiden.aqosa.model.ArchModel;

public class FixGenotype extends ArchGenotype {

	public FixGenotype(ArchModel model) {
		super(model);
	}

	@Override
	public void init(Random random) {
		super.init(random);
		
		EdgesGenotype edgesGenotype = new FixEdges(model.getRepository().getBuses());
		edgesGenotype.init(random, noComponents);
		put(noComponents+2, edgesGenotype);
		
		ConnectionGenotype connectionGenotype = new FixConnections();
		connectionGenotype.init(random, noComponents);
		put(noComponents+3, connectionGenotype);
	}
	
}
