package experiments.fixtopo.ipc;

import java.util.Random;

import edu.leiden.aqosa.genes.ConnectionGenotype;

@SuppressWarnings("serial")
public class FixConnections extends ConnectionGenotype {

	public FixConnections() {
		this(null);
	}

	public FixConnections(Integer edges) {
		super(edges);
	}

	public void init(Random random) {
		super.init(random, FixNodes.NO_NODES);
		this.edges = FixEdges.NO_BUSES;
	}
	
	@Override
	public void init(Random random, int maxNodes) {
		this.init(random);
	}
	
	@Override
	protected Boolean getPositionN(int row, int col) {
		// Bus1 : T,T,F,F,F,F
		// Bus2 : T,F,T,T,T,F
		// Bus3 : T,F,F,F,F,T
		switch (row) {
		case 0:
			if (col < 2)
				return true;
			else
				return false;
		case 1:
			if (col == 1 || col == 5)
				return false;
			else
				return true;
		case 2:
			if (col == 0 || col == 5)
				return true;
			else
				return false;
		default:
			return false;
		}
	}
}
