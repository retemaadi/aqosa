package experiments.fixtopo.ipc;

import java.util.List;
import java.util.Random;

import edu.leiden.aqosa.genes.EdgesGenotype;

@SuppressWarnings("serial")
public class FixEdges extends EdgesGenotype {

	public final static int NO_BUSES = 3;
	
	public FixEdges(List<String> buses) {
		super(buses);
	}

	public void init(Random random) {
		super.init(random, NO_BUSES);
	}
	
	@Override
	public void init(Random random, int n) {
		this.init(random);
	}
	
}
