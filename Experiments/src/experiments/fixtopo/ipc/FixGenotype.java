package experiments.fixtopo.ipc;

import java.util.Random;

import edu.leiden.aqosa.genes.ArchGenotype;
import edu.leiden.aqosa.genes.DeployGenotype;
import edu.leiden.aqosa.model.ArchModel;

public class FixGenotype extends ArchGenotype {

	public FixGenotype(ArchModel model) {
		super(model);
	}

	@Override
	public void init(Random random) {
		super.init(random);
		
		FixNodes nodesGenotype = new FixNodes(model.getRepository().getCPUs());
		nodesGenotype.init(random);
		put(noComponents+1, nodesGenotype);
		
		FixEdges edgesGenotype = new FixEdges(model.getRepository().getBuses());
		edgesGenotype.init(random);
		put(noComponents+2, edgesGenotype);
		
		FixConnections connectionGenotype = new FixConnections();
		connectionGenotype.init(random);
		put(noComponents+3, connectionGenotype);
	}
	
	@Override
	public boolean validate() {
		DeployGenotype deployGene = getDeploy();
		if (deployGene.getNodes() > FixNodes.NO_NODES)
			return false;
		
		return super.validate();
	}
}
