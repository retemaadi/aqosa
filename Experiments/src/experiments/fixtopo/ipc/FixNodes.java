package experiments.fixtopo.ipc;

import java.util.List;
import java.util.Random;

import edu.leiden.aqosa.genes.NodesGenotype;

@SuppressWarnings("serial")
public class FixNodes extends NodesGenotype {

	public final static int NO_NODES = 6;
	
	public FixNodes(List<String> processors) {
		super(processors);
	}
	
	public void init(Random random) {
		super.init(random, NO_NODES);
	}
	
	@Override
	public void init(Random random, int n) {
		this.init(random);
	}
	
}
