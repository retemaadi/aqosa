package experiments.gecco2;

import java.io.FileInputStream;

import edu.leiden.aqosa.launcher.CompleterType;
import edu.leiden.aqosa.launcher.MultipleRun;
import edu.leiden.aqosa.logger.EvaluationLogger.DetailLevel;

public class Run2 {

	public static void main(String[] args) throws Exception {
		final FileInputStream modelFile = new FileInputStream("model/Saab-IPC_hadoop.aqosa");
		final int generations = 60;
		final int alpha = 256;
		final int mu = 64;
		final int lambda = 64;
		final int archiveSize = 32;
		
		MultipleRun run = new MultipleRun(30, modelFile, "temp", "archive-seq", "optimums-seq");
		run.setEA(generations, alpha, mu, lambda);
		run.setCompleter(CompleterType.SEQUENTIAL);
		run.setArchiveSize(archiveSize);
		run.setLoggerLevel(DetailLevel.HALF);
		run.setTimeLog(true);
		run.call();
		
		for (String result : run.getResults()) {
			System.out.println(result);
		}
	}
		
}
