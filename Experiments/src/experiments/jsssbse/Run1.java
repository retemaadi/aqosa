package experiments.jsssbse;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.leiden.aqosa.featureModel.FMConfigs;
import edu.leiden.aqosa.launcher.FeatureRun;
import edu.leiden.aqosa.launcher.MatingApproach;
import edu.leiden.aqosa.logger.EvaluationLogger.DetailLevel;

public class Run1 {

	static final List<String> conf1 = Arrays.asList("car", "interior_lights", "ignition_switch", "key_ignition", "dashboard", "simple_dashboard", "airbag_system", "front_airbag");
	static final List<String> conf2 = Arrays.asList("car", "interior_lights", "ignition_switch", "key_ignition", "dashboard", "simple_dashboard", "airbag_system", "front_airbag", "antilock_braking_system", "traction_control_system", "stability_control_system", "basic_skid_control");
	static final List<String> conf3 = Arrays.asList("car", "interior_lights", "ignition_switch", "key_ignition", "dashboard", "extended_dashboard", "airbag_system", "front_airbag", "sides_airbag", "antilock_braking_system", "traction_control_system", "stability_control_system", "basic_skid_control", "cruise_control", "basic_cruise_control");
	static final List<String> conf4 = Arrays.asList("car", "theft_alarm", "interior_lights", "ignition_switch", "button_ignition", "dashboard", "extended_dashboard", "airbag_system", "front_airbag", "sides_airbag", "antilock_braking_system", "traction_control_system", "stability_control_system", "extended_skid_control", "cruise_control", "adaptive_cruise_control");
	static final List<String> conf5 = Arrays.asList("car", "theft_alarm", "park_assist", "interior_lights", "ignition_switch", "button_ignition", "dashboard", "extended_dashboard", "airbag_system", "front_airbag", "sides_airbag", "passengers_airbag", "antilock_braking_system", "traction_control_system", "stability_control_system", "extended_skid_control", "cruise_control", "fullyAdaptive_cruise_control");
	
	@SuppressWarnings("unchecked")
	static final List<List<String>> confs4Demo = Arrays.asList(conf1, conf2, conf3, conf4, conf5);
	
	public static void main(String[] args) throws Exception {
		final String featureFile = new String("model/feature/carCaseStudy.als");
		final FileInputStream modelFile = new FileInputStream("model/feature/automotiveCaseStudy_Vasilis.aqosa");
		
		final int generations = 200;
		final int alpha = 2000;
		final int mu = 100;
		final int lambda = 50;
		final int archiveSize = 25;
		
//		final int delta = 4;
//		final int omega = 2;
		
		FMConfigs configs = new FMConfigs(featureFile);
		FeatureRun run	  = new FeatureRun(60, configs2Run(confs4Demo, configs.getConfigs()), 
				featureFile, modelFile, "logger/exp1", "archive", "optimum");
		
		run.setEA(generations, alpha, mu, lambda);
		run.setArchiveSize(archiveSize);
		run.setMatingAppraoch(MatingApproach.CROSSOVER_MUTATE);
		run.setLoggerLevel(DetailLevel.OFF);
		run.setSerializedLog(false);
		run.call();
	}
	
	protected static List<Integer> configs2Run(List<List<String>> desiredConfs, List<ArrayList<String>> allConfs) {
		List<Integer> ret = new ArrayList<Integer>();
		
		for (List<String> c : desiredConfs) {
			for (int i = 0; i < allConfs.size(); i++ ) {
				List<String> ac = allConfs.get(i);
				if (c.size() == ac.size() && c.containsAll(ac)) {
					ret.add(i);
					break;
				}
			}
		}
		return ret;
	}
	
	/*
	public static void main(String[] args) throws Exception {
		final String featureFile = new String("model/feature/carCaseStudy.als");
		final FileInputStream modelFile = new FileInputStream("model/feature/automotiveCaseStudy_Vasilis.aqosa");
		
		final int generations = 20;
		final int alpha = 2000;
		final int mu = 100;
		final int lambda = 50;
		final int archiveSize = 25;
		int delta = 9;
		boolean calcBusDistance = false;
		
		long startTime = System.currentTimeMillis();
		
		PrintStream out = new PrintStream(new FileOutputStream("logger/Run1.log"));
		System.setOut(out);
		

		FMConfigs configs = new FMConfigs(featureFile);
		FeatureRun run	  = new FeatureRun(FeatureRun.configs2Run(confs4Demo, 
										configs.getConfigs()), 
										featureFile, 
										modelFile, 
										"logger/run1", "archive", "optimum");
		
		AQOSAModel aqosaModel = run.getArchModel().getAQOSAModel();
		FeatureMapper fm 	  = new FeatureMapper(aqosaModel, configs);
		List<ResourceClaim> claims = ConfigsResourceClaim
									.calcAllConfigsClaim(configs.getConfigs(), 
														 aqosaModel, 
														 fm);
		
		System.out.println("Total Alloy Features: " + configs.getFeaturesIDs().size()  + " -- " + configs.getFeaturesIDs().toString());
		System.out.println("Total Configurations: " + configs.getConfigs().size());
		System.out.println("Resource Claims (by network usage):");
		for (ResourceClaim rc : claims) {
			System.out.print(rc.getBandwidthConsumption()+",");
		}
		System.out.println("\nResource Claims (by cpu usage):");
		ConfigsResourceClaim.sortClaimsByCPU((ArrayList<ResourceClaim>) claims);
		for (ResourceClaim rc : claims) {
			System.out.print(rc.getCpuConsumption()+",");
		}
		System.out.println("\nRunning experiment with the specific configurations.\nProof that the right configurations are selected (showing leaf features):");
		for (Integer i : FeatureRun.configs2Run(confs4Demo, configs.getConfigs()) ) {
			System.out.println (run.getFeatures()
								.getConfigurations()
								.get(i)
								.getFeatures() );
		}
		
		run.setEA(generations, alpha, mu, lambda);
		run.setArchiveSize(archiveSize);
		run.setMatingAppraoch(MatingApproach.CROSSOVER_MUTATE);
		run.setLoggerLevel(DetailLevel.OFF);
		run.setSerializedLog(false);
		run.call();
		
//		for (String result : run.getResults()) {
//			System.out.println(result);
//		}
		
		// All solutions
		for (int i=0; i < ArchDistance.allIterationSolutions.size(); i++) {
			List<Entry<ArchSolution,Quality>> l = ArchDistance.allIterationSolutions.get(i);
			System.out.println( "***Number of solutions for configuration "+ (i+1) +": "+ l.size() );
			
			// Print solutions of every Pareto
			for (Entry<ArchSolution, Quality> entry : l) {
				ArchSolution s = entry.getKey();
				System.out.println(s);
			}
			System.out.println("");
		}
		
		// Common solutions
		PrintWriter writer = new PrintWriter("temp/Delta_Experiments.txt");
		writer.println("Experiments testing different delta values");
		writer.println("======================================================\n");
		
		int count;
		for (count = 0; count <= delta; count++) {
			writer.println("EXPERIMENT "+ count);
			writer.println("Delta = "+count);
			writer.println("******************************************************\n");
			
			System.out.println("***Common Solutions: within "+count+" delta:");
			writer.println("***Common Solutions: within "+count+" delta:");
			
			List<SolutionsDistance> commonSolutions = ArchDistance.getCommonSolutions(count, calcBusDistance);
			String tmp = ArchDistance.printCommonSolutionsFromGivenDelta(commonSolutions, -1, calcBusDistance);
			writer.println(tmp);
			
			// Create js visualization file
			if (commonSolutions.size() > 0)
				ArchDistance.visualizeSolutions(commonSolutions, "temp/visualizeGivenDelta"+count+".js", calcBusDistance);
		}
		
		// Common solutions with min distance method
		System.out.println("***Common Solutions: sum of minimum distances\n");
		List<List<SolutionsDistance>> returned = ArchDistance.getCommonConnectedSolutionsMinDist(calcBusDistance);
		List<SolutionsDistance> connectedSolutions = returned.get(0);
		List<SolutionsDistance> minDistSolutions   = returned.get(1);
		
		ArchDistance.printBestSolutions(minDistSolutions, 5, calcBusDistance);
		System.out.println("");
		ArchDistance.printCommonSolutionsFromGivenDelta(connectedSolutions, -1, calcBusDistance);
		
		// Create js visualization file
		ArchDistance.visualizeSolutions(connectedSolutions, "temp/visualizeMinDist.js", calcBusDistance);
		
		long endTime = System.currentTimeMillis();
		System.out.println("That took "+(endTime - startTime)/1000.0+" seconds");
		writer.println("Duration: "+(endTime - startTime)/1000.0+" seconds");
		
		out.close();
		writer.close();
		
		WriteOutput.writeOutput("temp/R-output.tsv", ArchDistance.getAllSolutions());

	}
	*/
}
