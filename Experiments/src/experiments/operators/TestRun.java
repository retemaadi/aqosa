package experiments.operators;

import java.io.File;
import java.io.FileInputStream;

import edu.leiden.aqosa.launcher.MultipleApprRun;

/**
 * 
 * @author Ramin
 */
public class TestRun {
	
	public static void main(String[] args) throws Exception {
		//final FileInputStream modelFile = new FileInputStream("model/Saab-IPC2.aqosa");
		final FileInputStream modelFile = new FileInputStream("model/CRN.aqosa");
		
		/*
		run = new SingleRun(modelFile);
		run.setEA(generations, alpha, mu, lambda);
		run.setArchiveSize(archiveSize);
		run.setMatingAppraoch(MatingApproach.CROSSOVER_MUTATE);
		run.setArchiveLogger("logger/mytest.tsv");
		run.setViewer();
		run.call();
		*/
		/*
		run = new SingleRun(modelFile);
		ExpParams param = new ExpParams("single", 50, 25, 25, 25, 25);
		param.config(run);
		run.setMatingAppraoch(MatingApproach.CROSSOVER_MUTATE);
		run.setViewer();
		run.call();
		*/
		
		/*
		run = new MultipleRun(5, modelFile, "logger/test", "archive", "optimum");
		run.setEA(generations, alpha, mu, lambda);
		run.setArchiveSize(archiveSize);
		run.call();
		*/
		
		/*
		run = new MultipleAlgoRun(5, modelFile, "logger/test", "archive", "optimum");
		run.setEA(generations, alpha, mu, lambda);
		run.setArchiveSize(archiveSize);
		run.call();
		*/
		
		/*
		run = new MultipleApprRun(5, modelFile, "logger/test", "archive", "optimum");
		run.setEA(generations, alpha, mu, lambda);
		run.setArchiveSize(archiveSize);
		run.call();
		*/
		
		/*
		List<ExpParams> exps = new ArrayList<ExpParams>();
		exps.add(new ExpParams("short1"	, 20, 20, 5, 5, 10));
		exps.add(new ExpParams("short2"	, 20, 50, 10, 10, 15));
		exps.add(new ExpParams("short3"	, 30, 50, 10, 10, 15));
		exps.add(new ExpParams("med1"	, 50, 50, 15, 15, 20));
		exps.add(new ExpParams("med2"	, 50, 100, 15, 15, 20));
		exps.add(new ExpParams("med3"	, 100, 100, 15, 15, 20));
		exps.add(new ExpParams("long1"	, 200, 50, 20, 20, 25));
		exps.add(new ExpParams("long2"	, 500, 100, 20, 20, 25));
		exps.add(new ExpParams("long3"	, 1000, 100, 25, 25, 50));
		
		final double[] crossoverRates = {0.25, 0.5, 0.75, 0.9, 0.99};
		for (ExpParams exp : exps) {
			for (double cr : crossoverRates) {
				run = new MultipleApprRun(30, modelFile, "logger/operators/" + exp.getName() + File.separator + cr, "archive", "optimum");
				exp.config(run);
				run.setCrossoverRate(cr);
				run.call();
			}
		}
		*/
		
		final double[] heuristicRates = {0.75, 0.85 ,0.95};
		final double[] crossoverRates = {0.15, 0.25, 0.35};
		
		MultipleApprRun run;
		ExpParams param = new ExpParams("single", 50, 25, 25, 25, 25);
		for (double hr : heuristicRates) {
			for (double cr : crossoverRates) {
				run = new MultipleApprRun(30, modelFile, "logger/HRexp/" + hr + File.separator + cr, "archive", "optimum");
				param.config(run);
				run.setHeuristicRate(hr);
				run.setCrossoverRate(cr);
				run.call();
			}
		}
	}
	
}

class ExpParams {
	final String name;
	final int generations;
	final int alpha;
	final int mu;
	final int lambda;
	final int archiveSize;

	public ExpParams(String name, int generations, int alpha, int mu, int lambda, int archiveSize) {
		this.name = name;
		this.generations = generations;
		this.alpha = alpha;
		this.mu = mu;
		this.lambda = lambda;
		this.archiveSize = archiveSize;
	}
	
	public void config(MultipleApprRun run) {
		run.setEA(generations, alpha, mu, lambda);
		run.setArchiveSize(archiveSize);
	}
	
	public String getName() {
		return name;
	}
}
