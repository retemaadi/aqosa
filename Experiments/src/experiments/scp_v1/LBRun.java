package experiments.scp_v1;

import java.io.FileInputStream;

import edu.leiden.aqosa.launcher.MatingApproach;
import edu.leiden.aqosa.launcher.MultipleRun;

/**
 * 
 * @author Ramin
 */
public class LBRun {
	
	public static void main(String[] args) throws Exception {
		final FileInputStream modelFile1 = new FileInputStream("model/CruiseControl.aqosa");
		final int generations = 10000;
		final int alpha = 100;
		final int mu = 50;
		final int lambda = 50;
		final int archiveSize = 50;
		
		//List<Integer> iruns = Arrays.asList(new Integer[]{15,16,17,18,19,20,21,22,23,24,25});
		//MultipleRun mrun = new MultipleRun(iruns, modelFile1, "logger", "archive", "optimums");
		MultipleRun mrun = new MultipleRun(25, modelFile1, "logger", "archive", "optimums");
		mrun.setEA(generations, alpha, mu, lambda);
		mrun.setArchiveSize(archiveSize);
		mrun.setMatingAppraoch(MatingApproach.CROSSOVER_MUTATE);
		//mrun.setEvaluationLogger(false);
		mrun.call();
		
		for (String result : mrun.getResults()) {
			System.out.println(result);
		}
	}
		
}
