package experiments.scp_v2;

import java.io.FileInputStream;

import edu.leiden.aqosa.launcher.MatingApproach;
import edu.leiden.aqosa.launcher.MultipleRun;
import edu.leiden.aqosa.logger.EvaluationLogger.DetailLevel;

/**
 * 
 * @author Ramin
 */
public class TopoRun {
	
	public static void main(String[] args) throws Exception {
		final FileInputStream modelFile1 = new FileInputStream("model/Saab-IPC_d1.aqosa");
		final int generations = 5000;
		final int alpha = 1000;
		final int mu = 100;
		final int lambda = 50;
		final int archiveSize = 50;
		
		MultipleRun mrun = new MultipleRun(30, modelFile1, "temp/topo", "archive", "optimums");
		mrun.setEA(generations, alpha, mu, lambda);
		mrun.setArchiveSize(archiveSize);
		mrun.setMatingAppraoch(MatingApproach.CROSSOVER_MUTATE);
		mrun.setLoggerLevel(DetailLevel.HALF);
		mrun.call();
		
		for (String result : mrun.getResults()) {
			System.out.println(result);
		}
	}
		
}
